# ADF7021 Driver, JNI and HAL for Android

https://www.analog.com/en/products/adf7021.html

<!--
## Slot Assignment and RF Command timing diagrams

![Slot Assignment](yEd/slot_assignment.jpg "slot assignment")

## Class Diagrams

### HAL Layer for ADF

![HAL](vpp-uml/svg/HAL.svg "HAL Class Diagram")

## JNI Layer for Pathfinder

![JNI](vpp-uml/svg/JNI.svg "JNI Class Diagram")
-->

## Driver

- File operations
  - `open`, `release`, `read`, `poll`, and `ioctl`
- GPIO
  - IN: `SREAD,` `CLKOUT`, `TXRX_DATA`( RX data )
  - OUT: `SDATA`, `SCLK`, `CE`, `SLE`, `TXRX_CLK`( TX data ), `TX_EN`, `RX_EN`
  - IRQ: `SWD`( rising edge ), `CLKOUT`( both edge )
  - ISR
    - `SWD`: Start `timeout_timer` with `RX_TIMEOUT`
    - `CLKOUT`: TX data at falling edge. RX data at rising edge
      - All data TX or RX completed, queue work `rf_done_work` into `timeout_wq`
      - If timeout occurred, queue work `timeout_work` into `timeout_wq` by `timeout_timer` handler.
- HR Timer: reference( 2 secs ), slot( 80 ms ), timeout_timer( RX or TX timeout )
  - ISR: All timer handler: queue work into workqueue
- List
  - `CMD_LIST`: command list
  - `DELAYED_LIST`: delayed command list. check at TX_DONE
  - `RX_LIST`: RX packets and NOTIFY messages to HAL
  - `PAIR_LIST`: pairing device list. used for initialize refetence time.
  - `DUMP_LIST`: TX / RX packet dump list. used for debugging

## Threads on HAL

### Adf7021

> Wait until data in Reader's Queue
>
> `Dequeue` and `Process Packet`: GPS data and NOTIFY from Driver.

### Reader

> Use `poll()` for read RX data or Notify messages from Driver.
>
> `Enqueue` packet.

### Commander

> Wait until command in Commander's Queue.
>
> `Dequeue` and `Process Command`: send command to driver using `ioctl()`.



## Sequence Diagrams

### Application Start and Stop

```mermaid
%% Example of sequence diagram
sequenceDiagram
	participant App
	participant JNI
	participant HAL
	participant ANDROID
	note over App: APP START

	App->>+JNI: JNI_OnLoad
		JNI->>HAL: Init
		note over HAL, ANDROID: Find and Load adf.default.so
		HAL->>+ANDROID: hw_get_module
		ANDROID-->>-HAL: return
		note over JNI, ANDROID: Open device file and Start the class Adf7021's Thread
		JNI->>HAL: Open
		JNI->>HAL: SetCallbacks
		JNI->>HAL: Start
		JNI->>HAL: InitJNI
	JNI-->>-App: return
	note over App, HAL: APP RUNNING: Do something...
	note over App, HAL: APP RUNNING: ...

	note over App: APP STOP
	App->>+JNI: JNI_OnUnLoad
		JNI->>HAL: Stop
		JNI->>HAL: Close
	JNI-->>-App: return
  
```

### Start Threads on HAL

```mermaid
%% Example of sequence diagram
sequenceDiagram
	participant App
	participant JNI
	participant Pathfinder
	participant Adf7021
	participant Reader
	participant Commander

	Pathfinder->>Adf7021: Start
	Adf7021->>+Adf7021: Wait
	App->>+JNI: setDeviceList
		JNI->>+Pathfinder: Update
			Pathfinder->>Adf7021: Update
			Pathfinder->>Adf7021: Signal
		Pathfinder-->>-JNI: return
	JNI-->>-App: return
	Adf7021->>-Adf7021: Init
	Adf7021->>Reader: Start
	Adf7021->>Commander: Start  
```

### Reader Queue

```mermaid
%% Example of sequence diagram
sequenceDiagram
	participant Adf7021
	participant Reader
	participant Driver

	Adf7021->>+Reader: Wait
		Reader->>+Driver: poll
		note over Reader, Driver: Wait until enqueue packet
		Driver-->>-Reader: return
		Reader->>+Driver: read
		Driver-->>-Reader: ADF_PACKET
		Reader->>Reader: Enqueue
	Reader-->>-Adf7021: Signal

	Adf7021->>+Reader: Dequeue
	Reader-->>-Adf7021: ADF_PACKET
	
	Adf7021->>Adf7021: ProcessPacket
	
```

### Commander Queue

```mermaid
%% Example of sequence diagram
sequenceDiagram
	participant App
	participant JNI
	participant Adf7021
	participant Commander
	participant Driver

	Commander->>+Commander: wait
	App->>+JNI: Command
	JNI->>+Adf7021: SendCommand
	Adf7021->>Commander: Enqueue
	Adf7021->>Commander: Signal
	Adf7021-->>-JNI: PostProcessCommand
	JNI-->>-App: return
	
	Commander->>-Commander: Dequeue
	Commander->>+Commander: ProcessCommand
	Commander->>+Driver: ioctl
	Driver-->>-Commander: return
	Commander-->>-Commander: return

```

