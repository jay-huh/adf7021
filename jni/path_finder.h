#ifndef _PATH_FINDER_H_
#define _PATH_FINDER_H_

#include <string>

#include <jni.h>

#include "adf7021.h"

typedef struct JNI_METHOD_INFO_S
{
	std::string name;
	std::string signature;
	jmethodID	methodID;
} JNI_METHOD_INFO;

class JniClassBase
{
private:
	const std::string	m_name;
	const jclass		m_thiz;	//	class object

	std::map< int, jobject > m_objectMap;

	/*	Global References	*/
	JNI_METHOD_INFO		m_constructor;

protected:
public:
	JniClassBase( const std::string name, const jclass thiz = NULL )
		: m_name( name ), m_thiz( thiz )
	{
		m_objectMap.clear();
	};
	virtual ~JniClassBase() {};

	void SetConstructor( const jmethodID methodID, const std::string name, const std::string signature )
	{
		m_constructor.name = name;
		m_constructor.methodID = methodID;
		m_constructor.signature = signature;
	}

	bool CreateJobject( const int index, const std::string name = "" )
	{
		return true;
	};
};

class DeviceDetailInfo : public JniClassBase
{
private:
protected:
public:
	DeviceDetailInfo( const std::string name, const jclass thiz = NULL )
		: JniClassBase( name, thiz ) {};
	~DeviceDetailInfo() {};
};

class JniInterface
{
public:

protected:
	typedef std::pair< std::string, jclass >	JCLASS_PAIR;
	typedef std::pair< std::string, jobject >	JOBJECT_PAIR;

private:
	const JavaVM	*m_pVm;
	const jint		m_version;
	//JNIEnv		*m_pEnv;
	//jobject		m_job;
	//jclass 		m_jclass;
	//jmethodID	m_jmethodID;
	const JNINativeMethod	*m_pNativeMethods;

	std::map< int, JCLASS_PAIR > m_classMap;
	std::map< int, JOBJECT_PAIR > m_jobjectMap;

protected:
	jclass GetClassID( JNIEnv * const pEnv, const char *pClassName );
	jobject NewGlobalRef( JNIEnv * const pEnv, const char *pClassName );

public:
	JniInterface( const JavaVM *pVm = NULL, const jint version = JNI_VERSION_1_6 );
	virtual ~JniInterface();

	JNIEnv* GetJNIEnv( bool *pNeedDetach );
	void DetachJNI();

	void SetVM( const JavaVM *pVm );
	JavaVM* GetVM( void ) { return const_cast< JavaVM * >( m_pVm ); };
	void SetNativiMethods( const JNINativeMethod *pNativeMethods );
	bool RegisterClass( int index, std::string name );
	bool CreateJobject( int index, std::string name );
	bool RemoveClass( int index, std::string name );
	bool RemoveJobject( int index, std::string name );
	//jobject FindJobject( int index, std::string name );
	bool FindClass( int index, std::string name, jclass *pClazz );
	bool FindJobject( int index, std::string name, jobject *pJobj );

	virtual bool InitJNI( const JavaVM *pVm, const JNINativeMethod *pNativeMethods = NULL ) = 0;
	virtual void nativeClassInit( JNIEnv *pEnv, jclass clazz ) {};

	//static void nativeClassInit(JNIEnv *_env, jclass glImplClass);
	//static jlong nativeInit(JNIEnv* env, jclass clazz, jobject receiverObj,
};

namespace Pettra
{
	//class Adf7021;

class Pathfinder : public JniInterface
{
private:
	const std::string	m_moduleId;

	Pettra::HalModule	*m_pModule;
	Pettra::AdfHwDevice	*m_pHwDevice;
	Pettra::Adf7021		*m_pAdf7021;

#define DEVICE_INFO( slot ) ( SLOT_##slot_DEVICE_INFO_JOBJ )

	static const int	DEVICE_INFO( 0 ) = 0;

#if 1
#define CLASS_CONNECTOR_SERVICE		"com/dogtra/pf/handheld/service/ConnectorService"
#define CLASS_DEVICE_DETAIL_INFO	"com/dogtra/pf/handheld/data/DeviceDetailInfo"
#define CLASS_CONNECTOR_INFO		"com/dogtra/pf/handheld/data/ConnectorInfo"
#define CLASS_DEVICE_INFO			"com/dogtra/pf/handheld/data/DeviceInfo"

#define CLASS_IDX_CONNECTOR_SERVICE		0
#define CLASS_IDX_DEVICE_DETAIL_INFO	1
#else

	static const char *s_pConnectorServiceClassName = "com/dogtra/pf/handheld/service/ConnectorService";
	static const char *s_pDeviceDetailInfoClassName = "com/dogtra/pf/handheld/data/DeviceDetailInfo";
	static const char *s_pConnectorInfoClassName = "com/dogtra/pf/handheld/data/ConnectorInfo";
	static const char *s_pDeviceInfoClassName = "com/dogtra/pf/handheld/data/DeviceInfo";
#endif
protected:
public:
	Pathfinder( const JavaVM *pVm = NULL, const std::string moduleId = "adf" );
	virtual ~Pathfinder();

	virtual bool InitJNI( const JavaVM *pVm, const JNINativeMethod *pNativeMethods = NULL );
	virtual void nativeClassInit( JNIEnv *pEnv = NULL, jclass clazz = NULL );

	int SetCallbacks( const ADF_CALLBACKS *pCallbacks );
	int Update( const std::map< int, ::DEVICE_INFO > &deviceMap );
	int Init( void );
	int Open( void );
	int Close( void );
	int Start( void );
	int Stop( void );

	int SendCommand( const uint32_t command, const void * const pArg = NULL );
	bool GetConnectorInfo( ::CONNECTOR_INFO &connectorInfo );
};
}	//	namespace Pettra
#endif	//	_PATH_FINDER_H_
/*	vim: set ft=cpp:	*/
