#include	"path_finder.h"

#include	"debug.h"

#include	"adf7021.h"

using namespace		Pettra;

Pathfinder::Pathfinder( const JavaVM *pVm, const std::string moduleId )
	: JniInterface( pVm, JNI_VERSION_1_6 ), m_moduleId( moduleId )
{
	//assert( pVm != NULL );

	m_pModule = NULL;
	m_pHwDevice = NULL;
	m_pAdf7021 = NULL;
}

Pathfinder::~Pathfinder()
{
}

int Pathfinder::Init( void )
{
	if( ::hw_get_module( m_moduleId.c_str(), ( const ::hw_module_t ** )( &m_pModule ) ) != 0 )
	{
		LOGE( "%s module not found!!", m_moduleId.c_str() );
		ERROR_PRINTF( "%s module not found!!\n", m_moduleId.c_str() );
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int Pathfinder::SetCallbacks( const ADF_CALLBACKS *pCallbacks )
{
	if( m_pHwDevice == NULL ) return EXIT_FAILURE;

	return m_pHwDevice->setCallbacks( m_pHwDevice, pCallbacks );
}

int Pathfinder::Update( const std::map< int, ::DEVICE_INFO > &deviceMap )
{
	if( m_pHwDevice == NULL ) return EXIT_FAILURE;

	return m_pHwDevice->update( m_pHwDevice, deviceMap );
}

int Pathfinder::Open( void )
{
	if( m_pModule == NULL ) return EXIT_FAILURE;

	if( m_pModule->methods->open( m_pModule, m_moduleId.c_str(), ( hw_device_t ** )( &m_pHwDevice ) )!= 0 )
	{
		LOGE( "pModule->methods->open failed!!!\n" );
		ERROR_PRINTF( "pModule->methods->open failed!!!\n" );
		return EXIT_FAILURE;
	}

	m_pAdf7021 = const_cast< Pettra::Adf7021 * >( m_pHwDevice->GetDevice() );
	return m_pHwDevice->open( m_pHwDevice );
}

int Pathfinder::Close( void )
{
	if( m_pHwDevice == NULL ) return EXIT_FAILURE;

	return m_pHwDevice->close( m_pHwDevice );
}

int Pathfinder::Start( void )
{
	if( m_pHwDevice == NULL ) return EXIT_FAILURE;

	return m_pHwDevice->start( m_pHwDevice );
}

int Pathfinder::Stop( void )
{
	if( m_pHwDevice == NULL ) return EXIT_FAILURE;

	return m_pHwDevice->stop( m_pHwDevice );
}

int Pathfinder::SendCommand( const uint32_t command, const void * const pArg )
{
	assert( ( m_pHwDevice != NULL ) && ( command != 0x0 ) );
	//if( m_pHwDevice == NULL ) return EXIT_FAILURE;

	return m_pHwDevice->sendCommand( m_pHwDevice, command, pArg );
}

bool Pathfinder::GetConnectorInfo( ::CONNECTOR_INFO &connectorInfo )
{
	assert( m_pHwDevice != NULL );

	return m_pHwDevice->getConnectorInfo( m_pHwDevice, connectorInfo );
}

bool Pathfinder::InitJNI( const JavaVM *pVm, const JNINativeMethod *pNativeMethods )
{
	SetVM(  pVm );
	SetNativiMethods( pNativeMethods );

#if 1
	RegisterClass( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
	RegisterClass( CLASS_IDX_DEVICE_DETAIL_INFO, CLASS_DEVICE_DETAIL_INFO );

	//CreateJobject( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
	//CreateJobject( CLASS_IDX_DEVICE_DETAIL_INFO, CLASS_DEVICE_DETAIL_INFO );
#endif

	return true;
}

void Pathfinder::nativeClassInit( JNIEnv *pEnv, jclass clazz ) {};

JniInterface::JniInterface( const JavaVM *pVm, const jint version )
	: m_pVm( pVm ), m_version( version )
{
	//assert( pVm != NULL );
	m_classMap.clear();
	m_jobjectMap.clear();
}

JniInterface::~JniInterface()
{
	m_classMap.clear();
	m_jobjectMap.clear();
}

void JniInterface::SetVM( const JavaVM *pVm )
{
	assert( pVm != NULL );

	m_pVm = pVm;
	return;
}

void JniInterface::SetNativiMethods( const JNINativeMethod *pNativeMethods )
{
	//assert( pNativeMethods != NULL );
	m_pNativeMethods = pNativeMethods;
}

#if 0
struct JavaVMAttachArgs {
    jint        version;    /* must be >= JNI_VERSION_1_2 */
    const char* name;       /* NULL or name of thread as modified UTF-8 str */
    jobject     group;      /* global ref of a ThreadGroup object, or NULL */
};
#endif
JNIEnv* JniInterface::GetJNIEnv( bool *pNeedsDetach )
{
	JNIEnv *pEnv = NULL;
	jint ret;
	JavaVMAttachArgs args = { m_version, NULL, NULL };

	assert( m_pVm != NULL );
	assert( pNeedsDetach != NULL );

	*pNeedsDetach = false;

	ret = const_cast< JavaVM * >( m_pVm )->GetEnv( ( void** )&pEnv, JNI_VERSION_1_6 );

	switch( ret )
	{
		case	JNI_OK :
			LOGV( "GetEnv: return JNI_OK. pEnv: %p", pEnv );
			break;
		case	JNI_EDETACHED :
			LOGV( "GetEnv: return JNI_EDETACHED. pEnv: %p", pEnv );
			// Thread not attached
			// TODO : If calling AttachCurrentThread() on a native thread
			// must call DetachCurrentThread() in future.
			// see: http://developer.android.com/guide/practices/design/jni.html
			//if( m_pVm->AttachCurrentThread( &pEnv, NULL ) < 0 )
			ret = const_cast< JavaVM * >( m_pVm )->AttachCurrentThread( &pEnv, &args );
			if( ret != JNI_OK )
			{
				LOGE( "Failed to AttachCurrentThread(), pEnv: %p ret: 0x%08x", pEnv, ret );
				break;
			}
			LOGV( "AttachCurrentThread: pEnv: %p", pEnv );
			*pNeedsDetach = true;
			break;
		case JNI_EVERSION :
			LOGW( "JNI interface version 1.6 not supported." );
		default:
			LOGE( "Failed to get the environment by GetEnv()" );
			break;
	}
	return pEnv;
}

jclass JniInterface::GetClassID( JNIEnv * const pEnv, const char *pClassName )
{
	jclass clazz = NULL;

	assert( pEnv != NULL );
	assert( pClassName != NULL );

	clazz = pEnv->FindClass( pClassName );
	if( clazz == NULL )
	{
		LOGE( "Failed to find class: %s", pClassName );
	}
	LOGV( "pClassName: %s, pEnv: %p, clazz: %p", pClassName, pEnv, clazz );

	return clazz;
}

jobject JniInterface::NewGlobalRef( JNIEnv * const pEnv, const char *pClassName )
{
	jclass clazz = NULL;
	jobject jobj = NULL;

	assert( pEnv != NULL );
	assert( pClassName != NULL );

	clazz = GetClassID( pEnv, pClassName );
	if( clazz == NULL )
	{
		LOGE( "Failed to find class: %s", pClassName );
		return NULL;
	}
	jobj = pEnv->NewGlobalRef( clazz );
	if( jobj == NULL )
	{
		LOGE( "NewGlobalRef() Failed!! class: %s", pClassName );
	}

	LOGI( "NewGlobalRef: pClassName: %s, pEnv: %p, clazz: %p jobj: %p", pClassName, pEnv, clazz, jobj );

	return jobj;
}

void JniInterface::DetachJNI()
{
	assert( m_pVm != NULL );

#if 0
	JavaVM *pVm = AndroidRuntime::getJavaVM();
#endif
	int ret = const_cast< JavaVM * >( m_pVm )->DetachCurrentThread();
	if( ret != JNI_OK )
	{
		LOGE( "thread detach failed: %#x", ret );
		return;
	}
	LOGV( "DetachCurrentThread OK..." );
	return;
}

bool JniInterface::RegisterClass( int index, std::string name )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jobject jobj = NULL;
	bool needDetach = false;
	JCLASS_PAIR class_pair;

	//typedef std::pair< std::string, jclass >	JCLASS_PAIR;
	assert( name != "" );

	pEnv = GetJNIEnv( &needDetach );
	if( pEnv == NULL )
	{
		LOGE( "GetJNIEnv() failed!!!" );
		return false;
	}
	jobj = NewGlobalRef( pEnv, name.c_str() );
	if( jobj == NULL )
	{
		LOGE( "NewGlobalRef() Failed!! class: %s", name.c_str() );
		if( needDetach == true )
		{
			DetachJNI();
		}
		return false;
	}
	clazz = reinterpret_cast< jclass >( jobj );

	class_pair = std::make_pair( name, clazz );
	m_classMap.insert( std::pair< int, JCLASS_PAIR >( index, class_pair ) );

	if( needDetach == true )
	{
		DetachJNI();
	}
	LOGI( "RegisterClass: name: %s, pEnv: %p, clazz: %p jobj: %p", name.c_str(), pEnv, clazz, jobj );

	return true;
}

bool JniInterface::CreateJobject( int index, std::string name )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jobject jobj = NULL;
	jobject newObject = NULL;
	bool needDetach = false;

	JOBJECT_PAIR jobj_pair;

	jmethodID constructor = NULL;

	assert( name != "" );

	if( FindClass( index, name, &clazz ) == false )
	{
		LOGE( "FindClass() failed!!!" );
		return false;
	}

	pEnv = GetJNIEnv( &needDetach );
	if( pEnv == NULL )
	{
		LOGE( "GetJNIEnv() failed!!!" );
		return false;
	}
    //constructor = pEnv->GetMethodID( clazz, "<init>", "(J)V");
    constructor = pEnv->GetMethodID( clazz, "<init>", "()V");

    newObject = pEnv->NewObject( clazz, constructor, "()V" );
	if( newObject == NULL )
	{
		LOGE( "newObject() Failed!! class: %s", name.c_str() );
		if( needDetach == true )
		{
			DetachJNI();
		}
		return false;
	}
    jobj = pEnv->NewGlobalRef( newObject );
	if( jobj == NULL )
	{
		LOGE( "NewGlobalRef() Failed!! class: %s", name.c_str() );
		//pEnv->DeleteLocalRef( newObject );
		if( needDetach == true )
		{
			DetachJNI();
		}
		return false;
	}

	jobj_pair = std::make_pair( name, jobj );
	m_jobjectMap.insert( std::pair< int, JOBJECT_PAIR >( index, jobj_pair ) );

	if( needDetach == true )
	{
		DetachJNI();
	}
	LOGI( "CreateJobject: name: %s, pEnv: %p, clazz: %p jobj: %p", name.c_str(), pEnv, clazz, jobj );

	return true;
}

bool JniInterface::RemoveClass( int index, std::string name )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	bool needDetach = false;

	assert( name != "" );

	std::map< int, JCLASS_PAIR >::iterator it;

	it = m_classMap.find( index );
	if( it != m_classMap.end() )
	{
		clazz = ( it->second ).second;
		LOGV( "Remove class_pair: idx: %d \"%s\" clazz: %p", index, name.c_str(), clazz );

		pEnv = GetJNIEnv( &needDetach );
		pEnv->DeleteGlobalRef( clazz );
		m_classMap.erase( it );
	}
	else
	{
		LOGW( "class_pair: idx: %d \"%s\" | not found!!", index, name.c_str() );
		return false;
	}

	if( needDetach == true )
	{
		DetachJNI();
	}

	return true;
}

bool JniInterface::RemoveJobject( int index, std::string name )
//bool JniInterface::RemoveJobject( std::string name, jobject jobj )
{
	JNIEnv *pEnv = NULL;
	jobject jobj = NULL;
	bool needDetach = false;

	assert( name != "" );

	std::map< int, JOBJECT_PAIR >::iterator it;

	it = m_jobjectMap.find( index );
	if( it != m_jobjectMap.end() )
	{
		jobj = ( it->second ).second;
		LOGV( "Remove jobject_pair: idx: %d \"%s\" jobj: %p", index, name.c_str(), jobj );

		pEnv = GetJNIEnv( &needDetach );
		pEnv->DeleteGlobalRef( jobj );
		m_jobjectMap.erase( it );
	}
	else
	{
		LOGW( "jobject_pair: idx: %d \"%s\" | not found!!", index, name.c_str() );
		return false;
	}

	if( needDetach == true )
	{
		DetachJNI();
	}

	return true;
}

bool JniInterface::FindClass( int index, std::string name, jclass *pClazz )
{
	jclass clazz = NULL;

	assert( name != "" );
	assert( pClazz != NULL );

	std::map< int, JCLASS_PAIR >::iterator it;

	it = m_classMap.find( index );
	if( it != m_classMap.end() )
	{
		clazz = ( it->second ).second;
		LOGV( "Found class_pair: idx: %d \"%s\" clazz: %p", index, name.c_str(), clazz );
	}
	else
	{
		LOGW( "class_pair: idx: %d \"%s\" | not found!!", index, name.c_str() );
		return false;
	}
	*pClazz = clazz;

	return true;
}

bool JniInterface::FindJobject( int index, std::string name, jobject *pJobj )
{
	jobject jobj = NULL;
	JNIEnv *pEnv = NULL;

	assert( name != "" );
	assert( pJobj != NULL );

	std::map< int, JOBJECT_PAIR >::iterator it;

	it = m_jobjectMap.find( index );
	if( it != m_jobjectMap.end() )
	{
		jobj = ( it->second ).second;
		LOGV( "Found jobject_pair: idx: %d \"%s\" jobj: %p", index, name.c_str(), jobj );
	}
	else
	{
		LOGW( "jobject_pair: idx: %d \"%s\" | not found!!", index, name.c_str() );
		return false;
	}
	*pJobj = jobj;

	return true;
}
