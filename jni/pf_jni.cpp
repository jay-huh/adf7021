//
// Created by khpark on 2017-12-12.
//

#include "com_dogtra_pf_handheld_service_ConnectorService.h"

//#include <android/log.h>
#include <stdio.h>
#include <stdlib.h>  // RAND_MAX
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

#include "path_finder.h"

#include	"debug.h"

#ifdef __cplusplus
extern "C"
{
#endif	//	__cplusplus

#if 1
#define CLASS_CONNECTOR_SERVICE		"com/dogtra/pf/handheld/service/ConnectorService"
#define CLASS_DEVICE_DETAIL_INFO	"com/dogtra/pf/handheld/data/DeviceDetailInfo"
#define CLASS_CONNECTOR_INFO		"com/dogtra/pf/handheld/data/ConnectorInfo"
#define CLASS_DEVICE_INFO			"com/dogtra/pf/handheld/data/DeviceInfo"

#define CLASS_IDX_CONNECTOR_SERVICE		0
#define CLASS_IDX_DEVICE_DETAIL_INFO	1

#endif

static Pettra::Pathfinder	*s_pPathfinder = NULL;

static void addDeviceCallback( void *pArg );
static void deviceInfoCallback( void *pArg );

static void slotConflictCallback( void *pArg );
static void scanResultCallback( void *pArg );
static void collarPairingModeCallback( void *pArg ) {};

static void eCollarCommandFailureCallback( void *pArg );
static void eCollarNickSuccessCallback( void *pArg );
static void batteryLevelCallback( void *pArg );

static void sharingInfoCallback( void *pArg );
static void sharingSuccessCallback( void *pArg );
static void sharingCancelCallback( void *pArg );

void scanResultCallback( void *pArg )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jmethodID callbackID = NULL;
	bool needDetach = false;

	::CH_SLOT *pChSlot = NULL;

	assert( pArg != NULL );

	pChSlot = reinterpret_cast< ::CH_SLOT * >( pArg );

	LOGV( "%s: slotId: %d freq: %d", __func__, pChSlot->slot, pChSlot->ch );

	assert( s_pPathfinder != NULL );

	pEnv = s_pPathfinder->GetJNIEnv( &needDetach );

	if( s_pPathfinder->FindClass( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE, &clazz ) == false )
	{
		LOGE( "Failed to find class: idx: %d %s", CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}

		return ;
	}

	callbackID = pEnv->GetStaticMethodID( clazz, "scanResultCallBack", "(II)V" );
	if( callbackID == NULL )
	{
		LOGE( "Failed to find method scanResultCallBack...");
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return ;
	}

	pEnv->CallStaticVoidMethod( clazz, callbackID, pChSlot->slot, pChSlot->ch );

	if( needDetach == true )
	{
		s_pPathfinder->DetachJNI();
	}

	return;
}

void slotConflictCallback( void *pArg )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jmethodID callbackID = NULL;
	bool needDetach = false;

	int *pSlotId = NULL;

	assert( pArg != NULL );

	pSlotId = reinterpret_cast< int * >( pArg );

	LOGV( "%s: slotId: %d", __func__, *pSlotId );

	assert( s_pPathfinder != NULL );

	pEnv = s_pPathfinder->GetJNIEnv( &needDetach );

	if( s_pPathfinder->FindClass( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE, &clazz ) == false )
	{
		LOGE( "Failed to find class: idx: %d %s", CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}

		return ;
	}

	callbackID = pEnv->GetStaticMethodID( clazz, "slotConflictCallBack", "(I)V" );
	if( callbackID == NULL )
	{
		LOGE( "Failed to find method sharingSuccessCallBack...");
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return ;
	}

	pEnv->CallStaticVoidMethod( clazz, callbackID, *pSlotId );

	if( needDetach == true )
	{
		s_pPathfinder->DetachJNI();
	}

	return;
}

void eCollarNickSuccessCallback( void *pArg )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jmethodID callbackID = NULL;
	bool needDetach = false;

	LOGV( "%s\n", __func__ );

	assert( s_pPathfinder != NULL );

	pEnv = s_pPathfinder->GetJNIEnv( &needDetach );

	if( s_pPathfinder->FindClass( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE, &clazz ) == false )
	{
		LOGE( "Failed to find class: idx: %d %s", CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}

		return ;
	}

	callbackID = pEnv->GetStaticMethodID( clazz, "eCollarNickSuccedCallBack", "()V" );
	if( callbackID == NULL )
	{
		LOGE( "Failed to find method eCollarNickSuccedCallBack...");
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return ;
	}

	pEnv->CallStaticVoidMethod( clazz, callbackID );

	if( needDetach == true )
	{
		s_pPathfinder->DetachJNI();
	}

	return;
}

void eCollarCommandFailureCallback( void *pArg )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jmethodID callbackID = NULL;
	bool needDetach = false;

	LOGV( "%s\n", __func__ );

	assert( s_pPathfinder != NULL );

	pEnv = s_pPathfinder->GetJNIEnv( &needDetach );

	if( s_pPathfinder->FindClass( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE, &clazz ) == false )
	{
		LOGE( "Failed to find class: idx: %d %s", CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}

		return ;
	}

	callbackID = pEnv->GetStaticMethodID( clazz, "eCollarCommandFailureCallBack", "()V" );
	if( callbackID == NULL )
	{
		LOGE( "Failed to find method eCollarCommandFailureCallBack...");
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return ;
	}

	pEnv->CallStaticVoidMethod( clazz, callbackID );

	if( needDetach == true )
	{
		s_pPathfinder->DetachJNI();
	}

	return;
}

void batteryLevelCallback( void *pArg )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jmethodID callbackID = NULL;
	bool needDetach = false;

	int *pLevel = NULL;

	assert( pArg != NULL );

	pLevel = reinterpret_cast< int * >( pArg );

	LOGV( "%s: level: %d", __func__, *pLevel );

	assert( s_pPathfinder != NULL );

	pEnv = s_pPathfinder->GetJNIEnv( &needDetach );

	if( s_pPathfinder->FindClass( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE, &clazz ) == false )
	{
		LOGE( "Failed to find class: idx: %d %s", CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}

		return ;
	}

	callbackID = pEnv->GetStaticMethodID( clazz, "batteryLevelCallBack", "(I)V" );
	if( callbackID == NULL )
	{
		LOGE( "Failed to find method sharingSuccessCallBack...");
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return ;
	}

	pEnv->CallStaticVoidMethod( clazz, callbackID, *pLevel);

	if( needDetach == true )
	{
		s_pPathfinder->DetachJNI();
	}

	return;
}

void sharingInfoCallback( void *pArg )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jmethodID callbackID = NULL;
	bool needDetach = false;

	::DEVICE_INFO *pInfo = NULL;
	jstring strName;

	assert( pArg != NULL );

	pInfo = reinterpret_cast< ::DEVICE_INFO * >( pArg );

	LOGV( "%s: id 0x%04x | f %d s %d p %d | shared: %d name: %16s\n", __func__, \
			pInfo->id, pInfo->freq, pInfo->slot, pInfo->period, pInfo->shared, pInfo->name );

	assert( s_pPathfinder != NULL );

	pEnv = s_pPathfinder->GetJNIEnv( &needDetach );
	strName = pEnv->NewStringUTF( pInfo->name );

	if( s_pPathfinder->FindClass( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE, &clazz ) == false )
	{
		LOGE( "Failed to find class: idx: %d %s", CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}

		return ;
	}

	callbackID = pEnv->GetStaticMethodID( clazz, "sharingInfoCallBack", "(IIILjava/lang/String;I)V" );
	if( callbackID == NULL )
	{
		LOGE( "Failed to find method sharingInfoCallBack...");
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return ;
	}

	pEnv->CallStaticVoidMethod( clazz, callbackID, \
			pInfo->id, reinterpret_cast< int >( pInfo->slot ), reinterpret_cast< int >( pInfo->freq ), strName );

	pEnv->DeleteLocalRef( strName );

	if( needDetach == true )
	{
		s_pPathfinder->DetachJNI();
	}

	return;
}

void sharingSuccessCallback( void *pArg )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jmethodID callbackID = NULL;
	bool needDetach = false;

	LOGV( "%s\n", __func__ );

	assert( s_pPathfinder != NULL );

	pEnv = s_pPathfinder->GetJNIEnv( &needDetach );

	if( s_pPathfinder->FindClass( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE, &clazz ) == false )
	{
		LOGE( "Failed to find class: idx: %d %s", CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}

		return ;
	}

	callbackID = pEnv->GetStaticMethodID( clazz, "sharingSuccessCallBack", "()V" );
	if( callbackID == NULL )
	{
		LOGE( "Failed to find method sharingSuccessCallBack...");
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return ;
	}

	pEnv->CallStaticVoidMethod( clazz, callbackID );

	if( needDetach == true )
	{
		s_pPathfinder->DetachJNI();
	}

	return;
}

void sharingCancelCallback( void *pArg )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jmethodID callbackID = NULL;
	bool needDetach = false;

	LOGV( "%s\n", __func__ );

	assert( s_pPathfinder != NULL );

	pEnv = s_pPathfinder->GetJNIEnv( &needDetach );

	if( s_pPathfinder->FindClass( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE, &clazz ) == false )
	{
		LOGE( "Failed to find class: idx: %d %s", CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}

		return ;
	}

	callbackID = pEnv->GetStaticMethodID( clazz, "sharingCanceledCallBack", "()V" );
	if( callbackID == NULL )
	{
		LOGE( "Failed to find method sharingCanceledCallBack...");
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return ;
	}

	pEnv->CallStaticVoidMethod( clazz, callbackID );

	if( needDetach == true )
	{
		s_pPathfinder->DetachJNI();
	}

	return;
}

static ADF_CALLBACKS s_callbacks =
{
	AddDeviceCallback : addDeviceCallback,
	DeviceInfoCallback : deviceInfoCallback,

	SlotConflictCallback : slotConflictCallback,
	ScanResultCallback : scanResultCallback,
	CollarPairingModeCallback : collarPairingModeCallback,

	eCollarCommandFailureCallback : eCollarCommandFailureCallback,
	eCollarNickSuccessCallback : eCollarNickSuccessCallback,
	batteryLevelCallback : batteryLevelCallback,

	SharingInfoCallback : sharingInfoCallback,
	SharingSuccessCallback : sharingSuccessCallback,
	SharingCancelCallback : sharingCancelCallback,
};

#if 0
JNIEnv* getJNIEnv( JavaVM* vm )
{
	if( vm == NULL )
	{
		LOGE( "Failed to get JNIEnv." );
		return NULL;
	}

	JNIEnv *pEnv = NULL;
	jint ret = vm->GetEnv( ( void** )&pEnv, JNI_VERSION_1_6 );

	switch( ret )
	{
		case	JNI_OK :
			LOGV( "GetEnv: return JNI_OK. pEnv: %p", pEnv );
			return pEnv;
		case	JNI_EDETACHED :
			LOGV( "GetEnv: return JNI_EDETACHED. pEnv: %p", pEnv );
			// Thread not attached
			// TODO : If calling AttachCurrentThread() on a native thread
			// must call DetachCurrentThread() in future.
			// see: http://developer.android.com/guide/practices/design/jni.html
			if( vm->AttachCurrentThread( &pEnv, NULL ) < 0 )
			{
				LOGE( "Failed to AttachCurrentThread()" );
				return NULL;
			}
			LOGV( "AttachCurrentThread: pEnv: %p", pEnv );
			return pEnv;
		case JNI_EVERSION :
			LOGD( "JNI interface version 1.6 not supported." );
		default:
			LOGD( "Failed to get the environment by GetEnv()" );
			return NULL;
	}
}

jclass getClassID( JNIEnv *pEnv, const char *className )
{
	jclass cls;

	if( ( pEnv == NULL ) || ( className == NULL ) )
	{
		return NULL;
	}

	cls = pEnv->FindClass( className );
	if( !cls )
	{
		LOGD( "Failed to find class: %s", className );
	}

	return cls;
}

typedef struct JniMethodInfo_S
{
	JNIEnv		*pEnv;
	jclass		classID;
	jmethodID	methodID;
} JniMethodInfo;

bool getStaticMethodInfo( JavaVM* vm, JniMethodInfo &methodinfo, \
		const char* className, const char *methodName, const char *paramCode)
{
	jmethodID methodID = 0;
	JNIEnv *pEnv = NULL;
	bool bRet = false;

	do
	{
		pEnv = getJNIEnv( vm );
		if( ! pEnv )
		{
			break;
		}

		jclass classID = getClassID( pEnv, className );

		methodID = pEnv->GetStaticMethodID( classID, methodName, paramCode );
		if( ! methodID )
		{
			LOGD( "Failed to find static method id of %s", methodName );
			break;
		}

		methodinfo.classID = classID;
		methodinfo.pEnv = pEnv;
		methodinfo.methodID = methodID;

		bRet = true;
	} while ( 0 );

	return bRet;
}

void callback( JavaVM* jvm, const char* value )
{
	JniMethodInfo methodInfo;
	if (! getStaticMethodInfo(jvm, methodInfo, "com/jeidee/glooxforandroid/TestClient", "testCallback", "(Ljava/lang/String;)V"))
	{
		return;
	}

	jstring stringArg = methodInfo.env->NewStringUTF(value);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, stringArg);
	methodInfo.env->DeleteLocalRef(stringArg);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

callback(m_jvm, "call testCallback()");
#endif

void addDeviceCallback( void *pArg )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jmethodID callbackID = NULL;
	bool needDetach = false;

	::DEVICE_INFO *pInfo = NULL;
	int id;
	int period;
	int slot;
	int freq;

	assert( pArg != NULL );

	//INFO_PRINTF( "addDeviceCallback\n");

	pInfo = reinterpret_cast< ::DEVICE_INFO * >( pArg );

	id = pInfo->id;
	period = pInfo->period;
	slot = pInfo->slot;
	freq = pInfo->freq;

	LOGV( "AddDevice: id 0x%04x ch: %d slot: %d\n", pInfo->id, pInfo->freq, pInfo->slot );

	assert( s_pPathfinder != NULL );

	pEnv = s_pPathfinder->GetJNIEnv( &needDetach );

	if( s_pPathfinder->FindClass( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE, &clazz ) == false )
	{
		LOGE( "Failed to find class: idx: %d %s", CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}

		return ;
	}

	callbackID = pEnv->GetStaticMethodID( clazz, "addDeviceCallBack", "(IIIII)V" );
	if( callbackID == NULL )
	{
		LOGE( "Failed to find method addDeviceCallback...");
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return ;
	}

	pEnv->CallStaticVoidMethod( clazz, callbackID, \
			id, period, slot, freq );

	if( needDetach == true )
	{
		s_pPathfinder->DetachJNI();
	}

	return;
}

void deviceInfoCallback( void *pArg )
{
	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jclass infoClazz = NULL;
	jmethodID callbackID = NULL;
	jobject infoJobj = NULL;
	jfieldID fieldID;
	bool needDetach = false;

	int id;

	int slot;
	int freq;
	int period;
	bool shared;

	int batteryLevel;
	int gpsSensitivity;
	int rfSensitivity;
	int rescueMode;

	double latitude;
	double longitude;
	double altitude;
	double speed;
	int dogState;
	int barkState;

	int bearing = -1;	//	Not used

	Pettra::DEVICE_DETAIL_INFO *pInfo = reinterpret_cast< Pettra::DEVICE_DETAIL_INFO * >( pArg );

	assert( pArg != NULL );

	//INFO_PRINTF( "deviceInfoCallback\n");

	latitude = pInfo->latitude;
	longitude = pInfo->longitude;
	altitude = pInfo->altitude;
	speed = pInfo->speed;

	id = pInfo->id;
	slot = pInfo->slotId;
	freq = pInfo->frequencyId;
	period = pInfo->gpsRate;
	shared = pInfo->shared;

	batteryLevel = pInfo->batteryLevel;
	gpsSensitivity = pInfo->gpsSensitivity;
	rfSensitivity = pInfo->rfSensitivity;
	rescueMode = pInfo->rescueMode;

	dogState = pInfo->dogState;
	barkState = pInfo->barkState;

	bearing = pInfo->bearing;

	LOGV( "DeviceInfo: pInfo: %p id: 0x%04x [s %d f %d p %d] GPS: lat: %.3f lng: %.3f\n", \
			pInfo, id, slot, freq, period, latitude, longitude );
	LOGV( "DeviceInfo: GPS: altitude: %.2f m speed: %.2f m/s rssi: %d dBm\n", altitude, speed, rfSensitivity );

	assert( s_pPathfinder != NULL );

	pEnv = s_pPathfinder->GetJNIEnv( &needDetach );

	if( s_pPathfinder->FindClass( CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE, &clazz ) == false )
	{
		LOGE( "Failed to find class: idx: %d %s", CLASS_IDX_CONNECTOR_SERVICE, CLASS_CONNECTOR_SERVICE );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}

		return ;
	}

	callbackID = pEnv->GetStaticMethodID( clazz, "deviceInfoCallBack", \
			"(Lcom/dogtra/pf/handheld/data/DeviceDetailInfo;)V");
	if( callbackID == NULL )
	{
		LOGE( "Failed to find method deviceInfoCallback...");
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return ;
	}

	if( s_pPathfinder->FindClass( CLASS_IDX_DEVICE_DETAIL_INFO, CLASS_DEVICE_DETAIL_INFO, &infoClazz ) == false )
	{
		LOGE( "Failed to find class: idx: %d %s", CLASS_IDX_DEVICE_DETAIL_INFO, CLASS_DEVICE_DETAIL_INFO );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}

		return ;
	}

	jmethodID constructor = pEnv->GetMethodID( infoClazz, "<init>", "()V");
    infoJobj = pEnv->NewObject( infoClazz, constructor, "()V" );

	fieldID = pEnv->GetFieldID( infoClazz, "id", "I");
	pEnv->SetIntField( infoJobj, fieldID, id );

	fieldID = pEnv->GetFieldID( infoClazz, "latitude", "D");
	pEnv->SetDoubleField( infoJobj, fieldID, latitude );

	fieldID = pEnv->GetFieldID( infoClazz, "longitude", "D");
	pEnv->SetDoubleField( infoJobj, fieldID, longitude );

	fieldID = pEnv->GetFieldID( infoClazz, "altitude", "D");
	pEnv->SetDoubleField( infoJobj, fieldID, altitude );

	fieldID = pEnv->GetFieldID( infoClazz, "speed", "D");
	pEnv->SetDoubleField( infoJobj, fieldID, speed );

	fieldID = pEnv->GetFieldID( infoClazz, "bearing", "I");
	pEnv->SetIntField( infoJobj, fieldID, bearing );

	fieldID = pEnv->GetFieldID( infoClazz, "slotId", "I");
	pEnv->SetIntField( infoJobj, fieldID, slot );

	fieldID = pEnv->GetFieldID( infoClazz, "frequencyId", "I");
	pEnv->SetIntField( infoJobj, fieldID, freq );

	fieldID = pEnv->GetFieldID( infoClazz, "batteryLevel", "I");
	pEnv->SetIntField( infoJobj, fieldID, batteryLevel );

	fieldID = pEnv->GetFieldID( infoClazz, "rescueMode", "I");
	pEnv->SetIntField( infoJobj, fieldID, rescueMode );

	fieldID = pEnv->GetFieldID( infoClazz, "gpsSensitivity", "I");
	pEnv->SetIntField( infoJobj, fieldID, gpsSensitivity );

	fieldID = pEnv->GetFieldID( infoClazz, "rfSensitivity", "I");
	pEnv->SetIntField( infoJobj, fieldID, rfSensitivity );

	fieldID = pEnv->GetFieldID( infoClazz, "gpsRate", "I");
	pEnv->SetIntField( infoJobj, fieldID, period );

	fieldID = pEnv->GetFieldID( infoClazz, "dogState", "I");
	pEnv->SetIntField( infoJobj, fieldID, dogState );

	fieldID = pEnv->GetFieldID( infoClazz, "barkState", "I");
	pEnv->SetIntField( infoJobj, fieldID, barkState );

	fieldID = pEnv->GetFieldID( infoClazz, "shared", "Z");
	pEnv->SetBooleanField( infoJobj, fieldID, shared );

	pEnv->CallStaticVoidMethod( clazz, callbackID, infoJobj );

	pEnv->DeleteLocalRef( infoJobj );

	if( needDetach == true )
	{
		s_pPathfinder->DetachJNI();
	}
	return;
}

jint JNI_OnLoad(JavaVM* vm, void* reserved) {
	if( vm == NULL )
	{
		LOGE( "Failed to get JNIEnv." );
		return JNI_FALSE;
	}

	s_pPathfinder = new Pettra::Pathfinder( vm );

	if( s_pPathfinder != NULL )
	{
		s_pPathfinder->Init();
		s_pPathfinder->Open();
		s_pPathfinder->SetCallbacks( &s_callbacks );
		s_pPathfinder->Start();
		s_pPathfinder->InitJNI( vm, NULL );
	}

	return JNI_VERSION_1_6;
}

void JNI_OnUnload(JavaVM* vm, void* reserved) {
	LOGD("JNI_OnUnload");

	if( s_pPathfinder != NULL )
	{
		s_pPathfinder->Stop();
		s_pPathfinder->Close();
	}
}

extern "C" JNIEXPORT jobject JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_getConnectorInfo(JNIEnv *env, jobject obj) {

	JNIEnv *pEnv = NULL;
	jclass clazz = NULL;
	jmethodID methodID = NULL;
	jobject newObject = NULL;
	bool needDetach = false;
	jfieldID fieldID = NULL;

	::CONNECTOR_INFO connectorInfo;

	assert( s_pPathfinder != NULL );

	s_pPathfinder->GetConnectorInfo( connectorInfo );

	pEnv = s_pPathfinder->GetJNIEnv( &needDetach );
	if( pEnv == NULL )
	{
		LOGE( "GetJNIEnv() failed!!!" );
		return NULL;
	}
	clazz = pEnv->FindClass( CLASS_CONNECTOR_INFO );
	if( clazz == NULL )
	{
		LOGW( "Failed to find class: %s", CLASS_CONNECTOR_INFO );
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return NULL;
	}

	methodID = pEnv->GetMethodID( clazz, "<init>", "()V" );
	if( methodID == NULL )
	{
		LOGE( "Failed to find method <init>...");
		if( needDetach == true )
		{
			s_pPathfinder->DetachJNI();
		}
		return NULL;
	}
	newObject = pEnv->NewObject( clazz, methodID, "()V");

	fieldID = pEnv->GetFieldID( clazz, "id", "I");
	env->SetIntField(newObject, fieldID, connectorInfo.id );

	fieldID = pEnv->GetFieldID( clazz, "sleepMode", "I");
	env->SetIntField(newObject, fieldID, connectorInfo.sleep );

	fieldID = pEnv->GetFieldID( clazz, "gpsRate", "I");
	env->SetIntField(newObject, fieldID, connectorInfo.period );

	fieldID = pEnv->GetFieldID( clazz, "slotId", "I");
	env->SetIntField(newObject, fieldID, connectorInfo.slot );

	fieldID = pEnv->GetFieldID( clazz, "frequencyId", "I");
	env->SetIntField(newObject, fieldID, connectorInfo.freq );

	fieldID = pEnv->GetFieldID( clazz, "nation", "I");
	env->SetIntField(newObject, fieldID, connectorInfo.nation );

	if( needDetach == true )
	{
		s_pPathfinder->DetachJNI();
	}

	return newObject;
}

//extern "C" JNIEXPORT jobjectArray JNICALL
//Java_com_dogtra_pf_handheld_service_ConnectorService_getDeviceList(JNIEnv *env, jobject obj) {
extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_setDeviceList(	JNIEnv *pEnv, jobject obj, \
		jobjectArray arr, jint size )
{
	jclass targetClass = pEnv->FindClass( CLASS_DEVICE_INFO );
	jfieldID fieldID;
	jsize length = pEnv->GetArrayLength( arr );

	std::map< int, ::DEVICE_INFO > deviceMap;
	::DEVICE_INFO devInfo;

	for( int i = 0 ; i < length ; i++ )
	{
		jobject jobj = pEnv->GetObjectArrayElement( arr, i );

		if( jobj == NULL )
		{
			return;
		}

		fieldID = pEnv->GetFieldID( targetClass, "id", "I" );
		devInfo.id = pEnv->GetIntField( jobj, fieldID );

		fieldID = pEnv->GetFieldID( targetClass, "slotId", "I" );
		devInfo.slot = pEnv->GetIntField( jobj, fieldID );

		fieldID = pEnv->GetFieldID( targetClass, "frequencyId", "I" );
		devInfo.freq = pEnv->GetIntField( jobj, fieldID );

		fieldID = pEnv->GetFieldID( targetClass, "shared", "Z" );
		devInfo.shared = pEnv->GetBooleanField( jobj, fieldID );

		fieldID = pEnv->GetFieldID( targetClass, "gpsRate", "I" );
		devInfo.period = pEnv->GetIntField( jobj, fieldID );

		devInfo.name[ 0 ] = '\0';

		deviceMap[ devInfo.slot ] = devInfo;
		//deviceMap.insert( std::pair< int, ::DEVICE_INFO >( devInfo.slot, devInfo ) );
	}

	if( s_pPathfinder != NULL )
	{
		s_pPathfinder->Update( deviceMap );
	}

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_startPairing(JNIEnv *, jobject)
{
	IOCTL_CMD_DATA	cmdData;

	assert( s_pPathfinder != NULL );

	cmdData.rf_cmd = PF_CMD_PAIRING;
	s_pPathfinder->SendCommand( ADF_CMD_PAIR_START, &cmdData );

	return ;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_cancelPairing(JNIEnv *, jobject)
{
	assert( s_pPathfinder != NULL );

	s_pPathfinder->SendCommand( ADF_CMD_PAIR_CANCEL );

	return ;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_setConnectorGpsData(JNIEnv *env, jobject obj, jdouble latitude, jdouble longitude, jdouble altitude, jdouble speed, jint bearing) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "setConnectorGpsData call latitude: %lf, longitude: %lf, altitude: %lf, speed: %lf, bearing: %d", latitude, longitude, altitude, speed, bearing);
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_changeGpsRate(JNIEnv *env, jobject obj, jint id, jint gpsRate) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "changeGpsRate call id: %d, gpsRate: %d", id, gpsRate);

	::IOCTL_CMD_DATA cmdData;

	assert( s_pPathfinder != NULL );

	cmdData.rf_cmd = PF_CMD_CHANGE_PERIOD_REQ;
	cmdData.changePeriod.id = id;
	cmdData.changePeriod.period = static_cast< ::PERIOD >( gpsRate );

	s_pPathfinder->SendCommand( ADF_CMD_CHANGE_PERIOD, &cmdData );

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_deleteDevice(JNIEnv *env, jobject obj, jint id) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "deleteDevice call id: %d", id);

	::IOCTL_CMD_DATA cmdData;

	assert( s_pPathfinder != NULL );

	cmdData.rf_cmd = PF_CMD_DELETE_DOG;
	cmdData.deleteDevice.id = id;

	s_pPathfinder->SendCommand( ADF_CMD_DELETE_DEVICE, &cmdData );

}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_startECollarConstant(JNIEnv *env, jobject obj, jint id, jint level) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "startECollarConstant call id: %d, level: %d", id, level);

	::IOCTL_CMD_DATA cmdData;

	assert( s_pPathfinder != NULL );

	cmdData.rf_cmd = PF_CMD_NICK_REQ;
	cmdData.eCollar.id = id;
	cmdData.eCollar.level = level;

	s_pPathfinder->SendCommand( ADF_CMD_EC_START_CONST, &cmdData );

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_startECollarTone(JNIEnv *env, jobject obj, jint id) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "startECollarTone call id: %d", id);

	::IOCTL_CMD_DATA cmdData;

	assert( s_pPathfinder != NULL );

	cmdData.rf_cmd = PF_CMD_TONE;
	cmdData.eCollar.id = id;
	cmdData.eCollar.level = 0x14;	//	0x14 means request ACK per 4 requests.

	s_pPathfinder->SendCommand( ADF_CMD_EC_START_TONE, &cmdData );

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_startECollarNick(JNIEnv *env, jobject obj, jint id, jint level) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "startECollarNick call id: %d, level: %d", id, level);

	::IOCTL_CMD_DATA cmdData;

	assert( s_pPathfinder != NULL );

	cmdData.rf_cmd = PF_CMD_NICK_ONE;
	cmdData.eCollar.id = id;
	cmdData.eCollar.level = level;

	s_pPathfinder->SendCommand( ADF_CMD_EC_NICK_ONE, &cmdData );

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_stopECollar(JNIEnv *env, jobject obj, jint id) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "stopECollar call id: %d", id);

	assert( s_pPathfinder != NULL );

	s_pPathfinder->SendCommand( ADF_CMD_EC_STOP );

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_scanId(JNIEnv *evn, jobject obj, jint slotId, jint frequencyId) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "scanId call slotId: %d, frequencyId: %d", slotId, frequencyId);

	::IOCTL_CMD_DATA cmdData;

	assert( s_pPathfinder != NULL );

	cmdData.rf_cmd = PF_CMD_UNDEFINED;
	//cmdData.changeSlotFreq.id = id;
	cmdData.changeSlotFreq.slotId = slotId;
	cmdData.changeSlotFreq.freq = static_cast< ::FREQ >( frequencyId );

	s_pPathfinder->SendCommand( ADF_CMD_SCAN_SLOT_FREQ, &cmdData );

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_changeId(JNIEnv *env, jobject obj, jint id, jint slotId, jint frequencyId) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "changeId call id: %d, slotId: %d, frequencyId: %d", id, slotId, frequencyId);

	::IOCTL_CMD_DATA cmdData;

	assert( s_pPathfinder != NULL );

	cmdData.rf_cmd = PF_CMD_CHANGE_SLOT_REQ;
	cmdData.changeSlotFreq.id = id;
	cmdData.changeSlotFreq.slotId = slotId;
	cmdData.changeSlotFreq.freq = static_cast< ::FREQ >( frequencyId );

	s_pPathfinder->SendCommand( ADF_CMD_CHANGE_SLOT_FREQ, &cmdData );

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_setRescueMode(JNIEnv *env, jobject obj, jint id, jint rescue) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "setRescueMode call id: %d, rescue: %d", id, rescue);

	::IOCTL_CMD_DATA cmdData;

	assert( s_pPathfinder != NULL );

	cmdData.rf_cmd = PF_CMD_RESCUE_MODE;
	cmdData.rescueMode.id = id;
	cmdData.rescueMode.mode = ( rescue == 0 ) ? false : true;

	s_pPathfinder->SendCommand( ADF_CMD_RESCUE_MODE, &cmdData );

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_setSleepMode(JNIEnv *env, jobject obj, jint sleep) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "setSleepMode call sleep: %d", sleep);

	::IOCTL_CMD_DATA cmdData;

	assert( s_pPathfinder != NULL );

	cmdData.rf_cmd = PF_CMD_SLEEP_MODE;
	cmdData.sleepMode.mode = ( sleep == 0 ) ? false : true;

	s_pPathfinder->SendCommand( ADF_CMD_SLEEP_MODE, &cmdData );

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_sharingStart(JNIEnv *env, jobject obj, jint id, jint slotId, jint frequencyId, jstring deviceName) {

	::IOCTL_CMD_DATA cmdData;
	const char *str = env->GetStringUTFChars(deviceName, NULL);

	LOGV( "sharingStart call id: %d, slotId: %d, frequencyId: %d, deviceName: %s", id, slotId, frequencyId, str);

	assert( s_pPathfinder != NULL );

	cmdData.devInfo.id = id;
	cmdData.devInfo.slot = slotId;
	cmdData.devInfo.freq = static_cast< ::FREQ >( frequencyId );
	cmdData.rf_cmd = ( IS_HUNTER( cmdData.devInfo.id ) == true ) ? PF_CMD_SHARE_HUNTER_INFO : PF_CMD_SHARE_DOG_INFO;
	::memcpy( cmdData.devInfo.name, str, HUNTER_NAME_LEN );

	s_pPathfinder->SendCommand( ADF_CMD_SHARING_SEND_INFO, &cmdData );

	env->ReleaseStringUTFChars(deviceName, str);

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_sharingWaitStart(JNIEnv *env, jobject obj) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "sharingWaitStart call");

	assert( s_pPathfinder != NULL );

	s_pPathfinder->SendCommand( ADF_CMD_SHARING_WAIT );

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_sharingCancel(JNIEnv *env, jobject obj) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "sharingCancel call");

	assert( s_pPathfinder != NULL );

	s_pPathfinder->SendCommand( ADF_CMD_SHARING_CANCEL );

	return;
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_changeDuplicatedId(JNIEnv *env, jobject obj, jint id, jint slotId, jint frequencyId) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "changeDuplicatedId call id: %d, slotId: %d, frequencyId: %d", id, slotId, frequencyId);
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_setChangeName(JNIEnv *env, jobject obj, jint id , jstring deviceName) {
	const char *str = env->GetStringUTFChars(deviceName, NULL);
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "setChangeName call id: %d, deviceName: %s", id, str);
	env->ReleaseStringUTFChars(deviceName, str);
}

extern "C" JNIEXPORT void JNICALL
Java_com_dogtra_pf_handheld_service_ConnectorService_appExit(JNIEnv *env, jobject obj) {
	__android_log_print(ANDROID_LOG_DEBUG, "log=>jni", "appExit call");

	if( s_pPathfinder != NULL )
	{
		s_pPathfinder->Stop();
		s_pPathfinder->Close();
	}
}

#ifdef __cplusplus
}
#endif	//	__cplusplus

