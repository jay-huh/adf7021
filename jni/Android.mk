LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libadf_jni
LOCAL_MODULE_TAGS := eng optional	#	eng, user, optional
LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)
#LOCAL_MODULE_RELATIVE_PATH := hw

ifeq ($(TARGET_ARCH_ABI), $(filter $(TARGET_ARCH_ABI), armeabi-v7a))
	LOCAL_CFLAGS := -march=armv7-a -mfloat-abi=softfp -mfpu=neon
	LOCAL_CFLAGS += -DHAVE_NEON=1
	LOCAL_LDFLAGS := -march=armv7-a -Wl,--fix-cortex-a8
endif

LOCAL_CFLAGS += -Wall
LOCAL_CPPFLAGS += -Wall #-frtti -fexceptions
LOCAL_LDFLAGS +=

LOCAL_CFLAGS += \
	-DHAL_HARDWARE_MODULE_ID=\"adf\" \
	-DLOG_TAG=\"adf\"

#	for Test and Debug
LOCAL_CFLAGS += -D __ANDROID_LOG__
#LOCAL_CFLAGS += -D __DEBUG__
#LOCAL_CFLAGS += -D __DEBUG_RF__
#LOCAL_CFLAGS += -D __DEBUG_THREAD__
#LOCAL_CFLAGS += -D __MEM_LEAK__

#LOCAL_CPP_FEATURES += rtti

LOCAL_SRC_FILES := \
	path_finder.cpp \
	pf_jni.cpp

#	debug.c \
	mutex.cpp \
	thread.cpp \

LOCAL_C_INCLUDES := \
	external/stlport/stlport \
	bionic \
	$(LOCAL_PATH)/../hal

LOCAL_SHARED_LIBRARIES := \
	libstlport \
	libhardware

LOCAL_LDLIBS := \
	-pthread \
	-llog

include $(BUILD_SHARED_LIBRARY)
