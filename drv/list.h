#ifndef	__LIST_H__
#define	__LIST_H__

#define		MAX_LIST			8
#define		LIST_NAME_LEN		32

typedef struct LIST_NODE_S
{
	void				*pData;
	size_t				size;		//	in bytes
	uint32_t		key;

	struct list_head	list;
#ifndef __MEM_LEAK__
//	32 kB: Best Performance by jyhuh 2018-04-10 17:08:49
} __attribute__ (( aligned( 32 * 1024 ) )) LIST_NODE;	//	32 kB: Best Performance by jyhuh 2018-04-10 17:08:49
#else	//	__MEM_LEAK__
} __attribute__ (( aligned( 1024 * 1024 ) )) LIST_NODE;	//	Test for Memory leak
#endif //	__MEM_LEAK__

typedef struct LIST_S
{
	char				name[ LIST_NAME_LEN ];
	struct list_head	*pHead;
	spinlock_t			spinlock;
	int					count;
} LIST;

int list_init( void );
int list_alloc( const struct list_head * pList, const char * pName );
bool list_free( const struct list_head * pList );
bool list_is_empty( const struct list_head *pList );
int list_count( const struct list_head *pList );

bool list_add_node( const struct list_head *pList, const void * pData, size_t size, const uint32_t key, const bool toHead );
int list_del_node( const struct list_head *pList, const uint32_t keyMask );

LIST_NODE* list_find_node( const struct list_head *pList, const uint32_t keyMask );
bool list_insert( const struct list_head *pList, const void * pData, size_t size, const uint32_t key );
bool list_insert_head( const struct list_head *pList, const void * pData, size_t size, const uint32_t key );
int list_remove( const struct list_head *pList, const uint32_t keyMask );
bool list_push( const struct list_head *pList, const void * pData, size_t size, const uint32_t key );
bool list_push_head( const struct list_head *pList, const void * pData, size_t size, const uint32_t key );
LIST_NODE* list_pop( const struct list_head *pList );
void* list_pop_data( const struct list_head *pList );
void* list_find_data( const struct list_head *pList, const uint32_t keyMask );

#endif	//	__LIST_H__
