#ifndef	__TIMER_H__
#define	__TIMER_H__

#define		INIT_DURATION	( 10 )

#define		MS_TO_NS( x )	( x * 1E6L )
#define		MS_TO_US( x )	( x * 1000 )

#define		US_TO_NS( x )	( x * 1000 )

#define		MAX_TIMER			4
#define		TIMER_NAME_LEN		32

typedef enum TIMER_TYPE_E
{
	TYPE_HRTIMER = 0,
	TYPE_TIMER,

	TIMER_TYPE_END
} TIMER_TYPE;

typedef		enum hrtimer_restart ( *HR_HANDLER )(struct hrtimer *);
typedef		void ( *TIMER_HANDLER )(unsigned long);

typedef struct TIMER_S
{
	TIMER_TYPE		type;
	char			* name[ TIMER_NAME_LEN ];
	void			* pTimer;
	void			* pTimer_handler;

	//HR_HANDLER	hrtimer_handler;
	//TIMER_HANDLER	timer_handler;
} TIMER;

/*	Timer	*/
int timer_init( void );
void timer_exit( const void * pData );

int timer_register( const  void * pData, const TIMER_TYPE type, const char * pName, void *pHandler );
int timer_deregister( const void * pData );

int timer_start( const void *pData, const int expire_time );
int timer_start_ns( const void *pData, const long expire_time );
int timer_stop( const void * pData );
int timer_restart( const void *pData, const int expire_time );
int64_t timer_remaining( const void *pData );

#endif	/*	__TIMER_H__	*/
