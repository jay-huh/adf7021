#ifndef	__PF_CMD_H__
#define	__PF_CMD_H__

#include	"adf7021.h"

#ifdef __cplusplus
extern "C" {
#endif

/*	PREAMBLE:	0xAAAA
 *	SYNCWORD:	0xA9AA66
 *	dummy:		0xAA
 *	MAX_COMMAND: 22 bytes
 *
 *  |<-------- tx_header ------->|
 *  |<---------------------- TX Command -------------------->|
 *	|PREAMBLE | SYNCWORD | dummy | COMMAND | DATA | CHECKSUM |
 *	                     |<----------- RX Command ---------->|
 *	*/

#define		PF_PREAMBLE				0xAAAAAAAA
#define		PF_SYNC_WORD			0xA9AA66
//#define		PF_SYNC_WORD			0xA96A9A	//	for Test
//#define		PF_MAX_PACKET_LEN		( 23 )
#define		PF_MAX_PACKET_LEN		MAX_PACKET_LEN	//	PREAMBLE + SYNCWORD + dummy + PACKET
//#define		MAX_SLOT_INDEX			( 22 )

#define		TONE_LEVEL_ACK_REQ		( 0x14 )

#define		REF_DURATION	( 2 * 1000 )	//	2 secs
#define		SLOT_DURATION	( 80 )			//	80 ms
#define		SHIFT_TIME		( 40 )	// time shifr from GPS information from collar

/*	10 kbps: 100 us per 1 bit
 *	Cmd in byte: cmd_length + tx_header( 6 ) + dummy( 1, 0xAA )
 *	25 bytes / packet: 25 * 8 * 100 = 20 ms
 *	28 bytes / packet: 28 * 8 * 100 = 22.4 ms
 *	30 bytes / packet: 30 * 8 * 100 <= 25 ms
 *
 *	MAX: GPS: 22 + 6 + 1 = 29 bytes = 23.2 ms
 *	*/
#define		TX_TIMEOUT		( 35 )		//	time for TX: syncword to last bit
//#define		TX_HEADER_TIME	( 5 )		//	6 byte = 48 x 100 us
#define		TX_HEADER_TIME	( 7 )		//	8 byte = 64 x 100 us

#define		RX_DURATION		( 40 )		//	time to wait for RX
#define		RX_TIMEOUT		( 30 )		//	time for RX: SWD to last bit
#define		RX_ACK_DURATION ( 20 )		//	time to wait for RX
#define		RX_SHARE_DURATION	( 45 )		//	time to wait for RX
//#define		RX_SHARE_DURATION	( 60 )		//	time to wait for RX

#define		GLOBAL_RX_MARGIN	( 20 )		//	prepare time for expected RX time.
//#define		GLOBAL_RX_MARGIN	( 10 )		//	prepare time for expected RX time.
#define		RX_MARGIN		( 20 )		//	prepare time for expected RX time.
#define		TX_CMD_MARGIN	( 35 )		//	process time for TX cmd in queue.

#define		DELAY_SHARE_CMD			( GLOBAL_RX_MARGIN )
#define		DELAY_PAIR_CMD			( GLOBAL_RX_MARGIN + 10 )
/*	PF ????		*/
//#define		DELAY_SHARE_CMD			( GLOBAL_RX_MARGIN + 5 )
//#define		DELAY_PAIR_CMD			( GLOBAL_RX_MARGIN )

#define		LIMIT_TX_CMD_COUNT		( 10 )
#define		LIMIT_SHARE_TX_COUNT	( 15 )

/*	NOTE: Command Byte ordering: LSB first!!!!
 *
 *	Bit stream:	|  0  |  1  |  2  |  3  |
 *	short		| LSB | MSB |
 *	int			| LSB |  1  |  2  | MSB |
 *
 *	stream data copied into struct by memcpy() in F/W.
 *
 *	htons() or ntohs() not required.
 *	htonl() used for Syncword for TX.
 *
 *	by jyhuh 2017-11-14 09:55:05	*/

#define		PF_CMD_UNDEFINED			( 0x00 )
/*	Global channel Command	*/
#define		PF_CMD_PAIRING				( 0x06 )
#define		PF_CMD_PAIR_RESPONSE		( 0x03 )
#define		PF_CMD_PAIR_STATUS			( 0x08 )
#define		PF_CMD_PAIR_ACK				( 0x09 )
#define		PF_CMD_SLEEP_MODE			( 0x10 )

#define		PF_CMD_SHARE_DOG_INFO		( 0x21 )
#define		PF_CMD_SHARE_HUNTER_INFO	( 0x22 )
#define		PF_CMD_SHARE_CHANGE_SLOT_REQ	( 0x24 )
#define		PF_CMD_SHARE_ACK			( 0x25 )

#define		PF_CMD_TX_TEST				( 0xF0 )
#define		PF_CMD_RX_TEST				( 0xF1 )

/*	Local channel Command	*/
#define		PF_CMD_CHANGE_PERIOD_REQ	( 0x04 )
#define		PF_CMD_CHANGE_SLOT_REQ		( 0x0F )
#define		PF_CMD_DELETE_DOG			( 0x05 )

#define		PF_CMD_CONST				( 0x0B )
#define		PF_CMD_NICK_ONE				( 0x0C )
//#define		PF_CMD_CONST_END			( 0x0D )
//#define		PF_CMD_BUZZER				( 0x0E )	//	Check level field is 0x14 at Collar
#define		PF_CMD_TONE					( 0x0E )	//	Check level field is 0x14 at Collar
#define		PF_CMD_NICK_ACK				( 0x13 )	//	NICK_ACK: 1 / 4 Req.( Nick or Buzzer ) => in Collar src.
#define		PF_CMD_NICK_REQ				( 0x14 )
#define		PF_CMD_RESCUE_MODE			( 0x15 )
#define		PF_CMD_GPS_INFO				( 0x07 )

#define		ACK_PER_REQ_TIMES			( 4 )
#define		E_COLLAR_LIMIT_TIME			( 12 * 1000 )

/*	slot information	*/
typedef struct PF_SLOT_INFO_S
{
    int			slotId;			//	1 ~ 22
	uint32_t	id;
	FREQ		freq;		//	1 ~ 5
	PERIOD		period;	//	1 ~ 5

	uint32_t	latitude;		//	real | frac: 16 bit | 16 bit
	uint32_t	longitude;		//	real | frac: 16 hit | 16 bit
	int			altitude;
	int			velocity;

	int			battery;
	int			gpsSensitivity;
	int			rfSensitivity;
	bool		rescueMode;
} PF_SLOT_INFO;

/*	structure for PF_COMMAND( TX / RX ) fields	*/
#ifndef INVALID_CH_SLOT

#define INVALID_CH_SLOT		( 0xFF )
typedef union CH_SLOT_S
{
	uint8_t ch_slot;

	struct
	{
		uint8_t	slot	:	5;
		uint8_t	ch		:	3;
	};
} CH_SLOT;
#endif	//	INVALID_CH_SLOT

typedef union BAT_INFO_S
{
	uint8_t battery;

	struct
	{
		uint8_t	level : 7;
		uint8_t	sleep : 1;	//	1: sleep
	};
} BAT_INFO;

typedef union DOG_INFO_S
{
	uint16_t dog_info;

	struct
	{
		uint16_t	gps_rssi	:	5;
		/*	reserved field
		 *	from collar: GPS_NV[ 8 ] / RESCUE_MODE[ 9 ]
		 *	RX and update field: rf_rssi[ 9:5 ]	*/
#if 1
		uint16_t	reserved	: 3;
		uint16_t	gps_nv		: 1;
		uint16_t	rescue_mode	: 1;
#else
		union
		{
			uint16_t	rf_rssi	: 5;

			struct
			{
				uint16_t	reserved	: 3;
				uint16_t	gps_nv		: 1;
				uint16_t	rescue_mode	: 1;
			};
		};
#endif
		uint16_t	period		:	3;
		/*	0: normal / 1: running / 2: pointing / 3: treed	*/
		uint16_t	three_axis	:	2;	//	dog state
		uint16_t	bark		:	1;	//	1: bark
	};
} DOG_INFO;

#if 0	//	Collar code
typedef struct RFDogGPS {
	uint8_t instruction;
	uint16_t deviceID;
	uint16_t hunterID;
	uint16_t GPS_LatitudeI; // 2 integer
	uint16_t GPS_LatitudeF; // 2 fractional
	uint16_t GPS_LongitudeI; // 2 integer
	uint16_t GPS_LongitudeF; // 2 fractional
	uint16_t GPS_Alt; // 2
	uint8_t GPS_Spd;
	uint8_t GPS_Cog;
	uint8_t Battrery;
	uint16_t NoBarkStatusRFGPS; // 15:노박, 14~13:3축  12~10:주기 , 9~5:슬롯, 4~0:GPS감도
	uint16_t CheckSum;
} RFDogGPS_Type;

void RF_MakeMsg(void)
{
	RFDogGPS_Type RFGPS;
	uint8_t cycle = 0;
	uint8_t size;

	size = sizeof(RFGPS);
	RFGPS.instruction = GS_GPS_DATA;
	RFGPS.deviceID = SYSInfo.deviceID;
	RFGPS.hunterID = SYSInfo.hunterID;
	if (nmeaGLRMC.NS == 'N') {
		RFGPS.GPS_LatitudeI = nmeaGLRMC.latitudeI;
		RFGPS.GPS_LatitudeF = nmeaGLRMC.latitudeF;
	} else {
		RFGPS.GPS_LatitudeI = nmeaGLRMC.latitudeI * -1;
		RFGPS.GPS_LatitudeF = nmeaGLRMC.latitudeF;
	}
	if (nmeaGLRMC.EW == 'E') {
		RFGPS.GPS_LongitudeI = nmeaGLRMC.longitudeI;
		RFGPS.GPS_LongitudeF = nmeaGLRMC.longitudeF;
	} else {
		RFGPS.GPS_LongitudeI = nmeaGLRMC.longitudeI * -1;
		RFGPS.GPS_LongitudeF = nmeaGLRMC.longitudeF;
	}

	RFGPS.GPS_Alt = (uint16_t) nmeaGGA_GSV.alt;
	RFGPS.GPS_Spd = (uint8_t) (nmeaGLRMC.spd * 10000 / 3600); // 2015.04.28 BJH km/h  -->  m/s*10
	//	RFGPS.GPS_Spd = (uint8_t) (pdop * 10);
	//	RFGPS.GPS_Spd = nmeaGGA_GSV.fixSatelliteCount * 10;
	RFGPS.GPS_Cog = SYSInfo.rfSlot & 0b00011111;
	RFGPS.GPS_Cog |= (SYSInfo.channel << 5) & 0b11100000;

	RFGPS.Battrery = SYSInfo.battery;
	if (CheckBit(RfFlag, RfFlag_ADF_SLEEP)) {
		RFGPS.Battrery += 0x80;
	}

	switch (SYSInfo.rfCycle) {
	case 2:
		cycle = 1;
		break;
	case 6:
		cycle = 2;
		break;
	case 10:
		cycle = 3;
		break;
	case 30:
		cycle = 4;
		break;
	case 120:
		cycle = 5;
		break;
	default:
		cycle = 0;
		break;
	}

	RFGPS.NoBarkStatusRFGPS = 0;
	if (CheckBitH(GpsFlag, GpsFlag_RMC_A) && (UTCFlag == TRUE)) {

	} else {
		RFGPS.NoBarkStatusRFGPS |= (0b1 << RFDogGPS_NV);
	}

	if (CheckBitH(SYSInfo.flag, SystemFlag_RESCUE)) {
		RFGPS.NoBarkStatusRFGPS |= (0b1 << RFDogGPS_RESCUEMODE);
	}

	if ((DogSense.bark & 0b1111) != 0 || CheckBit(dogFlag, dogFlag_BARK)) {
		DogSense.bark = 0;
		ClearBit(dogFlag, dogFlag_BARK);
		RFGPS.NoBarkStatusRFGPS |= (0b1 << RFDogGPS_BARK);
	}
	RFGPS.NoBarkStatusRFGPS |= ((dogFlag & 0b11) << RFDogGPS_3A); // 3A
	RFGPS.NoBarkStatusRFGPS |= ((cycle & 0x07) << RFDogGPS_CYCLE); //rfCycle
	RFGPS.NoBarkStatusRFGPS |= (nmeaGGA_GSV.averageRSSI & 0x1F) << RFDogGPS_GPSCN0; //GPS CN0

	RFGPS.CheckSum = fnGetCrc16((char*) &RFGPS, size);
	memcpy((char*) RfTxMsg, (char*) &RFGPS, size);
	NickCount = 3; // Nick Request
}
#endif	//	Collar code

/*	TX Command	*/
typedef struct PAIRING_S
{
	uint16_t	hunter_id;

	uint16_t	checksum;
} __attribute__ (( packed )) PAIRING;

typedef struct PAIR_STATUS_S
{
	uint16_t	dog_id;
	uint8_t		period;
	uint8_t		slot;
	uint8_t		rf_freq;

	uint16_t	checksum;
} __attribute__ (( packed )) PAIR_STATUS;

typedef struct RF_PERIOD_REQ_S
{
	uint16_t	hunter_id;
	uint16_t	dog_id;
	uint8_t		period;

	uint16_t	checksum;
} __attribute__ (( packed )) RF_PERIOD_REQ;

typedef struct CHANGE_SLOT_REQ_S
{
	uint16_t	hunter_id;
	uint16_t	dog_id;
	CH_SLOT		ch_slot;

	uint16_t	checksum;
} __attribute__ (( packed )) CHANGE_SLOT_REQ;

typedef struct DELETE_DOG_S
{
	uint16_t	hunter_id;
	uint16_t	dog_id;

	uint16_t	checksum;
} __attribute__ (( packed )) DELETE_DOG;

typedef struct NICK_S
{
	uint16_t	hunter_id;
	uint16_t	dog_id;
	uint8_t		level;

	uint16_t	checksum;
} __attribute__ (( packed )) NICK;
typedef NICK		CONST;
typedef NICK		NICK_ONE;
typedef NICK		NICK_REQ;
typedef NICK		TONE;

typedef struct BUZZER_S
{
	uint16_t	hunter_id;
	uint16_t	dog_id;
	uint8_t		level;

	uint16_t	checksum;
} __attribute__ (( packed )) BUZZER;

/*	NOTE: NICK_ACK and PAIR_ACK is same	*/
#if 0
typedef struct NICK_ACK_S
{
	uint16_t	hunter_id;
	//uint16_t	dog_id;

	uint16_t	checksum;
} __attribute__ (( packed )) NICK_ACK;
#endif

#define	RESCUE_MODE_OFF	0x00
#define	RESCUE_MODE_ON	0x80
typedef struct RESCUE_MODE_S
{
	uint16_t	hunter_id;
	uint16_t	dog_id;
	//uint8_t		value;	//	0x0 or 0x80
	uint8_t		mode;	//	0x0 or 0x80

	uint16_t	checksum;
} __attribute__ (( packed )) RESCUE_MODE;

#define	SLEEP_MODE_OFF	0x00
#define	SLEEP_MODE_ON	0x80
#define	SLEEP_ALL_DOGS_ID	0x0

typedef struct SLEEP_MODE_S
{
	uint16_t	hunter_id;
	uint16_t	dog_id;	//	maybe 0x0 ??
	uint8_t		mode;	//	0x00, 0x80

	uint16_t	checksum;
} __attribute__ (( packed )) SLEEP_MODE;

/*	RX Command	*/
typedef struct PAIR_RESPONSE_S
{
	uint16_t	dog_id;
	uint16_t	hunter_id;

	uint16_t	checksum;
} __attribute__ (( packed )) PAIR_RESPONSE;

typedef struct ACK_S
{
	uint16_t	hunter_id;

	uint16_t	checksum;
} __attribute__ (( packed )) ACK;
typedef ACK		PAIR_ACK;
typedef ACK		NICK_ACK;

#if 0
if (nmeaGLRMC.NS == 'N') {
	RFGPS.GPS_LatitudeI = nmeaGLRMC.latitudeI;
	RFGPS.GPS_LatitudeF = nmeaGLRMC.latitudeF;
} else {
	RFGPS.GPS_LatitudeI = nmeaGLRMC.latitudeI * -1;
	RFGPS.GPS_LatitudeF = nmeaGLRMC.latitudeF;
}
if (nmeaGLRMC.EW == 'E') {
	RFGPS.GPS_LongitudeI = nmeaGLRMC.longitudeI;
	RFGPS.GPS_LongitudeF = nmeaGLRMC.longitudeF;
} else {
	RFGPS.GPS_LongitudeI = nmeaGLRMC.longitudeI * -1;
	RFGPS.GPS_LongitudeF = nmeaGLRMC.longitudeF;
}

RFGPS.GPS_Alt = (uint16_t) nmeaGGA_GSV.alt;
RFGPS.GPS_Spd = (uint8_t) (nmeaGLRMC.spd * 10000 / 3600); // 2015.04.28 BJH km/h  -->  m/s*10
if (CheckBit(RfFlag, RfFlag_ADF_SLEEP)) {
	RFGPS.Battrery += 0x80;
}
RFGPS.NoBarkStatusRFGPS |= (nmeaGGA_GSV.averageRSSI & 0x1F) << RFDogGPS_GPSCN0; //GPS CN0
#endif
typedef struct GPS_INFO_S
{
	uint16_t	dog_id;
	uint16_t	hunter_id;

	uint16_t	lat_int;
	uint16_t	lat_frac;
	uint16_t	long_int;
	uint16_t	long_frac;
	uint16_t	altitude;	//	m * 10
	/*	GPS RMC spd field[ knots ] 1 knots = 1.852 km/h = 1852 / 3600 m/s
	 *	Collar F/W: velocity = spd * 10000 / 3600 => m/s * 10 ???
	 *	reverse calculate RMS spd = velocity * 3600 / 10000
	 *
	 *	velocity = spd * 1852 * 100 / 3600 cm/s
	 *	*/
	uint8_t		velocity;	//	m/s * 10
	CH_SLOT		ch_slot;	//	uint8_t
	BAT_INFO	battery;	//	uint8_t
	DOG_INFO	dog_info;	//	uint16_t

	uint16_t	checksum;
} __attribute__ (( packed )) GPS_INFO;

typedef struct SHARE_INFO_S
{
	uint16_t	hunter_id;
	uint16_t	id;

	CH_SLOT		ch_slot;	//	uint8_t
	uint8_t		name[ HUNTER_NAME_LEN ];

	uint16_t	checksum;
} __attribute__ (( packed )) SHARE_INFO;
typedef SHARE_INFO			SHARE_DOG_INFO;
typedef SHARE_INFO			SHARE_HUNTER_INFO;
typedef CHANGE_SLOT_REQ		SHARE_CHANGE_SLOT_REQ;
typedef ACK					SHARE_ACK;

/*	ADF TX / RX Command		*/
typedef struct PF_TX_COMMAND_S
{
	uint32_t	preamble;
	uint32_t	syncword;	//	with dummy 0xAA
	uint8_t		command;

	union
	{
		uint8_t	data[ PF_MAX_PACKET_LEN - ( sizeof( uint16_t ) + ( sizeof( uint32_t ) * 2 ) + sizeof( uint8_t ) ) ];

		PAIRING			pairing;			//	PAIR_RESPONSE
		PAIR_STATUS		pair_status;		//	PAIR_ACK
		RF_PERIOD_REQ	rf_period_req;
		CHANGE_SLOT_REQ	change_slot_req;
		DELETE_DOG		delete_dog;

		NICK			nick;			//	NICK_ACK
		TONE			tone;				//	NICK_ACK. 1 per 4 req.

		SLEEP_MODE		sleep_mode;
		RESCUE_MODE		rescue_mode;

		ACK				ack;
		SHARE_INFO		share_info;
	};
	int retry_cnt;
} __attribute__ (( packed )) PF_TX_COMMAND;

typedef struct PF_RX_COMMAND_S
{
	uint8_t	dummy;		//	0xAA
	uint8_t	command;

	union
	{
		uint8_t	data[ PF_MAX_PACKET_LEN - ( sizeof( uint8_t ) + sizeof( uint8_t ) + sizeof( int ) ) ];

		PAIR_RESPONSE	pair_response;
		ACK				ack;
		//NICK_ACK		nick_ack;
		GPS_INFO		gps_info;
		SHARE_INFO		share_info;
		//SHARE_ACK		share_ack;
	};
	int rssi_dBm;

} __attribute__ (( packed )) PF_RX_COMMAND;

typedef union PF_COMMAND_S
{
	uint8_t	data[ PF_MAX_PACKET_LEN ];

	PF_TX_COMMAND	tx_cmd;
	PF_RX_COMMAND	rx_cmd;
	//} __attribute__ (( packed )) PF_COMMAND;
} __attribute__ (( packed, aligned( PF_MAX_PACKET_LEN ) )) PF_COMMAND;

/*	PREAMBLE:	0xAAAA
 *	SYNCWORD:	0xA9AA66
 *	dummy:		0xAA
 *	MAX_COMMAND: 22 bytes
 *
 *  |<-------- tx_header ------->|
 *  |<---------------------- TX Command -------------------->|
 *	|PREAMBLE | SYNCWORD | dummy | COMMAND | DATA | CHECKSUM |
 *	                     |<----------- RX Command ---------->|
 *	*/

/*	Command Length: CMD to CHECKSUM
 *	Length = sizeof( CMD_STRUCT ) + command		*/
//#define		PF_CMD_LEN( cmd )				( sizeof( cmd ) + sizeof( uint8_t ) )

#define		PF_CMD_PAIRING_LEN				( sizeof( PAIRING ) + sizeof( uint8_t ) )
#define		PF_CMD_PAIR_RESPONSE_LEN		( sizeof( PAIR_RESPONSE ) + sizeof( uint8_t ) )
#define		PF_CMD_PAIR_STATUS_LEN			( sizeof( PAIR_STATUS ) + sizeof( uint8_t ) )
#define		PF_CMD_PAIR_ACK_LEN				( sizeof( PAIR_ACK ) + sizeof( uint8_t ) )
#define		PF_CMD_SLEEP_MODE_LEN			( sizeof( SLEEP_MODE ) + sizeof( uint8_t ) )

#define		PF_CMD_CHANGE_PERIOD_REQ_LEN	( sizeof( RF_PERIOD_REQ ) + sizeof( uint8_t ) )
#define		PF_CMD_CHANGE_SLOT_REQ_LEN		( sizeof( CHANGE_SLOT_REQ ) + sizeof( uint8_t ) )
#define		PF_CMD_DELETE_DOG_LEN			( sizeof( DELETE_DOG ) + sizeof( uint8_t ) )
#define		PF_CMD_CONST_LEN				( sizeof( CONST ) + sizeof( uint8_t ) )
#define		PF_CMD_NICK_ONE_LEN				( sizeof( NICK_ONE ) + sizeof( uint8_t ) )
//#define		PF_CMD_BUZZER_LEN				( sizeof( BUZZER ) + sizeof( uint8_t ) )
#define		PF_CMD_TONE_LEN					( sizeof( TONE ) + sizeof( uint8_t ) )
#define		PF_CMD_NICK_ACK_LEN				( sizeof( NICK_ACK ) + sizeof( uint8_t ) )
#define		PF_CMD_NICK_REQ_LEN				( sizeof( NICK_REQ ) + sizeof( uint8_t ) )
#define		PF_CMD_RESCUE_MODE_LEN			( sizeof( RESCUE_MODE ) + sizeof( uint8_t ) )
#define		PF_CMD_GPS_INFO_LEN				( sizeof( GPS_INFO ) + sizeof( uint8_t ) )

#define		PF_CMD_SHARE_DOG_INFO_LEN		( sizeof( SHARE_DOG_INFO ) + sizeof( uint8_t ) )
#define		PF_CMD_SHARE_HUNTER_INFO_LEN	( sizeof( SHARE_HUNTER_INFO ) + sizeof( uint8_t ) )
#define		PF_CMD_SHARE_CHANGE_SLOT_REQ_LEN	( sizeof( SHARE_CHANGE_SLOT_REQ ) + sizeof( uint8_t ) )
#define		PF_CMD_SHARE_ACK_LEN			( sizeof( SHARE_ACK ) + sizeof( uint8_t ) )

#define		PF_CMD_MAX_LEN					PF_CMD_GPS_INFO_LEN

#ifdef	__KERNEL__

uint16_t fnGetCrc16( const uint8_t *nData, uint16_t wLength);
uint16_t AddCheckSum(uint8_t* buf, size_t size);

size_t get_command_len( const uint8_t command );
uint16_t get_checksum( const PF_RX_COMMAND * const pCmd );
uint16_t generate_checksum( const PF_TX_COMMAND * const pCmd );
bool verify_checksum( const PF_RX_COMMAND * const pCmd );

bool pf_cmd_gen( const void *const pData, RF_COMMAND * const pRfCmd, const PF_RX_COMMAND * const pResponse, const uint8_t pfCmd );
RF_COMMAND* pf_alloc_cmd_gen( const void *const pData, const IOCTL_CMD_DATA * const pCmdData, const uint8_t pfCmd );
RF_COMMAND* pf_cmd_gen_eCollar( const void *const pData, const uint8_t pfCmd, const bool isGlobalTime );
#endif	//	__KERNEL__

#ifdef __cplusplus
}
#endif

#endif	//	__PF_CMD_H__
