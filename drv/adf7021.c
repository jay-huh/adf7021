#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>

#include <linux/slab.h>			//	kzalloc
#include <linux/workqueue.h>
#include <linux/delay.h>
#include <linux/random.h>		//	get_random_bytes
#include <asm/gpio.h>

#include <linux/time.h>			//	do_gettimeofday
//#include <linux/kfifo.h>

#include <mach/platform.h>
#include <mach/devices.h>

#include	"adf7021.h"

#include	"common.h"
#include	"gpio.h"
#include	"timer.h"
#include	"queue.h"
#include	"list.h"
#include	"pf_cmd.h"

static int s_dump_mode = PF_DUMP_NONE;

static int s_tx_strength = 35;
module_param( s_tx_strength, int, 0 );
MODULE_PARM_DESC( s_tx_strength, "TX Strength" );

#ifdef __RF_TEST__
static int s_work_mode = ADF_WORK_NORMAL;
static int s_test_duration = 1000;
static int s_test_freq = FREQ_151_82;

module_param( s_work_mode, int, 0 );
MODULE_PARM_DESC( s_work_mode, "Test Mode" );
module_param( s_test_duration, int, 0 );
MODULE_PARM_DESC( s_test_duration, "Test Timer Duration" );
module_param( s_test_freq, int, 0 );
MODULE_PARM_DESC( s_test_freq, "Test frequency channel" );

static const char* adf_work_mode_str( const ADF_WORK_MODE mode )
{
	switch( mode )
	{
		case ADF_WORK_NORMAL  : return "NORMAL";
		case ADF_WORK_TEST_TX : return "TEST_TX";
		case ADF_WORK_TEST_RX : return "TEST_RX";
		default : return "Not Defined";
	}
	return "Not Defined";
}

#endif	//	__RF_TEST__

static ADF_STRUCT s_adf_struct;

/*	TODO: change array to hash table.
 *	all slot assignment.	*/
static uint16_t slot_map[ FREQ_END - 1 ][ MAX_SLOT_INDEX ];

/*	init and exit functions	*/
static int __init adf7021_init( void );
static void __exit adf7021_exit( void );

static int adf_init( void );
static void adf_exit( void );

/*	file operations	*/
static int adf_open( struct inode * pInode, struct file * pFile );
static int adf_release( struct inode * pInode, struct file * pFile );
static long adf_ioctl( struct file * pFile, uint32_t cmd, unsigned long arg );
static ssize_t adf_read( struct file * pFile, char __user * pBuf , size_t count, loff_t *pPos );
static ssize_t adf_write( struct file * pFile, const char __user * pBuf , size_t count, loff_t *pPos );
static uint32_t adf_poll( struct file * pFile, struct poll_table_struct * pWait );

/*	interrupt	*/
static bool adf_enable_irq( void * pData );
static bool adf_disable_irq( void * pData );
static bool adf_disable_irq_nosync( void * pData );

/*	timer handlers and workqueues	*/
static enum hrtimer_restart reference_handler( const struct hrtimer *pTimer );
static enum hrtimer_restart slot_handler( const struct hrtimer *pTimer );
static enum hrtimer_restart timeout_handler( const struct hrtimer *pTimer );

static void timeout( struct work_struct * pWork );
static void rf_done( struct work_struct * pWork );
static void ref_slot( struct work_struct * pWork );
static void slot( struct work_struct * pWork );

static bool forced_rf_done( void );

static int slot_sequence( ADF_STRUCT *pAdf_struct );
static int adf_current_slot( void );

/*	ADF Register control	*/
static void adf_reg_init( ADF_REG regs[] );
static void adf_write_reg( ADF_REG	* pReg );
static const ADF_READBACK adf_readback( READBACK_MODE mode, READBACK_ADC_MODE adc_mode );
static int calculate_rssi( void );

/*	RF mode control	*/
static ADF_MODE adf_mode( void );
static bool adf_sleep( void );
static bool adf_tx_mode( RF_COMMAND * pCmd );
static bool adf_rx_mode( RF_COMMAND * pCmd );
static bool adf_mode_change( RF_COMMAND * pCmd );
/*	packet processing	*/
static int adf_packet_alloc( void * pData );
static int adf_packet_dealloc( void * pData );
static bool adf_packet_post_process( void *pData );

static const uint32_t list_assign_key( const uint8_t command );
static void adf_notify_add_device( void *pData, const uint16_t id );
static void adf_notify_share_info( void *pData, const PF_RX_COMMAND * const pResponse );
static void adf_notify_void( void *pData, const NOTIFY_TYPE notifyType );
static void adf_notify_int( void *pData, const NOTIFY_TYPE notifyType, const int intArg );
static bool adf_notify( void *pData, const NOTIFY_TYPE notifyType, const unsigned long arg );

/*	time synchronization	*/
static bool adf_init_reference( void *pData );
static bool adf_init_reference_time( void *pData, void *pPacketData );
static bool adf_sync_reference_time( void *pData );
static long adf_sync_slot_time( struct work_struct * pWork, const int slot );

static void print_slot_map( void );
#ifdef __STATISTICS__
static void print_slot_info( void );
#endif	//	__STATISTICS__

static const uint16_t adf_update_connector_info_file( void );
static const uint16_t adf_load_connector_info_file( CONNECTOR_INFO *pConnectorInfo );
static const int adf_update_slot_map_file( void );
static const int adf_load_slot_map_file( void );

#ifdef __DUMP_PACKET__
void print_dump_list( void )
{
	ADF_STRUCT *pAdf_struct = &s_adf_struct;
	PF_COMMAND *pPfCmd = NULL;
	int tx_header_size = sizeof( uint32_t ) + sizeof( uint32_t );	/* preamble + syncword */
	int len;
	char *pStr = NULL;

	if( list_empty( &pAdf_struct->dump_list ) == true ) return;

	printk( "\n---------------|-----------|-----------|-----------|-----------|-----------|--------\n");
	printk( "       len     |00       03|04       07|08       11|12       15|16       19|20 21 22\n");
	while( list_empty( &pAdf_struct->dump_list ) == false )
	{
		pPfCmd = ( PF_COMMAND * )list_find_data( &pAdf_struct->dump_list, LIST_KEY_TX | LIST_KEY_RX );
		if( pPfCmd != NULL )
		{
			if( (( PF_TX_COMMAND * )( pPfCmd ))->syncword == htonl( ( ( PF_SYNC_WORD ) << 8 ) | 0xAA ) )
			{
				len = get_command_len( (( PF_TX_COMMAND * )( pPfCmd ))->command ) + tx_header_size;
				pStr = "TX";
			}
			else
			{
				len = get_command_len( (( PF_RX_COMMAND * )( pPfCmd ))->command ) + 1;
				pStr = "RX";
			}
			printk( "DUMP: %4d: %2s: %*ph\n", len, pStr, len, pPfCmd );
			//printk( "DUMP: %4d: %2s: %*phC\n", i++, pStr, len, pPfCmd );
			//printk( "DUMP: %4d: %2s: %*phD\n", i++, pStr, len, pPfCmd );
			//printk( "DUMP: %4d: %2s: %*phN\n", i++, pStr, len, pPfCmd );
			kfree( pPfCmd );
		}
		pPfCmd = NULL;
	}
	printk( "---------------|-----------|-----------|-----------|-----------|-----------|--------\n");
}
#endif	//	__DUMP_PACKET__

static const uint16_t adf_load_connector_info_file( CONNECTOR_INFO *pConnectorInfo )
{
	struct file *fp      = NULL;
	mm_segment_t oldfs   = { 0x0 };
	loff_t pos = 0;
	char *pFilename = "/data/misc/adf/connector_info";
	ssize_t len;
	uint16_t id = 0x0;

	ASSERT( pConnectorInfo != NULL );

	memset( pConnectorInfo, 0x0, sizeof( CONNECTOR_INFO ) );

	fp = filp_open( pFilename, O_RDONLY, 0666 );

	if( IS_ERR(fp) )
	{
		fp = filp_open( pFilename, O_RDWR | O_CREAT, 0666 );
		if( IS_ERR(fp) )
		{
			ERR_ADF( "FILE: %s: open error\n",	pFilename );
			return 0x0;
		}
		/*	default value in PF
		 *	slot: ( id % ( FREQ_END - 1 ) ) + 1 -> 1 ~ 5
		 *	freq: ( id % MAX_SLOT_INDEX ) + 1 -> 1 ~ 22
		 *	period: 1 -> 2 sec
		 *	*/
		get_random_bytes( &id, 2 );
		HUNTER_ID( id );
		pConnectorInfo->id = id;
		pConnectorInfo->period = PERIOD_2_SECS;
		pConnectorInfo->slot = ( id % MAX_SLOT_INDEX ) + 1;
		pConnectorInfo->freq = ( id % ( FREQ_END - 1) ) + 1;
		pConnectorInfo->sleep = true;
		pConnectorInfo->nation = NATION_US;
		memcpy( pConnectorInfo->name, "PFHH_HUNTER", HUNTER_NAME_LEN );

		oldfs = get_fs();
		set_fs( get_ds() );

		len = vfs_write( fp, ( char __user * )pConnectorInfo, sizeof( CONNECTOR_INFO ), &pos );

		set_fs( oldfs );
	}
	else
	{
		oldfs = get_fs();
		set_fs( get_ds() );

		len = vfs_read( fp, ( char __user * )pConnectorInfo, sizeof( CONNECTOR_INFO ), &pos );

		set_fs( oldfs );
	}

	if( len > 0)
	{
		init_connector_info( pConnectorInfo );
	}

	return id;
}

static const uint16_t adf_update_connector_info_file( void )
{
	struct file *fp      = NULL;
	mm_segment_t oldfs   = { 0x0 };
	loff_t pos = 0;
	char *pFilename = "/data/misc/adf/connector_info";
	ssize_t len;
	const CONNECTOR_INFO * const pConnectorInfo = get_connector_info();

	ASSERT( pConnectorInfo != NULL );

	fp = filp_open( pFilename, O_RDONLY, 0666 );

	if( IS_ERR(fp) )
	{
		fp = filp_open( pFilename, O_RDWR | O_CREAT, 0666 );
		if( IS_ERR(fp) )
		{
			ERR_ADF( "FILE: %s: open error\n",	pFilename );
			return 0x0;
		}
	}
	oldfs = get_fs();
	set_fs( get_ds() );

	len = vfs_write( fp, ( char __user * )pConnectorInfo, sizeof( CONNECTOR_INFO ), &pos );

	set_fs( oldfs );

	if( len > 0)
	{
		//set_connector_info( pConnectorInfo );
	}

	return pConnectorInfo->id;
}

static const int adf_update_slot_map_file( void )
{
	struct file *fp      = NULL;
	mm_segment_t oldfs   = { 0x0 };
	loff_t pos = 0;
	char *pFilename = "/data/misc/adf/slot_map";
	ssize_t len;

	fp = filp_open( pFilename, O_RDWR, 0666 );

	if( IS_ERR(fp) )
	{
		fp = filp_open( pFilename, O_RDWR | O_CREAT, 0666 );
		if( IS_ERR(fp) )
		{
			ERR_ADF( "FILE: %s: open error\n",	pFilename );
			return -EFAULT;
		}
	}
	oldfs = get_fs();
	set_fs( get_ds() );

	len = vfs_write( fp, ( char __user * )slot_map, sizeof( uint16_t ) * ( FREQ_END - 1 ) * MAX_SLOT_INDEX, &pos );

	set_fs( oldfs );

	if( len > 0)
	{
		INFO_ADF( "sizeof( slot_map ): %d", sizeof( slot_map ) );
		print_slot_map();
#ifdef __STATISTICS__
		print_slot_info();
#endif	//	__STATISTICS__
	}

	return 0;
}

static const int adf_load_slot_map_file( void )
{
	struct file *fp      = NULL;
	mm_segment_t oldfs   = { 0x0 };
	loff_t pos = 0;
	char *pFilename = "/data/misc/adf/slot_map";
	ssize_t len;

	fp = filp_open( pFilename, O_RDONLY, 0666 );

	if( IS_ERR(fp) )
	{
		fp = filp_open( pFilename, O_RDWR | O_CREAT, 0666 );
		if( IS_ERR(fp) )
		{
			ERR_ADF( "FILE: %s: open error\n",	pFilename );
			return -EFAULT;
		}
		oldfs = get_fs();
		set_fs( get_ds() );

		len = vfs_write( fp, ( char __user * )slot_map, sizeof( uint16_t ) * ( FREQ_END - 1 ) * MAX_SLOT_INDEX, &pos );

		set_fs( oldfs );
	}
	else
	{
		oldfs = get_fs();
		set_fs( get_ds() );

		len = vfs_read( fp, ( char __user * )slot_map, sizeof( uint16_t ) * ( FREQ_END - 1 ) * MAX_SLOT_INDEX, &pos );

		set_fs( oldfs );
	}

	if( len > 0)
	{
		INFO_ADF( "sizeof( slot_map ): %d", sizeof( slot_map ) );
		print_slot_map();
#ifdef __STATISTICS__
		print_slot_info();
#endif	//	__STATISTICS__
	}

	return 0;
}


static bool adf_init_start_ref_timer( void  )
{
	ADF_STRUCT	* pAdf_struct = &s_adf_struct;
	bool active = hrtimer_active( &pAdf_struct->reference_timer );

	ASSERT( pAdf_struct != NULL );

	if( active == true )
	{
		INFO_TIMER( "No Listening slot. reference_timer is active already!!!!!\n" );
		return true;
	}

	INFO_TIMER( "START reference_timer: active: %d\n", active );

	/*	NOTE: if RF is working, then forced RF_DONE and wait until rf_done work.	*/
	forced_rf_done();

	pAdf_struct->slot_seq = FLAG_SLOT_END;
	pAdf_struct->reference_timestamp.tv64 = 0LL;
	pAdf_struct->reference_next_timestamp.tv64 = 0LL;

	pAdf_struct->init_reference = false;
	pAdf_struct->init_reference_timestamp.tv64 = 0ULL;

	pAdf_struct->last_pairing_slot_index = -1;

	timer_start( ( void * )( & pAdf_struct->reference_timer ), INIT_DURATION );

	return true;
}

/*	assign functions	*/
static const CH_SLOT adf_assign_slot_freq( void *pData );
static FREQ adf_assign_freq( ADF_SLOT_INFO slot_info[] );

/*	find slot	*/
static int adf_find_empty_slot( ADF_SLOT_INFO slot_info[] );
static int adf_find_pairing_slot( ADF_SLOT_INFO slot_info[] );
static int adf_find_listening_slot( ADF_SLOT_INFO slot_info[] );
static int adf_find_valid_slot( ADF_SLOT_INFO slot_info[] );
static int adf_find_rx_time_valid_slot( ADF_SLOT_INFO slot_info[] );

static bool adf_slot_is_empty( const int slot_index );
static bool adf_slot_is_listening( const int slot_index );
static bool adf_slot_is_valid( void *pData, const int slot_index );
static bool slot_update_remain( void *pData, const int slot_index );

/*	slot_info interfaces	*/
static void adf_clear_slot_info( void *pData, const int slot_index );
static void adf_set_slot_info( void *pData, const int slot_index, const ADF_SLOT_INFO *pSlotInfo );
static void adf_get_slot_info( void *pData, const int slot_index, ADF_SLOT_INFO *pSlotInfo );
static void adf_set_slot_info_field( void *pData, const int slot_index, const SLOT_FIELD field, const void *pValue );
static void adf_get_slot_info_field( void *pData, const int slot_index, const SLOT_FIELD field, void *pValue );

static const char* adf_slot_mode_str( const SLOT_MODE mode );

static bool adf_valid_gps_packet( void *pData, ADF_PACKET *pPacket );

static void print_eCollar_info( void )
{
	const E_COLLAR_INFO * const pECollarInfo = get_eCollar_info();

	INFO_ADF( "E_COLLAR_INFO\n");
	INFO_ADF( "----------------------------\n");
	INFO_ADF( "cmd: 0x%02x id: 0x%04x level: %d slot_index: %d send_cnt: %d retry: %d action_time: %d ms\n", \
			pECollarInfo->command, pECollarInfo->id, pECollarInfo->level, pECollarInfo->slot_index, \
			pECollarInfo->send_cnt, pECollarInfo->retry_cnt, pECollarInfo->action_time );

	return;
}

bool clear_eCollar_info( void )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )&s_adf_struct;

	ASSERT( pAdf_struct != NULL );

	mutex_lock( & pAdf_struct->eCollar_mutex );

	//print_eCollar_info();

	if( 1 )
	{
		int totalCount, count;

		totalCount = list_count( &pAdf_struct->cmd_list );
		count = list_remove( &pAdf_struct->cmd_list, LIST_KEY_E_COLLAR );
		NOTICE_ADF( "CMD_LIST: Remove E_COLLAR count: %d / %d\n", count, totalCount );
		totalCount = list_count( &pAdf_struct->delayed_cmd_list );
		count = list_remove( &pAdf_struct->delayed_cmd_list, LIST_KEY_E_COLLAR );
		NOTICE_ADF( "TX_LIST: Remove E_COLLAR count: %d / %d\n", count, totalCount );
	}
	pAdf_struct->eCollar_info.id = 0x0;
	pAdf_struct->eCollar_info.level = 0;
	pAdf_struct->eCollar_info.command = PF_CMD_UNDEFINED;
	pAdf_struct->eCollar_info.slot_index = -1;
	pAdf_struct->eCollar_info.send_cnt = -1;
	pAdf_struct->eCollar_info.action_time = 0;
	pAdf_struct->eCollar_info.retry_cnt = 0;

	mutex_unlock( & pAdf_struct->eCollar_mutex );

	return true;
}

bool set_eCollar_info( const E_COLLAR_INFO * const pECollarInfo )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )&s_adf_struct;

	ASSERT( pAdf_struct != NULL );
	ASSERT( pECollarInfo != NULL );

	mutex_lock( & pAdf_struct->eCollar_mutex );
	pAdf_struct->eCollar_info = *pECollarInfo;
	pAdf_struct->eCollar_info.slot_index = adf_find_slot_listening( ( const uint16_t )pECollarInfo->id );
	pAdf_struct->eCollar_info.send_cnt = -1;
	pAdf_struct->eCollar_info.action_time = 0;
	pAdf_struct->eCollar_info.retry_cnt = 0;

	if( pAdf_struct->eCollar_info.slot_index < 0 )
	{
		mutex_unlock( & pAdf_struct->eCollar_mutex );
		clear_eCollar_info();
		return false;
	}
	//print_eCollar_info();

	mutex_unlock( & pAdf_struct->eCollar_mutex );
	return true;
}

const E_COLLAR_INFO * const get_eCollar_info( void )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )&s_adf_struct;

	ASSERT( pAdf_struct != NULL );

	return &pAdf_struct->eCollar_info;
}

int update_eCollar_info( const int milli_sec )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )&s_adf_struct;
	bool ackReq = false;
	bool retry = false;

	ASSERT( pAdf_struct != NULL );

	retry = test_and_clear_bit( FLAG_NEED_EC_ACK, &pAdf_struct->flags );

	mutex_lock( & pAdf_struct->eCollar_mutex );

	pAdf_struct->eCollar_info.action_time += milli_sec;
	pAdf_struct->eCollar_info.send_cnt++;

	if( retry == true )
	{
		pAdf_struct->eCollar_info.retry_cnt++;
	}

	ackReq = ( ( pAdf_struct->eCollar_info.send_cnt % ACK_PER_REQ_TIMES ) == 0 ) ? true : false;
	if( ackReq == true )
	{
		if( test_bit( FLAG_CONST, & pAdf_struct->flags ) == 1 )
		{
			pAdf_struct->eCollar_info.command = PF_CMD_NICK_REQ;
		}
		else if( test_bit( FLAG_TONE, & pAdf_struct->flags ) == 1 )
		{
			pAdf_struct->eCollar_info.command = PF_CMD_TONE;
			pAdf_struct->eCollar_info.level = TONE_LEVEL_ACK_REQ;
		}
	}
	else
	{
		if( test_bit( FLAG_CONST, & pAdf_struct->flags ) == 1 )
		{
			pAdf_struct->eCollar_info.command = PF_CMD_CONST;
		}
		else if( test_bit( FLAG_TONE, & pAdf_struct->flags ) == 1 )
		{
			pAdf_struct->eCollar_info.command = PF_CMD_TONE;
			pAdf_struct->eCollar_info.level = 0x0;
		}
	}

	mutex_unlock( & pAdf_struct->eCollar_mutex );
	return pAdf_struct->eCollar_info.action_time;
}

bool is_eCollar_ack_req( void )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )&s_adf_struct;
	bool ack_req = false;

	ASSERT( pAdf_struct != NULL );

	mutex_lock( & pAdf_struct->eCollar_mutex );
	ack_req = ( ( pAdf_struct->eCollar_info.send_cnt % ACK_PER_REQ_TIMES ) == 0 ) ? true : false;
	mutex_unlock( & pAdf_struct->eCollar_mutex );

	return ack_req;
}

const uint8_t get_eCollar_cmd( void )
{
	const E_COLLAR_INFO * const pECollarInfo = get_eCollar_info();

	ASSERT( pECollarInfo != NULL );

	return pECollarInfo->command;
}

bool set_eCollar_cmd( const uint8_t command )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )&s_adf_struct;

	ASSERT( pAdf_struct != NULL );

	mutex_lock( & pAdf_struct->eCollar_mutex );
	pAdf_struct->eCollar_info.command = command;
	mutex_unlock( & pAdf_struct->eCollar_mutex );

	return true;
}

bool set_eCollar_level( const uint8_t level )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )&s_adf_struct;

	ASSERT( pAdf_struct != NULL );

	mutex_lock( & pAdf_struct->eCollar_mutex );
	pAdf_struct->eCollar_info.level = level;
	mutex_unlock( & pAdf_struct->eCollar_mutex );

	return true;
}

int get_eCollar_time( void )
{
	const E_COLLAR_INFO * const pECollarInfo = get_eCollar_info();

	ASSERT( pECollarInfo != NULL );

	return pECollarInfo->action_time;
}

int get_eCollar_slot_index( void )
{
	const E_COLLAR_INFO * const pECollarInfo = get_eCollar_info();

	ASSERT( pECollarInfo != NULL );

	return pECollarInfo->slot_index;
}

bool check_eCollar( const bool isGlobalTime )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )&s_adf_struct;
	RF_COMMAND *pCmd = NULL;
	uint32_t keyMask = 0x0;

	ASSERT( pAdf_struct != NULL );

	if( test_bit( FLAG_E_COLLAR, & pAdf_struct->flags ) == 1 )
	{
		if( update_eCollar_info( SLOT_DURATION ) >= E_COLLAR_LIMIT_TIME )
		{
			NOTICE_ADF( "NOTIFY TIME_OVER E_COLLAR_LIMIT_TIME( %d ms )\n", E_COLLAR_LIMIT_TIME );
			clear_bit( FLAG_E_COLLAR, & pAdf_struct->flags );
			clear_bit( FLAG_CONST, & pAdf_struct->flags );
			clear_bit( FLAG_TONE, & pAdf_struct->flags );
			clear_eCollar_info();
		}
		else
		{
			pCmd = pf_cmd_gen_eCollar( pAdf_struct, get_eCollar_cmd(), isGlobalTime );
			if( pCmd == NULL )
			{
				ERR_ADF( "pf_cmd_gen_eCollar() failed!!!!\n" );
			}
		}
	}

	if( pCmd == NULL ) return false;

	if( isGlobalTime == true )
	{
		keyMask = LIST_KEY_GLOBAL_CMD | LIST_KEY_E_COLLAR | pCmd->id;
	}
	else
	{
		keyMask = LIST_KEY_LOCAL_CMD | LIST_KEY_E_COLLAR | pCmd->id;
	}
	list_push_head( &pAdf_struct->cmd_list, pCmd, sizeof( RF_COMMAND ), keyMask );

	return true;
}

static bool adf_notify( void *pData, const NOTIFY_TYPE notifyType, const unsigned long arg )
{
	ASSERT( pData != NULL );

	switch( notifyType )
	{
		//case NOTIFY_NONE :
		//	break;
		case NOTIFY_ADD_DEVICE :
			{
				uint16_t id = arg & 0xFFFF;
				adf_notify_add_device( pData, id );
				break;
			}
		case NOTIFY_SHARE_INFO :
			{
				PF_RX_COMMAND *pRxCmd = ( PF_RX_COMMAND * )arg;
				adf_notify_share_info( pData, pRxCmd );
				break;
			}
		case NOTIFY_SHARE_ACK :
		case NOTIFY_SHARE_CANCELLED :
		case NOTIFY_EC_ACK :
		case NOTIFY_EC_FAIL :
		case NOTIFY_IN_PAIRING :
			{
				adf_notify_void( pData, notifyType );
				break;
			}
		case NOTIFY_SLOT_CONFLICT :	//	int slotId
		case NOTIFY_BAT_LEVEL :	//	int level
		case NOTIFY_SCAN :	// int slotId, int freq
			{
				int intArg = ( int )arg;
				adf_notify_int( pData, notifyType, intArg );
				break;
			}
		default :
			WARN_ADF( "invalid notify_type %d\n", notifyType );
			ASSERT( 1 );
			return false;
	}
	return true;
}

static void adf_notify_add_device( void *pData, const uint16_t id )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )pData;
	ADF_PACKET *pPacket = NULL;
	DEVICE_INFO *pInfo = NULL;
	ADF_SLOT_INFO slotInfo;
	int slot_index;

	ASSERT( pData != NULL );

	slot_index = adf_find_slot_listening( id );
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	pPacket = ( ADF_PACKET * )kzalloc( sizeof( ADF_PACKET ), GFP_KERNEL );
	if( pPacket == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		return ;
	}

	pPacket->bit_index = -1;
	pPacket->length = pAdf_struct->init.max_packet_length;
	pPacket->response_length = 0;
	do_gettimeofday( & pPacket->tv );
	pPacket->notify_type = NOTIFY_ADD_DEVICE;

	adf_get_slot_info( pAdf_struct, slot_index, &slotInfo );

	pInfo = ( DEVICE_INFO * )( pPacket->packetData );
	pInfo->id = slotInfo.id;
	pInfo->period = slotInfo.period;
	pInfo->slot = slot_index + 1;
	pInfo->freq = slotInfo.freq;
	pInfo->shared = false;
	pInfo->name[ 0 ] = '\0';

	list_push( & pAdf_struct->rx_list, pPacket, sizeof( ADF_PACKET ), \
			LIST_KEY_NOTIFY | ( pInfo->id & LIST_ID_BROADCAST ) );
	wake_up_interruptible( & pAdf_struct->poll_waitq );

#ifdef __STATISTICS__
	pAdf_struct->slot_info[ slot_index ].rx_stat.total = 0;
	pAdf_struct->slot_info[ slot_index ].rx_stat.sucess = 0;
	pAdf_struct->slot_info[ slot_index ].rx_stat.err = 0;
#endif	//	__STATISTICS__
	return;
}

static void adf_notify_share_info( void *pData, const PF_RX_COMMAND * const pResponse )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )pData;
	ADF_PACKET *pPacket = NULL;
	DEVICE_INFO *pInfo = NULL;

	ASSERT( pData != NULL );

	pPacket = ( ADF_PACKET * )kzalloc( sizeof( ADF_PACKET ), GFP_KERNEL );
	if( pPacket == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		return ;
	}

	pPacket->bit_index = -1;
	pPacket->length = pAdf_struct->init.max_packet_length;
	pPacket->response_length = 0;
	do_gettimeofday( & pPacket->tv );
	pPacket->notify_type = NOTIFY_SHARE_INFO;

	pInfo = ( DEVICE_INFO * )( pPacket->packetData );
	pInfo->id = pResponse->share_info.id;
	pInfo->slot = pResponse->share_info.ch_slot.slot;
	pInfo->freq = pResponse->share_info.ch_slot.ch;
	//pInfo->period = slotInfo.period;
	memcpy( pInfo->name, pResponse->share_info.name, HUNTER_NAME_LEN );
	pInfo->shared = true;

	list_push( & pAdf_struct->rx_list, pPacket, sizeof( ADF_PACKET ), \
			LIST_KEY_NOTIFY | ( pInfo->id & LIST_ID_BROADCAST ) );
	wake_up_interruptible( & pAdf_struct->poll_waitq );

	return;
}

static void adf_notify_void( void *pData, const NOTIFY_TYPE notifyType )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )pData;
	ADF_PACKET *pPacket = NULL;

	ASSERT( pData != NULL );

	pPacket = ( ADF_PACKET * )kzalloc( sizeof( ADF_PACKET ), GFP_KERNEL );
	if( pPacket == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		return ;
	}

	pPacket->bit_index = -1;
	pPacket->length = pAdf_struct->init.max_packet_length;
	pPacket->response_length = 0;
	do_gettimeofday( & pPacket->tv );
	pPacket->notify_type = notifyType;

	list_push( & pAdf_struct->rx_list, pPacket, sizeof( ADF_PACKET ), \
			LIST_KEY_NOTIFY | ( get_connector_info()->id & LIST_ID_BROADCAST ) );
	wake_up_interruptible( & pAdf_struct->poll_waitq );

	return;
}

static void adf_notify_int( void *pData, const NOTIFY_TYPE notifyType, const int intArg )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )pData;
	ADF_PACKET *pPacket = NULL;

	ASSERT( pData != NULL );

	pPacket = ( ADF_PACKET * )kzalloc( sizeof( ADF_PACKET ), GFP_KERNEL );
	if( pPacket == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		return ;
	}

	pPacket->bit_index = -1;
	pPacket->length = pAdf_struct->init.max_packet_length;
	pPacket->response_length = 0;
	do_gettimeofday( & pPacket->tv );
	pPacket->notify_type = notifyType;

	switch( notifyType )
	{
		case NOTIFY_SCAN :
			*( CH_SLOT * )( pPacket->packetData ) = *( CH_SLOT * )intArg;
			break;
		default :
			*( int * )( pPacket->packetData ) = intArg;
			break;
	}

	list_push( & pAdf_struct->rx_list, pPacket, sizeof( ADF_PACKET ), \
			LIST_KEY_NOTIFY | ( get_connector_info()->id & LIST_ID_BROADCAST ) );
	wake_up_interruptible( & pAdf_struct->poll_waitq );

	return;
}

static bool adf_pairing( void *pData )
{
	ADF_STRUCT *pAdf_struct = NULL;
	ADF_PACKET *pPacket = NULL;
	RF_COMMAND cmd = { ADF_MODE_SLEEP, FREQ_UNKNOWN, 0x0, 0, 0, { 0x0, }, 0, 0 };
	PF_RX_COMMAND *pResponse = NULL;
	int assign_slot = -1;
	bool ret = false;

	ASSERT( pData != NULL );

	pAdf_struct = ( ADF_STRUCT * )pData;
	pPacket = ( ADF_PACKET * )list_find_data( &pAdf_struct->rx_list, LIST_KEY_PAIRING );
	if( pPacket == NULL ) return false;

	pResponse = ( PF_RX_COMMAND * )( pPacket->packetData );

	switch( pResponse->command )
	{
		case PF_CMD_PAIR_RESPONSE :
			if( pResponse->pair_response.hunter_id == get_connector_info()->id )
			{
				if(	pf_cmd_gen( pAdf_struct, & cmd, pResponse, PF_CMD_PAIR_STATUS ) == true )
				{
					/*	Dog index: 1 to 22 / slot index: 0 to 21	*/
					assign_slot = ( ( PF_TX_COMMAND * )( cmd.packetData ) )->pair_status.slot - 1;

					adf_slot_set_mode( pAdf_struct, assign_slot, SLOT_MODE_PAIRING );
					adf_mode_change( & cmd );
					ret = true;
				}
			}
			break;

		case PF_CMD_PAIR_ACK :
			if( pResponse->ack.hunter_id == get_connector_info()->id )
			{
				assign_slot = adf_find_pairing_slot( pAdf_struct->slot_info );
				if( assign_slot >= 0 )
				{
					adf_slot_set_mode( pAdf_struct, assign_slot, SLOT_MODE_LISTEN );
					clear_bit( FLAG_PAIRING, & pAdf_struct->flags );
					if( test_and_clear_bit( FLAG_SCAN, & pAdf_struct->flags ) == 1 )
					{
						pAdf_struct->scan_ch_slot.ch_slot = INVALID_CH_SLOT;
						pAdf_struct->scan_result_ch_slot.ch_slot = INVALID_CH_SLOT;
					}
					{
						int *pSlotIndex = ( int * )kzalloc( sizeof( int ), GFP_KERNEL );
						if( pSlotIndex == NULL )
						{
							ERR_ADF( "ERROR [%s,%d] kzalloc failed!!!!\n", __func__, __LINE__ );
							return -ENOMEM;
						}
						*pSlotIndex = assign_slot;
						list_push_head( &pAdf_struct->pair_list, pSlotIndex, sizeof( int ), LIST_KEY_PAIR( *pSlotIndex ) );
					}
					adf_notify( pAdf_struct, NOTIFY_ADD_DEVICE, adf_get_slot_info_id( assign_slot ) );
				}
				NOTICE_ADF( "REF_SLOT_%d: PAIR_ACK H_ID: 0x%04x [ assign_slot: %d freq: %d ]\n", \
						pAdf_struct->slot_seq, pResponse->ack.hunter_id, assign_slot, \
						pAdf_struct->slot_info[ assign_slot ].freq );
#ifdef __INTERNAL_AFC__
				NOTICE_ADF( "reg10: KI: %d KP: %d\n", pAdf_struct->regs[ 10 ].REG_10.KI, \
						pAdf_struct->regs[ 10 ].REG_10.KP );
#endif	//	__INTERNAL_AFC__
			}
			break;

		default :
			break;
	}
#if 0
	{
		ktime_t start;
		struct timeval tv_start, now;

		do_gettimeofday( &now );

		start = timeval_to_ktime( pPacket->tv_swd );
		start = ktime_sub_ns( start, ( u64 )MS_TO_NS( TX_HEADER_TIME ) );
		tv_start = ktime_to_timeval( start );

		INFO_ADF( "[ PAIR ] s: %d cmd: 0x%02x now [ %9ld.%06ld ] start [ %9ld.%06ld ]\n", adf_current_slot(), \
				pResponse->command, now.tv_sec, now.tv_usec, tv_start.tv_sec, tv_start.tv_usec );
	}
#endif
#ifdef __DEBUG_QUEUE_CNT__
	pAdf_struct->dealloc_cnt++;
#endif	//	__DEBUG_QUEUE_CNT__
	kfree( pPacket );
	pPacket = NULL;
	return ret;
}

void adf_cancel_pairing( void *pData, const int slot_index )
{
	ADF_STRUCT *pAdf_struct = NULL;

	ASSERT( pData != NULL );
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	pAdf_struct = ( ADF_STRUCT * )pData;

	if( adf_slot_is_listening( slot_index ) == true )
	{
		adf_clear_slot_info( pAdf_struct, slot_index );
	}

	/*	not init reference time => wait until 1st GPS information
	 *	TODO: valid slot first...	*/
#if 1
	if( adf_find_rx_time_valid_slot( pAdf_struct->slot_info ) < 0 )
	{
		//pAdf_struct->slot_flags = 0x0UL;	//	clear valid slots
		pAdf_struct->init_reference = false;
		adf_init_start_ref_timer();
	}
#else
	if( adf_find_listening_slot( pAdf_struct->slot_info ) < 0 )
	{
		pAdf_struct->init_reference = false;
		adf_init_start_ref_timer();
	}
#endif

	return ;
}

static bool adf_sharing( void *pData )
{
	ADF_STRUCT *pAdf_struct = NULL;
	ADF_PACKET *pPacket = NULL;
	PF_RX_COMMAND *pResponse = NULL;
	bool ret = false;

	ASSERT( pData != NULL );

	pAdf_struct = ( ADF_STRUCT * )pData;
	pPacket = ( ADF_PACKET * )list_find_data( &pAdf_struct->rx_list, LIST_KEY_SHARE );
	if( pPacket == NULL ) return false;

	pResponse = ( PF_RX_COMMAND * )( pPacket->packetData );

	switch( pResponse->command )
	{
		case PF_CMD_SHARE_DOG_INFO :
			{
				/*	TODO: Send SHARE_ACK
				 *	check slot conflict and store SHARE_INFO
				 *	- conflict: APP -> DRV | change slot in my list.		*/
				IOCTL_CMD_DATA cmdData;
				RF_COMMAND *pRfCmd = NULL;
				uint32_t hunter_id = pResponse->share_info.hunter_id;

				ktime_t start;
				struct timeval tv_start, now;

				do_gettimeofday( &now );
				cmdData.devInfo.id = hunter_id;
				pRfCmd = pf_alloc_cmd_gen( pAdf_struct, &cmdData, PF_CMD_SHARE_ACK );
				if( pRfCmd != NULL )
				{
					if( pAdf_struct->init_reference == false )
					{
						pRfCmd->delay_ms = 0;
					}
					adf_mode_change( pRfCmd );
					clear_bit( FLAG_SHARING_INFO, & pAdf_struct->flags );
					clear_bit( FLAG_SHARING_RX, & pAdf_struct->flags );
					kfree( pRfCmd );
					pRfCmd = NULL;
					ret = true;
					/* TODO: NOTIFY_CB: Share RX information -> DEVICE_INFO	*/
					adf_notify( pAdf_struct, NOTIFY_SHARE_INFO, ( const unsigned long )pResponse );
				}

				{
					int desired_slot_index = pResponse->share_info.ch_slot.slot - 1;
					bool empty = adf_slot_is_empty( desired_slot_index );

					if( empty == true )
					{
						/* NOTE: Set slot info usign SHARE_INFO and Notify sharing sucess to App.
						 * NOTIFY_ADD_DEVICE with Sharing flag	*/
						ADF_SLOT_INFO slotInfo;

						memset( &slotInfo, 0x0, sizeof( ADF_SLOT_INFO ) );

						slotInfo.id = pResponse->share_info.id;
						slotInfo.freq = pResponse->share_info.ch_slot.ch;
						slotInfo.period = PERIOD_UNKNOWN;
						slotInfo.period_remain = 1;
						slotInfo.shared = true;
						slotInfo.mode = SLOT_MODE_LISTEN;
						slotInfo.last_rx_time.tv_sec = 0;
						slotInfo.last_rx_time.tv_usec = 0;

						adf_clear_slot_info( pAdf_struct, desired_slot_index );
						adf_set_slot_info( pAdf_struct, desired_slot_index, &slotInfo );

						adf_init_start_ref_timer();
						if( pAdf_struct->init_reference == false )
						{
							{
								int *pSlotIndex = ( int * )kzalloc( sizeof( int ), GFP_KERNEL );
								if( pSlotIndex == NULL )
								{
									ERR_ADF( "ERROR [%s,%d] kzalloc failed!!!!\n", __func__, __LINE__ );
									return -ENOMEM;
								}
								*pSlotIndex = desired_slot_index;
								list_push_head( &pAdf_struct->pair_list, pSlotIndex, sizeof( int ), LIST_KEY_PAIR( *pSlotIndex ) );
							}
						}
					}
					else
					{
						CH_SLOT ch_slot = adf_assign_slot_freq( pAdf_struct );

						if( ch_slot.ch_slot == INVALID_CH_SLOT ) { /* NOTE: slot not found */ };

						/* TODO: NOTIFY_CB: Share RX slot conflict -> recommend ch / slot	*/
						adf_notify( pAdf_struct, NOTIFY_SLOT_CONFLICT, ch_slot.slot );

					}
				}
				start = timeval_to_ktime( pPacket->tv_swd );
				start = ktime_sub_ns( start, ( u64 )MS_TO_NS( TX_HEADER_TIME ) );
				tv_start = ktime_to_timeval( start );

				INFO_ADF( "[ SH_DOG ] s: %d now [ %9ld.%06ld ] start [ %9ld.%06ld ]\n", adf_current_slot(), \
						now.tv_sec, now.tv_usec, tv_start.tv_sec, tv_start.tv_usec );
				break;
			}
		case PF_CMD_SHARE_HUNTER_INFO :
			{
				/*	TODO: Send SHARE_ACK
				 *	check slot conflict and store SHARE_INFO
				 *	- conflict: APP -> DRV | change slot in my list.		*/
				IOCTL_CMD_DATA cmdData;
				RF_COMMAND *pRfCmd = NULL;
				uint32_t hunter_id = pResponse->share_info.hunter_id;

				ktime_t start;
				struct timeval tv_start, now;

				do_gettimeofday( &now );
				cmdData.devInfo.id = hunter_id;
				memcpy( cmdData.devInfo.name, get_connector_info()->name, HUNTER_NAME_LEN );

				if( test_bit( FLAG_SHARING_TX, & pAdf_struct->flags ) == 1 )
				{
					pRfCmd = pf_alloc_cmd_gen( pAdf_struct, &cmdData, PF_CMD_SHARE_ACK );
					if( pRfCmd != NULL )
					{
						if( pAdf_struct->init_reference == false )
						{
							pRfCmd->delay_ms = 0;
						}
						adf_mode_change( pRfCmd );
						clear_bit( FLAG_SHARING_TX, & pAdf_struct->flags );
						kfree( pRfCmd );
						pRfCmd = NULL;
						ret = true;
						/* TODO: NOTIFY_CB: Share RX information -> DEVICE_INFO	*/
						adf_notify( pAdf_struct, NOTIFY_SHARE_INFO, ( const unsigned long )pResponse );
					}
					{
						pRfCmd = ( RF_COMMAND * )list_find_data( &pAdf_struct->cmd_list, LIST_KEY_SHARE );
						if( pRfCmd != NULL )
						{
							PF_TX_COMMAND *pTxCmd = ( PF_TX_COMMAND * )( pRfCmd->packetData );
							int id = pRfCmd->id;
							if( pTxCmd->command == PF_CMD_SHARE_HUNTER_INFO )
							{
								INFO_ADF( "SH_HUNTER TX: s: %d Send SHARE_ACK => Free H_INFO!!! id: 0x%04x\n", \
										adf_current_slot(), id );
								kfree( pRfCmd );
							}
							else
							{
								WARN_ADF( "RF_DONE: SHARE_ACK: does not match id: 0x%04x cmd: 0x%02x\n", \
										id, pTxCmd->command );
								kfree( pRfCmd );
							}
						}
					}
				}
				else if( test_bit( FLAG_SHARING_RX, & pAdf_struct->flags ) == 1 )
				{
					pRfCmd = pf_alloc_cmd_gen( pAdf_struct, &cmdData, PF_CMD_SHARE_HUNTER_INFO );
					if( pRfCmd != NULL )
					{
						adf_mode_change( pRfCmd );
						kfree( pRfCmd );
						pRfCmd = NULL;
						ret = true;
					}
				}

				{
					int desired_slot_index = pResponse->share_info.ch_slot.slot - 1;
					bool empty = adf_slot_is_empty( desired_slot_index );

					if( empty == true )
					{
						/* NOTE: Set slot info usign SHARE_INFO and Notify sharing sucess to App.
						 * NOTIFY_ADD_DEVICE with Sharing flag	*/
						ADF_SLOT_INFO slotInfo;

						memset( &slotInfo, 0x0, sizeof( ADF_SLOT_INFO ) );

						slotInfo.id = pResponse->share_info.id;
						slotInfo.freq = pResponse->share_info.ch_slot.ch;
						slotInfo.period = PERIOD_UNKNOWN;
						slotInfo.period_remain = 1;
						slotInfo.shared = true;
						slotInfo.mode = SLOT_MODE_LISTEN;
						slotInfo.last_rx_time.tv_sec = 0;
						slotInfo.last_rx_time.tv_usec = 0;

						adf_clear_slot_info( pAdf_struct, desired_slot_index );
						adf_set_slot_info( pAdf_struct, desired_slot_index, &slotInfo );

#if 0
						adf_init_start_ref_timer();
						if( pAdf_struct->init_reference == false )
						{
							pAdf_struct->last_pairing_slot_index = desired_slot_index;
						}
#endif
						adf_notify( pAdf_struct, NOTIFY_SHARE_INFO, ( const unsigned long )pResponse );
					}
					else
					{
						CH_SLOT ch_slot = adf_assign_slot_freq( pAdf_struct );

						/* TODO: NOTIFY_CB: Share RX slot conflict -> recommend ch / slot	*/
						adf_notify( pAdf_struct, NOTIFY_SLOT_CONFLICT, ch_slot.slot );

						if( ch_slot.ch_slot == INVALID_CH_SLOT ) { /* NOTE: slot not found */ };
					}
				}
				start = timeval_to_ktime( pPacket->tv_swd );
				start = ktime_sub_ns( start, ( u64 )MS_TO_NS( TX_HEADER_TIME ) );
				tv_start = ktime_to_timeval( start );

				INFO_ADF( "[ SH_HUNTER ] s: %d now [ %9ld.%06ld ] start [ %9ld.%06ld ]\n", adf_current_slot(), \
						now.tv_sec, now.tv_usec, tv_start.tv_sec, tv_start.tv_usec );
				break;
			}
		case PF_CMD_SHARE_ACK :
			{
				ktime_t start;
				struct timeval tv_start, now;

				clear_bit( FLAG_SHARING_TX, & pAdf_struct->flags );
				clear_bit( FLAG_SHARING_RX, & pAdf_struct->flags );
				clear_bit( FLAG_SHARING_RX_ACK, & pAdf_struct->flags );
				/* TODO: NOTIFY_CB: Share TX Sucess -> NONE */
				adf_notify( pAdf_struct, NOTIFY_SHARE_ACK, -1 );

				do_gettimeofday( &now );

				start = timeval_to_ktime( pPacket->tv_swd );
				start = ktime_sub_ns( start, ( u64 )MS_TO_NS( TX_HEADER_TIME ) );
				tv_start = ktime_to_timeval( start );

				INFO_ADF( "[ SH_ACK ] s: %d now [ %9ld.%06ld ] start [ %9ld.%06ld ]\n", adf_current_slot(), \
						now.tv_sec, now.tv_usec, tv_start.tv_sec, tv_start.tv_usec );
				break;
			}
	}

#ifdef __DEBUG_QUEUE_CNT__
	pAdf_struct->dealloc_cnt++;
#endif	//	__DEBUG_QUEUE_CNT__
	kfree( pPacket );
	pPacket = NULL;
	return ret;
}

static bool adf_init_reference( void *pData )
{
	ADF_STRUCT	* pAdf_struct = NULL;
	RF_COMMAND cmd = { ADF_MODE_SLEEP, FREQ_UNKNOWN, 0x0, 0, 0, { 0x0, }, 0, 0 };
	int			listening_slot = -1;
	int timeout;
	int period;

	ASSERT( pData != NULL );

	pAdf_struct = ( ADF_STRUCT * )pData;

	if( test_bit( FLAG_PAIRING, & pAdf_struct->flags ) == 1 ) return false;

	if( pAdf_struct->last_pairing_slot_index < 0 )
	{
	 	/*	TODO: valid slot first...	*/
		int *pSlotIndex = ( int * )list_pop_data( &pAdf_struct->pair_list );
		if( pSlotIndex == NULL )
		{
			static int cnt = 0;
			if( ++cnt >= 20 )
			{
				INFO_ADF( "pair_list is empty!!!! init_reference: %d\n", pAdf_struct->init_reference );
				cnt = 0;
			}
			if( test_bit( FLAG_SHARING_RX, & pAdf_struct->flags ) == 1 )
			{
				bool active = hrtimer_active( &pAdf_struct->reference_timer );
				if( active == false )
				{
					INFO_ADF( "call adf_init_start_ref_timer() by FLAG_SHARING_RX. pair_list empty\n" );
					adf_init_start_ref_timer();
				}
			}
			return false;
		}
		list_push( &pAdf_struct->pair_list, pSlotIndex, sizeof( int ), LIST_KEY_PAIR( *pSlotIndex ) );
		listening_slot = *pSlotIndex;
		pAdf_struct->last_pairing_slot_index = listening_slot;

		switch( adf_get_slot_info_period( listening_slot ) )
		{
			case PERIOD_2_SECS   : period = ( 2 * 1000 ); break;
			case PERIOD_6_SECS   : period = ( 6 * 1000 ); break;
			case PERIOD_10_SECS  : period = ( 10 * 1000 ); break;
			case PERIOD_30_SECS  : period = ( 30 * 1000 ); break;
			case PERIOD_120_SECS : period = ( 120 * 1000 ); break;
			default: period = ( 2 * 1000 ); break;
		}
		timeout = period + ( 1 * 1000 );	//	+ 1 sec
	}
	else
	{
		listening_slot = pAdf_struct->last_pairing_slot_index;
		switch( adf_get_slot_info_period( listening_slot ) )
		{
			case PERIOD_2_SECS   : period = ( 2 * 1000 ); break;
			case PERIOD_6_SECS   : period = ( 6 * 1000 ); break;
			case PERIOD_10_SECS  : period = ( 10 * 1000 ); break;
			case PERIOD_30_SECS  : period = ( 30 * 1000 ); break;
			case PERIOD_120_SECS : period = ( 120 * 1000 ); break;
			default: period = ( 2 * 1000 ); break;
		}
		/*	NOTE: Maybe SKIP condition by other Collar. Not timeout sequence.	*/
		timeout = period - SLOT_DURATION;
	}

	if( listening_slot < 0 )
	{
		return false;
	}

#if 1
	if( adf_slot_is_listening( listening_slot ) == false )
	{
		WARN_ADF( "last_pairing_slot_index: %d slot_index %d is not listening.\n", pAdf_struct->last_pairing_slot_index, listening_slot );
		return false;
	}
#else
	ASSERT( adf_slot_is_listening( listening_slot ) == true );
#endif

	/*	stop reference timer and slot timer.
	 *	RX command for 1st GPS Info.
	 *	*/
	timer_stop( ( void * )( & pAdf_struct->reference_timer ) );
	timer_stop( ( void * )( & pAdf_struct->slot_timer ) );

	//pAdf_struct->slot_seq = FLAG_SLOT_R_0 - 1;
	pAdf_struct->slot_seq = FLAG_SLOT_END;
	pAdf_struct->reference_timestamp.tv64 = 0LL;
	pAdf_struct->reference_next_timestamp.tv64 = 0LL;

	cmd.response_length = 0;
	cmd.id = adf_get_slot_info_id( listening_slot );

	cmd.mode = ADF_MODE_RX;
	cmd.freq = adf_get_slot_info_freq( listening_slot );
	cmd.length = get_command_len( PF_CMD_GPS_INFO ) + 1;
	cmd.timeout = -1;	//	No timeout.
	cmd.timeout = timeout;
	cmd.delay_ms = 0;

	adf_mode_change( & cmd );

	INFO_ADF( "RX for 1st GPS: id: 0x%04x freq: %d mode: %d => %d timeout: %d ms pair_list: %d\n", \
			pAdf_struct->slot_info[ listening_slot ].id, cmd.freq, adf_mode(), cmd.mode, cmd.timeout, \
			list_count( &pAdf_struct->pair_list ) );

	return true;
}

static bool adf_init_reference_time( void *pData, void *pPacketData )
{
	ADF_STRUCT *pAdf_struct = NULL;
	ADF_PACKET *pPacket = NULL;
	PF_RX_COMMAND *pResponse = NULL;
	GPS_INFO *pGpsInfo = NULL;
	int slot_index;
	ADF_SLOT_INFO slotInfo;
	bool isValid = false;

	ASSERT( ( pData != NULL ) && ( pPacketData != NULL ) );

	pAdf_struct = ( ADF_STRUCT * )pData;
	pPacket = ( ADF_PACKET * )pPacketData;

	pResponse = ( PF_RX_COMMAND * )( pPacket->packetData );
	if( pResponse->command != PF_CMD_GPS_INFO )
	{
		return false;
	}
	isValid = adf_valid_gps_packet( pAdf_struct, pPacket );

	pGpsInfo = &pResponse->gps_info;
	slot_index = adf_find_slot_listening( pGpsInfo->dog_id );
	if( slot_index < 0 )
	{
		slot_index = pGpsInfo->ch_slot.slot - 1;
	}

	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	//INFO_ADF( "GPS [ slot: %d freq: %d id: 0x%04x ] slot_mode: %s\n", pGpsInfo->ch_slot.slot - 1,
	//		pGpsInfo->ch_slot.ch, pGpsInfo->dog_id, adf_slot_mode_str( pAdf_struct->slot_info[ slot_index ].mode ) );
	if( isValid == false )
	{
		bool isValid_slotIndex = false;
		bool isValid_freq = false;

		adf_get_slot_info( pAdf_struct, slot_index, &slotInfo );

		isValid_slotIndex = ( ( pGpsInfo->ch_slot.slot - 1 ) == slot_index ) ? true : false;
		isValid_freq = ( pGpsInfo->ch_slot.ch == slotInfo.freq ) ? true : false;

		if( ( isValid_slotIndex == true ) && ( isValid_freq == true ) )
		{
			/*	CONFLICT	*/
			WARN_ADF( "NOTIFY_1: CONFLICT!! slot: %d freq: %d %d id: 0x%04x != 0x%04x mode: %s\n", slot_index, \
					slotInfo.freq, pGpsInfo->ch_slot.ch, slotInfo.id, pGpsInfo->dog_id, \
					adf_slot_mode_str( slotInfo.mode ) );
			/* TODO: NOTIFY_CB: Share RX slot conflict -> recommend ch / slot	*/
			adf_notify( pAdf_struct, NOTIFY_SLOT_CONFLICT, slot_index + 1 );
			adf_cancel_pairing( pAdf_struct, slot_index );
		}
		else
		{
			/*	SKIP: retry RX for 1st GPS information	*/
			INFO_ADF( "SKIP: [ slot: %d freq: %d id: 0x%04x ] slot_mode: %s\n", pGpsInfo->ch_slot.slot - 1, \
					pGpsInfo->ch_slot.ch, pGpsInfo->dog_id, adf_slot_mode_str( slotInfo.mode ) );
			adf_init_reference( pAdf_struct );
		}

		return false;
	}
	INFO_ADF( "last_pairing_slot_index: %d slot_index: %d pair_list: %d\n", \
			pAdf_struct->last_pairing_slot_index, slot_index, list_count( &pAdf_struct->pair_list ) );
	pAdf_struct->last_pairing_slot_index = -1;
	{
		int remain;
		PERIOD period = adf_get_slot_info_period( slot_index );
		switch( period )
		{
			case PERIOD_2_SECS   : remain = ( 2 * 1000 ) / REF_DURATION; break;
			case PERIOD_6_SECS   : remain = ( 6 * 1000 ) / REF_DURATION; break;
			case PERIOD_10_SECS  : remain = ( 10 * 1000 ) / REF_DURATION; break;
			case PERIOD_30_SECS  : remain = ( 30 * 1000 ) / REF_DURATION; break;
			case PERIOD_120_SECS : remain = ( 120 * 1000 ) / REF_DURATION; break;
			default: remain = ( 2 * 1000 ) / REF_DURATION; break;
		}

		adf_set_slot_info_field( pAdf_struct, slot_index, SLOT_FIELD_PERIOD_REMAIN, &remain );
	}
	return adf_sync_reference_time( pData );
}

static bool adf_valid_gps_packet( void *pData, ADF_PACKET *pPacket )
{
	ADF_STRUCT *pAdf_struct = NULL;
	PF_RX_COMMAND *pResponse = NULL;
	GPS_INFO *pGpsInfo = NULL;

	int slot_index;
	ADF_SLOT_INFO slotInfo;
	bool isValid_hunterId = true;
	bool isValid_dogId = false;
	bool isValid_slotIndex = false;
	bool isValid_freq = false;

	ASSERT( ( pData != NULL ) && ( pPacket != NULL ) );

	pAdf_struct = ( ADF_STRUCT * )pData;
	pResponse = ( PF_RX_COMMAND * )( pPacket->packetData );
	if( pResponse->command != PF_CMD_GPS_INFO ) return false;

	pGpsInfo = &pResponse->gps_info;
	slot_index = adf_find_slot_listening( pGpsInfo->dog_id );

	if( slot_index < 0 ) return false;

	adf_get_slot_info( pAdf_struct, slot_index, &slotInfo );

	if( slotInfo.shared == false )
	{
		isValid_hunterId = ( pGpsInfo->hunter_id == get_connector_info()->id ) ? true : false;
	}
	isValid_dogId = ( pGpsInfo->dog_id == slotInfo.id ) ? true : false;
	isValid_slotIndex = ( ( pGpsInfo->ch_slot.slot - 1 ) == slot_index ) ? true : false;
	isValid_freq = ( pGpsInfo->ch_slot.ch == slotInfo.freq ) ? true : false;

	DEBUG_ADF( "isValid_hunterId: %d isValid_dogId: %d isValid_slotIndex: %d isValid_freq: %d\n", \
			isValid_hunterId, isValid_dogId, isValid_slotIndex, isValid_freq );

	return ( isValid_hunterId && isValid_dogId && isValid_slotIndex && isValid_freq );
}

static bool adf_valid_period_gps_packet( void *pData, ADF_PACKET *pPacket )
{
	ADF_STRUCT *pAdf_struct = NULL;
	PF_RX_COMMAND *pResponse = NULL;
	GPS_INFO *pGpsInfo = NULL;

	int slot_index;
	ADF_SLOT_INFO slotInfo;

	ASSERT( ( pData != NULL ) && ( pPacket != NULL ) );

	pAdf_struct = ( ADF_STRUCT * )pData;
	pResponse = ( PF_RX_COMMAND * )( pPacket->packetData );
	if( pResponse->command != PF_CMD_GPS_INFO ) return false;

	pGpsInfo = &pResponse->gps_info;
	slot_index = adf_find_slot_listening( pGpsInfo->dog_id );

	if( slot_index < 0 ) return false;

	adf_get_slot_info( pAdf_struct, slot_index, &slotInfo );

	/* NOTE: check period for SHARING.
	 * */
	if( slotInfo.period != pGpsInfo->dog_info.period )
	{
		INFO_ADF( "PERIOD: slot_index %d period %d -> %d\n", slot_index, slotInfo.period, pGpsInfo->dog_info.period );

		slotInfo.period = pGpsInfo->dog_info.period;
		slotInfo.period_remain = 1;
		slotInfo.last_rx_time.tv_sec = 0;
		slotInfo.last_rx_time.tv_usec = 0;

		adf_set_slot_info( pAdf_struct, slot_index, &slotInfo );
	}
	return true;
}

static bool adf_sync_reference_time( void *pData )
{
	ADF_STRUCT *pAdf_struct = NULL;
	ktime_t now, ref_time;
	struct timeval tv;
	int slot_index = -1;
	long delta = 0;
	long delta_sum = 0;
	int valid_cnt = 0;

	ASSERT( pData != NULL );

	pAdf_struct = ( ADF_STRUCT * )pData;

	slot_index = adf_find_valid_slot( pAdf_struct->slot_info );

#if 0
	if( ( slot_index < 0 ) || ( slot_index >= MAX_SLOT_INDEX ) )
	{
		INFO_ADF( "valid slot not found!! pair_list: %d\n", list_count( &pAdf_struct->pair_list ) );

		return false;
	}
#else
	if( ( slot_index < 0 ) || ( slot_index >= MAX_SLOT_INDEX ) ) return false;
#endif

	/*	NOTE: calculate reference_time using received GPS time.
	 *	TX_TIME: GPS( 22 ) + syncworkd( 4 ) + dummy( 2 ) + 0xAA
	 *	10 kbps: 100 us per 1 bit
	 *	29 bytes / packet: 29 * 8 * 100 = 23.2 ms
	 *
	 *	Collar:    GPS      TX_START  SWD     TX_DONE
	 *	            |      ...     |  |    ..    |
	 *	Hunter:                   (TS)        RX_DONE
	 *
	 *	SLOT_TIME = TX_START = SWD - TX_HEADER_TIME
	 *	NEXT_REF_TIME =  SLOT_TIME + ( SLOT_DURATION * ( MAX_SLOT_INDEX - slot_index ) )
	 *
	 *	delta time for timer: average of calculated ref_time.	*/
	for( slot_index = 0 ; slot_index < MAX_SLOT_INDEX ; slot_index++ )
	{
		if( adf_slot_is_listening( slot_index ) == false )
		//if( ( adf_get_slot_info_mode( slot_index ) & SLOT_MODE_LISTEN ) == 0x0 )
		{
			continue;
		}
		if( adf_slot_is_valid( pAdf_struct, slot_index ) == true )
		{
			valid_cnt++;

			do_gettimeofday( &tv );
			now = timeval_to_ktime( tv );
			ref_time = timeval_to_ktime( adf_get_slot_info_last_rx_time( slot_index ) );

			/* after initialized, last_rx_time is last GPS information received time.
			 * */
			if( pAdf_struct->init_reference == true )
			{
				ref_time = ktime_add_ns( ref_time, ( u64 )MS_TO_NS( REF_DURATION ) );
			}
			/*	Next reference time: start point of slot.	*/
			ref_time = ktime_add_us( ref_time, ( SLOT_DURATION * 1000 ) * ( MAX_SLOT_INDEX - slot_index ) \
					- ( SHIFT_TIME * 1000 ) - ( GLOBAL_RX_MARGIN * 1000 ));
			delta_sum += ( long )ktime_us_delta( ref_time, now ) / 1000;

			/*	NOTE: last slot only condition.
			 *	start reference timer now	*/
			if( ( valid_cnt == 1 ) && ( slot_index == ( MAX_SLOT_INDEX - 1 ) ) \
					&& ( pAdf_struct->init_reference == false ) )
			{
				delta_sum = 0;
			}
		}
	}

	if( valid_cnt == 0 ) return false;

	delta = delta_sum / valid_cnt;

	if( delta < 0 )
	{
		WARN_ADF( "[ SYNC ] ref: [ %9ld.%06ld ] / valid_cnt: %d delta: %ld\n",
				( ktime_to_timeval( ref_time ) ).tv_sec, ( ktime_to_timeval( ref_time ) ).tv_usec,
				valid_cnt, delta );

		if( pAdf_struct->init_reference == false )
		{
			//delta += REF_DURATION;
			INFO_ADF( "update delta: %d\n", delta );
		}
		else
		{
			return false;
		}
	}

	/*	Change time base to ktime.	*/
	if( pAdf_struct->init_reference == true )
	{
		now = ktime_get();
		pAdf_struct->reference_next_timestamp = ktime_add_us( now, delta * 1000 );
	}

	timer_start( ( void * )( & pAdf_struct->reference_timer ), delta );

#if 0
	if( pAdf_struct->init_reference == false )
	{
		INFO_ADF( "[ SYNC ] ref: [ %9ld.%06ld ] / valid_cnt: %d delta: %ld slot_flags: 0x%08lx\n",
				( ktime_to_timeval( ref_time ) ).tv_sec, ( ktime_to_timeval( ref_time ) ).tv_usec,
				valid_cnt, delta, pAdf_struct->slot_flags );
	}
#endif
	//INFO_ADF( "[ SYNC ] ref: [ %9ld.%06ld ] / valid_cnt: %d delta: %ld slot_flags: 0x%08lx\n",
	//		( ktime_to_timeval( ref_time ) ).tv_sec, ( ktime_to_timeval( ref_time ) ).tv_usec,
	//		valid_cnt, delta, pAdf_struct->slot_flags );
	//INFO_ADF( "[ SYNC ] now: [ %09ld.%06ld ] ref: [ %9ld.%06ld ] delta: %ld\n",
	//		tv.tv_sec, tv.tv_usec, ( ktime_to_timeval( ref_time ) ).tv_sec, ( ktime_to_timeval( ref_time ) ).tv_usec,
	//		delta );

	return true;
}

static long adf_sync_slot_time( struct work_struct * pWork, const int slot )
{
	ADF_STRUCT	* pAdf_struct = NULL;
	ktime_t		now = ktime_get();
	int next_slot;
	int next_slot_index;
	bool next_slot_is_valid = false;
	struct timeval tv;
	ktime_t next;
	long delta;

	ASSERT( pWork != NULL );

	pAdf_struct = container_of( pWork, ADF_STRUCT, slot_work );

	next_slot = ( slot + 1 ) % FLAG_SLOT_END;

	if( next_slot >= FLAG_SLOT_0 )
	{
		next_slot_index = next_slot - FLAG_SLOT_0;
		ASSERT( ( next_slot_index >= 0 ) && ( next_slot_index < MAX_SLOT_INDEX ) );
		next_slot_is_valid = adf_slot_is_valid( pAdf_struct, next_slot_index );
	}

	/*	NOTE:	Compensate time...
	 *	expected Time:	from ref_time to next slot_time.	*/
	if( next_slot_is_valid == true )
	{
		next = timeval_to_ktime( adf_get_slot_info_last_rx_time( next_slot_index ) );
		next = ktime_add_ns( next, ( u64 )MS_TO_NS( REF_DURATION ) );
		do_gettimeofday( &tv );
		now = timeval_to_ktime( tv );

		delta = ( ( long )ktime_us_delta( next, now ) / 1000 ) - RX_MARGIN;
	}
	else
	{
		/*	NOTE: Maybe slot is R_1	*/
		if( slot < FLAG_SLOT_R_2 )
		{
			delta = ( ( slot + 1 ) * SLOT_DURATION );
			/*	NOTE: need RX margin for SHARE_ACK or SHARE_HUNTER_INFO	*/
			if( ( test_bit( FLAG_SHARING_TX, & pAdf_struct->flags ) == 0 ) \
					&& ( test_bit( FLAG_SHARING_RX, & pAdf_struct->flags ) == 0 ) )
			{
				delta += GLOBAL_RX_MARGIN;
			}
		}
		else
		{
			delta = ( ( slot + 1 ) * SLOT_DURATION );
			delta += GLOBAL_RX_MARGIN;
			delta += ( SHIFT_TIME - RX_MARGIN );
		}

		delta = delta - ( ( long )ktime_us_delta( now, pAdf_struct->reference_timestamp ) / 1000 );
	}

	return delta;
}

#ifdef __TEST_POLL__
static int push_test_packet( void )
{
	static uint32_t seq = 0;
	ADF_STRUCT		* pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );
	ADF_PACKET	*pPacket = NULL;

	pPacket = ( ADF_PACKET * )kzalloc( sizeof( ADF_PACKET ), GFP_KERNEL );
	if( pPacket == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		return -EINVAL;
	}
	//pPacket->bit_index = -1;
	pPacket->bit_index = ( seq++ ) & 0x1F;
	pPacket->length = pAdf_struct->init.max_packet_length;
	pPacket->response_length = 0;
	*( uint32_t * )( pPacket->packetData ) = 0x12345678;

	do_gettimeofday( & pPacket->tv );

	//list_push( & pAdf_struct->rx_list, pPacket, sizeof( ADF_PACKET ), list_assign_key( pPacket->pData[ 1 ] ) );
	list_push( & pAdf_struct->rx_list, pPacket, sizeof( ADF_PACKET ), LIST_KEY_TEST );
	if( ( seq % 100 ) == 0 )
		printk( "seq: %d count: %d\n", seq % 0x1F, list_count( &pAdf_struct->rx_list ) );

	wake_up_interruptible_all( & pAdf_struct->rf_done_waitq );
	//wake_up_interruptible( & pAdf_struct->rf_done_waitq );

	return 0;
}

#endif	//	__TEST_POLL__

int adf_find_slot_listening( const uint16_t id )
{
	int		idx;

	if( id == 0x0 ) return -1;

	for( idx = 0 ; idx < MAX_SLOT_INDEX ; idx++ )
	{
		if( adf_get_slot_info_id( idx ) == id )
		{
			if( ( adf_get_slot_info_mode( idx ) & SLOT_MODE_LISTEN ) != 0x0 )
			{
				return idx;
			}
		}
	}
	return -1;
}

static int adf_find_empty_slot( ADF_SLOT_INFO slot_info[] )
{
#if 1
	uint8_t random;
	int slot_index, idx;

	ASSERT( slot_info != NULL );

	get_random_bytes( &random, 1 );
	for( idx = 0 ; idx < MAX_SLOT_INDEX ; idx++ )
	{
		slot_index = ( random + idx ) % MAX_SLOT_INDEX;
		if( slot_info[ slot_index ].mode == SLOT_MODE_NOT_ASSIGN )
		{
			return slot_index;
		}
	}
	return -1;
#else
	int		idx;

	ASSERT( slot_info != NULL );

	for( idx = 0 ; idx < MAX_SLOT_INDEX ; idx++ )
	{
		if( adf_get_slot_info_id( idx ) == 0x0 )
		{
			return idx;
		}
	}
	return -1;
#endif
}

static int adf_find_pairing_slot( ADF_SLOT_INFO slot_info[] )
{
	int		idx;

	ASSERT( slot_info != NULL );

	for( idx = 0 ; idx < MAX_SLOT_INDEX ; idx++ )
	{
		if( adf_get_slot_info_mode( idx ) == SLOT_MODE_PAIRING )
		{
			return idx;
		}
	}
	return -1;
}

static int adf_find_listening_slot( ADF_SLOT_INFO slot_info[] )
{
	ADF_STRUCT	* pAdf_struct = NULL;
	int		idx;

	ASSERT( slot_info != NULL );

	pAdf_struct = container_of( slot_info, ADF_STRUCT, slot_info );
	//pAdf_struct = container_of( slot, ADF_STRUCT, slot[ MAX_SLOT_INDEX ] );

	for( idx = 0 ; idx < MAX_SLOT_INDEX ; idx++ )
	{
		if( adf_slot_is_listening( idx ) == true )
		{
			return idx;
		}
	}
	return -1;
}

int adf_find_slot_changing( const uint16_t id )
{
	int		idx;

	for( idx = 0 ; idx < MAX_SLOT_INDEX ; idx++ )
	{
		if( adf_get_slot_info_id( idx ) == id )
		{
			if( ( adf_get_slot_info_mode( idx ) & SLOT_MODE_CHANGING ) != 0x0 )
			{
				return idx;
			}
		}
	}
	//if( idx == MAX_SLOT_INDEX ) idx = -1;
	//INFO_ADF( "Changing: id 0x%04x slot_index: %d\n", id, idx );
	return -1;
}

static int adf_find_valid_slot( ADF_SLOT_INFO slot_info[] )
{
	ADF_STRUCT	* pAdf_struct = NULL;

	ASSERT( slot_info != NULL );

	pAdf_struct = container_of( slot_info, ADF_STRUCT, slot_info );
	//pAdf_struct = container_of( slot, ADF_STRUCT, slot[ MAX_SLOT_INDEX ] );

	return ( find_next_bit( &pAdf_struct->slot_flags, FLAG_SLOT_END, FLAG_SLOT_0 ) - FLAG_SLOT_0 );
}

static int adf_find_rx_time_valid_slot( ADF_SLOT_INFO slot_info[] )
{
	ADF_STRUCT	* pAdf_struct = NULL;
	int slot_index;

	ASSERT( slot_info != NULL );

	pAdf_struct = container_of( slot_info, ADF_STRUCT, slot_info );
	//pAdf_struct = container_of( slot, ADF_STRUCT, slot[ MAX_SLOT_INDEX ] );

	for( slot_index = 0 ; slot_index < MAX_SLOT_INDEX ; slot_index++ )
	{
		if( adf_get_slot_info_last_rx_time( slot_index ).tv_sec != 0 )
		{
			return slot_index;
		}
	}
	return -1;
}

static bool adf_slot_is_valid( void *pData, const int slot_index )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;

	ASSERT( pData != NULL );
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	return test_bit( FLAG_SLOT_0 + slot_index, &pAdf_struct->slot_flags );
}

static bool slot_update_remain( void *pData, const int slot_index )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )pData;
	ADF_SLOT_INFO *pSlotInfo = NULL;
	ktime_t last;
	int remain;

	ASSERT( pData != NULL );
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	mutex_lock( & pAdf_struct->slot_mutex[ slot_index ] );

	pSlotInfo = &pAdf_struct->slot_info[ slot_index ];

	switch( pSlotInfo->period )
	{
		case PERIOD_2_SECS   : remain = ( 2 * 1000 ) / REF_DURATION; break;
		case PERIOD_6_SECS   : remain = ( 6 * 1000 ) / REF_DURATION; break;
		case PERIOD_10_SECS  : remain = ( 10 * 1000 ) / REF_DURATION; break;
		case PERIOD_30_SECS  : remain = ( 30 * 1000 ) / REF_DURATION; break;
		case PERIOD_120_SECS : remain = ( 120 * 1000 ) / REF_DURATION; break;
		default: remain = ( 2 * 1000 ) / REF_DURATION; break;
	}

	if( pSlotInfo->last_rx_time.tv_sec == 0 )
	{
		pSlotInfo->period_remain = 1;
	}

	pSlotInfo->period_remain--;
#if 0
	if( ( pSlotInfo->period_remain <= 0 ) || ( pAdf_struct->init_reference == false ) )
#else
	if( pSlotInfo->period_remain <= 0 )
#endif
	{
		pSlotInfo->period_remain = remain;
		clear_bit( slot_index + FLAG_SLOT_0, &pAdf_struct->slot_flags );
		//INFO_ADF( "OK?? slot: %d period: %d remain: %d\n", slot_index, pSlotInfo->period, pSlotInfo->period_remain );

		mutex_unlock( & pAdf_struct->slot_mutex[ slot_index ] );
		return true;
	}

	/*	if GPS_INFO available, Update last_rx_time		*/
	if( adf_slot_is_valid( pAdf_struct, slot_index ) == true )
	{
		last = ktime_add_ns( timeval_to_ktime( pSlotInfo->last_rx_time ), ( u64 )MS_TO_NS( REF_DURATION ) );
		pSlotInfo->last_rx_time = ktime_to_timeval( last );
	}

	//INFO_ADF( "slot: %d period: %d remain: %d\n", slot_index, pSlotInfo->period, pSlotInfo->period_remain );

	mutex_unlock( & pAdf_struct->slot_mutex[ slot_index ] );

	return false;
}

static FREQ adf_assign_freq( ADF_SLOT_INFO slot_info[] )
{
	uint8_t random;

	ASSERT( slot_info != NULL );

	//return FREQ_154_57;

	get_random_bytes( &random, 1 );

	/*	Freq. index: 1 to 5, random: 0 to 4	*/
	return ( FREQ )( ( random % ( FREQ_END - 1 ) ) + 1 );
}

static const CH_SLOT adf_assign_slot_freq( void *pData )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;
	int			slot_index;
	int			freq;
	int			i, j, zero_bit;
	unsigned long slot_flags = 0x0;
	bool found = false;

	CH_SLOT ch_slot;
	ch_slot.ch_slot = INVALID_CH_SLOT;

	ASSERT( pData != NULL );

	slot_index = adf_find_empty_slot( pAdf_struct->slot_info );
	if( slot_index < 0 ) return ch_slot;

	freq = adf_assign_freq( pAdf_struct->slot_info );

	/*	find id in current slot_map	*/
	for( i = 0 ; i < MAX_SLOT_INDEX ; i++ )
	{
		ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );
		if( test_and_set_bit( FLAG_SLOT_0 + slot_index, &slot_flags ) == 0 )
		{
			if( adf_get_slot_info_mode( slot_index ) == SLOT_MODE_NOT_ASSIGN )
			{
				for( j = 0 ; j < ( FREQ_END - 1 ) ; j++ )
				{
					ASSERT( ( freq >= FREQ_151_82 ) && ( freq < FREQ_END ) );
					if( slot_map[ freq - 1 ][ slot_index ] == 0x0 )
					{
						found = true;
						break;
					}
					freq = ( freq  % ( FREQ_END - 1 ) ) + 1;
				}
			}
		}
		if( found == true ) break;
		zero_bit = find_next_zero_bit( &slot_flags, FLAG_SLOT_END, FLAG_SLOT_0 );
		if( zero_bit == FLAG_SLOT_END ) break;
		slot_index = zero_bit - FLAG_SLOT_0;
	}

	if( found == false ) return ch_slot;

	ch_slot.ch = freq;
	ch_slot.slot = slot_index + 1;
	INFO_ADF( "recommend: 0x%02x [s %d f %d] slot_flags: 0x%08lx\n", ch_slot.ch_slot, ch_slot.slot, \
			ch_slot.ch, slot_flags >> FLAG_SLOT_0 );

	return ch_slot;
}

int adf_assign_slot_info( void *pData, const int id )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;
	int			slot_index;
	int			freq;
	int			i, j;
	bool found = false;
	ADF_SLOT_INFO slotInfo;
	CH_SLOT ch_slot;

	ASSERT( pData != NULL );

	slot_index = adf_find_slot_listening( id );
	if( slot_index >= 0 ) return slot_index;

	slot_index = adf_find_empty_slot( pAdf_struct->slot_info );
	if( slot_index < 0 ) return slot_index;

	/*	find id in current slot_map	*/
	for( i = 0 ; i < MAX_SLOT_INDEX ; i++ )
	{
		for( j = 0 ; j < ( FREQ_END - 1 ) ; j++ )
		{
			if( slot_map[ j ][ i ] == id )
			{
				slot_index = i;
				freq = j + 1;
				found = true;
				break;
			}
		}
		if( found == true )
		{
			/*	NOTE: if slot assigned already for the other collar then,
			 *	clear previous assigned info and re-assign slot info.
			 *	by jyhuh 2018-06-20 11:23:21	*/
			if( adf_get_slot_info_id( slot_index ) != 0x0 )
			{
				INFO_ADF( "clear previous info: ID 0x%04x [slot: %d freq: %d ]\n", id, slot_index, freq );
				slot_map[ freq - 1 ][ slot_index ] = 0x0;
				found = false;
			}
			break;
		}
	}

	if( found == false )
	{
		ch_slot = adf_assign_slot_freq( pAdf_struct );
		if( ch_slot.ch_slot != INVALID_CH_SLOT ) found = true;
		slot_index = ch_slot.slot - 1;
		freq = ch_slot.ch;
	}
	if( found == false ) return -1;

	INFO_ADF( "assign: ID 0x%04x [slot_index: %d freq: %d ]\n", id, slot_index, freq );

	slotInfo.id = id;
	slotInfo.freq = freq;
	slotInfo.period = PERIOD_2_SECS;
	//slotInfo.period = PERIOD_6_SECS;
	slotInfo.period_remain = 1;
	slotInfo.shared = false;
	slotInfo.mode = SLOT_MODE_IDLE;
	slotInfo.last_rx_time.tv_sec = 0;
	slotInfo.last_rx_time.tv_usec = 0;

	adf_clear_slot_info( pAdf_struct, slot_index );
	adf_set_slot_info( pAdf_struct, slot_index, &slotInfo );
	slot_update_remain( pAdf_struct, slot_index );

	return slot_index;
}

static void adf_clear_slot_info( void *pData, const int slot_index )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;

	ASSERT( pData != NULL );
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	mutex_lock( & pAdf_struct->slot_mutex[ slot_index ] );

	pAdf_struct->slot_info[ slot_index ].id = 0x0;
	pAdf_struct->slot_info[ slot_index ].freq = FREQ_UNKNOWN;
	pAdf_struct->slot_info[ slot_index ].period = PERIOD_UNKNOWN;
	pAdf_struct->slot_info[ slot_index ].period_remain = 0;
	pAdf_struct->slot_info[ slot_index ].shared = false;
	pAdf_struct->slot_info[ slot_index ].mode = SLOT_MODE_NOT_ASSIGN;
	pAdf_struct->slot_info[ slot_index ].last_rx_time.tv_sec = 0;
	pAdf_struct->slot_info[ slot_index ].last_rx_time.tv_usec = 0;
#ifdef __STATISTICS__
	pAdf_struct->slot_info[ slot_index ].rx_stat.total = 0;
	pAdf_struct->slot_info[ slot_index ].rx_stat.sucess = 0;
	pAdf_struct->slot_info[ slot_index ].rx_stat.err = 0;
#endif	//	__STATISTICS__

	clear_bit( FLAG_SLOT_0 + slot_index, &pAdf_struct->slot_flags );

	if( pAdf_struct->last_pairing_slot_index == slot_index )
	{
		pAdf_struct->last_pairing_slot_index = -1;
	}
	list_remove( &pAdf_struct->pair_list, LIST_KEY_PAIR( slot_index ) );

	mutex_unlock( & pAdf_struct->slot_mutex[ slot_index ] );

	INFO_ADF( "CLEAR_SLOT: %d pair_list: %d\n", slot_index, list_count( &pAdf_struct->pair_list ) );

	return;
}

static void adf_set_slot_info( void *pData, const int slot_index, const ADF_SLOT_INFO *pSlotInfo )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;

	ASSERT( pData != NULL );
	ASSERT( pSlotInfo != NULL );
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	mutex_lock( & pAdf_struct->slot_mutex[ slot_index ] );

	pAdf_struct->slot_info[ slot_index ] = *pSlotInfo;

	mutex_unlock( & pAdf_struct->slot_mutex[ slot_index ] );

	return;
}

static void adf_get_slot_info( void *pData, const int slot_index, ADF_SLOT_INFO *pSlotInfo )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;

	ASSERT( pData != NULL );
	ASSERT( pSlotInfo != NULL );
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	mutex_lock( & pAdf_struct->slot_mutex[ slot_index ] );

	*pSlotInfo = pAdf_struct->slot_info[ slot_index ];

	mutex_unlock( & pAdf_struct->slot_mutex[ slot_index ] );

	return;
}

static void adf_set_slot_info_field( void *pData, const int slot_index, const SLOT_FIELD field, const void *pValue )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )pData;

	ASSERT( pData != NULL );
	ASSERT( pValue != NULL );
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );
	ASSERT( ( field >= SLOT_FIELD_ID ) && ( field < SLOT_FIELD_END ) );

	mutex_lock( & pAdf_struct->slot_mutex[ slot_index ] );

	switch( field )
	{
		case SLOT_FIELD_ID :
			pAdf_struct->slot_info[ slot_index ].id = *( uint32_t * )pValue;
			break;
		case SLOT_FIELD_FREQ :
			pAdf_struct->slot_info[ slot_index ].freq = *( FREQ * )pValue;
			break;
		case SLOT_FIELD_PERIOD :
			pAdf_struct->slot_info[ slot_index ].period = *( PERIOD * )pValue;
			break;
		case SLOT_FIELD_PERIOD_REMAIN :
			pAdf_struct->slot_info[ slot_index ].period_remain = *( int * )pValue;
			break;
		case SLOT_FIELD_MODE :
			pAdf_struct->slot_info[ slot_index ].mode = *( SLOT_MODE * )pValue;
			break;
		case SLOT_FIELD_LAST_RX_TIME :
			pAdf_struct->slot_info[ slot_index ].last_rx_time = *( struct timeval * )pValue;
			break;
		default:
			break;
	}

	mutex_unlock( & pAdf_struct->slot_mutex[ slot_index ] );

	return;
}

static void adf_get_slot_info_field( void *pData, const int slot_index, const SLOT_FIELD field, void *pValue )
{
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )pData;

	ASSERT( pData != NULL );
	ASSERT( pValue != NULL );
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );
	ASSERT( ( field >= SLOT_FIELD_ID ) && ( field < SLOT_FIELD_END ) );

	mutex_lock( & pAdf_struct->slot_mutex[ slot_index ] );

	switch( field )
	{
		case SLOT_FIELD_ID :
			*( uint32_t * )pValue = pAdf_struct->slot_info[ slot_index ].id;
			break;
		case SLOT_FIELD_FREQ :
			*( FREQ * )pValue = pAdf_struct->slot_info[ slot_index ].freq;
			break;
		case SLOT_FIELD_PERIOD :
			*( PERIOD * )pValue = pAdf_struct->slot_info[ slot_index ].period;
			break;
		case SLOT_FIELD_PERIOD_REMAIN :
			*( int * )pValue = pAdf_struct->slot_info[ slot_index ].period_remain;
			break;
		case SLOT_FIELD_MODE :
			*( SLOT_MODE * )pValue = pAdf_struct->slot_info[ slot_index ].mode;
			break;
		case SLOT_FIELD_LAST_RX_TIME :
			*( struct timeval * )pValue = pAdf_struct->slot_info[ slot_index ].last_rx_time;
			break;
		default:
			break;
	}

	mutex_unlock( & pAdf_struct->slot_mutex[ slot_index ] );

	return;
}

uint32_t adf_get_slot_info_id( const int slot_index )
{
	uint32_t id;

	adf_get_slot_info_field( &s_adf_struct, slot_index, SLOT_FIELD_ID, ( void *)&id );
	return id;
}

FREQ adf_get_slot_info_freq( const int slot_index )
{
	FREQ freq;

	adf_get_slot_info_field( &s_adf_struct, slot_index, SLOT_FIELD_FREQ, ( void *)&freq );
	return freq;
}

PERIOD adf_get_slot_info_period( const int slot_index )
{
	PERIOD period;

	adf_get_slot_info_field( &s_adf_struct, slot_index, SLOT_FIELD_PERIOD, ( void *)&period );
	return period;
}

int adf_get_slot_info_period_remain( const int slot_index )
{
	int period_remain;

	adf_get_slot_info_field( &s_adf_struct, slot_index, SLOT_FIELD_PERIOD_REMAIN, ( void *)&period_remain );
	return period_remain;
}

int adf_get_slot_info_mode( const int slot_index )
{
	SLOT_MODE mode;

	adf_get_slot_info_field( &s_adf_struct, slot_index, SLOT_FIELD_MODE, ( void *)&mode );
	return mode;
}

struct timeval adf_get_slot_info_last_rx_time( const int slot_index )
{
	struct timeval tv;

	adf_get_slot_info_field( &s_adf_struct, slot_index, SLOT_FIELD_LAST_RX_TIME, ( void *)&tv);
	return tv;
}

static const char* adf_slot_mode_str( const SLOT_MODE mode )
{
	switch( mode )
	{
		case SLOT_MODE_NOT_ASSIGN :	return "N/A";
		case SLOT_MODE_IDLE :		return "IDLE";
		case SLOT_MODE_PAIRING :	return "PAIRING";
		case SLOT_MODE_LISTEN :		return "LISTEN";
		case SLOT_MODE_SLEEP :		return "SLEEP";
		case SLOT_MODE_CHANGING:	return "CHANGING";
		case SLOT_MODE_CHANGING_PERIOD :	return "CH_PERIOD";
		case SLOT_MODE_ME :		return "ME";
		default :					return "Not Defined";
	}
	return "Not Defined";
}

void adf_slot_set_mode( void *pData, const int slot_index, const SLOT_MODE mode )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;

	ASSERT( pData != NULL );
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	adf_set_slot_info_field( pAdf_struct, slot_index, SLOT_FIELD_MODE, &mode );

	//INFO_ADF( "slot_index %d mode: %s\n", slot_index, adf_slot_mode_str( mode ) );
	return;
}

static int adf_current_slot( void )
{
	ADF_STRUCT		* pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );

	return pAdf_struct->slot_seq;
}

SLOT_MODE adf_slot_get_mode( const int slot_index )
{
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	return adf_get_slot_info_mode( slot_index );
}

#if 0
static bool adf_slot_is_pairing( void *pData, int slot_index )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;

	ASSERT( pData != NULL );
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	return ( pAdf_struct->slot_info[ slot_index ].mode == SLOT_MODE_PAIRING ) ? true : false;
}
#endif

static bool adf_slot_is_empty( const int slot_index )
{
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	return ( adf_get_slot_info_mode( slot_index ) == SLOT_MODE_NOT_ASSIGN ) ? true : false;
}

static bool adf_slot_is_listening( const int slot_index )
{
	bool isListening = false;
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

	if( ( adf_get_slot_info_mode( slot_index ) & SLOT_MODE_LISTEN ) != 0x0 )
	{
		isListening = true;
	}
	return isListening;
}

static const uint32_t list_assign_key( const uint8_t command )
{
	switch( command )
	{
		case	PF_CMD_PAIR_RESPONSE	:
		case	PF_CMD_PAIR_ACK			: return LIST_KEY_PAIRING;

		case	PF_CMD_NICK_ACK			: return LIST_KEY_E_COLLAR;
		case	PF_CMD_GPS_INFO			: return LIST_KEY_GPS_INFO;

		case	PF_CMD_SHARE_DOG_INFO	:
		case	PF_CMD_SHARE_HUNTER_INFO:
		case	PF_CMD_SHARE_ACK		: return LIST_KEY_SHARE;
		default:
			return LIST_KEY_ADF_PACKET;
	}
	return LIST_KEY_ADF_PACKET;
}

static ADF_MODE adf_mode( void )
{
	ADF_STRUCT		* pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );
	return pAdf_struct->mode;
}

static bool adf_sleep( void )
{
	ADF_STRUCT		* pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );

#ifndef	__PARON__
	gpio_set_value( RX_EN, 0 );
	gpio_set_value( TX_EN, 0 );
#endif	//	__PARON__

	gpio_set_value( CE_GPIO_NUM, 0 );
	pAdf_struct->freq = FREQ_UNKNOWN;
	pAdf_struct->mode = ADF_MODE_SLEEP;

	return true;
}

static bool adf_tx_mode( RF_COMMAND * pCmd )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );
	ADF_PACKET	* pPacket = NULL;

	pPacket = pAdf_struct->pHandled_packet;

	ASSERT( pCmd != NULL );
	ASSERT( pPacket != NULL );

	memcpy( pPacket->packetData, pCmd->packetData, pCmd->length );

	if( adf_mode() == ADF_MODE_SLEEP )
	{
		gpio_set_value( CE_GPIO_NUM, 1 );
		udelay( 10 );
		//usleep_range( 10, 10 );
		adf_write_reg( & pAdf_struct->regs[ 1 ] );	//	turns on VCO
		//udelay( 700 );
		udelay( 20 );
		//usleep_range( 20, 20 );
		adf_write_reg( & pAdf_struct->regs[ 3 ] );	//	turns on Clocks
		udelay( 5 );
		//usleep_range( 5, 5 );
		adf_write_reg( & pAdf_struct->regs[ 0 ] );
		//udelay( 40 );
		udelay( 20 );
		//usleep_range( 20, 20 );
		adf_write_reg( & pAdf_struct->regs[ 2 ] ); //init Tx
		udelay( 5 );
		//usleep_range( 5, 5 );

		adf_write_reg( & pAdf_struct->regs[ 15 ] );
		udelay( 5 );
		//usleep_range( 5, 5 );
	}

#ifndef	__PARON__
	gpio_set_value( RX_EN, 0 );
	gpio_set_value( TX_EN, 1 );
#endif	//	__PARON__

	switch( pCmd->freq )
	{
		case	FREQ_151_82	:
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_FRAC	= 28106;
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_INT		= 30;
			break;
		case	FREQ_151_88	:
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_FRAC	= 28505;
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_INT		= 30;
			break;
		case	FREQ_151_94:
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_FRAC	= 28905;
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_INT		= 30;
			break;
		case	FREQ_154_57:
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_FRAC	= 13653;
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_INT		= 31;
			break;
		case	FREQ_154_60:
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_FRAC	= 13853;
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_INT		= 31;
			break;
		default:
			break;
	}
	pAdf_struct->regs[ 0 ].REG_0.TX_RX = 0;	//	0: TX

	adf_write_reg( & pAdf_struct->regs[ 2 ] ); //init Tx
	udelay( 5 );
	//usleep_range( 5, 5 );

	adf_write_reg( & pAdf_struct->regs[ 0 ] );

	pAdf_struct->freq = pCmd->freq;
	pAdf_struct->mode = ADF_MODE_TX;

	return true;
}

static bool adf_rx_mode( RF_COMMAND * pCmd )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );
	ADF_PACKET	* pPacket = NULL;

	ASSERT( pCmd != NULL );
	ASSERT( pAdf_struct != NULL );

	pPacket = pAdf_struct->pHandled_packet;

	ASSERT( pPacket != NULL );

#if 0
	pPacket->length = pAdf_struct->init.max_packet_length * 8;
#endif
	//printk("[RX_MODE] length= %d\n", pCmd->length);
#ifdef __MEM_LEAK__
	pCmd->length = MAX_PACKET_LEN;
#endif //	__MEM_LEAK__
#ifdef __DEBUG_QUEUE_CNT__
	pAdf_struct->alloc_cnt++;
#endif	//	__DEBUG_QUEUE_CNT__

	if( adf_mode() == ADF_MODE_SLEEP )
	{
		gpio_set_value( CE_GPIO_NUM, 1 );
		udelay( 10 );
		//usleep_range( 10, 10 );

		adf_write_reg( & pAdf_struct->regs[ 1 ] );	//	turns on VCO
		//udelay( 700 );
		udelay( 20 );
		//usleep_range( 20, 20 );
		adf_write_reg( & pAdf_struct->regs[ 3 ] );	//	turns on Clocks
		udelay( 10 );
		//usleep_range( 10, 10 );
		//adf_write_reg( & pAdf_struct->regs[ 6 ] );	//	optional
		adf_write_reg( & pAdf_struct->regs[ 5 ] );
		//udelay( 200 );
		udelay( 150 );
		//usleep_range( 150, 150 );
		adf_write_reg( & pAdf_struct->regs[ 11 ] );
		udelay( 10 );
		//usleep_range( 10, 10 );
		pAdf_struct->regs[ 12 ].REG_12.DATA_PACKET_LENGTH	= pAdf_struct->init.max_packet_length;
		adf_write_reg( & pAdf_struct->regs[ 12 ] );
		udelay( 10 );
		//usleep_range( 10, 10 );
		adf_write_reg( & pAdf_struct->regs[ 0 ] );
		//udelay( 40 );
		udelay( 20 );
		//usleep_range( 20, 20 );
		adf_write_reg( & pAdf_struct->regs[ 4  ] );
		//adf_write_reg( & pAdf_struct->regs[ 10 ] );	//	optional
		udelay( 5 );
		//usleep_range( 5, 5 );

		adf_write_reg( & pAdf_struct->regs[ 15 ] );	//	TxRxCLK on CLKOUT
		udelay( 5 );
		//usleep_range( 5, 5 );
	}

#ifndef	__PARON__
	gpio_set_value( TX_EN, 0 );
	gpio_set_value( RX_EN, 1 );
#endif	//	__PARON__

	switch( pCmd->freq )
	{
		case	FREQ_151_82	:
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_FRAC	= 27440;
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_INT		= 30;
			break;
		case	FREQ_151_88	:
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_FRAC	= 27839;
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_INT		= 30;
			break;
		case	FREQ_151_94:
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_FRAC	= 28239;
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_INT		= 30;
			break;
		case	FREQ_154_57:
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_FRAC	= 12987;
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_INT		= 31;
			break;
		case	FREQ_154_60:
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_FRAC	= 13187;
			pAdf_struct->regs[ 0 ].REG_0.DIVIDE_INT		= 31;
			break;
		default:
			break;
	}
	pAdf_struct->regs[ 0 ].REG_0.TX_RX = 1;	//	1: RX

#ifdef __INTERNAL_AFC__
	adf_write_reg( & pAdf_struct->regs[ 10 ] );	//	AFC
	udelay( 5 );
	adf_write_reg( & pAdf_struct->regs[ 14 ] );	//	PEAK / LEAK
	udelay( 5 );
#endif	//	__INTERNAL_AFC__

	adf_write_reg( & pAdf_struct->regs[ 11 ] );
	udelay( 10 );
	//usleep_range( 10, 10 );
	pAdf_struct->regs[ 12 ].REG_12.DATA_PACKET_LENGTH	= pCmd->length;
	adf_write_reg( & pAdf_struct->regs[ 12 ] );
	udelay( 10 );
	//usleep_range( 10, 10 );

	adf_write_reg( & pAdf_struct->regs[ 4 ] );		//	Demod. setup
	udelay( 5 );
	//usleep_range( 5, 5 );

	adf_write_reg( & pAdf_struct->regs[ 0 ] );

    //ADF7021_MD_TXRXDATA_TRIS(PIN_INPUT);

	pAdf_struct->freq = pCmd->freq;
	pAdf_struct->mode = ADF_MODE_RX;

	return true;
}

static bool adf_mode_change( RF_COMMAND *pCmd )
{
	bool ret = false;
	ADF_STRUCT		* pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );
	int				duration = TX_TIMEOUT;
	int				retval;

	ASSERT( pCmd != NULL );

	if( pCmd->delay_ms > 0 )
	{
		//INFO_ADF( "mode: %d freq: %d length: %d, response_length: %d, delay: %d\n",
		//		pCmd->mode, pCmd->freq, pCmd->length, pCmd->response_length, pCmd->delay_ms );
		msleep( pCmd->delay_ms );
	}

	/*	TODO:	Check TX/RX done status.
	 *	In progress: wait until completed..	*/
#ifdef __DEBUG_QUEUE_CNT__
	if(	test_bit( FLAG_RF_DONE, & pAdf_struct->flags ) == 0 )
	{
		pAdf_struct->wait_cnt++;
		//INFO_QUEUE( "pid: %d wait--------------------- mode: %d => %d\n", current->pid, pAdf_struct->mode, pCmd->mode );
		//INFO_QUEUE( "pid: %d WAIT: alloc_cnt: %ld dealloc_cnt: %ld\n", current->pid, pAdf_struct->alloc_cnt, pAdf_struct->dealloc_cnt );
		//INFO_QUEUE( "pid: %d CNT: tmp_cnt: %ld wait_cnt: %ld\n", current->pid, pAdf_struct->tmp_cnt, pAdf_struct->wait_cnt );
	}
#endif	//	__DEBUG_QUEUE_CNT__

	do
	{
		if(	test_bit( FLAG_RF_DONE, & pAdf_struct->flags ) == 0 )
		{
			retval = wait_event_interruptible_timeout( pAdf_struct->rf_done_waitq, \
					test_bit( FLAG_RF_DONE, & pAdf_struct->flags ), msecs_to_jiffies( 40 ) );

			if( retval < 0 )	//	error
			{
				INFO_ADF( "pid: %d ------retval: %d\n", current->pid, retval );
			}
			else if( retval == 0 )
			{
				INFO_ADF( "pid: %d WQ_TIMEOUT. mode: %d => %d busy: 0x%08x flags: 0x%lx length: %d tmp: %ld\n", current->pid, \
						pAdf_struct->mode, pCmd->mode, work_busy( & pAdf_struct->rf_done_work ), pAdf_struct->flags, \
						pAdf_struct->pHandled_packet->length, pAdf_struct->tmp_cnt );
				INFO_ADF( "pid: %d TO: tmp_cnt: %ld wait_cnt: %d\n", current->pid, pAdf_struct->tmp_cnt, pAdf_struct->wait_cnt );
				if( pCmd->mode == ADF_MODE_SLEEP )
				{
					break;
				}
				//break;
			}
		}
		else
		{
			break;
		}
	} while( 1 );

	pAdf_struct->tmp_cnt++;

	if( pCmd->mode != ADF_MODE_SLEEP )
	{
		mutex_lock( & pAdf_struct->alloc_mutex );

		adf_packet_alloc( pAdf_struct );

		if( pCmd->length == 0 )
		{
			WARN_ADF( "pCmd->length: %d\n", pCmd->length );
			pCmd->length = pAdf_struct->init.max_packet_length;
		}
		pAdf_struct->pHandled_packet->length = pCmd->length << 3;	//	packet length in bits.
		pAdf_struct->pHandled_packet->response_length = pCmd->response_length << 3;	//response  acket length in bits.

		mutex_unlock( & pAdf_struct->alloc_mutex );
	}

#if 1
	switch( pCmd->mode )
	{
		case	ADF_MODE_SLEEP		:
			if( test_and_set_bit( FLAG_RF_DONE, & pAdf_struct->flags ) == 0 )
			{
				INFO_ADF("SLEEP--\n");
				INFO_ADF( "pid: %d SLEEP. mode: %d busy: 0x%08x flags: %lx tmp: %ld\n", current->pid, \
						pAdf_struct->mode, work_busy( & pAdf_struct->rf_done_work ), pAdf_struct->flags, pAdf_struct->tmp_cnt );
				INFO_ADF( "pid: %d SL: tmp_cnt: %ld wait_cnt: %d\n", current->pid, pAdf_struct->tmp_cnt, pAdf_struct->wait_cnt );
				wake_up_interruptible_all( & pAdf_struct->rf_done_waitq );
				//wake_up_interruptible( & pAdf_struct->rf_done_waitq );
			}
			ret = adf_sleep();
			adf_disable_irq( pAdf_struct );
			break;
		case	ADF_MODE_TX:
			if( test_and_clear_bit( FLAG_RF_DONE, & pAdf_struct->flags ) == 0 )
			{
				INFO_ADF("TX-----\n");
				INFO_ADF( "pid: %d TX. mode: %d busy: 0x%08x flags: %lx tmp: %ld\n", current->pid, \
						pAdf_struct->mode, work_busy( & pAdf_struct->rf_done_work ), pAdf_struct->flags, pAdf_struct->tmp_cnt );
				INFO_ADF( "pid: %d TX: tmp_cnt: %ld wait_cnt: %d\n", current->pid, pAdf_struct->tmp_cnt, pAdf_struct->wait_cnt );
			}
			//INFO_ADF( "TEST: length: %d response_length: %d\n",
			//		pAdf_struct->pHandled_packet->length, pAdf_struct->pHandled_packet->response_length );
			ret = adf_tx_mode( pCmd );
			adf_enable_irq( pAdf_struct );
			duration = TX_TIMEOUT;
			break;
		case	ADF_MODE_RX:
			if( test_and_clear_bit( FLAG_RF_DONE, & pAdf_struct->flags ) == 0 )
			{
				INFO_ADF("RX-----\n");
				INFO_ADF( "pid: %d RX. mode: %d busy: 0x%08x flags: %lx tmp: %ld\n", current->pid, \
						pAdf_struct->mode, work_busy( & pAdf_struct->rf_done_work ), pAdf_struct->flags, pAdf_struct->tmp_cnt );
				INFO_ADF( "pid: %d RX: tmp_cnt: %ld wait_cnt: %d\n", current->pid, pAdf_struct->tmp_cnt, pAdf_struct->wait_cnt );
			}
			ret = adf_rx_mode( pCmd );
			adf_enable_irq( pAdf_struct );
			duration = RX_DURATION;
			/*	NOTE: RX duration for last slot	*/
			if( adf_current_slot() == FLAG_SLOT_21 )
			{
				duration = RX_DURATION - 10;
			}
			break;
		default:
			break;
	}
#else
	switch( pCmd->mode )
	{
		case	ADF_MODE_SLEEP		:
			set_bit( FLAG_RF_DONE, & pAdf_struct->flags );
			ret = adf_sleep();
			adf_disable_irq( pAdf_struct );
			break;
		case	ADF_MODE_TX:
			clear_bit( FLAG_RF_DONE, & pAdf_struct->flags );
			ret = adf_tx_mode( pCmd );
			adf_enable_irq( pAdf_struct );
			duration = TX_TIMEOUT;
			break;
		case	ADF_MODE_RX:
			clear_bit( FLAG_RF_DONE, & pAdf_struct->flags );
			ret = adf_rx_mode( pCmd );
			adf_enable_irq( pAdf_struct );
			duration = RX_DURATION;
			/*	NOTE: RX duration for last slot	*/
			if( adf_current_slot() == FLAG_SLOT_21 )
			{
				duration = RX_DURATION - 10;
			}
			break;
		default:
			break;
	}
#endif

	/*	NOTE: Do NOT start Timer at SLEEP MODE.	*/
	if( adf_mode() != ADF_MODE_SLEEP )
	{
		if( pCmd->timeout > 0 )
		{
			duration = pCmd->timeout;
		}
		if( pCmd->timeout >= 0 )
		{
			timer_start( ( void * )( & pAdf_struct->timeout_timer ), duration );
		}
	}

	//printk( "pid: %d mode: %d freq: %d\n", current->pid, pCmd->mode, pCmd->freq );

	return ret;
}

static void adf_write_reg( ADF_REG	* pReg )
{
	uint32_t	val;
	int				bit = 31;

	ASSERT( pReg != NULL );

#if 0
	bit = 31;
	if( pReg->REG_7.ADDRESS == ADF_REG_READBACK_INDEX )
	{
		//bit = 8;
	}
#endif
	/*	DB_31, DB_30, ... DB_01, DB_00	*/
	for( /* bit = 31 */ ; bit >= 0 ; bit -- )
	{
		val = ( pReg->data >> bit ) & 0x1;

		gpio_set_value( SCLK_GPIO_NUM, 0 );
		gpio_set_value( SDATA_GPIO_NUM, val );
		/*	t4 > 25 ns	*/
		ndelay( 25 );
		gpio_set_value( SCLK_GPIO_NUM, 1 );
		/*	t3 > 25 ns	*/
		ndelay( 25 );
	}
	gpio_set_value( SCLK_GPIO_NUM, 0 );
	/*	t5 > 10 ns	*/
	//ndelay( 10 );
	gpio_set_value( SDATA_GPIO_NUM, 0 );

	gpio_set_value( SLE_GPIO_NUM, 1 );
	/*	t6 > 20 ns	*/
	ndelay( 20 );

	if( pReg->REG_7.ADDRESS != ADF_REG_READBACK_INDEX )
	{
		gpio_set_value( SLE_GPIO_NUM, 0 );
	}

	DEBUG_ADF( "idx: %d data: 0x%08x\n", pReg->REG_0.ADDRESS, pReg->data );

	return;
}

static const ADF_READBACK adf_readback( READBACK_MODE mode, READBACK_ADC_MODE adc_mode )
{
	uint16_t		val = 0x0;
	int				bit;
	ADF_REG			adf_reg = { 0x0, };

	adf_reg.REG_7.ADDRESS = ADF_REG_READBACK_INDEX;
	adf_reg.REG_7.READBACK_MODE = mode;
	adf_reg.REG_7.ADC_MODE = adc_mode;
	adf_reg.REG_7.READBACK_ENABLE = 1;

	adf_write_reg( & adf_reg );

	/*	1sr clock cycle: latch operation.	*/
	gpio_set_value( SCLK_GPIO_NUM, 1 );
	/*	t3 > 25 ns	*/
	ndelay( 25 );
	gpio_set_value( SCLK_GPIO_NUM, 0 );
	/*	t4 > 25 ns	*/
	ndelay( 25 );

	/*	DB_15, DB_14, ... DB_01, DB_00	*/
	for( bit = 15 ; bit >= 0 ; bit -- )
	{
		gpio_set_value( SCLK_GPIO_NUM, 1 );
		/*	t3 > 25 ns	*/
		ndelay( 25 );
		if( gpio_get_value( SREAD_GPIO_NUM ) == 0x1 )
		{
			val |= ( 0x1 << bit );
		}
		gpio_set_value( SCLK_GPIO_NUM, 0 );
		/*	t4 > 25 ns	*/
		ndelay( 25 );
	}
	gpio_set_value( SCLK_GPIO_NUM, 1 );
	/*	t10 > 10 ns	*/
	ndelay( 10 );
	gpio_set_value( SLE_GPIO_NUM, 0 );
	ndelay( 10 );
	gpio_set_value( SCLK_GPIO_NUM, 0 );

	if( (  val != 0x0 ) && ( val != 0xFFFF ) )
	{
		DEBUG_ADF( "READBACK: mode: 0x%08x adc_mode: 0x%08x data: 0x%08x read: 0x%08x\n", \
				mode, adc_mode, adf_reg.data, val );
	}

	return ( ADF_READBACK )val;
}

static void adf_reg_init( ADF_REG regs[] )
{
	ADF_STRUCT		* pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );
#ifdef	__DEBUG_ADF__
	int idx;
#endif	//	__DEBUG_ADF__

#if 0
    regs[0].data = (0UL << 29) | (0UL << 28) | (0UL << 27) | (30UL << 19) | (28106UL << 4); //151.82
    regs[1].data = (1UL << 25) | (0UL << 23) | (2UL << 19) | (1UL << 18) | (1UL << 17) | (0UL << 15) | (2UL << 13) | (1UL << 12) | (0UL << 11) | (0UL << 7) | (2UL << 4) | 1UL; // 2015.06.04 BJH Currrent

    regs[2].data = (0UL << 30) | (0UL << 28) | (46UL << 19) | (25UL << 13) | (2UL << 11) | (0UL << 8) | (1UL << 7) | (1UL << 4) | 2UL; // 3.5k deviation
    regs[3].data = (10UL << 26) | (197UL << 18) | (20UL << 10) | (3UL << 6) | (2UL << 4) | 3UL; //10kbps 3.5k deviation
    regs[4].data = (2UL << 30) | (8UL << 20) | (476UL << 10) | (2UL << 8) | (1UL << 7) | (1UL << 4) | 4UL; //10Kbps 3.5k deviation
    regs[5].data = (0UL << 31) | (0UL << 30) | (0UL << 25) | (0UL << 24) | (0UL << 20) | (394UL << 5) | (1UL << 4) | 5UL;

    regs[6].data = (0UL << 30) | (0UL << 28) | (40UL << 21) | (75UL << 13) | (150UL << 5) | (1UL << 4) | 6UL;
    regs[7].data = (7UL << 6) | (0UL << 4) | 7UL;
    regs[8].data = 8UL;
    regs[9].data = (0UL << 28) | (0UL << 26) | (0UL << 25) | (0UL << 24) | (0UL << 22) | (0UL << 20) | (0UL << 18) | (70UL << 11) | (30UL << 4) | 9UL;
    regs[10].data = (50UL << 24) | (4UL << 21) | (11UL << 17) | (426UL << 5) | (0UL << 4) | 10UL;

    regs[11].data = ( ADF_SYNC_WORD << 8) | (0UL < 6) | (3UL << 4) | 11UL;
    regs[12].data = (23UL << 8) | (2UL << 6) | (2UL << 4) | 12UL;
    regs[13].data = 13UL;
    regs[14].data = 14UL;
    regs[15].data = 15UL;

	for( idx = 0 ; idx < MAX_ADF_REG_INDEX ; idx ++ )
	{
		printk( "idx: %d: regs: 0x%08x\n", idx, regs[ idx ].data );
		regs[ idx ].data = 0x0;
	}
#endif

	regs[ 0 ].REG_0.ADDRESS					= 0;
	regs[ 0 ].REG_0.DIVIDE_FRAC				= 28106;
	regs[ 0 ].REG_0.DIVIDE_INT				= 30;
	regs[ 0 ].REG_0.TX_RX					= 0;	//	0: TX
	if( pAdf_struct->init.rf_mode == ADF_RF_GPIO )
	{
		regs[ 0 ].REG_0.UART_MODE			= 0;
	}
	else
	{
		regs[ 0 ].REG_0.UART_MODE			= 1;	//	1: EBABLE
	}
	//regs[ 0 ].REG_0.MUX_OUT				= 4;	//	Tx_Rx
	regs[ 0 ].REG_0.MUX_OUT					= 5;	//	LOGIC_ZERO

	regs[ 1 ].REG_1.ADDRESS					= 1;
	regs[ 1 ].REG_1.R_COUNTER				= 2;
	regs[ 1 ].REG_1.CLKOUT_DIVIDE			= 0;
	regs[ 1 ].REG_1.XTAL_DOUBLER			= 0;
	regs[ 1 ].REG_1.XOSC_ENABLE				= 1;
	regs[ 1 ].REG_1.XTAL_BIAS				= 2;
	regs[ 1 ].REG_1.CP_CURRENT				= 0;
	regs[ 1 ].REG_1.VCO_ENABLE				= 1;
	regs[ 1 ].REG_1.RF_DIVIDE_BY_2			= 1;
	regs[ 1 ].REG_1.VCD_BIAS				= 2;
	regs[ 1 ].REG_1.VCO_ADJUST				= 0;
	regs[ 1 ].REG_1.VCO						= 1;

	regs[ 2 ].REG_2.ADDRESS					= 2;
	regs[ 2 ].REG_2.MOD_SCHEME				= 1;
	regs[ 2 ].REG_2.PA_ENABLE				= 1;
	regs[ 2 ].REG_2.PA_RAMP					= 0;
	regs[ 2 ].REG_2.PA_BIAS					= 2;
	/*	INFO: USA / EU
	 *	Init : 25 / 30
	 *	Low  : 25 / 18 20??
	 *	High : 35 / 18 20??
	 *
	 *	Pairing: Low setting
	 *	*/
	regs[ 2 ].REG_2.PA_LEVEL				= s_tx_strength;
	//regs[ 2 ].REG_2.PA_LEVEL				= 35;
	regs[ 2 ].REG_2.TX_DEVIATION			= 46;
	regs[ 2 ].REG_2.TX_DATA_INVERT			= 0;
	regs[ 2 ].REG_2.R_COSINE_ALPHA			= 0;

	regs[ 3 ].REG_3.ADDRESS					= 3;
	regs[ 3 ].REG_3.BBOS_CLK_DIVIDE			= 2;
	regs[ 3 ].REG_3.DEM_CLK_DIVIDE			= 3;
	regs[ 3 ].REG_3.CDR_CLK_DIVIDE			= 20;
	regs[ 3 ].REG_3.SEQ_CLK_DIVIDE			= 197;
	regs[ 3 ].REG_3.AGC_CLK_DIVIDE			= 10;

	regs[ 4 ].REG_4.ADDRESS					= 4;
	regs[ 4 ].REG_4.DEMOD_SCHEME			= 1;
	regs[ 4 ].REG_4.DOT_PRODUCT				= 1;
	regs[ 4 ].REG_4.RX_INVERT				= 2;
	regs[ 4 ].REG_4.DISCRIM_BW				= 476;
	regs[ 4 ].REG_4.POST_DEMOD_BW			= 8;
	regs[ 4 ].REG_4.IF_BW					= 2;

	regs[ 5 ].REG_5.ADDRESS					= 5;
	regs[ 5 ].REG_5.IF_CAL					= 1;
	regs[ 5 ].REG_5.IF_FILTER_DIVIDE		= 394;
	regs[ 5 ].REG_5.IF_FILTER_ADJUST		= 0;
	regs[ 5 ].REG_5.IR_PHASE_ADJUST			= 0;
	regs[ 5 ].REG_5.IR_PHASE_ADJUST_IQ		= 0;
	regs[ 5 ].REG_5.IR_GAIN_ADJUST			= 0;
	regs[ 5 ].REG_5.IR_GAIN_ADJUST_IQ		= 0;
	regs[ 5 ].REG_5.IR_GAIN_ADJUST_UD		= 0;

	regs[ 6 ].REG_6.ADDRESS					= 6;
	regs[ 6 ].REG_6.IF_FINE_CAL				= 1;
	regs[ 6 ].REG_6.IF_CAL_LOWER_DIVIDE		= 150;
	regs[ 6 ].REG_6.IF_CAL_UPPER_DIVIDE		= 75;
	regs[ 6 ].REG_6.IF_CAL_DWELL_TIME		= 40;
	regs[ 6 ].REG_6.IF_CAL_SRC_DRV_LEVEL	= 0;
	regs[ 6 ].REG_6.IF_CAL_SRC_DIVIDE_2		= 0;

	regs[ 7 ].REG_7.ADDRESS					= 7;
	regs[ 7 ].REG_7.ADC_MODE				= 0;
	regs[ 7 ].REG_7.READBACK_MODE			= 3;
	regs[ 7 ].REG_7.READBACK_ENABLE			= 1;

	regs[ 8 ].REG_8.ADDRESS					= 8;
	regs[ 8 ].REG_8.SYNTH_ENABLE			= 0;
	regs[ 8 ].REG_8.RESERVED_0				= 0;
	regs[ 8 ].REG_8.LNA_MIXER_ENABLE		= 0;
	regs[ 8 ].REG_8.FILTER_ENABLE			= 0;
	regs[ 8 ].REG_8.ADC_ENABLE				= 0;
	regs[ 8 ].REG_8.DEMOD_ENABLE			= 0;
	regs[ 8 ].REG_8.LOG_AMP_ENABLE			= 0;
	regs[ 8 ].REG_8.TX_RX_SWITCH			= 0;
	regs[ 8 ].REG_8.PA_ENABLE				= 0;
	regs[ 8 ].REG_8.RX_RESET				= 0;
	regs[ 8 ].REG_8.COUNTER_RESET			= 0;

	/*	AGC Register	*/
	regs[ 9 ].REG_9.ADDRESS					= 9;
	regs[ 9 ].REG_9.AGC_LOW_THRESHOLD		= 30;
	regs[ 9 ].REG_9.AGC_HIGH_THRESHOLD		= 70;
	regs[ 9 ].REG_9.AGC_MODE				= 0;	//	0: Auto AGC 1: Manual 2: Freeze 3: Reserved
	regs[ 9 ].REG_9.LNA_GAIN				= 0;	//	need for RSSI
	regs[ 9 ].REG_9.FILTER_GAIN				= 0;	//	need for RSSI
	regs[ 9 ].REG_9.FILTER_CURRENT			= 0;	//	0: Low 1: High
	regs[ 9 ].REG_9.LNA_MODE				= 0;	//	0: default 1: Reduced Gain
	regs[ 9 ].REG_9.LNA_CURRENT				= 0;	//	0: default
	regs[ 9 ].REG_9.MIXER_LINEARITY			= 0;	//	0: default 1:High

#ifdef __INTERNAL_AFC__
	regs[ 10 ].REG_10.ADDRESS				= 10;
	regs[ 10 ].REG_10.AFC_ENABLE			= 1;
	regs[ 10 ].REG_10.AFC_SCALING_FACTOR	= 426;
	regs[ 10 ].REG_10.KI					= 8;
	regs[ 10 ].REG_10.KP					= 2;
	regs[ 10 ].REG_10.MAX_AFC_RANGE			= 50;
#else	//	__INTERNAL_AFC__
	regs[ 10 ].REG_10.ADDRESS				= 10;
	regs[ 10 ].REG_10.AFC_ENABLE			= 0;
	regs[ 10 ].REG_10.AFC_SCALING_FACTOR	= 426;
	regs[ 10 ].REG_10.KI					= 11;
	regs[ 10 ].REG_10.KP					= 4;
	regs[ 10 ].REG_10.MAX_AFC_RANGE			= 50;
#endif	//	__INTERNAL_AFC__

	regs[ 11 ].REG_11.ADDRESS				= 11;
	regs[ 11 ].REG_11.SYNC_BYTE_LENGTH		= 3;
	regs[ 11 ].REG_11.MATCHING_TOLERANCE	= 0;
	regs[ 11 ].REG_11.SYNC_BYTE_SEQUENCE	= pAdf_struct->init.syncword;

	regs[ 12 ].REG_12.ADDRESS				= 12;
	regs[ 12 ].REG_12.LOCK_THRESHOLD_MODE	= 2;
	regs[ 12 ].REG_12.SWD_MODE				= 2;
	regs[ 12 ].REG_12.DATA_PACKET_LENGTH	= pAdf_struct->init.max_packet_length;

	regs[ 13 ].REG_13.ADDRESS				= 13;
	regs[ 14 ].REG_14.ADDRESS				= 14;
	regs[ 15 ].REG_15.ADDRESS				= 15;
	if( pAdf_struct->init.rf_mode == ADF_RF_SPI )
	{
    	regs[ 15 ].REG_15.CLK_MUX				= 7;	//	TxRxCLK on CLKOUT
	}

#ifdef	__DEBUG_ADF__
	for( idx = 0 ; idx < MAX_ADF_REG_INDEX ; idx ++ )
	{
		DEBUG_ADF( "UNION: idx: %d: regs: 0x%08x\n", idx, regs[ idx ].data );
	}
#endif	//	__DEBUG_ADF__

	/*	Power VCO			On Reset: Pairing Freq.	*/

	gpio_set_value( CE_GPIO_NUM, 1 );
    udelay( 100 );
	//usleep_range( 100, 100 );

    adf_write_reg( & regs[1] ); //turns on VCO
    udelay( 20 );
	//usleep_range( 20, 20 );
    adf_write_reg( & regs[3] ); //turn on clocks
    udelay( 5 );
	//usleep_range( 5, 5 );
    //adf_write_reg( & regs[0] );
    //udelay( 40 );
    adf_write_reg( & regs[2] ); //init Tx
    udelay( 5 );
	//usleep_range( 5, 5 );

    //regs[15].data = (3UL << 30) | 15UL;
    regs[ 15 ].REG_15.CAL_OVERRIDE = 3;
    adf_write_reg( & regs[15] );
    udelay( 5 );
	//usleep_range( 5, 5 );
    adf_write_reg( & regs[4] );
    udelay( 5 );
	//usleep_range( 5, 5 );
    adf_write_reg( & regs[6] );
    udelay( 100 );
	//usleep_range( 100, 100 );
    adf_write_reg( & regs[5] );
    udelay( 150 );
	//usleep_range( 150, 150 );
    adf_write_reg( & regs[11] );
    udelay( 5 );
	//usleep_range( 5, 5 );
    adf_write_reg( & regs[12] );
    udelay( 5 );
	//usleep_range( 5, 5 );
    adf_write_reg( & regs[0] );
    udelay( 20 );
	//usleep_range( 20, 20 );
    adf_write_reg( & regs[9] );
    udelay( 5 );
	//usleep_range( 5, 5 );

#ifdef __INTERNAL_AFC__
    adf_write_reg( & regs[10] );
    udelay( 5 );
	regs[ 14 ].REG_14.ED_PEAK_RESPONSE = 0;
	regs[ 14 ].REG_14.ED_LEAK_FACTOR = 0;
	regs[ 15 ].REG_15.RX_TEST_MODES = 9;
    adf_write_reg( & regs[14] );
    udelay( 5 );
#endif	//	__INTERNAL_AFC__

    //regs[15].data = (2UL << 24) | (3UL << 30) | 15UL;
    regs[ 15 ].REG_15.CAL_OVERRIDE			= 3;
    regs[ 15 ].REG_15.ANALOG_TEST_MODES		= 2;
#ifdef __RF_TEST__
    //regs[ 15 ].REG_15.TX_TEST_MODES = 4;	//	"1010" pattern
#endif	//	__RF_TEST__
    adf_write_reg( & regs[15] );
    udelay( 5 );
	//usleep_range( 5, 5 );
    adf_write_reg( & regs[10] );
    udelay( 5 );
	//usleep_range( 5, 5 );

	adf_readback( READBACK_SILICON_REV, 0 );

#if 0
	/*	TEMP:	Init mode: tx	*/
	pAdf_struct->freq = FREQ_151_82;
	pAdf_struct->mode = ADF_MODE_TX;
#endif
	//NOTICE_ADF( "syncword: init 0x%08x SYNCWORD 0x%08x htonl 0x%08x\n", \
	//		regs[ 11 ].REG_11.SYNC_BYTE_SEQUENCE, PF_SYNC_WORD, htonl( ( ( PF_SYNC_WORD ) << 8 ) | 0xAA ) );

	return;
}

static int calculate_rssi( void )
{
	/*	RSSI: refer ADF7021 datasheet...
	 *	Input_Power[dBm] = -130 + ( rssi_readback + gaine_mode_correction ) * 0.5
	 *	Range at input: -120 ~ -47 dBm
	 *	*/
	int rssi_dBm, gainModeCorection;
	int gainTable[ 3 ][ 3 ] = { { 0, }, };	// [ FG ][ LNA ]
	ADF_READBACK readback =	adf_readback( READBACK_ADC_OUTPUT, READBACK_ADC_RSSI );

	gainTable[ 0 ][ 0 ] = 86;
	gainTable[ 0 ][ 1 ] = 58;
	gainTable[ 1 ][ 1 ] = 38;
	gainTable[ 2 ][ 1 ] = 24;
	gainTable[ 2 ][ 2 ] = 0;

	/*	From Collar F/W source.
	RSSI = (int) (-130 + (tempRSSI + gainModeCorrection[filterGain][LAN]) / 2);
	RSSI = ((RSSI + 46)*-1 + 1) / 2;
	*/
	gainModeCorection = gainTable[ readback.RSSI.filter_gain ][ readback.RSSI.LNA_gain ];
	rssi_dBm = -130 + ( ( readback.RSSI.value + gainModeCorection ) / 2 );

	DEBUG_ADF( "RSSI: FG: %x LNA: %x value: 0x%02x rssi: %d dBm\n", \
			readback.RSSI.filter_gain, readback.RSSI.LNA_gain, readback.RSSI.value, rssi_dBm );

	ASSERT( rssi_dBm < 0 );

	return rssi_dBm;
}

static long adf_ioctl( struct file * pFile, uint32_t cmd, unsigned long arg )
{
	//uint32_t __user	* pArg = ( uint32_t __user * )arg;
	void __user		* pArg = ( void __user * )arg;
	ADF_STRUCT		* pAdf_struct = ( ADF_STRUCT * )( pFile->private_data );
	//uint8_t	* pBuf = NULL;
	long				ret = 0;

	switch( cmd )
	{
		case ADF_INIT :
			{
				ADF_INIT_INFO	init;
				const IOCTL_CMD_DATA *pCmdData = ( const IOCTL_CMD_DATA * )( pArg );

				/*	NOTE: Init. state is SLEEP	*/
				RF_COMMAND cmd = { ADF_MODE_INIT, FREQ_UNKNOWN, 0x0, 0, 0, { 0x0, }, 0, 0 };
				if( pArg != NULL )
				{
					if( copy_from_user( ( void * )&init, ( void __user * )&pCmdData->adfInit, sizeof( ADF_INIT_INFO ) ) != 0 )
					{
						ret = -ENOMEM;
					}
				}
				else
				{
					init.rf_mode = ADF_RF_SPI;
					init.syncword = PF_SYNC_WORD;
					init.max_packet_length = MAX_PACKET_LEN;
					init.work_mode = ADF_WORK_NORMAL;
				}
				pAdf_struct->init.rf_mode = init.rf_mode;
				pAdf_struct->init.syncword = init.syncword;
				pAdf_struct->init.max_packet_length = init.max_packet_length;
				adf_reg_init( pAdf_struct->regs );

				adf_mode_change( & cmd );

				//	for Test
				//timer_start( ( void * )( & pAdf_struct->reference_timer ), INIT_DURATION );

				break;
			}
		case ADF_CMD_PAIR_START :
			{
				bool pairing = test_bit( FLAG_PAIRING, & pAdf_struct->flags );
				INFO_ADF( "ADF_CMD_PAIR_START: %d => %d scan_ch_slot: 0x%02x\n", pairing, true, \
						pAdf_struct->scan_ch_slot.ch_slot );
				if( test_and_set_bit( FLAG_PAIRING, & pAdf_struct->flags ) == 0 )
				{
					adf_init_start_ref_timer();
				}
				break;
			}
		case ADF_CMD_PAIR_CANCEL :
			{
				bool pairing = test_bit( FLAG_PAIRING, & pAdf_struct->flags );
				INFO_ADF( "ADF_CMD_PAIR_CANCEL: %d => %d scan_ch_slot: 0x%02x\n", pairing, false, \
						pAdf_struct->scan_ch_slot.ch_slot );
				test_and_clear_bit( FLAG_PAIRING, & pAdf_struct->flags );
				//adf_update_slot_map_file();
				//pAdf_struct->last_pairing_slot_index = -1;
				break;
			}
		case ADF_CMD_SCAN_SLOT_FREQ :
			{
				IOCTL_CMD_DATA *pCmdData = ( IOCTL_CMD_DATA * )pArg;
				CH_SLOT ch_slot;
				//bool isEmptySlot = false;

				ASSERT( pArg != NULL );

				ch_slot.slot = pCmdData->changeSlotFreq.slotId;
				ch_slot.ch = pCmdData->changeSlotFreq.freq;
				//isEmptySlot = adf_slot_is_empty( ch_slot.slot - 1 );
				pAdf_struct->scan_ch_slot = ch_slot;
				pAdf_struct->scan_result_ch_slot.ch_slot = INVALID_CH_SLOT;
				//test_and_set_bit( FLAG_SCAN, & pAdf_struct->flags );
				INFO_ADF( "SCAN: s %d f %d\n", ch_slot.slot, ch_slot.ch );
				break;
			}
		case ADF_CMD_SLEEP_MODE :
			/*	NOTE: All dogs sleep
			 *	- Collar: Global time RX and GPS TX, ADF sleep the other time.
			 *	- Hunter: Check sleep state into GPS info
			 *	- All dogs are not sleeping, then TX at global time.	*/
		case ADF_CMD_CHANGE_PERIOD :
		case ADF_CMD_CHANGE_SLOT_FREQ :
			if( test_bit( FLAG_NEED_RETRY, & pAdf_struct->flags ) == 1 )
			{
				ret = -EINVAL;
				break;
			}
		case ADF_CMD_DELETE_DEVICE :
		case ADF_CMD_EC_NICK_ONE :
		case ADF_CMD_RESCUE_MODE :
			{
				IOCTL_CMD_DATA *pCmdData = ( IOCTL_CMD_DATA * )pArg;
				RF_COMMAND *pRfCmd = NULL;
				uint32_t keyMask = 0x0;

				ASSERT( pArg != NULL );

				pRfCmd = pf_alloc_cmd_gen( pAdf_struct, ( IOCTL_CMD_DATA * )pCmdData, pCmdData->rf_cmd );
				if( pRfCmd == NULL )
				{
					ret = -EINVAL;
				}
				else
				{
					PF_TX_COMMAND *pTxCmd = ( PF_TX_COMMAND * )( pRfCmd->packetData );
					switch( pTxCmd->command )
					{
						case PF_CMD_SLEEP_MODE :
							keyMask = LIST_KEY_GLOBAL_CMD | LIST_ID_BROADCAST;
							break;
						case PF_CMD_NICK_ONE :
							keyMask = LIST_KEY_LOCAL_CMD | LIST_KEY_NICK_ACK | pRfCmd->id;
							break;
						case PF_CMD_CHANGE_PERIOD_REQ :
						case PF_CMD_CHANGE_SLOT_REQ :
							keyMask = LIST_KEY_LOCAL_CMD | LIST_KEY_CHANGE | pRfCmd->id;
							test_and_set_bit( FLAG_NEED_RETRY, & pAdf_struct->flags );
							break;
						default :
							keyMask = LIST_KEY_LOCAL_CMD | pRfCmd->id;
							break;
					}

					list_push( &pAdf_struct->cmd_list, pRfCmd, sizeof( RF_COMMAND ), keyMask );
				}
				break;
			}
		case ADF_CMD_CHANGE_END :
			{
				bool changing = test_bit( FLAG_NEED_RETRY, & pAdf_struct->flags );
				INFO_ADF( "ADF_CMD_CHANGE_END: %d => %d cmd_list cnt: %d\n", changing, false, list_count( &pAdf_struct->cmd_list ) );
				test_and_clear_bit( FLAG_NEED_RETRY, & pAdf_struct->flags );
				list_remove( &pAdf_struct->cmd_list, LIST_KEY_CHANGE );
				break;
			}

		case ADF_CMD_EC_START_CONST :
		case ADF_CMD_EC_START_TONE :
			{
				IOCTL_CMD_DATA *pCmdData = ( IOCTL_CMD_DATA * )pArg;
				E_COLLAR_INFO *pECollarInfo = ( E_COLLAR_INFO * )( &pCmdData->eCollar );
				int isStarted = test_bit( FLAG_E_COLLAR, & pAdf_struct->flags );

				ASSERT( pArg != NULL );

				if( isStarted == false )
				{
					INFO_ADF( "ADF_CMD_EC_START_%s: %d => %d flags: 0x%04lx\n", \
							( cmd == ADF_CMD_EC_START_CONST ) ? "CONST" : "TONE", isStarted, true, \
							pAdf_struct->flags & 0xF000 );
					if( set_eCollar_info( pECollarInfo ) == true )
					{
						set_bit( FLAG_E_COLLAR, & pAdf_struct->flags );
						if( cmd == ADF_CMD_EC_START_CONST )
						{
							/*	TODO: send PF_CMD_NICK_REQ every slot.
							 *	Response PF_CMD_NICK_ACK per 4 PF_CMD_NICK_REQ.
							 *	*/
							set_bit( FLAG_CONST, & pAdf_struct->flags );
						}
						else
						{
							/*	TODO: send PF_CMD_BUZZER every slot.
							 *	If level is 0x14, Response PF_CMD_NICK_ACK per 4 PF_CMD_NICK_REQ.
							 *	*/
							set_bit( FLAG_TONE, & pAdf_struct->flags );
						}
					}
					else
					{
						WARN_ADF( "ADF_CMD_EC_START: failed. id: 0x%04x\n", pECollarInfo->id );
					}
				}
				break;
			}
		case ADF_CMD_EC_STOP :
			{
				int isStarted = test_bit( FLAG_E_COLLAR, & pAdf_struct->flags );
				if( isStarted == true )
				{
					INFO_ADF( "ADF_CMD_EC_STOP: %d => %d flags: 0x%04lx\n", isStarted, false, pAdf_struct->flags & 0xf000 );
					clear_bit( FLAG_E_COLLAR, & pAdf_struct->flags );
					clear_bit( FLAG_CONST, & pAdf_struct->flags );
					clear_bit( FLAG_TONE, & pAdf_struct->flags );

					clear_eCollar_info();
				}
				break;
			}
		case ADF_CMD_SHARING_SEND_INFO :
			{
				/*	TODO: Send SHARE_INFO as global command.
				 *	SHARE_HUNTER_INFO: TX( info ) -> RX( info ) -> RX( ack )
				 *	- response => SHARE_HUNTER_INFO( of shared device )
				 *	- response of received SHARE_HUNTER_INFO is SHARE_ACK.
				 *	SHARE_DOG_INFO: TX( info ) -> RX( ack )
				 *	- response => SHARE_ACK
				 *	*/
				bool sharingTx = test_bit( FLAG_SHARING_TX, & pAdf_struct->flags );
				bool sharingRx = test_bit( FLAG_SHARING_RX, & pAdf_struct->flags );
				IOCTL_CMD_DATA *pCmdData = ( IOCTL_CMD_DATA * )pArg;
				INFO_ADF( "ADF_CMD_SHARING_SEND_INFO: *TX %d RX %d\n", sharingTx, sharingRx );
				INFO_ADF( "id: 0x%04x name: %s cmd: 0x%02x\n", pCmdData->devInfo.id, pCmdData->devInfo.name, pCmdData->rf_cmd );
				if( test_and_set_bit( FLAG_SHARING_TX, & pAdf_struct->flags ) == 0 )
				{
					RF_COMMAND *pRfCmd = NULL;
					uint32_t keyMask = LIST_KEY_SHARE;

					ASSERT( pArg != NULL );

					pRfCmd = pf_alloc_cmd_gen( pAdf_struct, ( IOCTL_CMD_DATA * )pCmdData, pCmdData->rf_cmd );
					if( pRfCmd == NULL )
					{
						ERR_ADF( "pf_alloc_cmd_gen() failed..\n" );
						clear_bit( FLAG_SHARING_TX, & pAdf_struct->flags );
						ret = -EINVAL;
					}
					else
					{
						PF_TX_COMMAND *pTxCmd = ( PF_TX_COMMAND * )( pRfCmd->packetData );
						switch( pTxCmd->command )
						{
							case PF_CMD_SHARE_DOG_INFO :
							case PF_CMD_SHARE_HUNTER_INFO :
								keyMask |= LIST_KEY_GLOBAL_CMD | LIST_ID_BROADCAST;
								break;
							default :
								keyMask |= LIST_KEY_GLOBAL_CMD | LIST_ID_BROADCAST;
								break;
						}
						list_push_head( &pAdf_struct->cmd_list, pRfCmd, sizeof( RF_COMMAND ), keyMask );
					}
				}
				break;
			}
		case ADF_CMD_SHARING_WAIT :
			{
				/*	TODO: Global RX for Sharing information( Hunter or Collar info. )
				 *	*/
				bool sharingTx = test_bit( FLAG_SHARING_TX, & pAdf_struct->flags );
				bool sharingRx = test_bit( FLAG_SHARING_RX, & pAdf_struct->flags );
				INFO_ADF( "ADF_CMD_SHARING_WAIT: TX %d *RX %d\n", sharingTx, sharingRx );
				if( test_and_set_bit( FLAG_SHARING_RX, & pAdf_struct->flags ) == 0 )
				{
					adf_init_start_ref_timer();
				}
				break;
			}
		case ADF_CMD_SHARING_CANCEL :
			{
				bool sharingTx = test_bit( FLAG_SHARING_TX, & pAdf_struct->flags );
				bool sharingRx = test_bit( FLAG_SHARING_RX, & pAdf_struct->flags );
				INFO_ADF( "ADF_CMD_SHARING_CANCEL: *TX %d *RX %d\n", sharingTx, sharingRx );
				test_and_clear_bit( FLAG_SHARING_RX, & pAdf_struct->flags );
				test_and_clear_bit( FLAG_SHARING_TX, & pAdf_struct->flags );

				test_and_clear_bit( FLAG_SHARING_INFO, & pAdf_struct->flags );
				test_and_clear_bit( FLAG_SHARING_RX_ACK, & pAdf_struct->flags );
				break;
			}
		case ADF_CMD_SET_SLOT_INFO :
			{
				const IOCTL_CMD_DATA * const pCmdData = ( const IOCTL_CMD_DATA * const )( pArg );
				int slot_index = pCmdData->devInfo.slot - 1;
				ADF_SLOT_INFO slotInfo;
				//ADF_SLOT_INFO slotInfo = adf_get_slot_info( pAdf_struct, slot_index, &slotInfo );

				INFO_ADF( "CMD_SET_SLOT_INFO: slotId: %d idx: %d id: 0x%04x\n", \
						pCmdData->devInfo.slot, slot_index, pCmdData->devInfo.id );

				memset( &slotInfo, 0x0, sizeof( ADF_SLOT_INFO ) );

				slotInfo.id = pCmdData->devInfo.id;
				slotInfo.freq = pCmdData->devInfo.freq;
				slotInfo.period = pCmdData->devInfo.period;
				slotInfo.period_remain = 1;
				slotInfo.shared = pCmdData->devInfo.shared;
				slotInfo.mode = SLOT_MODE_LISTEN;
				slotInfo.last_rx_time.tv_sec = 0;
				slotInfo.last_rx_time.tv_usec = 0;

				adf_set_slot_info( pAdf_struct, slot_index, &slotInfo );

				adf_init_start_ref_timer();
				{
					int *pSlotIndex = ( int * )kzalloc( sizeof( int ), GFP_KERNEL );
					if( pSlotIndex == NULL )
					{
						ERR_ADF( "ERROR [%s,%d] kzalloc failed!!!!\n", __func__, __LINE__ );
						return -ENOMEM;
					}
					*pSlotIndex = slot_index;
					list_push_head( &pAdf_struct->pair_list, pSlotIndex, sizeof( int ), LIST_KEY_PAIR( *pSlotIndex ) );
				}

				break;
			}
		case ADF_CMD_GET_CONNECTOR_INFO :
			{
				CONNECTOR_INFO * const pConnectorInfo = ( CONNECTOR_INFO * const )( pArg );

				INFO_ADF( "CMD_GET_CONNECTOR_INFO:\n" );

				if( copy_to_user( pConnectorInfo, get_connector_info(), sizeof( CONNECTOR_INFO ) ) != 0 )
				{
					ERR_ADF( "copy_to_user() failed!!!!\n" );
				}
				else
				{
					INFO_ADF( "connector: id 0x%04x\n", pConnectorInfo->id );
				}

				break;
			}
		case ADF_CMD_STAT :
			{
				const IOCTL_CMD_DATA * const pCmdData = ( const IOCTL_CMD_DATA * const )( pArg );

				print_slot_map();
#ifdef __STATISTICS__
				if( pCmdData->stat.clear == true )
				{
					int idx;
					for( idx = 0 ; idx < MAX_SLOT_INDEX ; idx++ )
					{
						pAdf_struct->slot_info[ idx ].rx_stat.total = 0;
						pAdf_struct->slot_info[ idx ].rx_stat.sucess = 0;
						pAdf_struct->slot_info[ idx ].rx_stat.err = 0;
					}
				}
				print_slot_info();
#endif	//	__STATISTICS__

				break;
			}
#if 0
		case ADF_READBACK	:
			{
				READBACK_COMMAND	cmd;
                if( copy_from_user( ( void * )& cmd, ( void __user * )pArg, sizeof( READBACK_COMMAND ) ) )
				{
                    ret = -ENOMEM;
                }
				ret = adf_readback( cmd.mode, cmd.adc_mode );
				break;
			}
		case ADF_WRITE_REG	:
			{
                ADF_REG	adf_reg;
                if( copy_from_user( ( void * )& adf_reg, ( void __user * )pArg, sizeof( ADF_REG ) ) )
				{
                    ret = -ENOMEM;
                }
				adf_write_reg( & adf_reg );

				break;
			}
		case ADF_SLEEP		:
		case ADF_DISABLE	:
			{
				RF_COMMAND	cmd = { ADF_MODE_SLEEP, FREQ_UNKNOWN, 0x0, 0, 0, NULL, 0 };
				ret = adf_mode_change( & cmd );

				break;
			}
		case ADF_TX_MODE	:
			{
				RF_COMMAND	cmd = { ADF_MODE_TX, FREQ_UNKNOWN, 0x0, 0, 0, NULL, 0 };
				//RF_COMMAND	* pCmd = ( RF_COMMAND * )pArg;
                if( copy_from_user( ( void * )& cmd, ( void __user * )pArg, sizeof( RF_COMMAND ) ) )
				{
                    ret = -ENOMEM;
                }
				/*	TODO: alloc pCmd->pData and copy_to_user.	*/
				pBuf = ( uint8_t * )kzalloc( cmd.length, GFP_KERNEL );
				if( pBuf == NULL )
				{
					ERR_ADF( "ERROR [%s,%d] kzalloc failed!!!!\n", __func__, __LINE__ );
					return -ENOMEM;
				}

                if( copy_from_user( ( void * )pBuf, ( void __user * )cmd.pData, cmd.length ) )
				{
                    ret = -ENOMEM;
                }
				cmd.pData = pBuf;
				ret = adf_mode_change( & cmd );
				break;
			}
		case ADF_RX_MODE	:
			{
				RF_COMMAND	cmd = { ADF_MODE_RX, FREQ_UNKNOWN, 0x0, 0, 0, NULL, 0 };
                if( copy_from_user( ( void * )& cmd, ( void __user * )pArg, sizeof( RF_COMMAND ) ) )
				{
                    ret = -ENOMEM;
                }
				ret = adf_mode_change( & cmd );
				break;
			}
		case ADF_GET_VERSION:
			{
				break;
			}
		case ADF_GET_MODE	:
			{
				ret = ( long )adf_mode();
				break;
			}
#endif
		default				:
			ret = -EPERM;
			break;
	}

	return ret;
}

static ssize_t adf_read( struct file * pFile, char __user * pBuf , size_t count, loff_t *pPos )
{
	ADF_STRUCT		*pAdf_struct = ( ADF_STRUCT * )( pFile->private_data );
	ADF_PACKET	*pPacket = NULL;
	ssize_t	len = 0;

	ASSERT( pAdf_struct != NULL );

	/*	TODO:	dequeue from RX_FIFO..	*/
	if( list_is_empty( &( pAdf_struct->rx_list ) ) == true )
	{
		if( pFile->f_flags & O_NONBLOCK )
		{
			return -EAGAIN;
		}
		else
		{
			wait_event_interruptible( pAdf_struct->rf_done_waitq, \
					list_is_empty( &( pAdf_struct->rx_list ) ) == false );
		}
	}
#ifdef __TEST_POLL__
	pPacket = ( ADF_PACKET * )list_find_data( &pAdf_struct->rx_list, LIST_KEY_TEST | LIST_KEY_GPS_INFO | LIST_KEY_NOTIFY );
#else
	pPacket = ( ADF_PACKET * )list_find_data( &pAdf_struct->rx_list, \
			LIST_KEY_GPS_INFO | LIST_KEY_NOTIFY | LIST_KEY_ADF_PACKET );
#endif	//	__TEST_POLL__

	if( pPacket == NULL ) return -EFAULT;
	//ASSERT( pPacket != NULL );

	switch( pPacket->notify_type )
	{
		case NOTIFY_NONE :
			if( pAdf_struct->init_reference == false )
			{
				if( adf_init_reference_time( pAdf_struct, pPacket ) == true )
				{
					pAdf_struct->init_reference = true;
				}
				else
				{
					if( list_count( &pAdf_struct->rx_list ) > 5 )
					{
						INFO_ADF( "FREE: pPacket: %p packetData: %p list_count: %d\n", pPacket, pPacket->packetData, \
								list_count( &pAdf_struct->rx_list ) );
					}
					kfree( pPacket );
					return -EFAULT;
				}
			}
			{
				PF_RX_COMMAND *pResponse = NULL;
				pResponse = ( PF_RX_COMMAND * )( pPacket->packetData );
				if( pResponse->command != PF_CMD_GPS_INFO )
				{
					ERR_ADF( ">>>> pResponse->command 0x%02x != GPS 0x%02x\n", pResponse->command, PF_CMD_GPS_INFO );
					kfree( pPacket );
					return -EFAULT;
				}
				else
				{
					int slot_index = pResponse->gps_info.ch_slot.slot - 1;
					int *pSlotIndex = ( int * )list_find_data( &pAdf_struct->pair_list, LIST_KEY_PAIR( slot_index ) );
					if( pSlotIndex == NULL )
					{
						pSlotIndex = ( int * )kzalloc( sizeof( int ), GFP_KERNEL );
						if( pSlotIndex == NULL )
						{
							ERR_ADF( "ERROR [%s,%d] kzalloc failed!!!!\n", __func__, __LINE__ );
							return -EFAULT;
						}
						*pSlotIndex = slot_index;
					}
					list_push_head( &pAdf_struct->pair_list, pSlotIndex, sizeof( int ), LIST_KEY_PAIR( *pSlotIndex ) );
					//INFO_ADF( "push_head: slot_index: %d pair_list: %d\n", slot_index, list_count( &pAdf_struct->pair_list ) );
				}
			}
			break;
		case NOTIFY_SHARE_INFO :
			/* NOTE: set slot info using SHARE_INFO		*/
			{

			}
			break;
		case NOTIFY_SCAN :
			INFO_ADF( "SCAN_END: [ s %d f %d ]\n", \
					pAdf_struct->scan_result_ch_slot.slot, pAdf_struct->scan_result_ch_slot.ch );
			adf_update_slot_map_file();
			break;
		case NOTIFY_ADD_DEVICE :
		case NOTIFY_SHARE_ACK :
		case NOTIFY_EC_ACK :
			/* NOTE: bypass packet	*/
			break;
		case NOTIFY_SLOT_CONFLICT :
		case NOTIFY_SHARE_CANCELLED :
		case NOTIFY_EC_FAIL :

		case NOTIFY_IN_PAIRING :
		default :
			break;
	}

	if( copy_to_user( pBuf, pPacket, count ) == 0 )
	{
#if 0
		printk( "[ TEST ] [ %9ld.%06ld ] notify_type: %d length: %d idx: %d buf: %p len: %d\n", \
				pPacket->tv.tv_sec, pPacket->tv.tv_usec, pPacket->notify_type, \
				pPacket->length, pPacket->bit_index, pBuf, len );
#endif
		//len = sizeof( ADF_PACKET );
		len = count;
	}
	else
	{
		len = -EFAULT;
	}
	//INFO_ADF( "pPacket: %p packetData: %p pBuf: %p len: %d\n", pPacket, pPacket->packetData, pBuf, len );
	kfree( pPacket );

	return len;
}

static ssize_t adf_write( struct file * pFile, const char __user * pBuf , size_t count, loff_t *pPos )
{
	ADF_STRUCT		*pAdf_struct = ( ADF_STRUCT * )( pFile->private_data );
	RF_COMMAND		*pCmd = NULL;
	ssize_t		ret = 0;

	/*	TODO:	enqueue to TX_FIFO..	*/
	pCmd = ( RF_COMMAND * )kzalloc( sizeof( RF_COMMAND ), GFP_KERNEL );
	if( pCmd == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		return -ENOMEM;
	}
	if( copy_from_user( ( void * )pCmd, ( void __user * )pBuf, sizeof( RF_COMMAND ) ) )
	{
		ERR_ADF( "copy_from_user() failed!!!!\n" );
		kfree( pCmd );
		return -ENOMEM;
	}

	list_push( & pAdf_struct->cmd_list, pCmd, sizeof( RF_COMMAND ), LIST_KEY_RF_CMD | pCmd->id );

#if 0
	if( is_full( &( pAdf_struct->tx_fifo ) ) == true )
	{
		if( pFile->f_flags & O_NONBLOCK )
		{
			return -EAGAIN;
		}
		else
		{
			wait_event_interruptible( pAdf_struct->rf_done_waitq, is_full( &( pAdf_struct->tx_fifo ) ) == false );
		}
	}
#endif

	return ret;
}

static uint32_t adf_poll( struct file * pFile, struct poll_table_struct * pWait )
{
	ADF_STRUCT		*pAdf_struct = ( ADF_STRUCT * )( pFile->private_data );
	uint32_t	mask = 0x0;

	ASSERT( pAdf_struct != NULL );

	poll_wait( pFile, &( pAdf_struct->poll_waitq ), pWait );

	if( list_is_empty( & pAdf_struct->rx_list ) == false )
	{
		mask = POLLIN | POLLRDNORM;
	}
#if 0
	if( is_full( & pAdf_struct->tx_fifo ) == false )
	{
		mask = POLLOUT | POLLWRNORM;
	}
#endif

	return mask;
}

static int adf_open( struct inode * pInode, struct file * pFile )
{
	pFile->private_data = & s_adf_struct;

	adf_init();

	return 0;
}

static int adf_release( struct inode * pInode, struct file * pFile )
{
	adf_exit();

    return 0;
}

static struct file_operations adf_fops = {
	.owner = THIS_MODULE,
	.open = adf_open,
	.release = adf_release,
	.read = adf_read,
	.write = adf_write,
	.poll = adf_poll,
	.unlocked_ioctl = adf_ioctl,
};

static struct miscdevice adf_misc_dev = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = ADF_DEVICE_NAME,
    .fops = & adf_fops,
};

static void timeout( struct work_struct * pWork )
{
	ADF_STRUCT	* pAdf_struct = NULL;
	RF_COMMAND cmd = { ADF_MODE_SLEEP, FREQ_UNKNOWN, 0x0, 0, 0, { 0x0, }, 0, 0 };
	ADF_MODE		mode, pre_mode;
	struct timeval tv, tv_swd, tv_start;
#ifdef __DEBUG_ADF_TIME__
	struct timeval tv;
	int irq_cnt;
#endif	//	__DEBUG_ADF_TIME__

	ASSERT( pWork != NULL );

	pAdf_struct = container_of( pWork, ADF_STRUCT, timeout_work );

	ASSERT( pAdf_struct != NULL );
#if 1
	if( pAdf_struct->pHandled_packet == NULL )
	{
		WARN_ADF( "ASSERT!! pHandled_packet is NULL\n" );
	}
#else
	ASSERT( pAdf_struct->pHandled_packet != NULL );
#endif

	if( test_bit( FLAG_SCAN, & pAdf_struct->flags ) == 1 )
	{
		int slot_index = adf_current_slot() - FLAG_SLOT_0;
		if( slot_index >= 0 )
		{
			FREQ freq = pAdf_struct->scan_ch_slot.ch;
			if( freq != pAdf_struct->freq )
			{
				WARN_ADF( "SCAN: freq: %d != %d, slot_index: %d\n", freq, pAdf_struct->freq, slot_index );
			}
			else
			{
				if( test_bit( FLAG_PAIRING, & pAdf_struct->flags ) == 0 )
				{
					slot_map[ freq - 1 ][ slot_index ] = 0x0;
				}
			}

			if( slot_index == ( pAdf_struct->scan_ch_slot.slot - 1 ) )
			{
				pAdf_struct->scan_result_ch_slot = pAdf_struct->scan_ch_slot;
			}
		}
	}
#ifdef __DEBUG_ADF_TIME__
	irq_cnt = pAdf_struct->irq_cnt;
	do_gettimeofday( &tv );
#endif	//	__DEBUG_ADF_TIME__
	do_gettimeofday( &tv );
	if( pAdf_struct->pHandled_packet )
	{
		tv_start  = pAdf_struct->pHandled_packet->tv_start;
		tv_swd = pAdf_struct->pHandled_packet->tv_swd;
	}
	else
	{
		tv_swd.tv_sec = 100;
		tv_swd.tv_usec = 123456;
		tv_start = tv_swd;
	}

	pre_mode = pAdf_struct->mode;

	mutex_lock( & pAdf_struct->alloc_mutex );

	adf_packet_dealloc( pAdf_struct );

	mutex_unlock( & pAdf_struct->alloc_mutex );

	set_bit( FLAG_RF_DONE, & pAdf_struct->flags );
	wake_up_interruptible_all( & pAdf_struct->rf_done_waitq );
	//wake_up_interruptible( & pAdf_struct->rf_done_waitq );

	mode = pAdf_struct->mode;

	/*	NOTE: if cmd.mode is ADF_MODE_SLEEP then check delayed_cmd_list
	 *	Check remain time for global or local command slot	*/
	if( cmd.mode == ADF_MODE_SLEEP )
	{
		int slot_seq = adf_current_slot();
		uint32_t keyMask = 0x0;
		RF_COMMAND *pCmd = NULL;
		int64_t remain = timer_remaining( ( void * )( & pAdf_struct->slot_timer ) );

		if( remain >= ( TX_CMD_MARGIN * 1000 ) )
		{
			if( test_bit( FLAG_E_COLLAR, &pAdf_struct->flags ) == 1 )
			{
				keyMask |= LIST_KEY_E_COLLAR;
			}

			if( slot_seq == FLAG_SLOT_R_0 )	//	global command slot
			{
				keyMask |= LIST_KEY_GLOBAL_CMD;
			}
			else if( ( slot_seq > FLAG_SLOT_R_0 ) && ( slot_seq < FLAG_SLOT_0 )	)	//	Local command slot
			{
				keyMask |= LIST_KEY_LOCAL_CMD;
			}
		}
		if( keyMask != 0x0 )
		{
			pCmd = ( RF_COMMAND * )list_find_data( &pAdf_struct->delayed_cmd_list, keyMask );
			if( pCmd != NULL )
			{
				/*	TODO: if the command have response, push again into TX list head	*/
				memcpy( &cmd, pCmd, sizeof( RF_COMMAND ) );
				kfree( pCmd );
			}
			else { keyMask = 0x0; }
		}
#if 0	//	debug
		adf_mode_change( & cmd );
		INFO_ADF( "\n\tTIMEOUT: Now[ %9ld.%06ld ] START[ %9ld.%06ld ] SWD[ %9ld.%06ld ] mode: %d %d => %d\n", \
				tv.tv_sec, tv.tv_usec, tv_start.tv_sec, tv_start.tv_usec, tv_swd.tv_sec, tv_swd.tv_usec, \
				pre_mode, mode, adf_mode() );
		if( keyMask )
		{
			INFO_ADF( "slot_seq: %d keyMask: 0x%08x cmd.length: %d remain: %lld us\n", slot_seq, keyMask, cmd.length, remain );
		}
		return ;
#endif	//	debug
	}
	adf_mode_change( & cmd );

	if( adf_mode() != ADF_MODE_SLEEP )
	{
		DEBUG_ADF( "\n\tTIMEOUT: Now[ %9ld.%06ld ] START[ %9ld.%06ld ] SWD[ %9ld.%06ld ] mode: %d %d => %d\n", \
				tv.tv_sec, tv.tv_usec, tv_start.tv_sec, tv_start.tv_usec, tv_swd.tv_sec, tv_swd.tv_usec, \
				pre_mode, mode, adf_mode() );
	}
#ifdef __DEBUG_ADF_TIME__
	DEBUG_ADF_TIME( "TIMEOUT: Now[ %9ld.%06ld ] mode: %d %d => %d irq_cnt: %d\n", tv.tv_sec, tv.tv_usec, pre_mode, mode, adf_mode(), irq_cnt );
#else	//	__DEBUG_ADF_TIME__
	if( ( pAdf_struct->init_reference == false ) && ( s_work_mode == ADF_WORK_NORMAL ) )
	{
		//INFO_ADF( "TIMEOUT: RETRY -> 1st GPS info!!! cmd.mode: %d adf_mode: %d\n", cmd.mode, adf_mode() );
		pAdf_struct->last_pairing_slot_index = -1;
		adf_init_reference( pAdf_struct );
	}
	//INFO_ADF( "TIMEOUT: Mode: %d %d => %d\n", pre_mode, mode, adf_mode() );
#endif	//	__DEBUG_ADF_TIME__

	return;
}

static void rf_done( struct work_struct * pWork )
{
	RF_COMMAND cmd = { ADF_MODE_SLEEP, FREQ_UNKNOWN, 0x0, 0, 0, { 0x0, }, 0, 0 };
	ADF_STRUCT	* pAdf_struct = NULL;
	ADF_PACKET	* pPacket = NULL;
	ADF_MODE	mode;
	bool		needInitReference = false;
	bool		ret = false;
#ifdef __DEBUG_ADF_TIME__
	struct timeval tv_0, tv_1, tv_2;
	int irq_cnt;
#endif	//	__DEBUG_ADF_TIME__

	ASSERT( pWork != NULL );

	if( pWork == & s_adf_struct.rf_done_work )
	{
		pAdf_struct = container_of( pWork, ADF_STRUCT, rf_done_work );
	}
	else
	{
		pAdf_struct = container_of( pWork, ADF_STRUCT, rf_done_delayed_work.work );
	}

	ASSERT( pAdf_struct != NULL );

	timer_stop( ( void * )( & pAdf_struct->timeout_timer ) );
#if 1
	if( pAdf_struct->pHandled_packet == NULL )
	{
		WARN_ADF( "1. pAdf_struct->pHandled_packet is NULL! mode: %d\n", adf_mode() );
	}

	//ASSERT( pAdf_struct->pHandled_packet != NULL );
#else
	ASSERT( pAdf_struct->pHandled_packet != NULL );
#endif

#ifdef __DEBUG_ADF_TIME__
	irq_cnt = pAdf_struct->irq_cnt;
	do_gettimeofday( &tv_0 );
#endif	//	__DEBUG_ADF_TIME__

	/*	TODO:	TX or RX completed...
	 *	Disable IRQ
	 *	TX: response => change to RX mode for same frequency.
	 *	TX: no response => change to SLEEP mode.
	 *	RX: push to FIFO Queue
	 *	RX: response => change to TX mode for same frequency.
	 *	    => maybe response from app by ioctl. ==> to SLEEP mode.
	 *	RX: no response => change to SLEEP mode.
	 *	by jyhuh 2017-10-25 11:35:09	*/
	//adf_disable_irq( pAdf_struct );

	mode = adf_mode();

	mutex_lock( & pAdf_struct->alloc_mutex );

	pPacket = pAdf_struct->pHandled_packet;
#if 1
	if( pPacket == NULL )
	{
		WARN_ADF( "2. pAdf_struct->pHandled_packet is NULL! mode: %d\n", adf_mode() );
	}

	//ASSERT( pPacket != NULL );
#else
	ASSERT( pPacket != NULL );
#endif

	if( test_bit( FLAG_FORCE_RF_DONE, & pAdf_struct->flags ) == 0 )
	{
		ret = adf_packet_post_process( pAdf_struct );
	}
	/*	RX Error occurred and not initialized referennce time.
	 *	Retry RX to initialize reference time	*/
	//if( ( ret == false ) && ( pAdf_struct->init_reference == false ) )
	if( ( ret == false ) && ( pAdf_struct->init_reference == false ) \
			&& ( test_and_clear_bit( FLAG_FORCE_RF_DONE, & pAdf_struct->flags ) == 0 ) )
	{
		needInitReference = true;
	}

	/*	Prepare RX response.	*/
	if( ( mode == ADF_MODE_TX ) && ( pPacket->response_length > 0 ) )
	{
		//PF_TX_COMMAND *pTxCmd = ( PF_TX_COMMAND * )( pPacket->packetData );

		cmd.mode = ADF_MODE_RX;
		cmd.freq = pAdf_struct->freq;	//	is Right??
		cmd.length = pPacket->response_length >> 3;	//	bits to bytes
		cmd.id = 0x0;
		cmd.timeout = RX_ACK_DURATION;
	}

	//mutex_lock( & pAdf_struct->alloc_mutex );
	/*	NOTE: transmit Latency.
	 *	refer ADF7021 datasheet p.29	*/
	if( mode == ADF_MODE_TX )
	{
		msleep( 1 );
	}

	//mutex_lock( & pAdf_struct->alloc_mutex );

	adf_packet_dealloc( pAdf_struct );

	mutex_unlock( & pAdf_struct->alloc_mutex );

	DEBUG_ADF( "pid: %d RF_DONE:--------------------- mode: %d => %d flags: %lx\n", current->pid, pAdf_struct->mode, \
			cmd.mode, pAdf_struct->flags );

	set_bit( FLAG_RF_DONE, & pAdf_struct->flags );
	wake_up_interruptible_all( & pAdf_struct->rf_done_waitq );
	//wake_up_interruptible( & pAdf_struct->rf_done_waitq );

#ifdef __DEBUG_ADF_TIME__
	do_gettimeofday( &tv_1 );
#endif	//	__DEBUG_ADF_TIME__

	/*	NOTE: if cmd.mode is ADF_MODE_SLEEP then check delayed_cmd_list
	 *	Check remain time for global or local command slot	*/
	if( cmd.mode == ADF_MODE_SLEEP )
	{
		int slot_seq = adf_current_slot();
		uint32_t keyMask = 0x0;
		RF_COMMAND *pCmd = NULL;
		int64_t remain = timer_remaining( ( void * )( & pAdf_struct->slot_timer ) );

		if( remain >= ( TX_CMD_MARGIN * 1000 ) )
		{
			if( test_bit( FLAG_E_COLLAR, &pAdf_struct->flags ) == 1 )
			{
				keyMask |= LIST_KEY_E_COLLAR;
			}

			if( slot_seq == FLAG_SLOT_R_0 )	//	global command slot
			{
				keyMask |= LIST_KEY_GLOBAL_CMD;
			}
			else if( ( slot_seq > FLAG_SLOT_R_0 ) && ( slot_seq < FLAG_SLOT_0 )	)	//	Local command slot
			{
				keyMask |= LIST_KEY_LOCAL_CMD;
			}
		}
		if( keyMask != 0x0 )
		{
			pCmd = ( RF_COMMAND * )list_find_data( &pAdf_struct->delayed_cmd_list, keyMask );
			if( pCmd != NULL )
			{
				/*	TODO: if the command have response, push again into TX list head	*/
				memcpy( &cmd, pCmd, sizeof( RF_COMMAND ) );
				kfree( pCmd );
			}
			else { keyMask = 0x0; }
		}
#if 0	//	debug
		adf_mode_change( & cmd );
		if( needInitReference == true )
		{
			ASSERT( cmd.mode == ADF_MODE_SLEEP );

			WARN_ADF( "[ERR] 1st GPS info Error!!! => retry RX. cmd.mode: %d adf_mode: %d\n", cmd.mode, adf_mode() );
			adf_init_reference( pAdf_struct );
		}
		if( keyMask )
		{
			INFO_ADF( "slot_seq: %d keyMask: 0x%08x cmd.length: %d remain: %lld us\n", slot_seq, keyMask, cmd.length, remain );
		}
		return ;
#endif	//	debug
	}
#ifdef __RF_TEST__
	if( s_work_mode == ADF_WORK_TEST_RX )
	{
		cmd.mode = ADF_MODE_RX;
		cmd.freq = s_test_freq;
		//cmd.length = get_command_len( PF_CMD_RX_TEST );
		cmd.length = DUMP_PACKET_LEN;
		cmd.timeout = -1;	//	No timeout.
	}
#endif	//	__RF_TEST__
	adf_mode_change( & cmd );

	//if( needInitReference == true )
	if( ( needInitReference == true ) && ( s_work_mode == ADF_WORK_NORMAL ) )
	{
		ASSERT( cmd.mode == ADF_MODE_SLEEP );

		WARN_ADF( "[ERR] 1st GPS info Error!!! => retry RX. cmd.mode: %d adf_mode: %d\n", cmd.mode, adf_mode() );
		adf_init_reference( pAdf_struct );
	}
#ifdef __DEBUG_ADF_TIME__
	do_gettimeofday( &tv_2 );

	DEBUG_ADF_TIME( "RF_DONE: Now_0[ %9ld.%06ld ] mode: %d irq_cnt: %d\n", tv_0.tv_sec, tv_0.tv_usec, mode, irq_cnt );
	DEBUG_ADF_TIME( "RF_DONE: Now_1[ %9ld.%06ld ] before change mode\n", tv_1.tv_sec, tv_1.tv_usec );
	DEBUG_ADF_TIME( "RF_DONE: Now_2[ %9ld.%06ld ] mode change: %d => %d\n", tv_2.tv_sec, tv_2.tv_usec, mode, adf_mode() );
#else	//	__DEBUG_ADF_TIME__
	//INFO_ADF( "RF_DONE: Mode: %d => %d\n", mode, adf_mode() );
#endif	//	__DEBUG_ADF_TIME__

#ifdef __DUMP_PACKET__
	print_dump_list();
#endif	//	__DUMP_PACKET__

	return;
}

irqreturn_t swd_handler( int irq, void * pData )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;
	ADF_PACKET	* pPacket = NULL;

	ASSERT( pData != NULL );

	set_bit( FLAG_SWD_ASSERTED, & pAdf_struct->flags );

	pPacket = pAdf_struct->pHandled_packet;
	ASSERT( pPacket != NULL );
	do_gettimeofday( & pPacket->tv_swd );

	/*	Restart Timeout timer for RX.	*/
	timer_start( ( void * )( & pAdf_struct->timeout_timer ), RX_TIMEOUT );

	return IRQ_HANDLED;
}

irqreturn_t clkout_handler( int irq, void * pData )
{
	int bit, clkout;
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;
	ADF_PACKET	* pPacket = NULL;
	ADF_MODE	mode;

	ASSERT( pData != NULL );

	pPacket = pAdf_struct->pHandled_packet;
	mode = adf_mode();

#ifdef __DEBUG_ADF_TIME__
	pAdf_struct->irq_cnt++;
#endif	//	__DEBUG_ADF_TIME__

#if 1
	ASSERT( pPacket != NULL );
#else
	if( pPacket == NULL )
	{
		WARN_ADF( "pPacket( %p ) is NULL mode: %d\n", pPacket, adf_mode() );
		return IRQ_HANDLED;
	}
#endif

	if( test_bit( FLAG_IRQ_ENABLED, &pAdf_struct->flags ) == 0 )
	{
		return IRQ_HANDLED;
	}

	clkout = gpio_get_value( CLKOUT_GPIO_NUM );

	if( mode == ADF_MODE_TX )
	{
		/*	TX: at falling Edge */
		if( clkout ==  0 )
		{
			pPacket->bit_index++;
			/*	byte: bit_index / 8
			 *	bit: msb --> lsb. 7 - bit_index & 0x7	*/
			bit = ( pPacket->packetData[ pPacket->bit_index >> 3 ] >> ( ~pPacket->bit_index & 0x7 ) ) & 0x1;
			gpio_set_value( TXRX_CLK_GPIO_NUM, bit );
		}
	}
	else
	{
		/*	RX: at rising Edge */
		if( clkout ==  1 )
		{
			if( test_bit( FLAG_SWD_ASSERTED, & pAdf_struct->flags ) == 1 )
			{
				pPacket->bit_index++;
				if( gpio_get_value( TXRX_DATA_GPIO_NUM ) == 0x1 )
				{
					/*	byte: bit_index / 8
					 *	bit: msb --> lsb. 7 - bit_index & 0x7	*/
					pPacket->packetData[ pPacket->bit_index >> 3 ] |= ( 0x1 << ( ~pPacket->bit_index & 0x7 ) );
				}
			}
		}
	}

	if( pPacket->bit_index >= ( int )pPacket->length )
	{
		if( mode == ADF_MODE_RX )
		{
			set_bit( FLAG_RX_ENQUEUE, & pAdf_struct->flags );
			clear_bit( FLAG_SWD_ASSERTED, & pAdf_struct->flags );
		}

		adf_disable_irq_nosync( pAdf_struct );

		do_gettimeofday( & pPacket->tv );
		queue_work( pAdf_struct->pTimeout_wq, & pAdf_struct->rf_done_work );
		//queue_delayed_work( pAdf_struct->pTimeout_wq, & pAdf_struct->rf_done_delayed_work, msecs_to_jiffies( 4 ) );
	}

	return IRQ_HANDLED;
}

static bool adf_enable_irq( void * pData )
{
	ADF_STRUCT		* pAdf_struct = ( ADF_STRUCT * )pData;
	unsigned long	flags;

	ASSERT( pData != NULL );

	spin_lock_irqsave( & pAdf_struct->spinlock, flags );

	if( test_and_set_bit( FLAG_IRQ_ENABLED, & pAdf_struct->flags ) == 0 )
	{
		enable_irq( gpio_to_irq( CLKOUT_GPIO_NUM ) );
		enable_irq( gpio_to_irq( SWD_GPIO_NUM ) );
#ifdef __DEBUG_ADF_TIME__
		pAdf_struct->irq_cnt = 0;
#endif	//	__DEBUG_ADF_TIME__
	}

	spin_unlock_irqrestore( & pAdf_struct->spinlock, flags );

	return true;
}

static bool adf_disable_irq( void * pData )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;
	unsigned long	flags;

	ASSERT( pData != NULL );

	spin_lock_irqsave( & pAdf_struct->spinlock, flags );

	if( test_and_clear_bit( FLAG_IRQ_ENABLED, & pAdf_struct->flags ) == 1 )
	{
		clear_bit( FLAG_SWD_ASSERTED, & pAdf_struct->flags );
		disable_irq( gpio_to_irq( CLKOUT_GPIO_NUM ) );
		disable_irq( gpio_to_irq( SWD_GPIO_NUM ) );
	}

	spin_unlock_irqrestore( & pAdf_struct->spinlock, flags );

	return true;
}

static bool adf_disable_irq_nosync( void * pData )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;
	unsigned long	flags;

	ASSERT( pData != NULL );

	spin_lock_irqsave( & pAdf_struct->spinlock, flags );

	if( test_and_clear_bit( FLAG_IRQ_ENABLED, & pAdf_struct->flags ) == 1 )
	{
		clear_bit( FLAG_SWD_ASSERTED, & pAdf_struct->flags );
		disable_irq_nosync( gpio_to_irq( CLKOUT_GPIO_NUM ) );
		disable_irq_nosync( gpio_to_irq( SWD_GPIO_NUM ) );
	}

	spin_unlock_irqrestore( & pAdf_struct->spinlock, flags );

	return true;
}

/*	NOTE:	Do NOT call this function in interrupt context.
 *			by jyhuh 2017-10-25 08:35:05	*/
static int adf_packet_alloc( void * pData )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;
	ADF_PACKET	* pPacket = NULL;

	ASSERT( pData != NULL );
#if 1
	if( pAdf_struct->pHandled_packet != NULL )
	{
		WARN_ADF( "current slot: %d mode: %d\n", adf_current_slot(), adf_mode() );
	}
	ASSERT( pAdf_struct->pHandled_packet == NULL );
#else
	if( pAdf_struct->pHandled_packet != NULL )
	{
		WARN_ADF( "pHandled_packet: %p bit_index: %d flags: %lx\n", pAdf_struct->pHandled_packet, \
				pAdf_struct->pHandled_packet->bit_index, pAdf_struct->flags );
		WARN_ADF( "pid: %d pHandled_packet: %p tmp_cnt: %ld wait_cnt: %d\n", current->pid, pAdf_struct->pHandled_packet, \
				pAdf_struct->tmp_cnt, pAdf_struct->wait_cnt );
		adf_packet_dealloc( pAdf_struct );
	}
#endif

	pPacket = ( ADF_PACKET * )kzalloc( sizeof( ADF_PACKET ), GFP_KERNEL );
	if( pPacket == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		return -ENOMEM;
	}
	pPacket->bit_index = -1;
	pPacket->length = 0;
	pPacket->response_length = 0;
	pPacket->notify_type = NOTIFY_NONE;

	do_gettimeofday( &pPacket->tv_start );
	pPacket->tv = pPacket->tv_start;

	pAdf_struct->pHandled_packet = pPacket;

	return 0;
}

static int adf_packet_dealloc( void * pData )
{
	ADF_STRUCT	* pAdf_struct = ( ADF_STRUCT * )pData;
	ADF_PACKET	* pPacket = NULL;

	ASSERT( pData != NULL );

	pPacket = pAdf_struct->pHandled_packet;

	if( pPacket != NULL )
	{
		/*	NOT free enqueued data!!!!	*/
		if( test_and_clear_bit( FLAG_RX_ENQUEUE, & pAdf_struct->flags ) == 0 )
		{
#ifdef __DEBUG_QUEUE_CNT__
			pAdf_struct->dealloc_cnt++;
#endif	//	__DEBUG_QUEUE_CNT__
			kfree( pPacket );
			pPacket = NULL;
		}
		pAdf_struct->pHandled_packet = NULL;
	}

	return 0;
}

static bool adf_packet_post_process( void *pData )
{
	ADF_STRUCT *pAdf_struct = NULL;
	ADF_PACKET *pPacket = NULL;
	PF_RX_COMMAND *pResponse = NULL;
	ADF_MODE mode;

	ASSERT( pData != NULL );

	pAdf_struct = ( ADF_STRUCT * )pData;
	pPacket = pAdf_struct->pHandled_packet;

#if 1
	if( pPacket == NULL )
	{
		WARN_ADF( "pAdf_struct->pHandled_packet is NULL! mode: %d\n", adf_mode() );
		return false;
	}

	//ASSERT( pPacket != NULL );
#else
	ASSERT( pPacket != NULL );
#endif

	mode = adf_mode();

	//if( mode == ADF_MODE_RX )
	if( test_bit( FLAG_RX_ENQUEUE, & pAdf_struct->flags ) == 1 )
	{
		pResponse = ( PF_RX_COMMAND * )( pPacket->packetData );
		pResponse->rssi_dBm = calculate_rssi();

#ifdef __DUMP_PACKET__
		if( ( s_dump_mode & PF_DUMP_RX ) != 0x0 )
		{
			PF_COMMAND *pPfCmd = ( PF_COMMAND * )kzalloc( sizeof( PF_COMMAND ), GFP_KERNEL );

			memcpy( pPfCmd, pResponse, get_command_len( pResponse->command ) + 1 );
			list_push( &pAdf_struct->dump_list, pPfCmd, sizeof( PF_COMMAND ), LIST_KEY_RX );
			//printk( "RX: %*ph\n", get_command_len( pResponse->command ) + 1, pResponse );
		}

#endif	//	__DUMP_PACKET__

		if( verify_checksum( pResponse ) == true )
		{
			/*	NOTE: Calculate TX start time from swd time.
			 *	TODO: check ID validation for all response.
			 *	*/
			ktime_t start;
			struct timeval tv_start;
			bool isValid = true;
			uint32_t id = 0x0;
			uint32_t keyMask = list_assign_key( pResponse->command );

			if( pResponse->command == PF_CMD_GPS_INFO )
			{
				GPS_INFO *pGpsInfo = &pResponse->gps_info;
				int slot_index = pResponse->gps_info.ch_slot.slot - 1;

				id = pResponse->gps_info.dog_id;
				isValid = adf_valid_gps_packet( pAdf_struct, pPacket );

				if( isValid == true )
				{
					/*	NOTE: when period changed, 1st packet from Collar is not aligned period time.
					 *	then, 1st packet time ignore.	*/
					bool ignoreTime = false;
					/*	NOTE: Notify to upstream, at first time.	*/
					if( adf_get_slot_info_last_rx_time( slot_index ).tv_sec == 0 )
					{
						int changing_slot_index = adf_find_slot_changing( id );

						if( ( changing_slot_index >= 0 ) && ( changing_slot_index < MAX_SLOT_INDEX ) )
						{
							ignoreTime = true;
							if( adf_slot_is_listening( changing_slot_index ) == false )
							{
								adf_clear_slot_info( pAdf_struct, changing_slot_index );
							}
							else
							{
								/*	INFO: Change Period	*/
								adf_slot_set_mode( pAdf_struct, changing_slot_index, SLOT_MODE_LISTEN );
							}
						}
						else
						{
							/* NOTE: check period for sharing...	*/
							adf_valid_period_gps_packet( pAdf_struct, pPacket );
						}
						//INFO_ADF( "1st RX: slot: %d remain: %d\n", slot_index,
						//		adf_get_slot_info_period_remain( slot_index ) );

						//slot_update_remain( pAdf_struct, slot_index );

						//adf_notify_add_device( pAdf_struct, id );
#ifdef __STATISTICS__
						/* NOTE: clear all informations when 1st GPS received...	*/
						if( pAdf_struct->slot_info[ slot_index ].rx_stat.sucess == 0 )
						{
							pAdf_struct->slot_info[ slot_index ].rx_stat.total = 0;
							pAdf_struct->slot_info[ slot_index ].rx_stat.sucess = 0;
							pAdf_struct->slot_info[ slot_index ].rx_stat.err = 0;
						}
#endif	//	__STATISTICS__
					}
#ifdef __STATISTICS__
					pAdf_struct->slot_info[ slot_index ].rx_stat.sucess++;
#endif	//	__STATISTICS__
					start = timeval_to_ktime( pPacket->tv_swd );
					start = ktime_sub_ns( start, ( u64 )MS_TO_NS( TX_HEADER_TIME ) );
					tv_start = ktime_to_timeval( start );

					if( ignoreTime == false )
					{
						adf_set_slot_info_field( pAdf_struct, slot_index, SLOT_FIELD_LAST_RX_TIME, &tv_start );
						set_bit( FLAG_SLOT_0 + slot_index, &pAdf_struct->slot_flags );
					}

					//INFO_ADF( "[ GPS ] slot_index: %d start [ %9ld.%06ld ]\n", slot_index, tv_start.tv_sec, tv_start.tv_usec );
				}
				else if( pAdf_struct->init_reference == true )
				{
					ADF_SLOT_INFO slotInfo;

					slot_index = pResponse->gps_info.ch_slot.slot - 1;
					id = pResponse->gps_info.dog_id;
					adf_get_slot_info( pAdf_struct, slot_index, &slotInfo );

					if( slotInfo.id == 0x0 )
					{
						INFO_ADF( "GPS_SCAN: [ s %d f %d ] id: 0x%04x\n", slot_index + 1, pResponse->gps_info.ch_slot.ch, id );
					}
					else
					{
						/*	Conflict */
						WARN_ADF( "NOTIFY_2: CONFLICT!! [ s %d f %d %d ] id: 0x%04x 0x%04x mode: %s\n", \
								slot_index + 1, slotInfo.freq, pGpsInfo->ch_slot.ch, \
								slotInfo.id, pGpsInfo->dog_id, \
								adf_slot_mode_str( pAdf_struct->slot_info[ slot_index ].mode ) );
						/* TODO: NOTIFY_CB: Share RX slot conflict -> recommend ch / slot	*/
						adf_notify( pAdf_struct, NOTIFY_SLOT_CONFLICT, slot_index + 1 );
						/*	TODO: Conflict avoidance	*/
						//adf_cancel_pairing( pAdf_struct, slot_index );
					}
					isValid = false;
				}

				slot_map[ pResponse->gps_info.ch_slot.ch - 1 ][ slot_index ] = pResponse->gps_info.dog_id;
			}
			else if( pResponse->command == PF_CMD_NICK_ACK )
			{
				if( test_bit( FLAG_NEED_EC_ACK, &pAdf_struct->flags ) == 1 )
				{
					RF_COMMAND *pRfCmd = ( RF_COMMAND * )list_find_data( &pAdf_struct->cmd_list, LIST_KEY_NICK_ACK );
					if( pRfCmd != NULL )
					{
						PF_TX_COMMAND *pTxCmd = ( PF_TX_COMMAND * )( pRfCmd->packetData );
						id = pRfCmd->id;
						INFO_ADF( "NICK_ACK id: 0x%04x cmd: 0x%02x retry_cnt: %d\n", \
								id, pTxCmd->command, pTxCmd->retry_cnt );
						kfree( pRfCmd );
					}
					if( pResponse->ack.hunter_id == get_connector_info()->id )
					{
						/* TODO: NOTIFY_CB: NICK_ONE Sucess -> NONE	*/
						adf_notify( pAdf_struct, NOTIFY_EC_ACK, -1 );
						//INFO_ADF( "RF_DONE: NEED_EC_ACK: clear_bit!!!! send_cnt: %d slot: %d\n",
						//		get_eCollar_info()->send_cnt, adf_current_slot() );
						clear_bit( FLAG_NEED_EC_ACK, &pAdf_struct->flags );
					}
				}
				isValid = false;
			}
			else if( pResponse->command == PF_CMD_SHARE_ACK )
			{
				struct timeval now;
				do_gettimeofday( &now );
				INFO_ADF( "RX: [ SH_ACK ] now [ %9ld.%06ld ]\n", now.tv_sec, now.tv_usec );
				if( test_bit( FLAG_NEED_SHARE_ACK, &pAdf_struct->flags ) == 1 )
				{
					RF_COMMAND *pRfCmd = ( RF_COMMAND * )list_find_data( &pAdf_struct->cmd_list, LIST_KEY_SHARE );
					if( pRfCmd != NULL )
					{
						PF_TX_COMMAND *pTxCmd = ( PF_TX_COMMAND * )( pRfCmd->packetData );
						id = pRfCmd->id;
						pPacket->notify_type = NOTIFY_SHARE_ACK;
						//keyMask |= LIST_KEY_NOTIFY;
						if( ( pTxCmd->command == PF_CMD_SHARE_HUNTER_INFO ) \
								|| ( pTxCmd->command == PF_CMD_SHARE_DOG_INFO ) )
						{
							INFO_ADF( "RF_DONE: SHARE_ACK => Free!!!! id: 0x%04x keyMask: 0x%08x\n", id, keyMask );
							kfree( pRfCmd );
						}
						else
						{
							WARN_ADF( "RF_DONE: SHARE_ACK: does not match id: 0x%04x\n", id );
							kfree( pRfCmd );
						}
					}
					if( pResponse->ack.hunter_id == get_connector_info()->id )
					{
						//INFO_ADF( "RF_DONE: NEED_EC_ACK: clear_bit!!!! send_cnt: %d slot: %d\n",
						//		get_eCollar_info()->send_cnt, adf_current_slot() );
						clear_bit( FLAG_NEED_SHARE_ACK, &pAdf_struct->flags );
					}
				}
			}
			else if( ( pResponse->command == PF_CMD_SHARE_DOG_INFO ) \
					|| ( pResponse->command == PF_CMD_SHARE_HUNTER_INFO ) )
			{
				/*	TODO: Send SHARE_HUNTER_INFO or SHARE_ACK
				 *	- SHARE_RX: Send SHARE_HUNTER_INFO( response SHARE_ACK )
				 *	- SHARE_TX: Send SHARE_ACK
				 *	check slot conflict and store SHARE_INFO	*/
				if( test_bit( FLAG_SHARING_RX, & pAdf_struct->flags ) == 1 )
				{
					/* NOTE: if reference time has not been initialized, then start ref_timer to process RX packet	*/
					if( pAdf_struct->init_reference == false )
					{
						//do_gettimeofday( &tv_start );
						set_bit( FLAG_SHARING_INFO, & pAdf_struct->flags );
						adf_init_start_ref_timer();
						//INFO_ADF( "[ SH_DOG ] now [ %9ld.%06ld ]\n", tv_start.tv_sec, tv_start.tv_usec );
					}
				}
				else
				{
					isValid = false;
				}
				do_gettimeofday( &tv_start );
				INFO_ADF( "RX: [ SHARE_INFO ] now [ %9ld.%06ld ]\n", tv_start.tv_sec, tv_start.tv_usec );
				if( isValid == false )
				{
					clear_bit( FLAG_RX_ENQUEUE, & pAdf_struct->flags );
					return false;
				}
#if 0
				if( test_bit( FLAG_SHARING_RX, & pAdf_struct->flags ) == 1 )
				{
					clear_bit( FLAG_SHARING_RX, & pAdf_struct->flags );
				}
				else if( test_bit( FLAG_SHARING_TX, & pAdf_struct->flags ) == 1 )
				{
					clear_bit( FLAG_SHARING_TX, & pAdf_struct->flags );
				}
#endif
			}

			if( ( isValid == true ) || ( pAdf_struct->init_reference == false ) )
			{
				keyMask |= ( id & LIST_ID_BROADCAST );
				//list_push( & pAdf_struct->rx_list, pPacket, sizeof( ADF_PACKET ),
				//		list_assign_key( pResponse->command ) | ( id & LIST_ID_BROADCAST ) );
				list_push( & pAdf_struct->rx_list, pPacket, sizeof( ADF_PACKET ), keyMask );
				wake_up_interruptible( & pAdf_struct->poll_waitq );
			}
			else
			{
				clear_bit( FLAG_RX_ENQUEUE, & pAdf_struct->flags );
			}
		}
		else
		{
			clear_bit( FLAG_RX_ENQUEUE, & pAdf_struct->flags );
#ifdef __STATISTICS__
			if( pResponse->command == PF_CMD_GPS_INFO )
			{
				int slot_index = pResponse->gps_info.ch_slot.slot - 1;
				pAdf_struct->slot_info[ slot_index ].rx_stat.err++;
			}
#endif	//	__STATISTICS__
			NOTICE_ADF( "[ERR] length: %d idx: %d 1st: 0x%08x key: 0x%08x\n", \
					pPacket->length, pPacket->bit_index, \
					*( uint32_t * )( pPacket->packetData ), list_assign_key( pPacket->packetData[ 1 ] ) );
			return false;
		}
	}
	else if( mode == ADF_MODE_TX )
	{
		PF_TX_COMMAND *pCmd = ( PF_TX_COMMAND * )( pPacket->packetData );

#ifdef __DUMP_PACKET__
		if( ( s_dump_mode & PF_DUMP_TX ) != 0x0 )
		{
			PF_COMMAND *pPfCmd = ( PF_COMMAND * )kzalloc( sizeof( PF_COMMAND ), GFP_KERNEL );
			int tx_header_size = sizeof( uint32_t ) + sizeof( uint32_t );	/* preamble + syncword */

			memcpy( pPfCmd, pCmd, get_command_len( pCmd->command ) + tx_header_size );
			list_push( &pAdf_struct->dump_list, pPfCmd, sizeof( PF_COMMAND ), LIST_KEY_TX );
			//printk( "TX: %*phC\n", get_command_len( pCmd->command ) + tx_header_size, pCmd );
		}
#endif	//	__DUMP_PACKET__
		switch( pCmd->command )
		{
			case PF_CMD_SLEEP_MODE :
				{
					NOTICE_ADF( "SLEEP: 0x%02x\n", pCmd->sleep_mode.mode );

					break;
				}
			case PF_CMD_CHANGE_PERIOD_REQ :
				{
					PERIOD desired_period = pCmd->rf_period_req.period;
					uint16_t dog_id = pCmd->rf_period_req.dog_id;
					int slot_index;
					ADF_SLOT_INFO slotInfo;
					PERIOD period;

					slot_index = adf_find_slot_changing( dog_id );
					if( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) )
					{
						INFO_ADF( "CHANING: In progress id: 0x%04x, slot_index: %d\n", dog_id, slot_index );
						break;
					}
					slot_index = adf_find_slot_listening( dog_id );

					ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

					adf_get_slot_info( pAdf_struct, slot_index, &slotInfo );

					period = slotInfo.period;

					slotInfo.period = desired_period;
					slotInfo.period_remain = 1;
					//slotInfo.shared = false;
					slotInfo.mode = SLOT_MODE_CHANGING_PERIOD;
					slotInfo.last_rx_time.tv_sec = 0;
					slotInfo.last_rx_time.tv_usec = 0;

					adf_set_slot_info( pAdf_struct, slot_index, &slotInfo );
					slot_update_remain( pAdf_struct, slot_index );

					slot_map[ slotInfo.freq - 1 ][ slot_index ] = 0x0;

					NOTICE_ADF( "CHANGE: id 0x%04x | slot period: [ s: %d p: %d ] => [ s: %d p: %d ]\n", \
							dog_id, slot_index, period, slot_index, slotInfo.period );

					break;
				}
			case PF_CMD_CHANGE_SLOT_REQ :
				{
					int desired_slot_index = pCmd->change_slot_req.ch_slot.slot - 1;
					FREQ desired_freq = pCmd->change_slot_req.ch_slot.ch;
					uint16_t dog_id = pCmd->change_slot_req.dog_id;
					int slot_index;
					ADF_SLOT_INFO slotInfo;
					FREQ freq;

					slot_index = adf_find_slot_changing( dog_id );
					if( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) )
					{
						INFO_ADF( "CHANING: In progress id: 0x%04x, slot_index: %d\n", dog_id, slot_index );
						break;
					}
					slot_index = adf_find_slot_listening( dog_id );

					ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) );

					adf_get_slot_info( pAdf_struct, slot_index, &slotInfo );

					freq = slotInfo.freq;

					/*	Assign desired slot information	*/
					//slotInfo.id = dog_id;
					slotInfo.freq = desired_freq;
					//slotInfo.period = adf_get_slot_info_period( slot_index );
					slotInfo.period_remain = 1;
					//slotInfo.shared = false;
					slotInfo.mode = SLOT_MODE_LISTEN;
					slotInfo.last_rx_time.tv_sec = 0;
					slotInfo.last_rx_time.tv_usec = 0;

					adf_slot_set_mode( pAdf_struct, slot_index, SLOT_MODE_CHANGING );
					adf_set_slot_info( pAdf_struct, desired_slot_index, &slotInfo );
					slot_update_remain( pAdf_struct, desired_slot_index );

					slot_map[ freq - 1 ][ slot_index ] = 0x0;

					NOTICE_ADF( "CHANGE: id 0x%04x | slot freq: [ s: %d f: %d ] => [ s: %d f: %d ]\n", \
							dog_id, slot_index, freq, desired_slot_index, desired_freq );

					break;
				}
			case PF_CMD_DELETE_DOG :
				{
					uint16_t dog_id = pCmd->delete_dog.dog_id;
					int slot_index = adf_find_slot_listening( dog_id );

					NOTICE_ADF( "DELETE: id 0x%04x | slot : %d\n", dog_id, slot_index );
					if( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX ) )
					{
						adf_cancel_pairing( pAdf_struct, slot_index );
					}
					break;
				}
			case PF_CMD_NICK_ONE :
			case PF_CMD_NICK_REQ :
			case PF_CMD_TONE :
				{
					uint16_t dog_id = pCmd->nick.dog_id;
					int slot_index = adf_find_slot_listening( dog_id );

					ASSERT( slot_index >= 0 );

					if( pPacket->response_length > 0 )
					{
						set_bit( FLAG_NEED_EC_ACK, &pAdf_struct->flags );
						//NOTICE_ADF( "NICK_%s: id 0x%04x | slot: %d level: %d\n",
						//		( pCmd->command == PF_CMD_NICK_ONE ) ? "ONE" : "REQ", dog_id, slot_index, pCmd->nick.level );
					}
					break;
				}
			case PF_CMD_SHARE_HUNTER_INFO :
			case PF_CMD_SHARE_DOG_INFO :
				{
					uint16_t id = pCmd->share_info.id;
					//int slot_index = adf_find_slot_listening( dog_id );

					//ASSERT( slot_index >= 0 );

					//if( pPacket->response_length > 0 )
					//if( test_bit( FLAG_SHARING_RX_ACK, & pAdf_struct->flags ) == 1 )
					{
						struct timeval now;
						do_gettimeofday( &now );
						set_bit( FLAG_NEED_SHARE_ACK, &pAdf_struct->flags );
						NOTICE_ADF( "TX: SHARE_%s: id 0x%04x now [ %9ld.%06ld ] retry_cnt: %d\n",
								( pCmd->command == PF_CMD_SHARE_DOG_INFO ) ? "DOG" : "HUNTER", id, \
								now.tv_sec, now.tv_usec, pCmd->retry_cnt );
					}
					break;
				}
			case PF_CMD_SHARE_ACK :
				{
					struct timeval now;
					do_gettimeofday( &now );
					INFO_ADF( "TX: [ SHARE_ACK ] now [ %9ld.%06ld ]\n", now.tv_sec, now.tv_usec );
					break;
				}
			default :
				break;
		}
	}
	//else if( mode != ADF_MODE_TX )
	else
	{
		if( pAdf_struct->pHandled_packet != NULL )
		{
			WARN_ADF( "pid: %d pHandled_packet: %p bit_index: %d length: %d mode: %d flags: %lx\n", \
					current->pid, pAdf_struct->pHandled_packet, pAdf_struct->pHandled_packet->bit_index, \
					pAdf_struct->pHandled_packet->length, mode, pAdf_struct->flags );
		}
		else
		{
			WARN_ADF( "pid: %d pHandled_packet: %p mode: %d flags: %lx\n", \
					current->pid, pAdf_struct->pHandled_packet, mode, pAdf_struct->flags );
		}
	}

	return true;
}

static int slot_sequence( ADF_STRUCT *pAdf_struct )
{
	ASSERT( pAdf_struct != NULL );

	pAdf_struct->slot_seq++;
	if( pAdf_struct->slot_seq >= FLAG_SLOT_END )
	{
		pAdf_struct->slot_seq = FLAG_SLOT_R_0;
	}

	return pAdf_struct->slot_seq;
}

static enum hrtimer_restart reference_handler( const struct hrtimer *pTimer )
{
	ADF_STRUCT	* pAdf_struct = NULL;
	ktime_t		now = ktime_get();

	ASSERT( pTimer != NULL );

	pAdf_struct = container_of( pTimer, ADF_STRUCT, reference_timer );

	if( ktime_to_ns( pAdf_struct->reference_next_timestamp ) != 0 )
	{
		pAdf_struct->reference_next_timestamp = \
				ktime_add_ns( pAdf_struct->reference_next_timestamp, ( u64 )MS_TO_NS( REF_DURATION ) );
	}
	else
	{
		pAdf_struct->reference_next_timestamp = \
				ktime_add_ns( now, ( u64 )MS_TO_NS( REF_DURATION ) );
	}
	pAdf_struct->reference_timestamp = now;

	queue_work( pAdf_struct->pSlot_wq, & pAdf_struct->ref_slot_work );

	return HRTIMER_NORESTART;
}

static void ref_slot( struct work_struct * pWork )
{
	ADF_STRUCT	* pAdf_struct = NULL;
	ktime_t		now = ktime_get();
	int			slot = -1;
	long		delta = MS_TO_NS( REF_DURATION );
	RF_COMMAND cmd = { ADF_MODE_SLEEP, FREQ_UNKNOWN, 0x0, 0, 0, { 0x0, }, 0, 0 };
	RF_COMMAND *pCmd = NULL;
	ADF_MODE	mode;
	bool		res;

#ifdef __DEBUG_ADF_TIME__
	struct timeval tv;
	do_gettimeofday( &tv );
#endif	//	__DEBUG_ADF_TIME__

	ASSERT( pWork != NULL );

	pAdf_struct = container_of( pWork, ADF_STRUCT, ref_slot_work );

	slot = slot_sequence( pAdf_struct );

#if 1
	if( slot != FLAG_SLOT_R_0 )
	{
		WARN_ADF( "================ slot: %d\n", slot );
	}
#else
	ASSERT( slot == FLAG_SLOT_R_0 );
#endif

	//	Compensate time
	res = adf_sync_reference_time( pAdf_struct );

	delta = ( long )ktime_us_delta( pAdf_struct->reference_next_timestamp, now ) * 1000;	//	nsec
	delta = ( long )ktime_us_delta( pAdf_struct->reference_next_timestamp, now ) / 1000;	//	msec

	if( res == false )
	{
		timer_start( & pAdf_struct->reference_timer, delta );
		//INFO_ADF( "res is false. delta: %ld\n", delta );
	}

	delta = SLOT_DURATION - ( REF_DURATION - delta );

	/*	NOTE: need RX margin for SHARE_ACK or SHARE_HUNTER_INFO	*/
	if( ( test_bit( FLAG_SHARING_TX, & pAdf_struct->flags ) == 0 ) \
			&& ( test_bit( FLAG_SHARING_RX, & pAdf_struct->flags ) == 0 ) )
	{
		delta += GLOBAL_RX_MARGIN;
	}
	timer_start( ( void * )( & pAdf_struct->slot_timer ), delta );

	if( test_bit( FLAG_PAIRING, & pAdf_struct->flags ) == 1 )
	{
		static FREQ freq = FREQ_UNKNOWN;

		freq = ( freq % ( FREQ_END - 1 ) ) + 1;

		pAdf_struct->scan_ch_slot.ch = freq;
		pAdf_struct->scan_ch_slot.slot = 0;
	}

	/*	Scan	*/
	if( test_and_clear_bit( FLAG_SCAN, & pAdf_struct->flags ) == 1 )
	{
		if ( pAdf_struct->scan_ch_slot.slot != 0 )
		{
			if( pAdf_struct->scan_result_ch_slot.ch_slot == INVALID_CH_SLOT )
			{
				pAdf_struct->scan_result_ch_slot = adf_assign_slot_freq( pAdf_struct );
				if( pAdf_struct->scan_result_ch_slot.ch_slot == INVALID_CH_SLOT )
				{
					/*	NOTE: valid slot not found	*/
					pAdf_struct->scan_result_ch_slot.ch_slot = 0x0;
				}
			}
			adf_notify( pAdf_struct, NOTIFY_SCAN, ( const unsigned long )&pAdf_struct->scan_result_ch_slot );
		}
		pAdf_struct->scan_ch_slot.ch_slot = INVALID_CH_SLOT;
		pAdf_struct->scan_result_ch_slot.ch_slot = INVALID_CH_SLOT;
	}

	if( pAdf_struct->scan_ch_slot.ch_slot != INVALID_CH_SLOT )
	{
		set_bit( FLAG_SCAN, & pAdf_struct->flags );
		pAdf_struct->scan_result_ch_slot.ch_slot = INVALID_CH_SLOT;
	}

	/*	TODO:	slot 0 processing and start slot_timer
	 *	1. check CONST / TONE: push command into queue head.
	 *	2. Priority: PAIRING > NICK / TONE > other Global commands.
	 *	- Global Command processing: PF_CMD_PAIRING, PF_CMD_SLEEP_MODE, Sharing
	 *	- Check cmd_list and transmit Global Command.
	 *	*/
	check_eCollar( true );

	pCmd = ( RF_COMMAND * )list_find_data( &pAdf_struct->cmd_list, LIST_KEY_GLOBAL_CMD );
	if( test_bit( FLAG_PAIRING, & pAdf_struct->flags ) == 1 )
	{
		pf_cmd_gen( pAdf_struct, & cmd, NULL, PF_CMD_PAIRING );
		if( pCmd != NULL )
		{
			PF_TX_COMMAND *pTxCmd = ( PF_TX_COMMAND * )( pCmd->packetData );
			uint32_t keyMask = LIST_KEY_GLOBAL_CMD | LIST_ID_BROADCAST;
			if( pTxCmd->command == get_eCollar_cmd() )
			{
				keyMask = LIST_KEY_E_COLLAR | LIST_KEY_GLOBAL_CMD | pCmd->id;
			}

			if( ( pTxCmd->command == PF_CMD_SHARE_DOG_INFO ) || ( pTxCmd->command == PF_CMD_SHARE_HUNTER_INFO ) )
			{
				keyMask = LIST_KEY_SHARE | LIST_KEY_GLOBAL_CMD | pCmd->id;
				list_push( &pAdf_struct->cmd_list, pCmd, sizeof( RF_COMMAND ), keyMask );
			}
			else
			{
				list_push( &pAdf_struct->delayed_cmd_list, pCmd, sizeof( RF_COMMAND ), keyMask );
			}
		}
	}
	else if( test_bit( FLAG_SHARING_TX, & pAdf_struct->flags ) == 1 )
	{
		RF_COMMAND *pRfCmd = NULL;
		PF_TX_COMMAND *pTxCmd = NULL;
		uint32_t keyMask = 0x0;

		//ASSERT( pCmd != NULL );

		if( pCmd != NULL )
		{
			memcpy( &cmd, pCmd, sizeof( RF_COMMAND ) );

			pTxCmd = ( PF_TX_COMMAND * )( pCmd->packetData );

			if( ( pTxCmd->command == PF_CMD_SHARE_DOG_INFO ) || ( pTxCmd->command == PF_CMD_SHARE_HUNTER_INFO ) )
			{
				pTxCmd->retry_cnt++;
				if( pTxCmd->retry_cnt < LIMIT_SHARE_TX_COUNT )
				{
					keyMask = LIST_KEY_SHARE | LIST_KEY_GLOBAL_CMD | pCmd->id;
					list_push_head( &pAdf_struct->cmd_list, pCmd, sizeof( RF_COMMAND ), keyMask );
				}
				else
				{
					test_and_clear_bit( FLAG_SHARING_TX, & pAdf_struct->flags );
					test_and_clear_bit( FLAG_SHARING_INFO, & pAdf_struct->flags );
					test_and_clear_bit( FLAG_SHARING_RX_ACK, & pAdf_struct->flags );
					kfree( pCmd );
					pCmd = NULL;
				}
			}
			else
			{
				kfree( pCmd );
				pCmd = NULL;

				pCmd = ( RF_COMMAND * )list_find_data( &pAdf_struct->cmd_list, LIST_KEY_SHARE );
				if( pCmd != NULL )
				{
					pTxCmd = ( PF_TX_COMMAND * )( pCmd->packetData );

					/* NOTE: push to cmd_list: next SHARE_TX
					 *       push to delayed_cmd_list: SHARE_TX after process command( maybe e-collar command )
					 *       */
					pTxCmd->retry_cnt++;
					if( pTxCmd->retry_cnt < LIMIT_SHARE_TX_COUNT )
					{
						keyMask = LIST_KEY_SHARE | LIST_KEY_GLOBAL_CMD | pCmd->id;
						list_push_head( &pAdf_struct->cmd_list, pCmd, sizeof( RF_COMMAND ), keyMask );
					}
					else
					{
						test_and_clear_bit( FLAG_SHARING_TX, & pAdf_struct->flags );
						test_and_clear_bit( FLAG_SHARING_INFO, & pAdf_struct->flags );
						test_and_clear_bit( FLAG_SHARING_RX_ACK, & pAdf_struct->flags );
						kfree( pCmd );
						pCmd = NULL;
					}

					if( pCmd != NULL )
					{
						RF_COMMAND *pTmpCmd = ( RF_COMMAND * )kzalloc( sizeof( RF_COMMAND ), GFP_KERNEL );
						if( pTmpCmd == NULL )
						{
							ERR_ADF( "kzalloc failed!!!!\n" );
						}

						memcpy( pTmpCmd, pCmd, sizeof( RF_COMMAND ) );
						list_push_head( &pAdf_struct->delayed_cmd_list, pTmpCmd, sizeof( RF_COMMAND ), keyMask );
					}
				}
			}
		}
		else
		{
			pCmd = ( RF_COMMAND * )list_find_data( &pAdf_struct->cmd_list, LIST_KEY_SHARE );
			if( pCmd != NULL )
			{
				memcpy( &cmd, pCmd, sizeof( RF_COMMAND ) );

				pTxCmd = ( PF_TX_COMMAND * )( pCmd->packetData );

				pTxCmd->retry_cnt++;
				if( pTxCmd->retry_cnt < LIMIT_SHARE_TX_COUNT )
				{
					keyMask = LIST_KEY_SHARE | LIST_KEY_GLOBAL_CMD | pCmd->id;
					list_push_head( &pAdf_struct->cmd_list, pCmd, sizeof( RF_COMMAND ), keyMask );
				}
				else
				{
					test_and_clear_bit( FLAG_SHARING_TX, & pAdf_struct->flags );
					test_and_clear_bit( FLAG_SHARING_INFO, & pAdf_struct->flags );
					test_and_clear_bit( FLAG_SHARING_RX_ACK, & pAdf_struct->flags );
					kfree( pCmd );
					pCmd = NULL;
				}
			}
		}
	}
	else if( test_bit( FLAG_SHARING_RX, & pAdf_struct->flags ) == 1 )
	{
		if( test_bit( FLAG_SHARING_INFO, & pAdf_struct->flags ) == 1 )
		{
			adf_sharing( pAdf_struct );
		}
		else
		{
			cmd.mode = ADF_MODE_RX;
			cmd.freq = FREQ_GLOBAL;
			cmd.length = get_command_len( PF_CMD_SHARE_DOG_INFO ) + 1;
			cmd.response_length = 0;
			cmd.id = 0x0;
			cmd.timeout = 0;
			cmd.timeout = RX_SHARE_DURATION;

			/* NOTE: If reference time not initialized then RX until arrive SHARE_INFO.
			 * */
			if( pAdf_struct->init_reference == false )
			{
				timer_stop( ( void * )( & pAdf_struct->reference_timer ) );
				timer_stop( ( void * )( & pAdf_struct->slot_timer ) );

				pAdf_struct->slot_seq = FLAG_SLOT_END;
				pAdf_struct->reference_timestamp.tv64 = 0LL;
				pAdf_struct->reference_next_timestamp.tv64 = 0LL;

				cmd.timeout = -1;	//	No timeout
			}

			if( pCmd != NULL )
			{
				PF_TX_COMMAND *pTxCmd = ( PF_TX_COMMAND * )( pCmd->packetData );
				uint32_t keyMask = LIST_KEY_GLOBAL_CMD | LIST_ID_BROADCAST;
				if( pTxCmd->command == get_eCollar_cmd() )
				{
					keyMask = LIST_KEY_E_COLLAR | LIST_KEY_GLOBAL_CMD | pCmd->id;
				}
				list_push( &pAdf_struct->delayed_cmd_list, pCmd, sizeof( RF_COMMAND ), keyMask );
			}
			//INFO_ADF( "SHARING_RX\n");
		}
	}
	else	/*	No Pairing, No sharing	*/
	{
		if( pCmd != NULL )
		{
			memcpy( &cmd, pCmd, sizeof( RF_COMMAND ) );
			kfree( pCmd );
		}
	}

	mode = adf_mode();
	adf_mode_change( & cmd );

#ifdef __DEBUG_ADF_TIME__
	DEBUG_ADF_TIME( "MODE_CHANGE: slot: %d Now[ %9ld.%06ld ] mode: %d => %d\n", slot, tv.tv_sec, tv.tv_usec, mode, adf_mode() );
#else	//	__DEBUG_ADF_TIME__
	//INFO_TIMER( "REF_%d: pid: %d ts: %lld diff from ref: %ld delta: %8ld\n", slot, current->pid, ktime_to_us( now ),
	//		( long )ktime_us_delta( now, pAdf_struct->reference_timestamp ),
	//		( long )ktime_us_delta( pAdf_struct->reference_next_timestamp, now ) );
#if 0
	{
		//struct timeval tv;
		//do_gettimeofday( &tv );
		INFO_TIMER( "REF_%02d: now: [ %09ld.%06ld ] pid %d delta: %ld scan[ s %d f %d ]\n", slot, \
				tv.tv_sec, tv.tv_usec, current->pid, delta, pAdf_struct->scan_ch_slot.slot, pAdf_struct->scan_ch_slot.ch );
	}
#endif
#endif	//	__DEBUG_ADF_TIME__

#ifdef __TEST_POLL__
#if 0
	for( slot = 0 ; slot < 64 ; slot++ )
#endif
	{
		push_test_packet();
	}
#endif	//	__TEST_POLL__

	return;
}

static enum hrtimer_restart slot_handler( const struct hrtimer *pTimer )
{
	ADF_STRUCT	* pAdf_struct = NULL;

	ASSERT( pTimer != NULL );

	pAdf_struct = container_of( pTimer, ADF_STRUCT, slot_timer );

	queue_work( pAdf_struct->pSlot_wq, & pAdf_struct->slot_work );

	return HRTIMER_NORESTART;
}

static void slot( struct work_struct * pWork )
{
	ADF_STRUCT	* pAdf_struct = NULL;
	//ktime_t		now = ktime_get();
	int			slot = -1;
	long		delta = SLOT_DURATION;
	RF_COMMAND cmd = { ADF_MODE_SLEEP, FREQ_UNKNOWN, 0x0, 0, 0, { 0x0, }, 0, 0 };
	ADF_MODE	mode;
	bool		res = false;
	RF_COMMAND *pCmd = NULL;

#ifdef __DEBUG_ADF_TIME__
	struct timeval tv;
	do_gettimeofday( &tv );
#endif	//	__DEBUG_ADF_TIME__

	ASSERT( pWork != NULL );

	pAdf_struct = container_of( pWork, ADF_STRUCT, slot_work );

	slot = slot_sequence( pAdf_struct );
	delta = adf_sync_slot_time( pWork, slot );

	ASSERT( ( slot > FLAG_SLOT_R_0 ) && ( slot < FLAG_SLOT_END ) );

	if( slot < ( FLAG_SLOT_END - 1 ) )
	{
		timer_start( ( void * )( & pAdf_struct->slot_timer ), delta );
	}

	if( pAdf_struct->init_reference == false )
	{
		if( adf_init_reference( pAdf_struct ) == true )
		{
			return ;
		}
	}
	mode = adf_mode();

	/*	NOTE:	SLOT_R_1 / R_2 / SLOT_0 ~ SLOT_21
	 *	1. get Slot information from list
	 *	2. if not found => SLEEP
	 *	3. RX and update information	*/
	check_eCollar( false );

	/*	Dog slot	*/
	if( slot <= FLAG_SLOT_R_2 )
	{
		/*	NOTE: pair response and Local command processing
		 *	- Check cmd_list and transmit CHANGE_XXX command.
		 *	- Max 3 or 4 command processing per slot time
		 *	  - TX header( 8 bytes ) + dummy(0xAA) + cmd_len( 8 bytes ) = 15 bytes < 13 ms
		 *	  - TX + response( RX ) < 13 + 40 ms = 53 ms.
		 *	PF_CMD_PAIR( response ACK ): Highest Priority
		 *	PF_CMD_NICK_REQ( response ACK ), PF_CMD_CONST pr PF_CMD_TONE( response ACK )
		 *	PF_CMD_NICK_ONE( response ACK )
		 *	PF_CMD_CHANGE_PERIOD_REQ, PF_CMD_CHANGE_SLOT_REQ
		 *	PF_CMD_DELETE_DOG: Lowest Priority
		 *
		 *	- 1st command in cmd_list transmit.
		 *	  the other commands enqueue to delayed_cmd_list. these whill be checked at TX_DONE.
		 *	*/
		if( test_bit( FLAG_PAIRING, & pAdf_struct->flags ) == 1 )
		{
			/*	Check PAIR_RESPONSE and PAIR_ACK	*/
			res = adf_pairing( pAdf_struct );
		}

		pCmd = ( RF_COMMAND * )list_find_data( &pAdf_struct->cmd_list, LIST_KEY_LOCAL_CMD );
		if( pCmd != NULL )
		{
			/*	NOTE: if the command have response, push again into TX list HEAD
			 *	with Response command: re-push command into queue.
			 *                         check response flag and delete command in queue;
			 *	*/
			bool needRetry = ( pCmd->response_length > 0 ) ? true : false;
			bool sendCmd = true;
			PF_TX_COMMAND *pTxCmd = ( PF_TX_COMMAND * )( pCmd->packetData );

			if( test_bit( FLAG_NEED_RETRY, & pAdf_struct->flags ) == 1 )
			{
				needRetry = true;

				/*	send command every 4 seconds.	*/
				if( ( slot != FLAG_SLOT_R_1 ) || ( ( pTxCmd->retry_cnt % 2 ) != 0 ) )
				{
					sendCmd = false;
				}
			}

			//if( ( needRetry == true ) && ( pTxCmd->command == get_eCollar_cmd() ) )
			if( pTxCmd->command == get_eCollar_cmd() )
			{
				needRetry = false;
			}

			if( needRetry == true )
			{
				uint32_t keyMask = LIST_KEY_LOCAL_CMD | pCmd->id;

				if( ( pTxCmd->command == PF_CMD_NICK_ONE ) || ( pTxCmd->command == get_eCollar_cmd() ) )
				{
					keyMask |= LIST_KEY_NICK_ACK;
				}
				else
				{
					/*	INFO: CHANGE_SLOT_FREQ, CHANGE_PERIOD, DELETE_DOG	*/
					keyMask |= LIST_KEY_CHANGE;
				}
#if 1
				if( pTxCmd->retry_cnt < LIMIT_TX_CMD_COUNT )
				{
					if( sendCmd == true )
					{
						INFO_ADF( "HEAD: cmd: 0x%02x retry_cnt: %d\n", pTxCmd->command, pTxCmd->retry_cnt );
					}
					/*	increase count every 2 seconds.	*/
					if( slot == FLAG_SLOT_R_1 )	pTxCmd->retry_cnt++;
					list_push_head( &pAdf_struct->cmd_list, pCmd, sizeof( RF_COMMAND ), keyMask );
				}
				else
				{
					INFO_ADF( "FREE: L_CMD: cmd: 0x%02x id: 0x%04x retry_cnt: %d\n", \
							pTxCmd->command, pCmd->id, pTxCmd->retry_cnt );
					test_and_clear_bit( FLAG_NEED_RETRY, & pAdf_struct->flags );
					list_remove( &pAdf_struct->cmd_list, LIST_KEY_CHANGE );
					{
						int changing_slot_index = adf_find_slot_changing( pCmd->id );

						if( ( changing_slot_index >= 0 ) && ( changing_slot_index < MAX_SLOT_INDEX ) )
						{
							if( adf_slot_is_listening( changing_slot_index ) == false )
							{
								adf_clear_slot_info( pAdf_struct, changing_slot_index );
							}
							else
							{
								adf_slot_set_mode( pAdf_struct, changing_slot_index, SLOT_MODE_LISTEN );
							}
						}
					}
					kfree( pCmd );
					pCmd = NULL;
				}

				if( res == false )
				{
					if( ( pCmd != NULL ) && ( sendCmd == true ) )
					{
						memcpy( &cmd, pCmd, sizeof( RF_COMMAND ) );
					}
				}
#endif
			}
			else
			{
				if( res == false )
				{
					memcpy( &cmd, pCmd, sizeof( RF_COMMAND ) );
					kfree( pCmd );
				}
				else
				{
					uint32_t keyMask = LIST_KEY_LOCAL_CMD | pCmd->id;
					if( pTxCmd->command == get_eCollar_cmd() )
					{
						keyMask |= LIST_KEY_E_COLLAR;
					}
					//INFO_ADF( "PUSH_TX %d: id: 0x%04x keyMask: 0x%08x len: %d\n", slot, pCmd->id, keyMask, pCmd->length );
					list_push( &pAdf_struct->delayed_cmd_list, pCmd, sizeof( RF_COMMAND ), keyMask );
				}
			}
		}

		/* NOTE: Process SHARING.
		 * Do NOT use pairing with sharing...
		 * */
		if( test_bit( FLAG_PAIRING, & pAdf_struct->flags ) == 0 )
		{
			res = adf_sharing( pAdf_struct );
			//if( test_bit( FLAG_SHARING_TX, & pAdf_struct->flags ) == 1 )
			if( test_bit( FLAG_SHARING_RX_ACK , & pAdf_struct->flags ) == 1 )
			{
				uint32_t keyMask = LIST_KEY_SHARE | LIST_KEY_LOCAL_CMD | LIST_ID_BROADCAST;
				RF_COMMAND *pTmpCmd = ( RF_COMMAND * )kzalloc( sizeof( RF_COMMAND ), GFP_KERNEL );

				if( pTmpCmd == NULL )
				{
					ERR_ADF( "kzalloc failed!!!!\n" );
				}
				else
				{
					pTmpCmd->mode = ADF_MODE_RX;
					pTmpCmd->freq = FREQ_GLOBAL;
					pTmpCmd->length = get_command_len( PF_CMD_SHARE_ACK ) + 1;
					pTmpCmd->response_length = 0;
					pTmpCmd->id = 0x0;
					pTmpCmd->timeout = 0;
					pTmpCmd->timeout = RX_SHARE_DURATION;

					if( res == false && cmd.mode == ADF_MODE_SLEEP )
					{
						memcpy( &cmd, pTmpCmd, sizeof( RF_COMMAND ) );
						kfree( pTmpCmd );
						pTmpCmd = NULL;
					}
					else
					{
						list_push_head( &pAdf_struct->delayed_cmd_list, pTmpCmd, sizeof( RF_COMMAND ), keyMask );
					}
					//INFO_ADF( "SHARING Wait ACK\n");
				}
			}
			else if( test_bit( FLAG_SHARING_TX, & pAdf_struct->flags ) == 1 )
			{
				uint32_t keyMask = LIST_KEY_SHARE | LIST_KEY_LOCAL_CMD | LIST_ID_BROADCAST;
				RF_COMMAND *pTmpCmd = ( RF_COMMAND * )kzalloc( sizeof( RF_COMMAND ), GFP_KERNEL );

				if( pTmpCmd == NULL )
				{
					ERR_ADF( "kzalloc failed!!!!\n" );
				}
				else
				{
					pTmpCmd->mode = ADF_MODE_RX;
					pTmpCmd->freq = FREQ_GLOBAL;
					pTmpCmd->length = get_command_len( PF_CMD_SHARE_HUNTER_INFO ) + 1;
					pTmpCmd->response_length = 0;
					pTmpCmd->id = 0x0;
					pTmpCmd->timeout = 0;
					pTmpCmd->timeout = RX_SHARE_DURATION;

					if( res == false && cmd.mode == ADF_MODE_SLEEP )
					{
						memcpy( &cmd, pTmpCmd, sizeof( RF_COMMAND ) );
						kfree( pTmpCmd );
						pTmpCmd = NULL;
					}
					else
					{
						list_push_head( &pAdf_struct->delayed_cmd_list, pTmpCmd, sizeof( RF_COMMAND ), keyMask );
					}
				}
				INFO_ADF( "s: %d SHARING Wait HUNTER_INFO\n", adf_current_slot() );
			}
		}
		adf_mode_change( & cmd );
	}
	else	//	Dog slot processing
	{
		uint16_t id = adf_get_slot_info_id( slot - FLAG_SLOT_0 );

		/*	slot - FLAG_SLOT_0: slot for dog...	*/
		if( id != 0x0 )
		{
			if( adf_slot_is_listening( slot - FLAG_SLOT_0 ) == true )
			{
				/*	TODO: check remain time.	*/
				//slot_update_remain( pAdf_struct, slot - FLAG_SLOT_0 );
				if( slot_update_remain( pAdf_struct, slot - FLAG_SLOT_0 ) == true )
				{
					/*	NOTE: if eCollar is CONST => eCollar slot skip GPS RX	*/
					bool isECollarSlot = ( get_eCollar_slot_index() == ( slot - FLAG_SLOT_0 ) ) ? true : false;
					bool isConstSlot = ( isECollarSlot == true ) \
									   && ( test_bit( FLAG_CONST, &pAdf_struct->flags ) == 1 );
					if( isConstSlot == false )
					//if( isECollarSlot == false )
					{
						cmd.id = id;
						cmd.mode = ADF_MODE_RX;
						cmd.freq = adf_get_slot_info_freq( slot - FLAG_SLOT_0 );
						cmd.length = get_command_len( PF_CMD_GPS_INFO ) + 1;
						//cmd.timeout = RX_LONG_DURATION;
						//INFO_ADF( "SLOT_%d: id: 0x%08x freq: %d mode: %d => %d\n", slot,
						//		pAdf_struct->slot_info[ slot - FLAG_SLOT_0 ].id, cmd.freq, mode, cmd.mode );
#ifdef __STATISTICS__
						pAdf_struct->slot_info[ slot - FLAG_SLOT_0 ].rx_stat.total++;
#endif	//	__STATISTICS__
					}
				}
			}
			else
			{
				/*	TODO: connector slot: TX for SHARING	*/
				/*	slot pairing failed.	*/
				if( adf_slot_get_mode( slot - FLAG_SLOT_0 ) == SLOT_MODE_PAIRING )
				{
					INFO_ADF( "slot %d ---> maybe pairing failed\n", slot - FLAG_SLOT_0 );
#if 1
					{
						int assign_slot = adf_find_pairing_slot( pAdf_struct->slot_info );
						if( assign_slot >= 0 )
						{
							adf_slot_set_mode( pAdf_struct, assign_slot, SLOT_MODE_LISTEN );
							clear_bit( FLAG_PAIRING, & pAdf_struct->flags );
							if( test_and_clear_bit( FLAG_SCAN, & pAdf_struct->flags ) == 1 )
							{
								pAdf_struct->scan_ch_slot.ch_slot = INVALID_CH_SLOT;
								pAdf_struct->scan_result_ch_slot.ch_slot = INVALID_CH_SLOT;
							}

							{
								int *pSlotIndex = ( int * )kzalloc( sizeof( int ), GFP_KERNEL );
								if( pSlotIndex == NULL )
								{
									ERR_ADF( "ERROR [%s,%d] kzalloc failed!!!!\n", __func__, __LINE__ );
									//return ;
								}
								*pSlotIndex = assign_slot;
								list_push_head( &pAdf_struct->pair_list, pSlotIndex, sizeof( int ), LIST_KEY_PAIR( *pSlotIndex ) );
							}
							adf_notify( pAdf_struct, NOTIFY_ADD_DEVICE, adf_get_slot_info_id( assign_slot ) );
						}
						NOTICE_ADF( "SLOT_%d: force PAIR [ assign_slot: %d freq: %d ]\n", \
								pAdf_struct->slot_seq, assign_slot, pAdf_struct->slot_info[ assign_slot ].freq );
					}
#else
					//if( adf_pairing( pAdf_struct ) == false )
					{
						adf_clear_slot_info( pAdf_struct, slot - FLAG_SLOT_0 );
					}
#endif
				}
			}
		}
		else
		{
			if( test_bit( FLAG_SCAN, & pAdf_struct->flags ) == 1 )
			{
				FREQ freq = pAdf_struct->scan_ch_slot.ch;

				if( freq != FREQ_UNKNOWN )
				{
					//if( slot_map[ freq - 1 ][ slot - FLAG_SLOT_0 ] == 0x0 )
					{
						cmd.id = LIST_ID_BROADCAST;
						cmd.mode = ADF_MODE_RX;
						cmd.freq = freq;
						cmd.length = get_command_len( PF_CMD_GPS_INFO ) + 1;
						//id = 0x0;
						//INFO_ADF( "SCAN [ s %d f %d ] mode: %d => %d\n", slot - FLAG_SLOT_0 + 1, freq, mode, cmd.mode );
					}
				}
			}
		}

		if( test_bit( FLAG_E_COLLAR, &pAdf_struct->flags ) == 1 )
		{
			pCmd = ( RF_COMMAND * )list_find_data( &pAdf_struct->cmd_list, LIST_KEY_E_COLLAR );
			if( pCmd != NULL )
			{
				PF_TX_COMMAND *pTxCmd = ( PF_TX_COMMAND * )( pCmd->packetData );
				if( cmd.mode == ADF_MODE_SLEEP )
				{
					memcpy( &cmd, pCmd, sizeof( RF_COMMAND ) );
					kfree( pCmd );
				}
				else
				{
					uint32_t keyMask = LIST_KEY_LOCAL_CMD | pCmd->id;
					if( pTxCmd->command == get_eCollar_cmd() )
					{
						keyMask |= LIST_KEY_E_COLLAR;
					}
					//INFO_ADF( "PUSH_TX %d: id: 0x%04x cmd: 0x%02x keyMask: 0x%08x len: %d\n", slot, pCmd->id,
					//		pTxCmd->command, keyMask, pCmd->length );
					list_push( &pAdf_struct->delayed_cmd_list, pCmd, sizeof( RF_COMMAND ), keyMask );
				}
			}
		}

		adf_mode_change( & cmd );
#ifdef __DEBUG_ADF_TIME__
		if( adf_slot_is_listening( slot - FLAG_SLOT_0 ) == true )
		{
			DEBUG_ADF_TIME( "MODE_CHANGE: slot: %d Now[ %9ld.%06ld ] mode: %d\n", slot, tv.tv_sec, tv.tv_usec, mode, adf_mode() );
		}

#endif	//	__DEBUG_ADF_TIME__
	}

	DEBUG_TIMER( "SLOT_%02d: pid %d ts: %lld diff: %8ld\n", slot, current->pid, ktime_to_us( now ), \
			( long )ktime_us_delta( now, pAdf_struct->reference_timestamp ) );
#if 0
	if( slot < FLAG_SLOT_1 )
	{
		//struct timeval tv;
		//do_gettimeofday( &tv );
		INFO_TIMER( "SLOT_%02d: now: [ %09ld.%06ld ] pid %d delta: %d\n", slot, \
				tv.tv_sec, tv.tv_usec, current->pid, delta );
	}
#endif

#ifdef __TEST_POLL__
#if 0
	for( slot = 0 ; slot < 64 ; slot++ )
#endif
	{
		push_test_packet();
	}
#endif	//	__TEST_POLL__

	return;
}

static bool forced_rf_done( void )
{
	int ret = -1;
	ADF_STRUCT *pAdf_struct = &s_adf_struct;
	ADF_PACKET *pPacket = pAdf_struct->pHandled_packet;

	/*	NOTE: if RF is working, then forced RF_DONE and wait until rf_done work.	*/
	if(	test_bit( FLAG_RF_DONE, & pAdf_struct->flags ) == 1 )
	{
		INFO_ADF( "RF is not working now!!!\n" );
		return true;
	}

	if( pPacket == NULL ) return false;

	/*	forced RF_DONE	*/
	set_bit( FLAG_FORCE_RF_DONE, & pAdf_struct->flags );
	//*( uint32_t * )( pPacket->packetData ) = 0x12345678;
	pPacket->bit_index = pPacket->length;

	/*	wait for RF_DONE work	*/
	ret = wait_event_interruptible_timeout( pAdf_struct->rf_done_waitq, \
			test_bit( FLAG_RF_DONE, & pAdf_struct->flags ), msecs_to_jiffies( 40 ) );

	INFO_ADF( "FORCED_RF_DONE: ret: %d\n", ret );

	return true;
}

static enum hrtimer_restart timeout_handler( const struct hrtimer *pTimer )
{
	ADF_STRUCT	* pAdf_struct = NULL;

	ASSERT( pTimer != NULL );

	pAdf_struct = container_of( pTimer, ADF_STRUCT, timeout_timer );

	//adf_disable_irq( pAdf_struct );
	adf_disable_irq_nosync( pAdf_struct );

	queue_work( pAdf_struct->pTimeout_wq, & pAdf_struct->timeout_work );

	return HRTIMER_NORESTART;
}

#ifdef __RF_TEST__
static enum hrtimer_restart test_handler( const struct hrtimer *pTimer )
{
	ADF_STRUCT	* pAdf_struct = NULL;

	ASSERT( pTimer != NULL );

	pAdf_struct = container_of( pTimer, ADF_STRUCT, test_timer );

	timer_start( ( void * )( & s_adf_struct.test_timer), s_test_duration );
	queue_work( pAdf_struct->pTest_wq, & pAdf_struct->test_work );

	return HRTIMER_NORESTART;
}

static void test_func( struct work_struct * pWork )
{
	ADF_STRUCT	* pAdf_struct = NULL;
	RF_COMMAND cmd = { ADF_MODE_SLEEP, FREQ_UNKNOWN, 0x0, 0, 0, { 0x0, }, 0, 0 };
	static uint32_t cnt = 0;

	ASSERT( pWork != NULL );

	pAdf_struct = container_of( pWork, ADF_STRUCT, timeout_work );

	ASSERT( pAdf_struct != NULL );
	//ASSERT( pAdf_struct->pHandled_packet != NULL );

	//if( ( cnt % 2 ) == 0 )
	{
		cmd.mode = ADF_MODE_TX;
		cmd.freq = s_test_freq;
		cmd.timeout = s_test_duration * 90 / 100;	//	90 %
		pf_cmd_gen( pAdf_struct, & cmd, NULL, PF_CMD_TX_TEST );
		//pf_cmd_gen( pAdf_struct, & cmd, NULL, PF_CMD_PAIRING );
		adf_mode_change( & cmd );
	}
	//else
	{
		//INFO_ADF( "TEST: cnt: %d length: %d\n", cnt, cmd.length );
	}
	DEBUG_ADF( "TEST: cnt: %d length: %d timeout: %d\n", cnt, cmd.length, cmd.timeout );
	cnt++;

	return;
}

#endif	//	__RF_TEST__

static void print_slot_map( void )
{
	int slot, freq;

	printk( "\n---------------------------------------\n");
	printk( "     Frequency Slot Mapping\n");
	printk( "---------------------------------------\n");
	printk( "          fr_1  fr_2  fr_3  fr_4  fr_5\n");
	printk( "---------------------------------------\n");
	for( slot = 0 ; slot < MAX_SLOT_INDEX ; slot++ )
	{
		printk( "slot_%02d :", slot + 1 );
		for( freq = 0 ; freq < ( FREQ_END - 1) ; freq ++ )
		{
			if( slot_map[ freq ][ slot ] == 0x0 )
			{
				printk( " ---- " );
			}
			else
			{
				printk( " %04x ", slot_map[ freq ][ slot ] );
			}
		}
		printk("\n" );
	}
	printk( "---------------------------------------\n");

	return;
}

#ifdef __STATISTICS__
static void print_slot_info( void )
{
	int slot;
	//int64_t rate;

	printk( "\n---------------------------------------\n");
	printk( "     Slot Information\n");
	printk( "---------------------------------------\n");
	printk( "          id  freq  period   mode   rx: total   sucess      err     fail\n");
	printk( "---------------------------------------\n");
	for( slot = 0 ; slot < MAX_SLOT_INDEX ; slot++ )
	{
		s_adf_struct.slot_info[ slot ].rx_stat.fail = \
			s_adf_struct.slot_info[ slot ].rx_stat.total - s_adf_struct.slot_info[ slot ].rx_stat.sucess \
			- s_adf_struct.slot_info[ slot ].rx_stat.err;
		//rate = ( ( s_adf_struct.slot_info[ slot ].rx_stat.sucess * 1000 ) / s_adf_struct.slot_info[ slot ].rx_stat.total );

		printk( "slot_%02d :", slot + 1 );
		printk( "%04x  ", s_adf_struct.slot_info[ slot ].id );
		printk( "%2d   ", s_adf_struct.slot_info[ slot ].freq );
		printk( "%2d ", s_adf_struct.slot_info[ slot ].period );
		printk( "%12s ", adf_slot_mode_str( s_adf_struct.slot_info[ slot ].mode ) );
		printk( "%9d ", s_adf_struct.slot_info[ slot ].rx_stat.total );
		printk( "%9d ", s_adf_struct.slot_info[ slot ].rx_stat.sucess );
		printk( "%9d ", s_adf_struct.slot_info[ slot ].rx_stat.err );
		printk( "%9d ", s_adf_struct.slot_info[ slot ].rx_stat.fail );
		//printk( "%2.02f ", rate );
		printk("\n" );
	}
	printk( "---------------------------------------\n");

	return;
}

#endif	//	__STATISTICS__

bool adf_set_connector_slot_info( const CONNECTOR_INFO * const pConnectorInfo )
{
	int slot_index = -1;
	ADF_SLOT_INFO slotInfo;
	const CONNECTOR_INFO * const pPrevInfo = get_connector_info();
	ADF_STRUCT *pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );

	ASSERT( pConnectorInfo != NULL );

	slot_map[ pPrevInfo->freq - 1 ][ pPrevInfo->slot - 1 ] = 0x0;

	memset( &slotInfo, 0x0, sizeof( ADF_SLOT_INFO ) );

	slot_index = pConnectorInfo->slot - 1;

	slotInfo.id = pConnectorInfo->id;
	slotInfo.freq = pConnectorInfo->freq;
	slotInfo.period = pConnectorInfo->period;
	//slotInfo.period_remain = 1;
	slotInfo.shared = false;
	slotInfo.mode = SLOT_MODE_ME;
	slotInfo.last_rx_time.tv_sec = 0;
	slotInfo.last_rx_time.tv_usec = 0;

	adf_clear_slot_info( pAdf_struct, pPrevInfo->slot - 1 );
	adf_set_slot_info( pAdf_struct, slot_index, &slotInfo );

	slot_map[ slotInfo.freq - 1 ][ slot_index ] = slotInfo.id;

	return true;
}

static int adf_init( void )
{
	int ret, idx;
	ADF_STRUCT		* pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );

	if( test_and_set_bit( FLAG_ADF_ENABLED, &pAdf_struct->flags ) == 1 )
	{
		NOTICE_ADF( "Already Enabled!!!!\n" );
		return 0;
	}

	pAdf_struct->mode = ADF_MODE_INIT;
	pAdf_struct->pHandled_packet = NULL;
	pAdf_struct->last_pairing_slot_index = -1;

	set_bit( FLAG_RF_DONE, & pAdf_struct->flags );
	pAdf_struct->tmp_cnt = 0;
	pAdf_struct->wait_cnt = 0;

	pAdf_struct->slot_seq = FLAG_SLOT_END;
	pAdf_struct->reference_timestamp.tv64 = 0LL;
	pAdf_struct->reference_next_timestamp.tv64 = 0LL;

	pAdf_struct->init_reference = false;
	pAdf_struct->init_reference_timestamp.tv64 = 0ULL;

	pAdf_struct->scan_ch_slot.ch_slot  = INVALID_CH_SLOT;
	pAdf_struct->scan_result_ch_slot.ch_slot = INVALID_CH_SLOT;

#ifdef __DEBUG_QUEUE_CNT__
	pAdf_struct->alloc_cnt = 0;
	pAdf_struct->dealloc_cnt = 0;
#endif	//	__DEBUG_QUEUE_CNT__

	for( idx = 0 ; idx < MAX_SLOT_INDEX ; idx ++ )
	{
		mutex_init( & pAdf_struct->slot_mutex[ idx ] );
		adf_clear_slot_info( pAdf_struct, idx );
	}
	mutex_init( & pAdf_struct->eCollar_mutex );
	clear_eCollar_info();

	set_connector_info_nation( NATION_US );
	set_connector_info( get_connector_info() );

	adf_load_slot_map_file();

	ret = gpio_init( ( void * )( pAdf_struct ) );
	if( ret < 0 )
	{
		ERR_ADF( "Initialize ADF GPIO failed\n" );
		return ret;
	}
#if 1
    pAdf_struct->pTimeout_wq = alloc_workqueue( "timeout_wq", WQ_HIGHPRI, 0 );
    if( pAdf_struct->pTimeout_wq == NULL )
	{
        return -ENOMEM;
	}
    pAdf_struct->pSlot_wq = alloc_workqueue( "slot_wq", WQ_HIGHPRI, 0 );
    if( pAdf_struct->pSlot_wq == NULL )
	{
        return -ENOMEM;
	}
#ifdef __RF_TEST__
    pAdf_struct->pTest_wq = alloc_workqueue( "timeout_wq", WQ_HIGHPRI, 0 );
    if( pAdf_struct->pTest_wq == NULL )
	{
        return -ENOMEM;
	}
#endif	//	__RF_TEST__

#else
    pAdf_struct->pTimeout_wq = create_singlethread_workqueue( "timeout_wq" );
    if( pAdf_struct->pTimeout_wq == NULL )
	{
        return -ENOMEM;
	}
    pAdf_struct->pSlot_wq = create_singlethread_workqueue( "slot_wq" );
    if( pAdf_struct->pSlot_wq == NULL )
	{
        return -ENOMEM;
	}
#endif

	/*	LIST	*/
	list_init();
	list_alloc( & pAdf_struct->cmd_list, "CMD_LIST" );
	list_alloc( & pAdf_struct->delayed_cmd_list, "DELAYED_LIST" );
	list_alloc( & pAdf_struct->rx_list, "RX_LIST" );
	list_alloc( & pAdf_struct->pair_list, "PAIR_LIST" );
	list_alloc( & pAdf_struct->dump_list, "DUMP_LIST" );

#ifdef __DUMP_PACKET__
	s_dump_mode = PF_DUMP_ALL;
#endif	//	__DUMP_PACKET__

	INIT_WORK( & pAdf_struct->timeout_work, timeout );
	INIT_WORK( & pAdf_struct->rf_done_work, rf_done );
	INIT_DELAYED_WORK( & pAdf_struct->rf_done_delayed_work, rf_done );

	INIT_WORK( & pAdf_struct->ref_slot_work, ref_slot );
	INIT_WORK( & pAdf_struct->slot_work, slot );

	/*	Lock and wait queue		*/

	spin_lock_init( & pAdf_struct->spinlock );
	mutex_init( & pAdf_struct->alloc_mutex );

    init_waitqueue_head( & pAdf_struct->rf_done_waitq );
    init_waitqueue_head( & pAdf_struct->poll_waitq );

	/*	Timers	*/
	timer_init();
	timer_register( ( void * )( & pAdf_struct->reference_timer ), TYPE_HRTIMER, "ADF_REF_TIMER", reference_handler );
	timer_register( ( void * )( & pAdf_struct->slot_timer ), TYPE_HRTIMER, "ADF_SLOT_TIMER", slot_handler );
	timer_register( ( void * )( & pAdf_struct->timeout_timer ), TYPE_HRTIMER, "ADF_TIMEOUT_HRTIMER", timeout_handler );
#ifdef __RF_TEST__
	INIT_WORK( & pAdf_struct->test_work, test_func );
	timer_register( ( void * )( & pAdf_struct->test_timer ), TYPE_HRTIMER, "ADF_TEST_HRTIMER", test_handler );
#endif	//	__RF_TEST__
	//NOTICE_ADF( "syncword: SYNCWORD 0x%08x htonl 0x%08x\n", PF_SYNC_WORD, htonl( ( ( PF_SYNC_WORD ) << 8 ) | 0xAA ) );

	NOTICE_ADF( "Enabled OK!!!\n" );

	return 0;
}

static void adf_exit( void )
{
	ADF_STRUCT		* pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );
	int idx;
#ifdef __DEBUG_ADF_TIME__
	struct timeval tv;
#endif	//	__DEBUG_ADF_TIME__

	if( test_and_clear_bit( FLAG_ADF_ENABLED, &pAdf_struct->flags ) == 0 )
	{
		NOTICE_ADF( "Already Disabled!!!!\n" );
		return ;
	}

	gpio_set_value( CE_GPIO_NUM, 0 );

	timer_deregister( ( void * )( & pAdf_struct->reference_timer ) );
	timer_deregister( ( void * )( & pAdf_struct->slot_timer ) );
	timer_deregister( ( void * )( & pAdf_struct->timeout_timer ) );
#ifdef __RF_TEST__
	timer_deregister( ( void * )( & pAdf_struct->test_timer ) );

	flush_workqueue( pAdf_struct->pTest_wq );
	destroy_workqueue( pAdf_struct->pTest_wq );
#endif	//	__RF_TEST__

	flush_workqueue( pAdf_struct->pTimeout_wq );
	destroy_workqueue( pAdf_struct->pTimeout_wq );

	flush_workqueue( pAdf_struct->pSlot_wq );
	destroy_workqueue( pAdf_struct->pSlot_wq );

	adf_packet_dealloc( pAdf_struct );

#ifdef __STATISTICS__
	print_slot_info();
#endif	//	__STATISTICS__

	for( idx = 0 ; idx < MAX_SLOT_INDEX ; idx ++ )
	{
		adf_clear_slot_info( pAdf_struct, idx );
		mutex_destroy( & pAdf_struct->slot_mutex[ idx ] );
	}
	mutex_destroy( & pAdf_struct->alloc_mutex );
	mutex_destroy( & pAdf_struct->eCollar_mutex );

#ifdef __DUMP_PACKET__
	print_dump_list();
#endif	//	__DUMP_PACKET__
	list_free( & pAdf_struct->cmd_list );
	list_free( & pAdf_struct->delayed_cmd_list );
	list_free( & pAdf_struct->rx_list );
	list_free( & pAdf_struct->pair_list );
	list_free( & pAdf_struct->dump_list );

	gpio_exit( ( void * )( pAdf_struct ) );

#ifdef __DEBUG_ADF_TIME__
	do_gettimeofday( &tv );
	INFO_ADF( "EXIT: Now[ %9ld.%06ld ] irq_cnt: %d\n", tv.tv_sec, tv.tv_usec, pAdf_struct->irq_cnt );
#endif	//	__DEBUG_ADF_TIME__
	NOTICE_ADF( "Disabled!!!!\n" );

	memset( pAdf_struct, 0x0, sizeof( ADF_STRUCT ) );

	adf_update_connector_info_file();
	adf_update_slot_map_file();

#ifdef __DUMP_PACKET__
	s_dump_mode = PF_DUMP_NONE;
#endif	//	__DUMP_PACKET__

	return;
}

static int __init adf7021_init( void )
{
	int ret;

	uint16_t id;
	CONNECTOR_INFO connector_info;

	memset( slot_map, 0x0, sizeof( slot_map ) );

	id = adf_load_connector_info_file( &connector_info );
	//set_connector_info_name( "JYHUH_PFHH" );

    ret = misc_register( & adf_misc_dev );
    if( ret )
	{
		ERR_ADF( "Register ADF MISC failed\n" );
		return ret;
    }

#ifdef __RF_TEST__
	//if( s_adf_struct.init.work_mode != ADF_WORK_NORMAL )
	if( s_work_mode != ADF_WORK_NORMAL )
	{
		RF_COMMAND cmd = { ADF_MODE_INIT, FREQ_UNKNOWN, 0x0, 0, 0, { 0x0, }, 0, 0 };

		s_adf_struct.init.rf_mode = ADF_RF_SPI;
		s_adf_struct.init.syncword = PF_SYNC_WORD;
		s_adf_struct.init.max_packet_length = MAX_PACKET_LEN;
		s_adf_struct.init.work_mode = s_work_mode;
		adf_init();
		adf_reg_init( s_adf_struct.regs );

		switch( s_adf_struct.init.work_mode )
		{
			case ADF_WORK_TEST_TX :
				{
					adf_mode_change( & cmd );
					timer_start( ( void * )( & s_adf_struct.test_timer), s_test_duration );
					break;
				}
			case ADF_WORK_TEST_RX :
				{
					cmd.mode = ADF_MODE_RX;
					cmd.freq = s_test_freq;
					//cmd.length = get_command_len( PF_CMD_RX_TEST );
					cmd.length = DUMP_PACKET_LEN;
					cmd.timeout = -1;	//	No timeout.
					adf_mode_change( & cmd );
					break;
				}
			default : break;
		}
	}
#endif	//	__RF_TEST__

	NOTICE_ADF( "INIT OK!!! connector id: 0x%04x\n", get_connector_info()->id );
	DEBUG_ADF( "sizeof ADF_STRUCT: %d regs: %d work_struct: %d hrtimer: %d\n", sizeof( ADF_STRUCT ), \
			sizeof( s_adf_struct.regs ), sizeof( struct work_struct ), sizeof( struct hrtimer )  );
	NOTICE_ADF( "TX strength: %d\n", s_tx_strength );
#ifdef __RF_TEST__
	{
		PF_TX_COMMAND txCmd;
		PF_RX_COMMAND rxCmd;
		int tx_time = MAX_PACKET_LEN * 8 / 10;	//	usecs
		int sleep_time;
		if( tx_time > ( s_test_duration * 90 / 100 ) )
		{
			tx_time = ( s_test_duration * 90 / 100 );
		}
		sleep_time = s_test_duration - tx_time;
		INFO_ADF( "TEST: work_mode: %d %s\n", s_adf_struct.init.work_mode, \
				adf_work_mode_str( s_adf_struct.init.work_mode ) );
		INFO_ADF( "TEST: data size: tx %d, rx %d\n", sizeof( txCmd.data ), sizeof( rxCmd.data ) );
		INFO_ADF( "TEST: frequency: %d\n", s_test_freq );
		INFO_ADF( "TEST: duration: %d => TX %d ms | sleep %d ms |...\n", s_test_duration, \
				tx_time, sleep_time );
	}
#endif	//	__RF_TEST__

	return 0;
}

static void __exit adf7021_exit( void )
{
	//ADF_STRUCT		* pAdf_struct = ( ADF_STRUCT * )( & s_adf_struct );

	adf_exit();
    misc_deregister( & adf_misc_dev );

	NOTICE_ADF( "EXIT OK!!!\n" );

	return;
}

module_init( adf7021_init );
module_exit( adf7021_exit );

MODULE_AUTHOR( "Jaeyeong Huh <jyhuh@dogtra.com>" );
MODULE_DESCRIPTION( "ADF702x RF driver for Path Finder" );
MODULE_LICENSE( "GPL" );
