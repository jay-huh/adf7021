#ifndef	__QUEUE_H__
#define	__QUEUE_H__

#define		MAX_QUEUE			4
#define		QUEUE_NAME_LEN		32

typedef enum NODE_TYPE_S
{
	NODE_TYPE_CHAR	= 0,
	NODE_TYPE_SHORT,
	NODE_TYPE_INT,

	NODE_TYPE_STRUCT,

	NODE_TYPE_END
} NODE_TYPE;

typedef struct QUEUE_S
{
	char			name[ QUEUE_NAME_LEN ];
	struct kfifo	* pQueue;

	NODE_TYPE		node_type;
	size_t			node_size;
	size_t			node_max;
	size_t			size;		//	in bytes

	spinlock_t		spinlock;
} QUEUE;

int queue_init( void );
int queue_alloc( const struct kfifo * pFifo, const char * pName, \
		NODE_TYPE node_type, size_t node_size, size_t node_max );
int queue_free( const struct kfifo * pFifo );

size_t enqueue( const struct kfifo * pFifo, const void * pNode );
size_t dequeue( const struct kfifo * pFifo, void * pNode );

bool is_empty( const struct kfifo * pFifo );
bool is_full( const struct kfifo * pFifo );
#endif	//	__QUEUE_H__
