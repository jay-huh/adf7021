#include <linux/module.h>
#include <linux/kernel.h>

#include <linux/list.h>
#include <linux/slab.h>			//	kzalloc

#include <mach/platform.h>
#include <mach/devices.h>

#include	"list.h"

#include	"common.h"

static LIST		*_pList[ MAX_LIST ] = { NULL, };
static DEFINE_MUTEX( lock );

static int find_list_by_ptr( const struct list_head * pList )
{
	int idx;

	mutex_lock( & lock );

	for( idx = 0 ; idx < MAX_LIST ; idx ++ )
	{
		if( _pList[ idx ] != NULL )
		{
			if( _pList[ idx ]->pHead == pList )
			{
				mutex_unlock( & lock );
				return idx;
			}
		}
	}

	mutex_unlock( & lock );

	return -1;
}

int list_init( void )
{
	int idx;

	mutex_lock( & lock );

	for( idx = 0 ; idx < MAX_LIST ; idx ++ )
	{
		_pList[ idx ] = NULL;
	}

	mutex_unlock( & lock );

	return 0;
}

int list_alloc( const struct list_head * pList, const char * pName )
{
	int				idx;

	ASSERT( pList != NULL );

	mutex_lock( & lock );

	for( idx = 0 ; idx < MAX_LIST ; idx ++ )
	{
		if( _pList[ idx ] == NULL ) break;
	}

	if( idx == MAX_LIST )
	{
		mutex_unlock( & lock );
		return -1;
	}

	_pList[ idx ] = ( LIST * )kzalloc( sizeof( LIST ), GFP_KERNEL );
	if( _pList[ idx ] == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		mutex_unlock( & lock );
		return -ENOMEM;
	}

	if( pName != NULL )
	{
		memcpy( _pList[ idx ]->name, pName, LIST_NAME_LEN );
	}
	else
	{
		memcpy( _pList[ idx ]->name, "No_Name", LIST_NAME_LEN );
	}
	INIT_LIST_HEAD( ( struct list_head * )pList );
	_pList[ idx ]->pHead = ( struct list_head * )pList;

	spin_lock_init( & _pList[ idx ]->spinlock );
	_pList[ idx ]->count = 0;

	INFO_LIST( "[ LIST_%d ] ALLOC: name: %s\n", idx, _pList[ idx ]->name );

	mutex_unlock( & lock );

	return 0;
}

bool list_free( const struct list_head *pList )
{
	LIST_NODE	*pNode = NULL;
	LIST_NODE	*pTmpNode;
	int			idx = -1;
	unsigned long	flags;

	ASSERT( pList != NULL );

	idx = find_list_by_ptr( pList );
	if( idx < 0 ) return -1;

	spin_lock_irqsave( & _pList[ idx ]->spinlock, flags );

	list_for_each_entry_safe( pNode, pTmpNode, _pList[ idx ]->pHead, list )
	{
		INFO_LIST( "LIST_%d: DEL: pNode: %p size: %d key: 0x%08x\n", idx, pNode, pNode->size, pNode->key );
		list_del( &pNode->list );
		_pList[ idx ]->count--;
		if( pNode->pData != NULL )
		{
			kfree( pNode->pData );
			pNode->pData = NULL;
		}
		kfree( pNode );
		pNode = NULL;
	}
	spin_unlock_irqrestore( & _pList[ idx ]->spinlock, flags );

	NOTICE_ADF( "[ LIST_%d ] FREE: name: %s is_empty: %d\n", idx, _pList[ idx ]->name, \
			list_is_empty( _pList[ idx ]->pHead ) );

	mutex_lock( & lock );

	kfree( _pList[ idx ] );
	_pList[ idx ] = NULL;

	mutex_unlock( & lock );

	return true;
}

bool list_add_node( const struct list_head *pList, const void * pData, size_t size, const uint32_t key, const bool toHead )
{
	LIST_NODE	*pNode = NULL;
	int			idx = -1;
	unsigned long	flags;

	ASSERT( pList != NULL );

	idx = find_list_by_ptr( pList );
	if( idx < 0 ) return false;

	pNode = ( LIST_NODE * )kzalloc( sizeof( LIST_NODE ), GFP_KERNEL );
	if( pNode == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		return -ENOMEM;
	}
	pNode->pData = ( void * )pData;
	pNode->size = size;
	pNode->key = key;
	INIT_LIST_HEAD( &pNode->list );

	spin_lock_irqsave( & _pList[ idx ]->spinlock, flags );

	if( toHead == true )
	{
		list_add( & pNode->list, _pList[ idx ]->pHead );
	}
	else
	{
		list_add_tail( & pNode->list, _pList[ idx ]->pHead );
	}
	_pList[ idx ]->count++;

	list_for_each_entry( pNode, _pList[ idx ]->pHead, list )
	{
		DEBUG_LIST( "LIST_%d: Add: pNode: %p pData: %p size: %d key: %d\n", idx, \
				pNode, pNode->pData, pNode->size, pNode->key );
	}
	spin_unlock_irqrestore( & _pList[ idx ]->spinlock, flags );

	return true;
}

int list_del_node( const struct list_head *pList, const uint32_t keyMask )
{
	LIST_NODE	*pNode = NULL;
	LIST_NODE	*pTmpNode;
	int			idx = -1;
	int			count = 0;
	unsigned long	flags;

	ASSERT( pList != NULL );

	idx = find_list_by_ptr( pList );
	if( idx < 0 ) return -1;

	spin_lock_irqsave( & _pList[ idx ]->spinlock, flags );

	list_for_each_entry_safe( pNode, pTmpNode, _pList[ idx ]->pHead, list )
	{
		if( pNode->key & keyMask )
		{
			DEBUG_LIST( "LIST_%d: DEL: pNode: %p pData: %p size: %d key: %d\n", idx, \
					pNode, pNode->pData, pNode->size, pNode->key );
			list_del( &pNode->list );
			_pList[ idx ]->count--;
			kfree( pNode );
			pNode = NULL;
			count++;
		}
	}
	spin_unlock_irqrestore( & _pList[ idx ]->spinlock, flags );

	return count;
}

LIST_NODE* list_find_node( const struct list_head *pList, const uint32_t keyMask )
{
	LIST_NODE	*pNode = NULL;
	LIST_NODE	*pTmpNode;
	int			idx = -1;
	unsigned long	flags;

	ASSERT( pList != NULL );

	idx = find_list_by_ptr( pList );
	if( idx < 0 ) return false;

	spin_lock_irqsave( & _pList[ idx ]->spinlock, flags );

	list_for_each_entry_safe( pNode, pTmpNode, _pList[ idx ]->pHead, list )
	{
		if( pNode->key & keyMask )
		{
			DEBUG_LIST( "LIST_%d: FIND: pNode: %p pData: %p size: %d key: %d\n", idx, \
					pNode, pNode->pData, pNode->size, pNode->key );
			list_del( &pNode->list );
			_pList[ idx ]->count--;

			spin_unlock_irqrestore( & _pList[ idx ]->spinlock, flags );

			return pNode;
		}
	}
	spin_unlock_irqrestore( & _pList[ idx ]->spinlock, flags );

	return NULL;
}

bool list_is_empty( const struct list_head *pList )
{
	int			idx = -1;

	ASSERT( pList != NULL );

	idx = find_list_by_ptr( pList );
	if( idx < 0 ) return true;

	return list_empty( _pList[ idx ]->pHead );
}

int list_count( const struct list_head *pList )
{
	int			idx = -1;

	ASSERT( pList != NULL );

	idx = find_list_by_ptr( pList );
	if( idx < 0 ) return -1;

	return _pList[ idx ]->count;
}

bool list_insert( const struct list_head *pList, const void * pData, size_t size, const uint32_t key )
{
	return list_add_node( pList, pData, size, key, false );
}

bool list_insert_head( const struct list_head *pList, const void * pData, size_t size, const uint32_t key )
{
	return list_add_node( pList, pData, size, key, true );
}

int list_remove( const struct list_head *pList, const uint32_t keyMask )
{
	return list_del_node( pList, keyMask );
}

bool list_push( const struct list_head *pList, const void * pData, size_t size, const uint32_t key )
{
	return list_add_node( pList, pData, size, key, false );
}

bool list_push_head( const struct list_head *pList, const void * pData, size_t size, const uint32_t key )
{
	return list_add_node( pList, pData, size, key, true );
}

LIST_NODE* list_pop( const struct list_head *pList )
{
	LIST_NODE	*pNode = NULL;
	LIST_NODE	*pTmpNode;
	int			idx = -1;
	unsigned long	flags;

	ASSERT( pList != NULL );

	idx = find_list_by_ptr( pList );
	if( idx < 0 ) return NULL;

	spin_lock_irqsave( & _pList[ idx ]->spinlock, flags );

	list_for_each_entry_safe( pNode, pTmpNode, _pList[ idx ]->pHead, list )
	{
		if( pNode != NULL )
		{
			DEBUG_LIST( "LIST_%d: POP: pNode: %p pData: %p size: %d key: %d\n", idx, \
					pNode, pNode->pData, pNode->size, pNode->key );
			list_del( &pNode->list );
			_pList[ idx ]->count--;

			spin_unlock_irqrestore( & _pList[ idx ]->spinlock, flags );

			return pNode;
		}
	}
	spin_unlock_irqrestore( & _pList[ idx ]->spinlock, flags );

	return NULL;
}

void* list_pop_data( const struct list_head *pList )
{
	void		*pData = NULL;
	LIST_NODE	*pNode = list_pop( pList );

	if( pNode == NULL ) return NULL;

	pData = pNode->pData;
	kfree( pNode );
	pNode = NULL;

	return pData;
}

void* list_find_data( const struct list_head *pList, const uint32_t keyMask )
{
	void		*pData = NULL;
	LIST_NODE	*pNode = list_find_node( pList, keyMask );

	if( pNode == NULL) return NULL;

	pData = pNode->pData;
	kfree( pNode );
	pNode = NULL;

	return pData;
}
