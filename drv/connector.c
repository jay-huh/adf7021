#include <linux/kernel.h>
#include <linux/types.h>

#include "connector.h"

#include "common.h"

#include "adf7021.h"

static CONNECTOR_INFO s_connector_info;

void print_connector_info( void )
{
	NOTICE_ADF( "CONNECTOR: id: 0x%04x [ s %d f %d p %d ] n %d name: %s\n", \
			s_connector_info.id, s_connector_info.slot, s_connector_info.freq, \
			s_connector_info.period, s_connector_info.nation, s_connector_info.name );
	return;
}

const CONNECTOR_INFO * const get_connector_info( void )
{
	return &s_connector_info;
}

void init_connector_info( const CONNECTOR_INFO * const pInfo )
{
	//memset( &s_connector_info, 0x0, sizeof( CONNECTOR_INFO ) );
	s_connector_info = *pInfo;
	print_connector_info();
	return;
}

void set_connector_info( const CONNECTOR_INFO * const pInfo )
{
	if( adf_set_connector_slot_info( pInfo ) == true )
	{
		s_connector_info = *pInfo;
	}
	print_connector_info();
	return;
}

void set_connector_info_id( const unsigned int id )
{
	ASSERT( id >= 0x8000 );
	s_connector_info.id = id;
	return;
}

void set_connector_info_period( const PERIOD period )
{
	ASSERT( ( period >= PERIOD_2_SECS ) && ( period < PERIOD_END ) );
	s_connector_info.period = period;
	return;
}

void set_connector_info_slot( const int slot_index )
{
	ASSERT( ( slot_index >= 0 ) && ( slot_index < MAX_SLOT_INDEX  ) );
	s_connector_info.slot = slot_index + 1;
	return;
}

void set_connector_info_freq( const FREQ freq )
{
	ASSERT( ( freq >= FREQ_151_82 ) && ( freq < FREQ_END ) );
	s_connector_info.freq = freq;
	return;
}

void set_connector_info_sleep( const bool sleep )
{
	s_connector_info.sleep = sleep;
	return;
}

void set_connector_info_nation( const NATION nation )
{
	ASSERT( ( nation >= NATION_US ) && ( nation < NATION_END ) );
	s_connector_info.nation = nation;
	return;
}

void set_connector_info_name( const char * const name[] )
{
	ASSERT( name != NULL );

	memcpy( s_connector_info.name, name, HUNTER_NAME_LEN );

	return;
}

