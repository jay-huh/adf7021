#include <linux/module.h>
#include <linux/kernel.h>

#include <linux/kfifo.h>
#include <linux/slab.h>			//	kzalloc

#include <mach/platform.h>
#include <mach/devices.h>

#include	"queue.h"

#include	"common.h"

/*
 *	kfifo_reset() kfifo_reset_out()
 *	kfifo_is_empty() kfifo_is_full()
 *	kfifo_avail()
 *	kfifo_from_user() kfifo_to_user()
 *
 * */
static QUEUE	* _pQueue[ MAX_QUEUE ] = { NULL, };
static DEFINE_MUTEX( lock );

static int find_queue_by_ptr( const struct kfifo * pFifo );
static int find_queue_by_type( const NODE_TYPE node_type );

static int find_queue_by_ptr( const struct kfifo * pFifo )
{
	int idx;

	mutex_lock( & lock );

	for( idx = 0 ; idx < MAX_QUEUE ; idx ++ )
	{
		if( _pQueue[ idx ] != NULL )
		{
			if( _pQueue[ idx ]->pQueue == pFifo )
			{
				mutex_unlock( & lock );
				return idx;
			}
		}
	}

	mutex_unlock( & lock );

	return -1;
}

static int find_queue_by_type( const NODE_TYPE node_type )
{
	int idx;

	mutex_lock( & lock );

	for( idx = 0 ; idx < MAX_QUEUE ; idx ++ )
	{
		if( _pQueue[ idx ] != NULL )
		{
			if( _pQueue[ idx ]->node_type == node_type )
			{
				mutex_unlock( & lock );
				return idx;
			}
		}
	}

	mutex_unlock( & lock );

	return -1;
}

int queue_init( void )
{
	int idx;

	mutex_lock( & lock );

	for( idx = 0 ; idx < MAX_QUEUE ; idx ++ )
	{
		_pQueue[ idx ] = NULL;
	}

	mutex_unlock( & lock );

	return 0;
}

int queue_alloc( const struct kfifo * pFifo, const char * pName, const NODE_TYPE node_type, const size_t node_size, const size_t node_max )
{
	int				idx;

	ASSERT( pFifo != NULL );

	mutex_lock( & lock );

	for( idx = 0 ; idx < MAX_QUEUE ; idx ++ )
	{
		if( _pQueue[ idx ] == NULL ) break;
	}

	if( idx == MAX_QUEUE )
	{
		mutex_unlock( & lock );
		return -1;
	}

	_pQueue[ idx ] = ( QUEUE * )kzalloc( sizeof( QUEUE ), GFP_KERNEL );
	if( _pQueue[ idx ] == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		mutex_unlock( & lock );
		return -ENOMEM;
	}

	if( pName != NULL )
	{
		memcpy( _pQueue[ idx ]->name, pName, QUEUE_NAME_LEN );
	}
	else
	{
		memcpy( _pQueue[ idx ]->name, "No_Name", QUEUE_NAME_LEN );
	}
	_pQueue[ idx ]->pQueue = ( struct kfifo * )pFifo;
	_pQueue[ idx ]->node_type = node_type;
	_pQueue[ idx ]->node_size = node_size;
	_pQueue[ idx ]->node_max = node_max;
	_pQueue[ idx ]->size = roundup_pow_of_two( node_size * node_max );

	if( kfifo_alloc( _pQueue[ idx ]->pQueue, _pQueue[ idx ]->size, GFP_KERNEL) )
	{
		ERR_ADF( "error kfifo_alloc \n");
		mutex_unlock( & lock );
		return -ENOMEM;
	}
	spin_lock_init( & _pQueue[ idx ]->spinlock );

	INFO_QUEUE( "[ Q_%d ] name: %s\n", idx, _pQueue[ idx ]->name );
	INFO_QUEUE( "[ Q_%d ] node_type: %d\n", idx, _pQueue[ idx ]->node_type );
	INFO_QUEUE( "[ Q_%d ] node_size: %d\n", idx, _pQueue[ idx ]->node_size );
	INFO_QUEUE( "[ Q_%d ] node_max: %d\n", idx, _pQueue[ idx ]->node_max );
	INFO_QUEUE( "[ Q_%d ] queue_size: %d\n", idx, _pQueue[ idx ]->size );
	INFO_QUEUE( "[ KFIFO ] queue size: %d\n", kfifo_size( _pQueue[ idx ]->pQueue ) );
	INFO_QUEUE( "[ KFIFO ] queue len: %d\n", kfifo_len( _pQueue[ idx ]->pQueue ) );
	INFO_QUEUE( "[ KFIFO ] queue avail: %d\n", kfifo_avail( _pQueue[ idx ]->pQueue ) );

	mutex_unlock( & lock );

	return 0;
}

int queue_free( const struct kfifo * pFifo )
{

	int		idx = -1;
	unsigned long	flags;

	idx = find_queue_by_ptr( pFifo );
	if( idx < 0 ) return -1;

	mutex_lock( & lock );

	spin_lock_irqsave( & _pQueue[ idx ]->spinlock, flags );

	INFO_QUEUE( "[ Q_%d ] name: %s\n", idx, _pQueue[ idx ]->name );
	INFO_QUEUE( "[ Q_%d ] node_type: %d\n", idx, _pQueue[ idx ]->node_type );
	INFO_QUEUE( "[ Q_%d ] node_size: %d\n", idx, _pQueue[ idx ]->node_size );
	INFO_QUEUE( "[ Q_%d ] node_max: %d\n", idx, _pQueue[ idx ]->node_max );
	INFO_QUEUE( "[ Q_%d ] queue_size: %d\n", idx, _pQueue[ idx ]->size );
	INFO_QUEUE( "[ KFIFO ] queue size: %d\n", kfifo_size( _pQueue[ idx ]->pQueue ) );
	INFO_QUEUE( "[ KFIFO ] queue len: %d\n", kfifo_len( _pQueue[ idx ]->pQueue ) );
	INFO_QUEUE( "[ KFIFO ] queue avail: %d\n", kfifo_avail( _pQueue[ idx ]->pQueue ) );

	kfifo_free( _pQueue[ idx ]->pQueue );

	spin_unlock_irqrestore( & _pQueue[ idx ]->spinlock, flags );

	kfree( _pQueue[ idx ] );
	_pQueue[ idx ] = NULL;

	mutex_unlock( & lock );

	return 0;
}

size_t enqueue( const struct kfifo * pFifo, const void * pNode )
{
	int		idx = -1;
	int		ret;

	/*	TODO:	NULL pointer Check	*/
	ASSERT( pNode != NULL );

	idx = find_queue_by_ptr( pFifo );

	if( idx < 0 ) return -1;

	ret = kfifo_in_spinlocked( _pQueue[ idx ]->pQueue, pNode, _pQueue[ idx ]->node_size, & _pQueue[ idx ]->spinlock );
	if( ret != _pQueue[ idx ]->node_size )
	{
		DEBUG_QUEUE( "[ EN_Q_%d : %s ] kfifo_in failed!! avail: %d len: %d\n", idx, _pQueue[ idx ]->name, \
				kfifo_avail( _pQueue[ idx ]->pQueue ), kfifo_len( _pQueue[ idx ]->pQueue ) );
	}
	else
	{
		DEBUG_QUEUE( "[ EN_Q_%d : %s ] pNode: %p avail: %d len: %d\n", idx, _pQueue[ idx ]->name, \
				pNode, kfifo_avail( _pQueue[ idx ]->pQueue ), kfifo_len( _pQueue[ idx ]->pQueue ) );
	}

	return 0;
}

size_t dequeue( const struct kfifo * pFifo, void * pNode )
{
	int		idx = -1;
	int		ret;

	/*	TODO:	NULL pointer Check	*/
	ASSERT( pNode != NULL );

	idx = find_queue_by_ptr( pFifo );

	if( idx < 0 ) return -1;

	ret = kfifo_out_spinlocked( _pQueue[ idx ]->pQueue, pNode, _pQueue[ idx ]->node_size, & _pQueue[ idx ]->spinlock );
	if( ret != _pQueue[ idx ]->node_size )
	{
		DEBUG_QUEUE( "[ DE_Q_%d : %s ] kfifo_in failed!! avail: %d len: %d\n", idx, _pQueue[ idx ]->name, \
				kfifo_avail( _pQueue[ idx ]->pQueue ), kfifo_len( _pQueue[ idx ]->pQueue ) );
	}
	else
	{
		DEBUG_QUEUE( "[ DE_Q_%d : %s ] pNode: %p avail: %d len: %d\n", idx, _pQueue[ idx ]->name, \
				pNode, kfifo_avail( _pQueue[ idx ]->pQueue ), kfifo_len( _pQueue[ idx ]->pQueue ) );
	}

	return ret;
}

bool is_empty( const struct kfifo * pFifo )
{
	int		idx = -1;
	//int		size;
	bool	ret = false;
	unsigned long	flags;

	idx = find_queue_by_ptr( pFifo );

	if( idx < 0 ) return -1;

	spin_lock_irqsave( & _pQueue[ idx ]->spinlock, flags );

#if 1
	ret = kfifo_is_empty( _pQueue[ idx ]->pQueue );
#else
	size = kfifo_len( _pQueue[ idx ]->pQueue );
	if( size < _pQueue[ idx ]->node_size )
	{
		ret = true;
	}
#endif

	spin_unlock_irqrestore( & _pQueue[ idx ]->spinlock, flags );

	return ret;
}

bool is_full( const struct kfifo * pFifo )
{
	int		idx = -1;
	//int		size;
	bool	ret = false;
	unsigned long	flags;

	idx = find_queue_by_ptr( pFifo );

	if( idx < 0 ) return -1;

	spin_lock_irqsave( & _pQueue[ idx ]->spinlock, flags );

#if 1
	ret = kfifo_is_full( _pQueue[ idx ]->pQueue );
#else
	size = kfifo_avail( _pQueue[ idx ]->pQueue );
	if( size < _pQueue[ idx ]->node_size )
	{
		ret = true;
	}
#endif

	spin_unlock_irqrestore( & _pQueue[ idx ]->spinlock, flags );

	return ret;
}
