#ifndef	__ADF7021_REG_H__
#define	__ADF7021_REG_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum READBACK_MODE_E
{
	READBACK_AFC_WORD = 0x0,
	READBACK_ADC_OUTPUT,
	READBACK_FILTER_CAL,
	READBACK_SILICON_REV,

	READBACK_MODE_END
} READBACK_MODE;

typedef enum READBACK_ADC_MODE_E
{
	READBACK_ADC_RSSI = 0x0,
	READBACK_ADC_BATTERY,
	READBACK_ADC_TEMP,
	READBACK_ADC_TO_EXT_PIN,

	READBACK_ADC_MODE_END
} READBACK_ADC_MODE;

#ifdef	__KERNEL__

#define		MAX_ADF_REG_INDEX		( 16 )
#define		ADF_REG_READBACK_INDEX	( 7 )

typedef union ADF_READBACK_U
{
	uint16_t value;

	struct
	{
		uint16_t value		:	7;
		uint16_t filter_gain:	2;
		uint16_t LNA_gain	:	2;
		uint16_t reserved	:	5;
	} RSSI;

} ADF_READBACK;

typedef union ADF_REG_U
{
	uint32_t data;

	struct
	{
		uint32_t ADDRESS	:	4;
		uint32_t DIVIDE_FRAC:	15;
		uint32_t DIVIDE_INT	:	8;
		uint32_t TX_RX		:	1;		//	TX: 0, RX: 1
		uint32_t UART_MODE	:	1;		//	SPI / UART: 1
		uint32_t MUX_OUT	:	3;
	} REG_0;

	struct
	{
		uint32_t ADDRESS		:	4;
		uint32_t R_COUNTER		:	3;
		uint32_t CLKOUT_DIVIDE	:	4;
		uint32_t XTAL_DOUBLER	:	1;
		uint32_t XOSC_ENABLE	:	1;
		uint32_t XTAL_BIAS		:	2;
		uint32_t CP_CURRENT		:	2;
		uint32_t VCO_ENABLE		:	1;
		uint32_t RF_DIVIDE_BY_2	:	1;
		uint32_t VCD_BIAS		:	4;
		uint32_t VCO_ADJUST		:	2;
		uint32_t VCO			:	1;
		uint32_t RESERVED		:	6;
	} REG_1;

	struct
	{
		uint32_t ADDRESS		:	4;
		uint32_t MOD_SCHEME		:	3;
		uint32_t PA_ENABLE		:	1;
		uint32_t PA_RAMP		:	3;
		uint32_t PA_BIAS		:	2;
		uint32_t PA_LEVEL		:	6;
		uint32_t TX_DEVIATION	:	9;
		uint32_t TX_DATA_INVERT	:	2;
		uint32_t R_COSINE_ALPHA	:	1;
		uint32_t RESERVED		:	1;
	} REG_2;

	struct
	{
		uint32_t ADDRESS		:	4;
		uint32_t BBOS_CLK_DIVIDE:	2;
		uint32_t DEM_CLK_DIVIDE	:	4;
		uint32_t CDR_CLK_DIVIDE	:	8;
		uint32_t SEQ_CLK_DIVIDE	:	8;
		uint32_t AGC_CLK_DIVIDE	:	6;
	} REG_3;

	struct
	{
		uint32_t ADDRESS		:	4;
		uint32_t DEMOD_SCHEME	:	3;
		uint32_t DOT_PRODUCT	:	1;
		uint32_t RX_INVERT		:	2;
		uint32_t DISCRIM_BW		:	10;
		uint32_t POST_DEMOD_BW	:	10;
		uint32_t IF_BW			:	2;
	} REG_4;

	struct
	{
		uint32_t ADDRESS			:	4;
		uint32_t IF_CAL				:	1;
		uint32_t IF_FILTER_DIVIDE	:	9;
		uint32_t IF_FILTER_ADJUST	:	6;
		uint32_t IR_PHASE_ADJUST	:	4;
		uint32_t IR_PHASE_ADJUST_IQ	:	1;
		uint32_t IR_GAIN_ADJUST		:	5;
		uint32_t IR_GAIN_ADJUST_IQ	:	1;
		uint32_t IR_GAIN_ADJUST_UD	:	1;
	} REG_5;

	struct
	{
		uint32_t ADDRESS				:	4;
		uint32_t IF_FINE_CAL			:	1;
		uint32_t IF_CAL_LOWER_DIVIDE	:	8;
		uint32_t IF_CAL_UPPER_DIVIDE	:	8;
		uint32_t IF_CAL_DWELL_TIME		:	7;
		uint32_t IF_CAL_SRC_DRV_LEVEL	:	2;
		uint32_t IF_CAL_SRC_DIVIDE_2	:	1;
		uint32_t RESERVED				:	1;
	} REG_6;

	struct
	{
		uint32_t ADDRESS				:	4;
		uint32_t ADC_MODE				:	2;
		uint32_t READBACK_MODE			:	2;
		uint32_t READBACK_ENABLE		:	1;
		uint32_t RESERVED				:	24;
	} REG_7;

	struct
	{
		uint32_t ADDRESS				:	4;
		uint32_t SYNTH_ENABLE			:	1;
		uint32_t RESERVED_0				:	1;
		uint32_t LNA_MIXER_ENABLE		:	1;
		uint32_t FILTER_ENABLE			:	1;
		uint32_t ADC_ENABLE				:	1;
		uint32_t DEMOD_ENABLE			:	1;
		uint32_t LOG_AMP_ENABLE			:	1;
		uint32_t TX_RX_SWITCH			:	1;
		uint32_t PA_ENABLE				:	1;
		uint32_t RX_RESET				:	2;
		uint32_t COUNTER_RESET			:	1;
		uint32_t RESERVED				:	16;
	} REG_8;

	struct
	{
		uint32_t ADDRESS				:	4;
		uint32_t AGC_LOW_THRESHOLD		:	7;
		uint32_t AGC_HIGH_THRESHOLD		:	7;
		uint32_t AGC_MODE				:	2;
		uint32_t LNA_GAIN				:	2;
		uint32_t FILTER_GAIN			:	2;
		uint32_t FILTER_CURRENT			:	1;
		uint32_t LNA_MODE				:	1;
		uint32_t LNA_CURRENT			:	2;
		uint32_t MIXER_LINEARITY		:	1;
		uint32_t RESERVED				:	3;
	} REG_9;

	struct
	{
		uint32_t ADDRESS				:	4;
		uint32_t AFC_ENABLE				:	1;
		uint32_t AFC_SCALING_FACTOR		:	12;
		uint32_t KI						:	4;
		uint32_t KP						:	3;
		uint32_t MAX_AFC_RANGE			:	8;
	} REG_10;

	struct
	{
		uint32_t ADDRESS				:	4;
		uint32_t SYNC_BYTE_LENGTH		:	2;
		uint32_t MATCHING_TOLERANCE		:	2;
		uint32_t SYNC_BYTE_SEQUENCE		:	24;
	} REG_11;

	struct
	{
		uint32_t ADDRESS				:	4;
		uint32_t LOCK_THRESHOLD_MODE	:	2;
		uint32_t SWD_MODE				:	2;
		uint32_t DATA_PACKET_LENGTH		:	8;
		uint32_t RESERVED				:	16;
	} REG_12;

	struct
	{
		uint32_t ADDRESS				:	4;
		uint32_t SLICER_THRESHOLD		:	7;
		uint32_t FSK_3_VITERBI_DETECTOR	:	1;
		uint32_t PHASE_CORRECTION		:	1;
		uint32_t VITERBI_PATH_MEMORY	:	2;
		uint32_t FSK_3_CDR_THRESHOLD	:	7;
		uint32_t FSK_3_PREAMBLE_TIME	:	4;
		uint32_t RESERVED				:	6;
	} REG_13;

	struct
	{
		uint32_t ADDRESS				:	4;
		uint32_t TEST_DAC_EN			:	1;
		uint32_t TEST_DAC_OFFSET		:	16;
		uint32_t TEST_DAC_GAIN			:	4;
		uint32_t ED_PEAK_RESPONSE		:	2;
		uint32_t ED_LEAK_FACTOR			:	3;
		uint32_t PULSE_EXTENSION		:	2;
	} REG_14;

	struct
	{
		uint32_t ADDRESS				:	4;
		uint32_t RX_TEST_MODES			:	4;
		uint32_t TX_TEST_MODES			:	3;
		uint32_t DELTA_SIGMA_TEST_MODES	:	3;
		uint32_t PFD_CP_TEST_MODES		:	3;
		uint32_t CLK_MUX				:	3;		//	SPI: 7( TxRxCLK )
		uint32_t PLL_TEST_MODES			:	4;
		uint32_t ANALOG_TEST_MODES		:	4;
		uint32_t FORCE_LD_HIGH			:	1;
		uint32_t REG_1_PD				:	1;
		uint32_t CAL_OVERRIDE			:	2;
	} REG_15;

} ADF_REG;

#endif	//	__KERNEL__

#ifdef __cplusplus
}
#endif

#endif	//	__ADF7021_REG_H__
