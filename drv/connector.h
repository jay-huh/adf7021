#ifndef __CONNECTOR_H__
#define __CONNECTOR_H__

#ifdef __cplusplus
extern "C" {
#endif

#define ID_FILE_NAME		"/data/misc/adf/id"

#define HUNTER_NAME_LEN		( 14 )
#define	HUNTER_ID( id )		( id |= 0x8000 )
#define	COLLAR_ID( id )		( id &= ~0x8000 )

#define	IS_HUNTER( id )		( ( (id) & 0x8000 ) ? true : false )

typedef enum FREQ_E
{
	FREQ_UNKNOWN = 0,
	FREQ_151_82 = 1,
	FREQ_GLOBAL = FREQ_151_82,
	FREQ_151_88 = 2,
	FREQ_151_94,
	FREQ_154_57,
	FREQ_154_60,

	FREQ_END
} FREQ;

typedef enum PERIOD_E
{
	PERIOD_UNKNOWN = 0,	//	Not Use...
	PERIOD_2_SECS = 1,
	PERIOD_6_SECS,
	PERIOD_10_SECS,
	PERIOD_30_SECS,
	PERIOD_120_SECS,

	PERIOD_END
} PERIOD;

typedef enum NATION_E
{
	NATION_UNKNOWN = -1,
	NATION_US = 1,
	NATION_EU = 2,

	NATION_END
} NATION;

/*
CONNECTOR_INFO
--------------------
int id;
int gpsRate;
int slotId;
int frequenceId;
int sleepMode;
int nation;
*/
typedef struct CONNECTOR_INFO_S
{
	unsigned int	id;
	PERIOD			period;			//	1 ~ 5: 2, 6, 10, 30 and 120 secs.
	int				slot;			//	1 ~ 22
	FREQ			freq;			//	1 ~ 5
	bool			sleep;
	NATION			nation;
	uint8_t			name[ HUNTER_NAME_LEN ];
} CONNECTOR_INFO;

#ifdef	__KERNEL__

void print_connector_info( void );
void init_connector_info( const CONNECTOR_INFO * const pInfo );
void set_connector_info( const CONNECTOR_INFO * const pInfo );
const CONNECTOR_INFO * const get_connector_info( void );
void set_connector_info_period( const PERIOD period );
void set_connector_info_slot( const int slot_index );
void set_connector_info_freq( const FREQ freq );
void set_connector_info_sleep( const bool sleep );
void set_connector_info_nation( const NATION nation );
void set_connector_info_name( const char * const name[] );

#endif	//	__KERNEL__

#ifdef __cplusplus
}
#endif

#endif	/*	__CONNECTOR_H__	*/
