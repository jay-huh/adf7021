#ifndef __COMMON_H__
#define __COMMON_H__

#ifndef __RELEASE__

#define	ASSERT( expr ) \
	do { \
		if( expr ) break; \
		printk( KERN_EMERG "ASSERT!! [ %s : %s : %d ] ( %s ) is false\n", \
				__FILE__, __func__, __LINE__, #expr ); \
		dump_stack(); BUG(); \
	} while( 0 )

#ifdef	__DEBUG_ADF__
#define DEBUG_ADF( fmt, ... )    \
    printk( KERN_DEBUG "[ ADF: %12s() : %4d ] DBG\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#else	//	__DEBUG__
#define DEBUG_ADF( fmt, ... )
#endif	//	__DEBUG__
#define INFO_ADF( fmt, ... )    \
    printk( KERN_INFO "[ ADF: %12s() : %4d ] p: %d INFO\t | " fmt, __FUNCTION__, __LINE__, current->pid, ##__VA_ARGS__ )

#define DEFAULT_ADF( fmt, ... )    \
    printk( KERN_DEFAULT "[ ADF: %12s() : %4d ] DEFAULT\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

#ifdef	__DEBUG_LIST__
#define DEBUG_LIST( fmt, ... )    \
    printk( KERN_INFO "[ LIST: %12s() : %4d ] DBG\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#else	//	__DEBUG__
#define DEBUG_LIST( fmt, ... )
#endif	//	__DEBUG__

#define INFO_LIST( fmt, ... )    \
    printk( KERN_INFO "[ LIST: %12s() : %4d ] INFO\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

#ifdef	__DEBUG_TIMER__
#define DEBUG_TIMER( fmt, ... )    \
    printk( KERN_INFO "[ TIMER: %12s() : %4d ] DBG\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#else	//	__DEBUG__
#define DEBUG_TIMER( fmt, ... )
#endif	//	__DEBUG__

#define INFO_TIMER( fmt, ... )    \
    printk( KERN_INFO "[ TIMER: %12s() : %4d ] INFO\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

#ifdef	__DEBUG_QUEUE__
#define DEBUG_QUEUE( fmt, ... )    \
    printk( KERN_INFO "[ QUEUE: %12s() : %4d ] DBG\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#else	//	__DEBUG__
#define DEBUG_QUEUE( fmt, ... )
#endif	//	__DEBUG__

#define INFO_QUEUE( fmt, ... )    \
    printk( KERN_INFO "[ QUEUE: %12s() : %4d ] INFO\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

#ifdef	__DEBUG_ADF_TIME__
#define DEBUG_ADF_TIME( fmt, ... )    \
    printk( KERN_INFO "[ ADF_TIME: %12s() : %4d ] DBG\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#else	//	__DEBUG__
#define DEBUG_ADF_TIME( fmt, ... )
#endif	//	__DEBUG__

#else	//	__RELEASE__

#define	ASSERT( expr )

#define INFO_ADF( fmt, ... )
#define DEBUG_ADF( fmt, ... )
#define DEFAULT_ADF( fmt, ... )

#define DEBUG_LIST( fmt, ... )
#define INFO_LIST( fmt, ... )
#define DEBUG_TIMER( fmt, ... )
#define INFO_TIMER( fmt, ... )
#define DEBUG_QUEUE( fmt, ... )
#define INFO_QUEUE( fmt, ... )
#define DEBUG_ADF_TIME( fmt, ... )

#endif	//	__RELEASE__

#define EMERG_ADF( fmt, ... )    \
    printk( KERN_EMERG "[ ADF: %12s() : %4d ] EMEMG\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#define ALERT_ADF( fmt, ... )    \
    printk( KERN_ALERT "[ ADF: %12s() : %4d ] ALERT\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#define CRIT_ADF( fmt, ... )    \
    printk( KERN_CRIT "[ ADF: %12s() : %4d ] CRIT\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#define ERR_ADF( fmt, ... )    \
    printk( KERN_ERR "[ ADF: %12s() : %4d ] p: %d ERR\t | " fmt, __FUNCTION__, __LINE__, current->pid, ##__VA_ARGS__ )
#define WARN_ADF( fmt, ... )    \
    printk( KERN_WARNING "[ ADF: %12s() : %4d ] p: %d WARN\t | " fmt, __FUNCTION__, __LINE__, current->pid, ##__VA_ARGS__ )
#define NOTICE_ADF( fmt, ... )    \
    printk( KERN_NOTICE "[ ADF: %12s() : %4d ] NOTICE\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

#define WARN_LIST( fmt, ... )    \
    printk( KERN_WARNING "[ LIST: %12s() : %4d ] WARN\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

#define WARN_TIMER( fmt, ... )    \
    printk( KERN_INFO "[ TIMER: %12s() : %4d ] WARN\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

#define WARN_QUEUE( fmt, ... )    \
    printk( KERN_WARNING "[ QUEUE: %12s() : %4d ] WARN\t | " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

#endif	//	__COMMON_H__
