#ifndef	__GPIO_H__
#define	__GPIO_H__

/*	GPIO	*/
#ifdef	__PARON__
#define		CE_GPIO_NUM			( PAD_GPIO_C + 25 )		//	ADF_CE

#define		SCLK_GPIO_NUM		( PAD_GPIO_C + 0 )		//	SPI_SCLK
#define		SDATA_GPIO_NUM		( PAD_GPIO_C + 2 )		//	SPI_MOSI
#define		SREAD_GPIO_NUM		( PAD_GPIO_C + 1 )		//	SPI_MISO
#define		SLE_GPIO_NUM		( PAD_GPIO_C + 27 )		//	GPIO OUT

#define		CLKOUT_GPIO_NUM		( PAD_GPIO_C + 9 )		//	SPI_SCLK
#define		TXRX_CLK_GPIO_NUM	( PAD_GPIO_C + 11 )		//	SPI_MISO
#define		TXRX_DATA_GPIO_NUM	( PAD_GPIO_C + 12 )		//	SPI_MOSI
#define		SWD_GPIO_NUM		( PAD_GPIO_C + 10 )		//	GPIO IRQ

#else

#define		CE_GPIO_NUM			( PAD_GPIO_E + 16 )		//	ADF_CE

#define		SCLK_GPIO_NUM		( PAD_GPIO_E + 14 )		//	SPI_SCLK
#define		SDATA_GPIO_NUM		( PAD_GPIO_E + 19 )		//	SPI_MOSI
#define		SREAD_GPIO_NUM		( PAD_GPIO_E + 18 )		//	SPI_MISO
#define		SLE_GPIO_NUM		( PAD_GPIO_E + 15 )		//	GPIO OUT

#define		CLKOUT_GPIO_NUM		( PAD_GPIO_C + 9 )		//	SPI_SCLK
#define		TXRX_CLK_GPIO_NUM	( PAD_GPIO_C + 12 )		//	SPI_MISO
#define		TXRX_DATA_GPIO_NUM	( PAD_GPIO_C + 11 )		//	SPI_MOSI
#define		SWD_GPIO_NUM		( PAD_GPIO_C + 10 )		//	GPIO IRQ

#define		TX_EN				( PAD_GPIO_E + 20 )
#define		RX_EN				( PAD_GPIO_E + 12 )
#endif

int gpio_init( void * pData );
void gpio_exit( void * pData );

extern irqreturn_t clkout_handler( int irq, void * pData );
extern irqreturn_t swd_handler( int irq, void * pData );

#endif	/*	__GPIO_H__	*/
