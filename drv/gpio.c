#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>

#include <mach/platform.h>
#include <mach/devices.h>

#include	"gpio.h"

#include	"common.h"

/**
 * struct gpio - a structure describing a GPIO with configuration
 * @gpio:	the GPIO number
 * @flags:	GPIO configuration as specified by GPIOF_*
 * @label:	a literal description string of this GPIO
 */
static struct gpio adf_gpio[] = {
	{ CE_GPIO_NUM,			GPIOF_OUT_INIT_LOW,	"ADF_CE" },

	/*	Serial Interface: Data Sampling at Rising edge of SCLK	*/
	{ SCLK_GPIO_NUM,		GPIOF_OUT_INIT_LOW,	"ADF_SCLK" },
	{ SDATA_GPIO_NUM,		GPIOF_OUT_INIT_LOW,	"ADF_SDATA" },
	{ SREAD_GPIO_NUM,		GPIOF_IN,			"ADF_SREAD" },
	{ SLE_GPIO_NUM,			GPIOF_OUT_INIT_LOW,	"ADF_SLE" },

	/*	RF Data: Data Sampling at Rising edge of CLKOUT or TXRX_CLK	*/
#if 1
	{ CLKOUT_GPIO_NUM,		GPIOF_IN,			"ADF_CLKOUT" },		//	SCLK
	{ TXRX_CLK_GPIO_NUM,	GPIOF_OUT_INIT_LOW,	"ADF_TXRX_CLK" },	//	MISO
	{ TXRX_DATA_GPIO_NUM,	GPIOF_IN,			"ADF_TXRX_DATA" },	//	MOSI
#endif
	{ SWD_GPIO_NUM,			GPIOF_IN,			"ADF_SWD" },	//	IRQ
#ifndef	__PARON__
	{ TX_EN,				GPIOF_OUT_INIT_LOW,	"ADF_TX_EN" },
	{ RX_EN,				GPIOF_OUT_INIT_LOW,	"ADF_RX_EN" },
#endif	//	__PARON__
};

int gpio_init( void * pData )
{
	int ret;
	ret = gpio_request_array( adf_gpio, ARRAY_SIZE( adf_gpio ) );
	if( ret < 0 )
	{
		ERR_ADF( "ADF GPIO request array failed\n" );
		return ret;
	}
	ret = request_irq( gpio_to_irq( CLKOUT_GPIO_NUM ),	\
			clkout_handler,							\
			IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING,		/*	flags	*/	\
			"ADF_CLKOUT_INT",		/* *name */		\
			pData );	//	*dev
	if( ret < 0 )
	{
		ERR_ADF( "ADF CLKOUT( %d ) GPIO irq( %d ) request failed\n", \
				CLKOUT_GPIO_NUM, gpio_to_irq(  CLKOUT_GPIO_NUM ) );
		return ret;
	}
	ret = request_irq( gpio_to_irq( SWD_GPIO_NUM ),	\
			swd_handler,							\
			IRQF_TRIGGER_RISING,		/*	flags	*/	\
			"SWD_INT",		/* *name */		\
			pData );	//	*dev
	if( ret < 0 )
	{
		ERR_ADF( "ADF CLKOUT( %d ) GPIO irq( %d ) request failed\n", \
				CLKOUT_GPIO_NUM, gpio_to_irq(  CLKOUT_GPIO_NUM ) );
		return ret;
	}
	disable_irq( gpio_to_irq( SWD_GPIO_NUM ) );
	disable_irq( gpio_to_irq( CLKOUT_GPIO_NUM ) );
	INFO_ADF( "GPIO INIT OK!!!\n" );

	return 0;
}

void gpio_exit( void * pData )
{
	gpio_set_value( CE_GPIO_NUM, 0 );

	free_irq( gpio_to_irq( CLKOUT_GPIO_NUM ), pData );
	free_irq( gpio_to_irq( SWD_GPIO_NUM ), pData );
	gpio_free_array( adf_gpio, ARRAY_SIZE( adf_gpio ) );
	INFO_ADF( "GPIO EXIT OK!!!\n" );
}
