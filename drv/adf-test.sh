#!/bin/sh

#ADF_MODULE=/storage/sdcard0/adf-test.ko
ADF_MODULE=/system/lib/modules/adf.ko

if [ -z $1 ] || [ "$1" = "-h" ] ; then
	echo "## Usage: $0 [ -m mode -s tx_strength | -f frequency | -d duration | -v ]"
	echo "          ADF7021 module insmod sctipt"
	echo "          -m: mode( 1: TX only, 2: RX only )"
	echo "          -s: tx_strenth( 0 ~ 63 )"
	echo "          -f: frequency( 1 ~ 5 )"
	echo "          -d: duration for test timer( 1000 ~ )"
	echo "          -v: verbose output"
	exit 0;
fi

while getopts ":m::s::f::d:v" opt ; do
	case $opt in
		m ) mode=$OPTARG ;
			option=$option" s_work_mode=$OPTARG" ;;
		s ) tx_strenth=$OPTARG ;
			option=$option" s_tx_strength=$OPTARG" ;;
		f ) frequency=$OPTARG ;
			option=$option" s_test_freq=$OPTARG" ;;
		d ) duration=$OPTARG ;
			option=$option" s_test_duration=$OPTARG" ;;
		v ) echo "Verbose: $opt $OPTARG";;
		\? ) echo "Invalid option $opt";;
	esac
done

echo "option: $option"
echo "insmod $ADF_MODULE $option"
rmmod adf
insmod $ADF_MODULE $option

