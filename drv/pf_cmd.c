#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/slab.h>			//	kzalloc

#include <mach/platform.h>
#include <mach/devices.h>

#include	"pf_cmd.h"

#include	"common.h"

/*	Used for GPS_INFO only	*/
uint16_t fnGetCrc16(const uint8_t *nData, uint16_t wLength) {
	static const uint16_t wCRCTable[] = {
		0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
		0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
		0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
		0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
		0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
		0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
		0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
		0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
		0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
		0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
		0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
		0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
		0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
		0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
		0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
		0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
		0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
		0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
		0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
		0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
		0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
		0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
		0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
		0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
		0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
		0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
		0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
		0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
		0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
		0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
		0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
		0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040
	};
	uint8_t nTemp = 0x0;
	uint16_t wCRCWord = 0x0;

	wLength -= 2;

	while (wLength--) {
		nTemp = (uint8_t) (*nData++ ^ wCRCWord);
		wCRCWord >>= 8;
		wCRCWord ^= wCRCTable[ nTemp ];
	}
	return wCRCWord;
}

/*
 * Function:        void AddCheckSum()
 * PreCondition:    None
 * Input:           buf : message, size :  message size
 * Output:          None
 * Side Effects:    None
 * Overview:        Add stuct check sum
 * Note:
 */
uint16_t AddCheckSum( uint8_t* buf, size_t size) {
#if 1
	uint32_t	exor = 0x0;
	uint32_t	sum = 0x0;
	uint32_t	sum_1 = 0x0;
	int i;

	for( i = 0 ; i < ( size - 2 ) ; i ++ )
	{
		exor ^= buf[ i ];
		sum_1 += buf[ i ];
		sum += sum_1;
	}
	*( buf + i ++ ) = exor & 0xFF;
	*( buf + i ) = ( ( sum >> 8 ) & 0xFF ) ^ ( sum & 0xFF );
#else
	uint8_t tempSum = 0x0, i, tmpByteH, tmpByteL;
	uint16_t CK_A = 0, CK_B = 0;
	for (i = 0; i < size - 2; i++) {
		tempSum ^= buf[i];
		CK_A += buf[i];
		CK_B += CK_A;
	}
	tmpByteH = (CK_B >> 8) & 0xFF;
	tmpByteL = CK_B & 0xFF;
	*(buf + i++) = tempSum;				//	LSB
	*(buf + i) = tmpByteH ^ tmpByteL;	//	MSB
#endif
	return ( *( buf + i ) << 8 ) | ( *( buf + ( i - 1 ) ) & 0xFF );
}

/*	Use for TX	*/
size_t get_command_len( const uint8_t command )
{
	switch( command )
	{
		case	PF_CMD_PAIRING			:	return PF_CMD_PAIRING_LEN;
		case	PF_CMD_PAIR_RESPONSE	:	return PF_CMD_PAIR_RESPONSE_LEN;
		case	PF_CMD_PAIR_STATUS		:	return PF_CMD_PAIR_STATUS_LEN;
		case	PF_CMD_PAIR_ACK			:	return PF_CMD_PAIR_ACK_LEN;
		case	PF_CMD_SLEEP_MODE		:	return PF_CMD_SLEEP_MODE_LEN;

		case	PF_CMD_CHANGE_PERIOD_REQ:	return PF_CMD_CHANGE_PERIOD_REQ_LEN;
		case	PF_CMD_CHANGE_SLOT_REQ	:	return PF_CMD_CHANGE_SLOT_REQ_LEN;
		case	PF_CMD_DELETE_DOG		:	return PF_CMD_DELETE_DOG_LEN;

		case	PF_CMD_CONST			:	return PF_CMD_CONST_LEN;
		case	PF_CMD_NICK_ONE			:	return PF_CMD_NICK_ONE_LEN;
		//case	PF_CMD_BUZZER			:	return PF_CMD_BUZZER_LEN;
		case	PF_CMD_TONE				:	return PF_CMD_TONE_LEN;
		case	PF_CMD_NICK_ACK			:	return PF_CMD_NICK_ACK_LEN;
		case	PF_CMD_NICK_REQ			:	return PF_CMD_NICK_REQ_LEN;
		case	PF_CMD_RESCUE_MODE		:	return PF_CMD_RESCUE_MODE_LEN;
		case	PF_CMD_GPS_INFO			:	return PF_CMD_GPS_INFO_LEN;
		case	PF_CMD_SHARE_DOG_INFO	:	return PF_CMD_SHARE_DOG_INFO_LEN;
		case	PF_CMD_SHARE_HUNTER_INFO:	return PF_CMD_SHARE_HUNTER_INFO_LEN;
		case	PF_CMD_SHARE_CHANGE_SLOT_REQ:	return PF_CMD_SHARE_CHANGE_SLOT_REQ_LEN;
		case	PF_CMD_SHARE_ACK		:	return PF_CMD_SHARE_ACK_LEN;
		case	PF_CMD_RX_TEST			:	return PF_MAX_PACKET_LEN;
		default:
			return PF_CMD_MAX_LEN;
	}
	return PF_CMD_MAX_LEN;
}

/*	Use for RX	*/
uint16_t get_checksum( const PF_RX_COMMAND * const pCmd )
{
	ASSERT( pCmd != NULL );

	switch( pCmd->command )
	{
		case	PF_CMD_PAIR_ACK			:
		case	PF_CMD_NICK_ACK			:
		case	PF_CMD_SHARE_ACK		:	return pCmd->ack.checksum;

		case	PF_CMD_PAIR_RESPONSE	:	return pCmd->pair_response.checksum;
		case	PF_CMD_GPS_INFO			:	return pCmd->gps_info.checksum;
		case	PF_CMD_SHARE_DOG_INFO	:	return pCmd->share_info.checksum;
		case	PF_CMD_SHARE_HUNTER_INFO:	return pCmd->share_info.checksum;
		default:
			return 0x0;
	}
	return 0x0;
}

uint16_t generate_checksum( const PF_TX_COMMAND * const pCmd )
{
	uint16_t	checksum;
	size_t		cmd_len;

	ASSERT( pCmd != NULL );

	cmd_len = get_command_len( pCmd->command );

	if( pCmd->command != PF_CMD_GPS_INFO )
	{
		checksum = AddCheckSum( ( uint8_t * )&( pCmd->command ), cmd_len );
	}
	else
	{
		/*	TODO CRC16 */
		checksum = fnGetCrc16( ( uint8_t * )&( pCmd->command ), cmd_len );
	}
	return checksum;
}

bool verify_checksum( const PF_RX_COMMAND * const pCmd )
{
	uint16_t	checksum, rx_checksum;
	size_t		cmd_len;

	ASSERT( pCmd != NULL );

	if( pCmd->command == PF_CMD_UNDEFINED ) return false;

	rx_checksum = get_checksum( pCmd );
	cmd_len = get_command_len( pCmd->command );

	if( pCmd->command != PF_CMD_GPS_INFO )
	{
		checksum = AddCheckSum( ( uint8_t * )&( pCmd->command ), cmd_len );

		//printk( "1. checksum: 0x%04x rx_checksum: 0x%04x len: %d\n", checksum, rx_checksum, cmd_len );
	}
	else
	{
		/*	TODO CRC16 */
		checksum = fnGetCrc16( ( uint8_t * )&( pCmd->command ), cmd_len );
		//printk( "2. checksum: 0x%04x rx_checksum: 0x%04x len: %d\n", checksum, rx_checksum, cmd_len );
	}
	return ( rx_checksum == checksum );
}

//bool pf_cmd_gen( const void *pData, RF_COMMAND * const pRfCmd, const PF_RX_COMMAND *pResponse, const uint8_t pfCmd )
bool pf_cmd_gen( const void *const pData, RF_COMMAND * const pRfCmd, const PF_RX_COMMAND * const pResponse,  const uint8_t pfCmd )
{
	ADF_STRUCT	*pAdf_struct = ( ADF_STRUCT * )pData;
	PF_TX_COMMAND *pCmd = NULL;
	int			assign_slot;
	int			tx_header_size = 0;

	ASSERT( pData != NULL );
	ASSERT( pRfCmd != NULL );

	pCmd = ( PF_TX_COMMAND * )( pRfCmd->packetData );

	pCmd->preamble = PF_PREAMBLE;
	pCmd->syncword = htonl( ( ( PF_SYNC_WORD ) << 8 ) | 0xAA );
	tx_header_size = sizeof( uint32_t ) + sizeof( uint32_t );	/* preamble + syncword */

	pCmd->command = pfCmd;
	pCmd->retry_cnt = 0;

	switch( pfCmd )
	{
		case	PF_CMD_PAIRING			:
			{
				pCmd->pairing.hunter_id = get_connector_info()->id;
				pCmd->pairing.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( PF_CMD_PAIRING ) + tx_header_size;
				pRfCmd->response_length = get_command_len( PF_CMD_PAIR_RESPONSE ) + 1;	//	0xAA + PAIRING_RESPONSE( 7 )
				pRfCmd->id = 0x0;
				pRfCmd->mode = ADF_MODE_TX;
				pRfCmd->freq = FREQ_GLOBAL;

				pRfCmd->delay_ms = DELAY_PAIR_CMD;

				break;
			}
		case	PF_CMD_PAIR_STATUS		:
			{
				assign_slot = adf_assign_slot_info( pAdf_struct, pResponse->pair_response.dog_id );

				if( assign_slot >= 0 )
				{
					pCmd->pair_status.dog_id = pResponse->pair_response.dog_id;
					pCmd->pair_status.period = pAdf_struct->slot_info[ assign_slot ].period;
					/*	Dog index: 1 to 22 / slot index: 0 to 21	*/
					pCmd->pair_status.slot = assign_slot + 1;
					pCmd->pair_status.rf_freq = pAdf_struct->slot_info[ assign_slot ].freq;
					pCmd->pair_status.checksum = generate_checksum( pCmd );
					pCmd->retry_cnt = 0;

					pRfCmd->length = get_command_len( PF_CMD_PAIR_STATUS ) + tx_header_size;
					pRfCmd->response_length = get_command_len( PF_CMD_PAIR_ACK ) + 1;	//	0xAA + PAIRING_RESPONSE( 7 )
					pRfCmd->id = 0x0;
					pRfCmd->mode = ADF_MODE_TX;
					pRfCmd->freq = FREQ_GLOBAL;

					pRfCmd->delay_ms = 0;
				}
				else
				{
					return false;
				}

				break;
			}
		case	PF_CMD_TX_TEST	:
			{
				/*	"1010" pattern	*/
				memset( pCmd->data, 0xAA, sizeof( pCmd->data ) );

				pRfCmd->length = sizeof( pCmd->data ) + tx_header_size;
				pRfCmd->response_length = 0;

				pRfCmd->delay_ms = 0;
			}
			break;
		default:
			break;
	}
#ifdef __DEBUG_QUEUE_CNT__
	pAdf_struct->alloc_cnt++;
#endif	//	__DEBUG_QUEUE_CNT__

	return true;
}

RF_COMMAND* pf_alloc_cmd_gen( const void *const pData, const IOCTL_CMD_DATA * const pCmdData, const uint8_t pfCmd )
{
	ADF_STRUCT	*pAdf_struct = ( ADF_STRUCT * )pData;
	PF_TX_COMMAND *pCmd = NULL;
	int			tx_header_size = 0;

	RF_COMMAND *pRfCmd = NULL;

	ASSERT( pData != NULL );
	ASSERT( pCmdData != NULL );

	pRfCmd = ( RF_COMMAND * )kzalloc( sizeof( RF_COMMAND ), GFP_KERNEL );
	if( pRfCmd == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		return NULL;
	}

	pCmd = ( PF_TX_COMMAND * )( pRfCmd->packetData );

	pCmd->preamble = PF_PREAMBLE;
	pCmd->syncword = htonl( ( ( PF_SYNC_WORD ) << 8 ) | 0xAA );
	tx_header_size = sizeof( uint32_t ) + sizeof( uint32_t );	/* preamble + syncword */

	pCmd->command = pfCmd;
	pCmd->retry_cnt = 0;

	switch( pfCmd )
	{
		case	PF_CMD_SLEEP_MODE :
			{
				pCmd->sleep_mode.hunter_id = get_connector_info()->id;
				pCmd->sleep_mode.dog_id = SLEEP_ALL_DOGS_ID;		//	 pCmd->sleep_mode.dog_id;
				pCmd->sleep_mode.mode = SLEEP_MODE_OFF;
				if( pCmdData->sleepMode.mode == true )
				{
					pCmd->sleep_mode.mode = SLEEP_MODE_ON;
				}
				pCmd->sleep_mode.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( PF_CMD_SLEEP_MODE ) + tx_header_size;
				pRfCmd->response_length = 0;
				//pRfCmd->response_length = get_command_len( PF_CMD_PAIR_ACK ) + 1;	//	0xAA + PAIRING_RESPONSE( 7 )
				pRfCmd->id = LIST_ID_BROADCAST;

				pRfCmd->mode = ADF_MODE_TX;
				pRfCmd->freq = FREQ_GLOBAL;

				//set_bit( FLAG_ALL_SLEEP, & pAdf_struct->flags );

				break;
			}
		case	PF_CMD_RESCUE_MODE :
			{
				int slot_index = adf_find_slot_listening( pCmdData->rescueMode.id );

				if( slot_index < 0 )
				{
					NOTICE_ADF( "NOTIFY RESCUE_MODE: id 0x%04x NOT found!!!!\n", pCmdData->rescueMode.id );
					kfree( pRfCmd );
					return NULL;
				}

				pCmd->rescue_mode.hunter_id = get_connector_info()->id;
				pCmd->rescue_mode.dog_id = pCmdData->rescueMode.id;
				pCmd->rescue_mode.mode = RESCUE_MODE_OFF;
				if( pCmdData->rescueMode.mode == true )
				{
					pCmd->rescue_mode.mode = RESCUE_MODE_ON;
				}
				pCmd->rescue_mode.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( PF_CMD_RESCUE_MODE ) + tx_header_size;
				pRfCmd->response_length = 0;
				pRfCmd->id = ( uint16_t )( pCmdData->rescueMode.id & 0xFFFF );

				pRfCmd->mode = ADF_MODE_TX;
				pRfCmd->freq = adf_get_slot_info_freq( slot_index );

				break;
			}
		case	PF_CMD_CHANGE_PERIOD_REQ :
			{
				int slot_index = adf_find_slot_listening( pCmdData->changePeriod.id );

				if( slot_index < 0 )
				{
					NOTICE_ADF( "NOTIFY CH_PERIOD: id 0x%04x NOT found!!!!\n", pCmdData->changePeriod.id );
					kfree( pRfCmd );
					return NULL;
				}

				pCmd->rf_period_req.hunter_id = get_connector_info()->id;
				pCmd->rf_period_req.dog_id = pCmdData->changePeriod.id;
				pCmd->rf_period_req.period = pCmdData->changePeriod.period;
				pCmd->rf_period_req.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( PF_CMD_CHANGE_PERIOD_REQ_LEN ) + tx_header_size;
				pRfCmd->response_length = 0;
				pRfCmd->id = ( uint16_t )( pCmdData->changePeriod.id & 0xFFFF );

				pRfCmd->mode = ADF_MODE_TX;
				pRfCmd->freq = adf_get_slot_info_freq( slot_index );

				break;
			}
		case	PF_CMD_CHANGE_SLOT_REQ :
			{
				int slot_index = adf_find_slot_listening( pCmdData->changeSlotFreq.id );
				int desired_slot_index = pCmdData->changeSlotFreq.slotId - 1;

				if( slot_index < 0 )
				{
					NOTICE_ADF( "NOTIFY CH_SLOT: id 0x%04x NOT found!!!!\n", pCmdData->changeSlotFreq.id );
					kfree( pRfCmd );
					return NULL;
					return false;
				}
				if( ( slot_index != desired_slot_index ) \
						&& ( adf_get_slot_info_mode( desired_slot_index ) != SLOT_MODE_NOT_ASSIGN ) )
				{
					NOTICE_ADF( "NOTIFY: Conflict changing slot: %d => %d\n", slot_index, desired_slot_index );
					kfree( pRfCmd );
					return NULL;
				}

				pCmd->change_slot_req.hunter_id = get_connector_info()->id;
				pCmd->change_slot_req.dog_id = pCmdData->changeSlotFreq.id;
				pCmd->change_slot_req.ch_slot.ch = pCmdData->changeSlotFreq.freq;
				pCmd->change_slot_req.ch_slot.slot = pCmdData->changeSlotFreq.slotId;
				pCmd->change_slot_req.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( PF_CMD_CHANGE_SLOT_REQ_LEN ) + tx_header_size;
				pRfCmd->response_length = 0;
				pRfCmd->id = ( uint16_t )( pCmdData->changeSlotFreq.id & 0xFFFF );

				pRfCmd->mode = ADF_MODE_TX;
				pRfCmd->freq = adf_get_slot_info_freq( slot_index );

				break;
			}
		case	PF_CMD_DELETE_DOG :
			{
				int slot_index = adf_find_slot_listening( pCmdData->deleteDevice.id );

				if( slot_index < 0 )
				{
					NOTICE_ADF( "NOTIFY DELETE: id 0x%04x NOT found!!!!\n", pCmdData->deleteDevice.id );
					kfree( pRfCmd );
					return NULL;
				}

				pCmd->delete_dog.hunter_id = get_connector_info()->id;
				pCmd->delete_dog.dog_id = pCmdData->deleteDevice.id;
				pCmd->delete_dog.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( PF_CMD_DELETE_DOG ) + tx_header_size;
				pRfCmd->response_length = 0;
				pRfCmd->id = ( uint16_t )( pCmdData->deleteDevice.id & 0xFFFF );

				pRfCmd->mode = ADF_MODE_TX;
				pRfCmd->freq = adf_get_slot_info_freq( slot_index );

				/*	post processing	*/
				if( pAdf_struct->init_reference == false )
				{
					adf_cancel_pairing( pAdf_struct, slot_index );
				}

				//slot_map[ freq - 1 ][ slot_index ] = 0x0;

				NOTICE_ADF( "DELETE: id 0x%04x | slot freq: [ s: %d f: %d ]\n", pCmdData->deleteDevice.id, \
						slot_index, pRfCmd->freq );

				break;
			}
		case	PF_CMD_NICK_ONE :
		//case	PF_CMD_NICK_REQ :
		//case	PF_CMD_TONE :
			{
				int slot_index = adf_find_slot_listening( pCmdData->eCollar.id );

				if( slot_index < 0 )
				{
					NOTICE_ADF( "NOTIFY NICK_ONE: id 0x%04x NOT found!!!!\n", pCmdData->eCollar.id );
					//slot_index = 0;
					kfree( pRfCmd );
					return NULL;
				}

				pCmd->nick.hunter_id = get_connector_info()->id;
				pCmd->nick.dog_id = pCmdData->eCollar.id;
				pCmd->nick.level = pCmdData->eCollar.level;
				pCmd->nick.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( pfCmd ) + tx_header_size;
				pRfCmd->response_length = 0;
				if( pfCmd == PF_CMD_NICK_ONE )
				{
					pRfCmd->response_length = get_command_len( PF_CMD_NICK_ACK ) + 1;	//	0xAA + RESPONSE
				}
				pRfCmd->id = ( uint16_t )( pCmdData->eCollar.id & 0xFFFF );

				pRfCmd->mode = ADF_MODE_TX;
				pRfCmd->freq = adf_get_slot_info_freq( slot_index );
				//pRfCmd->freq = FREQ_154_57;

				NOTICE_ADF( "NICK_ONE: id 0x%04x | slot freq: [ s: %d f: %d ]\n", pCmdData->eCollar.id, \
						slot_index, pRfCmd->freq );

				break;
			}
		case PF_CMD_SHARE_DOG_INFO :
			{
				int slot_index = adf_find_slot_listening( pCmdData->devInfo.id );

				if( slot_index < 0 )
				{
					NOTICE_ADF( "NOTIFY SH_DOG_INFO: id 0x%04x NOT found!!!!\n", pCmdData->devInfo.id );
					kfree( pRfCmd );
					return NULL;
				}

				pCmd->share_info.hunter_id = get_connector_info()->id;
				pCmd->share_info.id = pCmdData->devInfo.id;
#if 1
				pCmd->share_info.ch_slot.slot = slot_index + 1;
				pCmd->share_info.ch_slot.ch = adf_get_slot_info_freq( slot_index );
#else
				pCmd->share_info.ch_slot.slot = pCmdData->devInfo.slot;
				pCmd->share_info.ch_slot.ch = pCmdData->devInfo.freq;
#endif
				memcpy( pCmd->share_info.name, pCmdData->devInfo.name, HUNTER_NAME_LEN );
				pCmd->share_info.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( PF_CMD_SHARE_DOG_INFO ) + tx_header_size;
				pRfCmd->response_length = get_command_len( PF_CMD_SHARE_ACK ) + 1;
				pRfCmd->response_length = 0;
				//pRfCmd->id = ( uint16_t )( pCmdData->devInfo.id & 0xFFFF );
				pRfCmd->id = LIST_ID_BROADCAST;

				pRfCmd->mode = ADF_MODE_TX;
				pRfCmd->freq = FREQ_GLOBAL;

				pRfCmd->delay_ms = DELAY_SHARE_CMD;

				//clear_bit( FLAG_SHARING_TX, & pAdf_struct->flags );
				set_bit( FLAG_SHARING_RX_ACK, & pAdf_struct->flags );

				NOTICE_ADF( "SHARE_DOG_INFO: id 0x%04x\n", pCmdData->devInfo.id );
				break;
			}
		case PF_CMD_SHARE_HUNTER_INFO :
			{
				const CONNECTOR_INFO * const pConnectorInfo = get_connector_info();

				pCmd->share_info.hunter_id = pConnectorInfo->id;
				pCmd->share_info.id = pConnectorInfo->id;
				pCmd->share_info.ch_slot.slot = pConnectorInfo->slot;
				pCmd->share_info.ch_slot.ch = pConnectorInfo->freq;
				memcpy( pCmd->share_info.name, pCmdData->devInfo.name, HUNTER_NAME_LEN );
				pCmd->share_info.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( PF_CMD_SHARE_HUNTER_INFO ) + tx_header_size;
				if( test_bit( FLAG_SHARING_TX, & pAdf_struct->flags ) == 1 )
				{
					pRfCmd->response_length = get_command_len( PF_CMD_SHARE_HUNTER_INFO ) + 1;
					pRfCmd->delay_ms = DELAY_SHARE_CMD;
				}
				else if( test_bit( FLAG_SHARING_RX, & pAdf_struct->flags ) == 1 )
				{
					//clear_bit( FLAG_SHARING_RX, & pAdf_struct->flags );
					set_bit( FLAG_SHARING_RX_ACK, & pAdf_struct->flags );
					pRfCmd->response_length = get_command_len( PF_CMD_SHARE_ACK ) + 1;
					pRfCmd->delay_ms = 0;
					pRfCmd->delay_ms = DELAY_SHARE_CMD;
				}
				else
				{
					ERR_ADF( "NOT Sharing Mode!!!!!!\n" );
					kfree( pRfCmd );
					return NULL;
				}
				pRfCmd->response_length = 0;
				pRfCmd->id = LIST_ID_BROADCAST;

				pRfCmd->mode = ADF_MODE_TX;
				pRfCmd->freq = FREQ_GLOBAL;

				NOTICE_ADF( "SHARE_HUNTER_INFO: id 0x%04x\n", pCmdData->devInfo.id );
				break;
			}
		case	PF_CMD_SHARE_ACK :
			{
				pCmd->ack.hunter_id = ( uint16_t )( pCmdData->devInfo.id & 0xFFFF );
				pCmd->ack.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( PF_CMD_SHARE_ACK ) + tx_header_size;
				pRfCmd->response_length = 0;
				pRfCmd->id = ( uint16_t )( pCmdData->devInfo.id & 0xFFFF );
				//pRfCmd->id = ( uint16_t )( 0x0 );

				pRfCmd->mode = ADF_MODE_TX;
				pRfCmd->freq = FREQ_GLOBAL;

				pRfCmd->delay_ms = DELAY_SHARE_CMD;

				NOTICE_ADF( "SHARE_ACK: id 0x%04x\n", pCmdData->devInfo.id );
				break;
			}
		default:
			kfree( pRfCmd );
			pRfCmd = NULL;
			break;
	}
	return pRfCmd;
}

RF_COMMAND* pf_cmd_gen_eCollar( const void *const pData, const uint8_t pfCmd, const bool isGlobalTime )
{
	ADF_STRUCT	*pAdf_struct = ( ADF_STRUCT * )pData;
	RF_COMMAND *pRfCmd = NULL;
	PF_TX_COMMAND *pCmd = NULL;
	const E_COLLAR_INFO * const pECollarInfo = get_eCollar_info();
	int			tx_header_size = 0;

	ASSERT( pData != NULL );
	ASSERT( pECollarInfo != NULL );

	mutex_lock( & pAdf_struct->eCollar_mutex );

	pRfCmd = ( RF_COMMAND * )kzalloc( sizeof( RF_COMMAND ), GFP_KERNEL );
	if( pRfCmd == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		mutex_unlock( & pAdf_struct->eCollar_mutex );
		return NULL;
	}

	pCmd = ( PF_TX_COMMAND * )( pRfCmd->packetData );

	pCmd->preamble = PF_PREAMBLE;
	pCmd->syncword = htonl( ( ( PF_SYNC_WORD ) << 8 ) | 0xAA );
	tx_header_size = sizeof( uint32_t ) + sizeof( uint32_t );	/* preamble + syncword */

	pCmd->command = pfCmd;
	pCmd->retry_cnt = 0;

	switch( pfCmd )
	{
		case	PF_CMD_NICK_REQ :
		case	PF_CMD_CONST	:
			{
				int slot_index = pECollarInfo->slot_index;

				if( slot_index < 0 )
				{
					NOTICE_ADF( "NOTIFY NICK: id 0x%04x NOT found!!!!\n", pECollarInfo->id );
					kfree( pRfCmd );
					mutex_unlock( & pAdf_struct->eCollar_mutex );
					return NULL;
				}

				pCmd->nick.hunter_id = get_connector_info()->id;
				pCmd->nick.dog_id = ( uint16_t)( pECollarInfo->id & 0xFFFF );
				pCmd->nick.level = pECollarInfo->level;
				pCmd->nick.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( pfCmd ) + tx_header_size;
				pRfCmd->response_length = 0;

				if( pfCmd == PF_CMD_NICK_REQ )
				{
					pRfCmd->response_length = get_command_len( PF_CMD_NICK_ACK ) + 1;	//	0xAA + RESPONSE
				}
				pRfCmd->id = ( uint16_t )( pECollarInfo->id & 0xFFFF );

				pRfCmd->mode = ADF_MODE_TX;
				if( isGlobalTime == true )
				{
					pRfCmd->freq = FREQ_GLOBAL;
				}
				else
				{
					pRfCmd->freq = adf_get_slot_info_freq( slot_index );
				}

#if 0
				if( is_eCollar_ack_req() == true )
				{
					INFO_ADF( "NICK: id 0x%04x | slot freq: [ s: %d f: %d ] send_cnt: %d\n", pECollarInfo->id, \
							slot_index, pRfCmd->freq, pECollarInfo->send_cnt );
				}
#endif
				break;
			}
		case	PF_CMD_TONE :
			{
				int slot_index = pECollarInfo->slot_index;

				if( slot_index < 0 )
				{
					NOTICE_ADF( "NOTIFY TONE: id 0x%04x NOT found!!!!\n", pECollarInfo->id );
					kfree( pRfCmd );
					mutex_unlock( & pAdf_struct->eCollar_mutex );
					return NULL;
				}

				pCmd->tone.hunter_id = get_connector_info()->id;
				pCmd->tone.dog_id = ( uint16_t)( pECollarInfo->id & 0xFFFF );
				pCmd->tone.level = pECollarInfo->level;
				pCmd->tone.checksum = generate_checksum( pCmd );
				pCmd->retry_cnt = 0;

				pRfCmd->length = get_command_len( pfCmd ) + tx_header_size;
				pRfCmd->response_length = 0;

				if( pCmd->tone.level == TONE_LEVEL_ACK_REQ )
				{
					pRfCmd->response_length = get_command_len( PF_CMD_NICK_ACK ) + 1;	//	0xAA + RESPONSE
				}
				pRfCmd->id = ( uint16_t )( pECollarInfo->id & 0xFFFF );

				pRfCmd->mode = ADF_MODE_TX;
				if( isGlobalTime == true )
				{
					pRfCmd->freq = FREQ_GLOBAL;
				}
				else
				{
					pRfCmd->freq = adf_get_slot_info_freq( slot_index );
				}

#if 0
				NOTICE_ADF( "TONE: id 0x%04x | slot freq: [ s: %d f: %d ]\n", pECollarInfo->id, \
						slot_index, pRfCmd->freq );
#endif

				break;
			}
		default:
			kfree( pRfCmd );
			pRfCmd = NULL;
			break;
	}
	mutex_unlock( & pAdf_struct->eCollar_mutex );
	return pRfCmd;
}

