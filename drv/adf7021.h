#ifndef	__ADF7021_H__
#define	__ADF7021_H__

#include <linux/ioctl.h>
//#include <linux/kfifo.h>
//#include <linux/list.h>

#ifdef	__KERNEL__
#include <linux/module.h>
#include <linux/hrtimer.h>
#endif	//	__KERNEL__
#include	"adf7021_reg.h"
#include	"connector.h"

#ifdef __cplusplus
extern "C" {
#endif

#define __RF_TEST__

#define ADF_DEVICE_NAME			"adf7021"
#define ADF_MAGIC_KEY   		'A'

#define ADF_INIT				_IOW( ADF_MAGIC_KEY, 0x01, ADF_INIT_INFO )

#if 1
/*	Global commands	*/
#define	ADF_CMD_PAIR_START			_IO( ADF_MAGIC_KEY, 0x10 )
#define	ADF_CMD_PAIR_CANCEL			_IO( ADF_MAGIC_KEY, 0x11 )
#define ADF_CMD_SLEEP_MODE			_IOW( ADF_MAGIC_KEY, 0x12, CMD_SLEEP_MODE )
#define ADF_CMD_RESCUE_MODE			_IOW( ADF_MAGIC_KEY, 0x13, CMD_RESCUE_MODE )

/*	Local commands	*/
#define ADF_CMD_CHANGE_PERIOD		_IOW( ADF_MAGIC_KEY, 0x20, CMD_CHANGE_PERIOD )
#define ADF_CMD_CHANGE_SLOT_FREQ	_IOW( ADF_MAGIC_KEY, 0x21, CMD_CHANGE_SLOT_FREQ )
#define ADF_CMD_DELETE_DEVICE		_IOW( ADF_MAGIC_KEY, 0x22, CMD_DELETE_DEVICE )
#define ADF_CMD_SCAN_SLOT_FREQ		_IOW( ADF_MAGIC_KEY, 0x23, CMD_CHANGE_SLOT_FREQ )

#define ADF_CMD_CHANGE_END			_IO( ADF_MAGIC_KEY, 0x24 )

#define ADF_CMD_EC_START_CONST		_IOW( ADF_MAGIC_KEY, 0x30, CMD_E_COLLAR )
#define ADF_CMD_EC_NICK_ONE			_IOW( ADF_MAGIC_KEY, 0x31, CMD_E_COLLAR )
#define ADF_CMD_EC_START_TONE		_IOW( ADF_MAGIC_KEY, 0x32, CMD_E_COLLAR )
#define ADF_CMD_EC_STOP				_IOW( ADF_MAGIC_KEY, 0x33, CMD_E_COLLAR )

#define	ADF_CMD_SHARING_SEND_INFO	_IOW( ADF_MAGIC_KEY, 0x40, DEVICE_INFO )
#define	ADF_CMD_SHARING_WAIT		_IO( ADF_MAGIC_KEY, 0x41 )
#define	ADF_CMD_SHARING_CANCEL		_IO( ADF_MAGIC_KEY, 0x42 )

#define	ADF_CMD_SET_SLOT_INFO		_IOW( ADF_MAGIC_KEY, 0xA0, DEVICE_INFO )
#define	ADF_CMD_GET_CONNECTOR_INFO	_IOR( ADF_MAGIC_KEY, 0xA1, CONNECTOR_INFO )

#define	ADF_CMD_STAT				_IOW( ADF_MAGIC_KEY, 0xF0, CMD_STAT )
#else
#define ADF_CMD_SET_GPS_INFO	0x03

#define ADF_CMD_DUPLICATE_SHARING	0x11	//	conflict avoidance
#define ADF_CMD_CHANGE_NAME			0x12

#endif

#if 0
#define ADF_READBACK			_IOR( ADF_MAGIC_KEY, 2, READBACK_COMMAND )
#define ADF_WRITE_REG			_IOR( ADF_MAGIC_KEY, 3, ADF_REG )

#define ADF_SLEEP				_IO( ADF_MAGIC_KEY, 10 )
#define ADF_DISABLE				_IO( ADF_MAGIC_KEY, 11 )
#define ADF_TX_MODE				_IOR( ADF_MAGIC_KEY, 12, RF_COMMAND )
#define ADF_RX_MODE				_IOR( ADF_MAGIC_KEY, 13, RF_COMMAND )

#define ADF_GET_MODE			_IO( ADF_MAGIC_KEY, 20 )
#define ADF_GET_VERSION			_IO( ADF_MAGIC_KEY, 21 )
#endif

#define MAX_PACKET_LEN ( 64 )	//	aligned packet length

#ifdef __RF_TEST__
#undef MAX_PACKET_LEN
#define MAX_PACKET_LEN ( 512 )	//	aligned packet length
#endif	//	__RF_TEST__

#define	TX_FIFO_MAX				128		//	32 * 128 = 4096
#define	RX_FIFO_MAX				128

typedef enum ADF_MODE_E
{
	ADF_MODE_SLEEP = 0,
	ADF_MODE_INIT = ADF_MODE_SLEEP,
	ADF_MODE_TX,
	ADF_MODE_RX,

	ADF_MODE_END
} ADF_MODE;

typedef enum ADF_RF_MODE_E
{
	ADF_RF_GPIO = 0,
	ADF_RF_UART,
	ADF_RF_SPI,

	ADF_RF_END
} ADF_RF_MODE;

typedef enum ADF_WORK_MODE_E
{
	ADF_WORK_NORMAL = 0,
	ADF_WORK_TEST_TX,
	ADF_WORK_TEST_RX,

	ADF_WORK_END
} ADF_WORK_MODE;

#define DUMP_PACKET_LEN ( 16 )
typedef enum PF_DUMP_MODE_E
{
	PF_DUMP_NONE = 0,
	PF_DUMP_TX = 0x1,
	PF_DUMP_RX = 0x2,
	PF_DUMP_ALL = PF_DUMP_TX | PF_DUMP_RX,

	PF_DUMP_END
} PF_DUMP_MODE;;

/*	SLOT mode: NOT_ASSIGN, PAIRING, LISTEN, SLEEP	*/
typedef enum SLOT_MODE_E
{
	SLOT_MODE_NOT_ASSIGN = 0x0,
	SLOT_MODE_IDLE = 0x1,	//	Not Use...
	SLOT_MODE_PAIRING = 0x2,
	SLOT_MODE_LISTEN = 0x4,
	SLOT_MODE_SLEEP = 0x8,

	SLOT_MODE_CHANGING = 0x10,
	SLOT_MODE_CHANGING_PERIOD = SLOT_MODE_LISTEN | SLOT_MODE_CHANGING,

	SLOT_MODE_ME = 0x80,


	SLOT_MODE_END
} SLOT_MODE;

typedef enum NOTIFY_TYPE_E
{
	NOTIFY_NONE = 0,	//	GPS
	NOTIFY_ADD_DEVICE,
	NOTIFY_SLOT_CONFLICT,
	NOTIFY_SHARE_INFO,
	NOTIFY_SHARE_ACK,
	NOTIFY_SHARE_CANCELLED,
	NOTIFY_EC_ACK,
	NOTIFY_EC_FAIL,
	NOTIFY_BAT_LEVEL,
	NOTIFY_SCAN,

	NOTIFY_IN_PAIRING,
	//NOTIFY_CHANGE_SLOT_FREQ_ACK,
	//NOTIFY_CHANGE_RERIOD_ACK,

	NOTIFY_END
} NOTIFY_TYPE;

typedef struct RX_STAT_S
{
	uint32_t total;
	uint32_t sucess;
	uint32_t err;
	uint32_t fail;
} RX_STAT;

typedef struct ADF_SLOT_INFO_S
{
    //int				index;			//	1 ~ 22
	uint32_t	id;
	FREQ		freq;			//	1 ~ 5
	PERIOD		period;			//	1 ~ 5: 2, 6, 10, 30 and 120 secs.
	int			period_remain;
	bool		shared;

	SLOT_MODE	mode;

	struct timeval	last_rx_time;
	RX_STAT rx_stat;
} ADF_SLOT_INFO;

#ifndef INVALID_CH_SLOT

#define INVALID_CH_SLOT		( 0xFF )
typedef union CH_SLOT_S
{
	uint8_t ch_slot;

	struct
	{
		uint8_t	slot	:	5;
		uint8_t	ch		:	3;
	};
} CH_SLOT;
#endif	//	INVALID_CH_SLOT

typedef struct ADF_PACKET_S
{
#ifdef __cplusplus
	ADF_PACKET_S()
	{
		::memset( packetData, 0x0, sizeof( packetData ) );
	};
	~ADF_PACKET_S() {};
#endif	//	__cplusplus
	size_t			length;				//	in bits
	int				bit_index;
	size_t			response_length;	//	in bits

	struct timeval	tv;
	struct timeval	tv_swd;

	struct timeval	tv_start;

	uint8_t	packetData[ MAX_PACKET_LEN ];	//	TX: include syncword. RX: data only

	NOTIFY_TYPE		notify_type;
#ifndef __MEM_LEAK__
//} __attribute__ (( aligned( 32 ) )) ADF_PACKET;
} __attribute__ (( aligned( 512 ) )) ADF_PACKET;
#else	//	__MEM_LEAK__
} __attribute__ (( aligned( 32 * 1024 ) )) ADF_PACKET;	//	Test for Memory leak
#endif //	__MEM_LEAK__

typedef struct READBACK_COMMAND_S
{
	READBACK_MODE		mode;
	READBACK_ADC_MODE	adc_mode;
} READBACK_COMMAND;

typedef struct RF_COMMAND_S
{
	/*	TX
	 *	 - id: DOG_ID / response_length: length for RX
	 *	 - pData: PF_COMMAND as tx_cmd
	 *	RX
	 *	 - id: HUNTER_ID / response_length: length for TX
	 *	 - pData: PF_COMMAND as rx_cmd
	 *	*/
	ADF_MODE		mode;
	FREQ			freq;
	uint16_t		id;
	size_t			length;				//	packet_length. in bytes
	size_t			response_length;	//	in bytes
	uint8_t	packetData[ MAX_PACKET_LEN ];	//	include syncword

	int				timeout;
	int				delay_ms;
} RF_COMMAND;

typedef struct ADF_INIT_INFO_S
{
	ADF_RF_MODE		rf_mode;		//	GPIO, UART or SPI
	uint32_t		syncword;
	size_t			max_packet_length;

	ADF_WORK_MODE	work_mode;

} ADF_INIT_INFO;

//#define MAX_IOCTL_DATA_LEN  ( sizeof( ADF_PACKET ) )
#define MAX_IOCTL_DATA_LEN  ( 256 )

/*	Command strucrue for ioctl	*/
typedef struct CMD_INIT_S
{
	ADF_INIT_INFO	init;
} CMD_INIT;

/*	Global commands	*/
typedef struct CMD_SLEEP_MODE_S
{
	uint32_t	id;
	bool		mode;	//	On / Off
} CMD_SLEEP_MODE;

/*	Local commands	*/
typedef struct CMD_CHANGE_PERIOD_S
{
	uint32_t	id;

	PERIOD		period;	//	1 ~ 5
} CMD_CHANGE_PERIOD;

typedef struct CMD_CHANGE_SLOT_FREQ_S
{
	uint32_t	id;

	int			slotId;
	FREQ		freq;		//	1 ~ 5
} CMD_CHANGE_SLOT_FREQ;

typedef struct CMD_DELETE_DEVICE_S
{
	uint32_t	id;
} CMD_DELETE_DEVICE;

typedef struct CMD_STAT_S
{
	bool clear;
} CMD_STAT;

typedef struct CMD_E_COLLAR_S
{
	uint32_t	id;

	int			level;

	/*	E-Collar information	*/
	uint8_t		command;
	int			slot_index;
	int			send_cnt;
	int			action_time;
	int			retry_cnt;
} CMD_E_COLLAR;
typedef CMD_E_COLLAR	E_COLLAR_INFO;

typedef struct CHANGE_INFO_S
{
	uint32_t	id;

	uint8_t		command;

	uint32_t	slotId;
	FREQ		freq;		//	1 ~ 5
	PERIOD		period;	//	1 ~ 5
} CHANGE_INFO;

typedef struct CMD_RESCUE_MODE_S
{
	uint32_t	id;
	bool		mode;	//	On / Off
} CMD_RESCUE_MODE;

typedef struct DEVICE_INFO_S
{
	uint32_t id;
	int period;
	int slot;
	int freq;

	bool shared;
	char name[ HUNTER_NAME_LEN ];	//	shared hunter name
} DEVICE_INFO;

typedef struct IOCTL_CMD_DATA_S
{
	uint8_t			rf_cmd;

	union
	{
		uint8_t		data[ MAX_IOCTL_DATA_LEN ];

		ADF_INIT_INFO			adfInit;
		DEVICE_INFO				devInfo;

		CMD_DELETE_DEVICE		deleteDevice;
		CMD_SLEEP_MODE			sleepMode;
		CMD_RESCUE_MODE			rescueMode;
		CMD_CHANGE_PERIOD		changePeriod;
		CMD_CHANGE_SLOT_FREQ	changeSlotFreq;

		CMD_E_COLLAR			eCollar;	//	nick / constant / tone

		CMD_STAT				stat;
	};
} __attribute__ (( aligned( 512 ) )) IOCTL_CMD_DATA;

typedef struct IOCTL_CMD_S
{
#ifdef __cplusplus
	IOCTL_CMD_S()
	{
		//::memset( data, 0x0, sizeof( data ) );
		::memset( &data, 0x0, sizeof( data ) );
	};
	~IOCTL_CMD_S() {};
#endif	//	__cplusplus
	uint32_t		cmd;
	size_t			length;
	IOCTL_CMD_DATA	data;
} IOCTL_CMD;

#ifdef	__KERNEL__

//	Flags
enum ADF_FLAGS
{
	/*	main flags
	 *	[ 3 : 0 ] : ADF Flags
	 *	[ 7 : 4 ] : RF Flags
	 *	[ 11: 8 ] : Device Mode Flags
	 *	[ 15:12 ] : E-Collar Action Flags
	 *	*/
	FLAG_IRQ_ENABLED = 0,
	FLAG_ADF_ENABLED,
	FLAG_SWD_ASSERTED,

	FLAG_RF_DONE = 4,
	FLAG_FORCE_RF_DONE,
	FLAG_RX_ENQUEUE,

	FLAG_PAIRING = 8,
	FLAG_ALL_SLEEP,
	FLAG_SCAN,

	FLAG_E_COLLAR = 12,
	FLAG_CONST,
	FLAG_TONE,

	FLAG_NEED_EC_ACK = 16,
	FLAG_NEED_SHARE_ACK,
	FLAG_NEED_RETRY,

	FLAG_SHARING_INFO = 20,
	FLAG_SHARING_TX,
	FLAG_SHARING_RX,
	FLAG_SHARING_RX_ACK,

	//FLAG_RX_MARGIN = 24,

	/*	Slot flags	*/
	FLAG_SLOT_R_0 = 0,
	FLAG_SLOT_R_1,
	FLAG_SLOT_R_2,
	FLAG_SLOT_0 = 3,
	FLAG_SLOT_1,
	FLAG_SLOT_2,
	FLAG_SLOT_3,
	FLAG_SLOT_4,

	FLAG_SLOT_5 = 8,
	FLAG_SLOT_6,
	FLAG_SLOT_7,
	FLAG_SLOT_8,
	FLAG_SLOT_9,
	FLAG_SLOT_10,
	FLAG_SLOT_11,
	FLAG_SLOT_12,

	FLAG_SLOT_13 = 16,
	FLAG_SLOT_14,
	FLAG_SLOT_15,
	FLAG_SLOT_16,
	FLAG_SLOT_17,
	FLAG_SLOT_18,
	FLAG_SLOT_19,
	FLAG_SLOT_20,

	FLAG_SLOT_21 = 24,

	FLAG_SLOT_END,
};

#define		MAX_SLOT_INDEX			( FLAG_SLOT_END - FLAG_SLOT_0 )
#define		SLOT_MASK		( 0x01FFFFFF )

/*	key field in LIST_NODE
 *	TYPE[ 31:16 ] DEVICE_ID[ 15:0 ]
 *	*/
#define		LIST_ID_BROADCAST		( 0xFFFF )

/* for cmd_list, delayed_cmd_list, rx_list	*/
/*	RX	*/
#define		LIST_KEY_ADF_PACKET		( 0x1 << 16 )	//	0x0001_0000
#define		LIST_KEY_NOTIFY			( 0x1 << 17 )	//	0x0002_0000
#define		LIST_KEY_PAIRING		( 0x1 << 18 )	//	0x0004_0000 PAIRING
#define		LIST_KEY_GPS_INFO		( 0x1 << 19 )	//	0x0008_0000 GPS_INFO
#define		LIST_KEY_E_COLLAR		( 0x1 << 20 )	//	0x0010_0000 E_COLLAR
#define		LIST_KEY_SHARE			( 0x1 << 21 )	//	0x0020_0000

/*	TX	*/
//#define		LIST_KEY_GPS_RX			( 0x1 << 28 )	//	0x1000_0000
#define		LIST_KEY_CHANGE			( 0x1 << 26 )	//	0x0400_0000
#define		LIST_KEY_RF_CMD			( 0x1 << 27 )	//	0x0800_0000 Write()

#define		LIST_KEY_NICK_ACK		( 0x1 << 28 )	//	0x1000_0000
#define		LIST_KEY_GLOBAL_CMD		( 0x1 << 29 )	//	0x2000_0000
#define		LIST_KEY_LOCAL_CMD		( 0x1 << 30 )	//	0x4000_xxxx

#define		LIST_KEY_TEST			( 0x1 << 31 )	//	0x8000_0000

#define		LIST_KEY_ALL			( 0xFFFF0000 )

/* for pair_list	*/
#define		LIST_KEY_PAIR( slot_index ) ( 0x1 << ( slot_index ) )

/* for dump_list	*/
#define		LIST_KEY_RX				( 0x1 << 0 )
#define		LIST_KEY_TX				( 0x1 << 1 )

typedef enum SLOT_FIELD_E
{
	SLOT_FIELD_ID = 0,
	SLOT_FIELD_FREQ,
	SLOT_FIELD_PERIOD,
	SLOT_FIELD_PERIOD_REMAIN,
	SLOT_FIELD_MODE,
	SLOT_FIELD_LAST_RX_TIME,

	SLOT_FIELD_END,
} SLOT_FIELD;

typedef struct ADF_STRUCT_S
{
	struct hrtimer		reference_timer;
	struct hrtimer		slot_timer;
	struct hrtimer		timeout_timer;
#ifdef __RF_TEST__
	struct workqueue_struct		* pTest_wq;
	struct hrtimer		test_timer;
	struct work_struct	test_work;
#endif	//	__RF_TEST__

	struct workqueue_struct		* pTimeout_wq;
	struct work_struct	timeout_work;
	struct work_struct	rf_done_work;

	struct workqueue_struct		* pSlot_wq;
	struct work_struct	ref_slot_work;
	struct work_struct	slot_work;

    wait_queue_head_t	rf_done_waitq;
    wait_queue_head_t	poll_waitq;

	spinlock_t			spinlock;
	struct mutex		slot_mutex[ MAX_SLOT_INDEX ];
	struct mutex		alloc_mutex;
	struct mutex		eCollar_mutex;

	ADF_MODE			mode;	//	TX, RX, SLEEP
	FREQ				freq;

	ADF_REG				regs[ MAX_ADF_REG_INDEX ];
	ADF_INIT_INFO		init;

	struct list_head	cmd_list;	//	command from app.
	struct list_head	delayed_cmd_list;
	struct list_head	rx_list;
	struct list_head	pair_list;
	struct list_head	dump_list;

	ADF_PACKET			* pHandled_packet;

	unsigned long		flags;
	unsigned long		slot_flags;
	int					slot_seq;

	ktime_t				reference_timestamp;
	ktime_t				reference_next_timestamp;

	bool				init_reference;
	ktime_t				init_reference_timestamp;

	CONNECTOR_INFO		connector_info;
	ADF_SLOT_INFO		slot_info[ MAX_SLOT_INDEX ];

	int					last_pairing_slot_index;

	E_COLLAR_INFO		eCollar_info;

	CH_SLOT				scan_ch_slot;
	CH_SLOT				scan_result_ch_slot;
	/*	for debug by jyhuh 2017-11-03 18:47:54	*/
	struct delayed_work	rf_done_delayed_work;	//	for debug
#ifdef __DEBUG_QUEUE_CNT__
	unsigned long		alloc_cnt;
	unsigned long		dealloc_cnt;
#endif	//	__DEBUG_QUEUE_CNT__
	unsigned long		tmp_cnt;
	int					wait_cnt;
#ifdef __DEBUG_ADF_TIME__
	int irq_cnt;
#endif	//	 __DEBUG_ADF_TIME__
} ADF_STRUCT;

int adf_assign_slot_info( void *pData, const int id );
void adf_slot_set_mode( void *pData, const int slot_index, const SLOT_MODE mode );
SLOT_MODE adf_slot_get_mode( const int slot_index );
int adf_find_slot_listening( const uint16_t id );
int adf_find_slot_changing( const uint16_t id );

/*	set and get slot_info fields	*/
uint32_t adf_get_slot_info_id( const int slot_index );
FREQ adf_get_slot_info_freq( const int slot_index );
PERIOD adf_get_slot_info_period( const int slot_index );
int adf_get_slot_info_period_remain( const int slot_index );
int adf_get_slot_info_mode( const int slot_index );
struct timeval adf_get_slot_info_last_rx_time( const int slot_index );
bool adf_set_connector_slot_info( const CONNECTOR_INFO * const pConnectorInfo );

//void adf_clear_slot_info( void *pData, const int slot_index );
void adf_cancel_pairing( void *pData, const int slot_index );

bool clear_eCollar_info( void );
bool set_eCollar_info( const E_COLLAR_INFO * const pECollarInfo );
const E_COLLAR_INFO * const get_eCollar_info( void );
int update_eCollar_info( const int milli_sec );
int get_eCollar_time( void );
int get_eCollar_slot_index( void );
bool is_eCollar_ack_req( void );
bool set_eCollar_cmd( const uint8_t command );
const uint8_t get_eCollar_cmd( void );
bool set_eCollar_level( const uint8_t level );
bool check_eCollar( const bool isGlobalTime );
#endif	//	__KERNEL__

#ifdef __cplusplus
}
#endif

#endif	/*	__ADF7021_H__	*/
