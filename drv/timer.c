#include <linux/module.h>
#include <linux/kernel.h>

#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/slab.h>			//	kzalloc

//#include <mach/platform.h>
#include <mach/devices.h>

#include	"timer.h"

#include	"common.h"

static TIMER	* _pTimer[ MAX_TIMER ] = { NULL, };
static DEFINE_SPINLOCK( lock );

static int find_timer_by_ptr( const void * pData );

static int find_timer_by_ptr( const void * pData )
{
	int idx;
	unsigned long	flags;

    spin_lock_irqsave( & lock, flags );

	for( idx = 0 ; idx < MAX_TIMER ; idx ++ )
	{
		if( _pTimer[ idx ] != NULL )
		{
			if( _pTimer[ idx ]->pTimer == pData )
			{
    			spin_unlock_irqrestore( & lock, flags );
				return idx;
			}
		}
	}

    spin_unlock_irqrestore( & lock, flags );

	return -1;
}

int timer_init( void )
{
	int idx;
	unsigned long	flags;

    spin_lock_irqsave( & lock, flags );

	for( idx = 0 ; idx < MAX_TIMER ; idx ++ )
	{
		_pTimer[ idx ] = NULL;
	}

    spin_unlock_irqrestore( & lock, flags );

	return 0;
}

int timer_register( const  void * pData, const TIMER_TYPE type, const char * pName, void *pHandler )
{
	ktime_t				ktime;
	struct	hrtimer		* pHrtimer = NULL;
	struct timer_list	* pTimer = NULL;
	TIMER				* pTmpTimer = NULL;
	int				idx;
	unsigned long	flags;

	ASSERT( pData != NULL );

	pTmpTimer = ( TIMER * )kzalloc( sizeof( TIMER ), GFP_KERNEL );
	if( pTmpTimer == NULL )
	{
		ERR_ADF( "kzalloc failed!!!!\n" );
		return -ENOMEM;
	}

    spin_lock_irqsave( & lock, flags );

	for( idx = 0 ; idx < MAX_TIMER ; idx ++ )
	{
		if( _pTimer[ idx ] == NULL ) break;
	}

	if( idx == MAX_TIMER )
	{
		spin_unlock_irqrestore( & lock, flags );
		return -1;
	}

	_pTimer[ idx ] = pTmpTimer;

	if( pName != NULL )
	{
		memcpy( _pTimer[ idx ]->name, pName, TIMER_NAME_LEN );
	}
	else
	{
		memcpy( _pTimer[ idx ]->name, "No_Name", TIMER_NAME_LEN );
	}
	_pTimer[ idx ]->type = type;
	_pTimer[ idx ]->pTimer = ( void * )pData;

	/*	Initialize Timer	*/
	if( _pTimer[ idx ]->type == TYPE_HRTIMER )
	{
		pHrtimer = ( struct hrtimer * )( _pTimer[ idx ]->pTimer );

		_pTimer[ idx ]->pTimer_handler = pHandler;

		ktime = ktime_set( 0, MS_TO_NS( INIT_DURATION ) );
		hrtimer_init( pHrtimer, CLOCK_MONOTONIC, HRTIMER_MODE_REL );
		pHrtimer->function = _pTimer[ idx]->pTimer_handler;
	}
	else
	{
		pTimer = ( struct timer_list * )( _pTimer[ idx ]->pTimer );

		_pTimer[ idx ]->pTimer_handler = pHandler;

		setup_timer( pTimer, _pTimer[ idx ]->pTimer_handler, ( unsigned long )pData );
	}

    spin_unlock_irqrestore( & lock, flags );

	INFO_TIMER( "INIT OK!!!\n" );

	return 0;
}

int timer_deregister( const void * pData )
{
	int					idx = -1;
	unsigned long	flags;

	idx = find_timer_by_ptr( pData );

	if( idx < 0 ) return -1;

	timer_stop( _pTimer[ idx ]->pTimer );

    spin_lock_irqsave( & lock, flags );

	kfree( _pTimer[ idx ] );
	_pTimer[ idx ] = NULL;

    spin_unlock_irqrestore( & lock, flags );

	return 0;
}

void timer_exit( const void * pData )
{
	timer_deregister( pData );

	INFO_TIMER( "EXIT OK!!!\n" );
}

int64_t timer_remaining( const void *pData )
{
	ktime_t	ktime;
	struct	hrtimer		* pHrtimer = NULL;
	struct timer_list	* pTimer = NULL;
	int					idx = -1;
	int64_t				remain = -1;

	idx = find_timer_by_ptr( pData );

	if( idx < 0 ) return -1;

	if( _pTimer[ idx ]->type == TYPE_HRTIMER )
	{
		pHrtimer = ( struct hrtimer * )( _pTimer[ idx ]->pTimer );

		ktime = hrtimer_get_remaining( pHrtimer );
		remain = ktime_to_us( ktime );
	}
	else
	{
		pTimer = ( struct timer_list * )( _pTimer[ idx ]->pTimer );
	}

	DEBUG_TIMER( "Timer remain: %lld us!!!\n", remain );

	return remain;
}

int timer_start( const void *pData, const int expire_time )
{
	ktime_t	ktime;
	struct	hrtimer		* pHrtimer = NULL;
	struct timer_list	* pTimer = NULL;
	int					idx = -1;
	unsigned long expires = jiffies + msecs_to_jiffies( expire_time );

	idx = find_timer_by_ptr( pData );

	if( idx < 0 ) return -1;

	if( _pTimer[ idx ]->type == TYPE_HRTIMER )
	{
		pHrtimer = ( struct hrtimer * )( _pTimer[ idx ]->pTimer );

		ktime = ktime_set( 0, MS_TO_NS( expire_time ) );
		hrtimer_start( pHrtimer, ktime, HRTIMER_MODE_REL );
	}
	else
	{
		pTimer = ( struct timer_list * )( _pTimer[ idx ]->pTimer );

		/*	return 0: Inactive, 1: Active	*/
		mod_timer( pTimer, expires );
	}

	DEBUG_TIMER( "[ %s : %d ] Timer START: expire_time %d ms!!!\n", __func__, __LINE__, expire_time );

	return 0;
}

int timer_start_ns( const void *pData, const long expire_time )
{
	ktime_t	ktime;
	struct	hrtimer		* pHrtimer = NULL;
	struct timer_list	* pTimer = NULL;
	int					idx = -1;
	unsigned long expires = jiffies + msecs_to_jiffies( expire_time * 1E6L );

	idx = find_timer_by_ptr( pData );

	if( idx < 0 ) return -1;

	if( _pTimer[ idx ]->type == TYPE_HRTIMER )
	{
		pHrtimer = ( struct hrtimer * )( _pTimer[ idx ]->pTimer );

		ktime = ktime_set( 0, expire_time );
		hrtimer_start( pHrtimer, ktime, HRTIMER_MODE_REL );
	}
	else
	{
		pTimer = ( struct timer_list * )( _pTimer[ idx ]->pTimer );

		/*	return 0: Inactive, 1: Active	*/
		mod_timer( pTimer, expires );
	}

	DEBUG_TIMER( "[ %s : %d ] Timer START: expire_time %d ms!!!\n", __func__, __LINE__, expire_time );

	return 0;
}

int timer_stop( const void * pData )
{
	struct	hrtimer		* pHrtimer = NULL;
	struct timer_list	* pTimer = NULL;
	int					idx = -1;
	int ret;

	idx = find_timer_by_ptr( pData );

	if( idx < 0 ) return -1;

	if( _pTimer[ idx ]->type == TYPE_HRTIMER )
	{
		pHrtimer = ( struct hrtimer * )( _pTimer[ idx ]->pTimer );

		ret = hrtimer_cancel( pHrtimer );
		if( ret ) DEBUG_TIMER( "pid: %d | The timer was still in use...\n", current->pid );
	}
	else
	{
		pTimer = ( struct timer_list * )( _pTimer[ idx ]->pTimer );

		del_timer_sync( pTimer );
	}

	DEBUG_TIMER( "[ %s : %d ] Timer STOP!!!\n", __func__, __LINE__ );

	return 0;
}

int timer_restart( const void *pData, const int expire_time )
{
	ASSERT( pData != NULL );

	timer_stop( pData );

	timer_start( pData, expire_time );

	return 0;

}

