#include	"reader.h"

#include	<unistd.h>	//	read
#include	<sys/poll.h>
#include	<string.h>	//	strlen

#include	<errno.h>

#include	"debug.h"

using namespace Pettra;

#ifdef __VIRT_GPS__
#include <stdlib.h>	//	rand()
#include "../drv/pf_cmd.h"

#define NUM_VIRT_DATA	( 10 )

::ADD_DEVICE_INFO	s_addCmd[ NUM_VIRT_DATA ];
::PF_RX_COMMAND		s_rxCmd[ NUM_VIRT_DATA ];
#endif	//	__VIRT_GPS__

Reader::Reader()
	: DeviceAccessor< ::ADF_PACKET >( "READER", "RX_Q", "RX_T" )
{
	this->Clear();
#ifdef	__VIRT_GPS__
	for( int i = 0 ; i < NUM_VIRT_DATA ; i++ )
	{
		s_addCmd[ i ].dog_id = 0x1000 + i;
		s_addCmd[ i ].period = 1;
		s_addCmd[ i ].slot = i + 1;
		s_addCmd[ i ].rf_freq = ( i % 2 ) + 1;

		s_rxCmd[ i ].dummy = 0xaa;
		s_rxCmd[ i ].command = PF_CMD_GPS_INFO;
		s_rxCmd[ i ].gps_info.dog_id = 0x1000 + i;
		s_rxCmd[ i ].gps_info.hunter_id = 0x8123;
		s_rxCmd[ i ].gps_info.lat_int = 3731;
		s_rxCmd[ i ].gps_info.lat_frac = 1083 + ( rand() % 100 );
		s_rxCmd[ i ].gps_info.long_int = 12653;
		s_rxCmd[ i ].gps_info.long_frac = 8532 + ( rand() % 100 );
		s_rxCmd[ i ].gps_info.altitude = 1048;
		s_rxCmd[ i ].gps_info.velocity = 3;
		s_rxCmd[ i ].gps_info.ch_slot.ch = ( i % 2 ) + 1;
		s_rxCmd[ i ].gps_info.ch_slot.slot = i + 1;
		s_rxCmd[ i ].gps_info.battery = 70 + i;
		s_rxCmd[ i ].gps_info.dog_info.dog_info = 0x440f;
	}
#endif	//	__VIRT_GPS__
}

Reader::~Reader()
{
	this->Clear();
}

void Reader::Clear( void )
{
	::ADF_PACKET *pPacket = NULL;

	while( this->IsEmpty() == false )
	{
		THREAD_INFO_PRINTF( ">>> IsEmpty: %d size: %d pPacket: %p\n",
				this->IsEmpty(), this->Size(), pPacket );

		pPacket = this->Dequeue();

		assert( pPacket != NULL );

		THREAD_INFO_PRINTF( "DELETE: pPacket: %p\n", pPacket );

		delete pPacket;
		pPacket = NULL;
	}

	return;
}

bool Reader::Close( void ) const
{
	THREAD_INFO_PRINTF( "CLOSE: Reader\n" );

	//return DeviceAccessor< ::ADF_PACKET >::Close();
	return this->DeviceAccessor::Close();
}

int Reader::Poll( struct pollfd fds[], nfds_t numFds )
{
	int		ret = 0;
	ssize_t	len = 0;
	int 	timeout = 1 * 1000;	//	1 sec
	::ADF_PACKET *pPacket = NULL;

#ifdef __VIRT_GPS__
	timeout = 10;
#endif	//	__VIRT_GPS__
	ret = ::poll( fds, numFds, timeout );
	if( ret < 0 )
	{
		ERROR_PRINTF( "poll() failed. ret: %d errno: %d, %s", ret, errno, ::strerror( errno ) );
		return ret;
	}
	else if( ret == 0 )
	{
		THREAD_PRINTF( "POLL: timeout!!!\n" );
		return ret;
	}
	//THREAD_INFO_PRINTF( "POLLIN: fd: %d revents: 0x%08x ret: %d\n", fds[ 0 ].fd, fds[ 0 ].revents, ret );

	//for( int i = 0 ; i < numFds ; i++ )
	{
		if( fds[ 0 ].revents & ( POLLIN | POLLPRI ) )
		{
			pPacket = new ::ADF_PACKET();

			len = ::read( fds[ 0 ].fd, pPacket, sizeof( ::ADF_PACKET ) );
			if( len > 0 )
			{
#if 0
				THREAD_INFO_PRINTF( "POLLIN: fd: %d revents: 0x%08x len: %d\n", fds[ 0 ].fd, fds[ 0 ].revents, len );
				THREAD_INFO_PRINTF( "pPacket: %p pData: %p 1st: %08x\n", \
						pPacket, pPacket->pData, *( uint32_t * )( pPacket->pData ) );
				THREAD_INFO_PRINTF( "ret: %d TS[ %9ld.%06ld ] length: %d idx: %d\n", ret, \
						pPacket->tv.tv_sec, pPacket->tv.tv_usec, \
						pPacket->length, pPacket->bit_index );
#endif
				//THREAD_INFO_PRINTF( "GPS: 1st: %08x pPacket: %p pData: %p\n",
				//		*( uint32_t * )( pPacket->pData ), pPacket, pPacket->pData );

				this->Enqueue( pPacket );

				Thread::Lock();
				Thread::Signal();
				Thread::Unlock();
			}
			else
			{
				delete pPacket;
			}
			pPacket = NULL;
		}
	}
	return ret;
}
#ifdef __VIRT_GPS__

void* Reader::ThreadFunc( void *pArg )
{
	::ADF_PACKET *pPacket = NULL;

	struct pollfd	fds[ 2 ];
	nfds_t	numFds = 0;
	int seq = 0;

	fds[ numFds ].fd = this->GetFd();
	fds[ numFds++ ].events = POLLIN | POLLPRI | POLLERR;

	this->Init();

	THREAD_INFO_PRINTF( "START_VIRT: IsRunning(): %d fd: %d\n", this->IsRunning(), this->GetFd() );

	::sleep( 2 );

	for( int i = 0 ; i < NUM_VIRT_DATA ; i++ )
	{
		pPacket = new ::ADF_PACKET();

		//pPacket->bit_index = -1;
		//pPacket->length = MAX_PACKET_LEN;
		//pPacket->response_length = 0;
		//::gettimeofday( & pPacket->tv );
		reinterpret_cast< ADF_PACKET * >( pPacket )->notify_type = NOTIFY_ADD_DEVICE;
		::memcpy( reinterpret_cast< ADF_PACKET * >( pPacket )->packetData, &s_addCmd[ i % NUM_VIRT_DATA ], sizeof( ::ADD_DEVICE_INFO ) );

		this->Enqueue( pPacket );

		Thread::Lock();
		Thread::Signal();
		Thread::Unlock();

		::usleep( 100 * 1000 );
	}

	while( this->IsRunning() == true )
	{
		pPacket = new ::ADF_PACKET();
		::memcpy( reinterpret_cast< ADF_PACKET * >( pPacket )->packetData, &s_rxCmd[ seq % NUM_VIRT_DATA ], sizeof( ::PF_RX_COMMAND ) );
		seq = ( seq + 1 ) % NUM_VIRT_DATA;

		srand( time( NULL ) );
		srand( rand() % RAND_MAX );

		//if( ( seq / NUM_VIRT_DATA ) % 2 )
		if( rand() % 2 )
		{
			s_rxCmd[ seq % NUM_VIRT_DATA ].gps_info.lat_frac += ( rand() & 0xFF );
		}
		else
		{
			s_rxCmd[ seq % NUM_VIRT_DATA ].gps_info.lat_frac -= ( rand() & 0xFF );
		}

		if( rand() % 2 )
		{
			s_rxCmd[ seq % NUM_VIRT_DATA ].gps_info.long_frac += ( rand() & 0xFF );
		}
		else
		{
			s_rxCmd[ seq % NUM_VIRT_DATA ].gps_info.long_frac -= ( rand() & 0xFF );
		}

		this->Enqueue( pPacket );

		Thread::Lock();
		Thread::Signal();
		Thread::Unlock();

		pPacket = NULL;

		while( Poll( fds, numFds ) != 0 ) {};

		::usleep( ( 2000 / NUM_VIRT_DATA ) * 1000 );
	}
	THREAD_INFO_PRINTF( "END: IsRunning(): %d\n", this->IsRunning() );

	return NULL;
}

#else	//	__VIRT_GPS__

void* Reader::ThreadFunc( void *pArg )
{
	/*	TODO: poll() and packet processing
	 *	by jyhuh 2018-04-05 20:48:50
	 *	*/
	struct pollfd	fds[ 2 ];
	//int 			timeout = 2 * 1000;	//	1 sec

	int		ret = 0;
	//ssize_t	len = 0;
	nfds_t	numFds = 0;
	//T		*pPacket = NULL;

	fds[ numFds ].fd = this->GetFd();
	fds[ numFds++ ].events = POLLIN | POLLPRI | POLLERR;

	this->Init();

	THREAD_INFO_PRINTF( "START: %s IsRunning(): %d fd: %d\n", this->GetName().c_str(), this->IsRunning(), this->GetFd() );

	while( this->IsRunning() == true )
	{
		ret = Poll( fds, numFds );
	}

	THREAD_INFO_PRINTF( "END: %s IsRunning(): %d\n", this->GetName().c_str(),this->IsRunning() );

	return NULL;
}
#endif	//	__VIRT_GPS__
//template class Reader< ADF_PACKET >;
