#ifndef	_DEBUG_H_
#define	_DEBUG_H_

#include	<stdio.h>
#include	<unistd.h>

#ifdef	__RELEASE__
	#ifndef NDEBUG
	#define NDEBUG
	#endif
#else	//	__RELEASE__
	#ifdef NDEBUG
	#undef NDEBUG
	#endif
#endif	//	__RELEASE__

#include	<assert.h>

#ifdef __cplusplus
extern "C" {
#endif

int gettid(void);

#ifdef __cplusplus
}
#endif

#ifdef	__ANDROID_LOG__

#include <android/log.h>


#if 0	/*	Temp	*/
/*
 * Log a fatal error.  If the given condition fails, this stops program
 * execution like a normal assertion, but also generating the given message.
 * It is NOT stripped from release builds.  Note that the condition test
 * is -inverted- from the normal assert() semantics.
 */
#ifndef LOG_ALWAYS_FATAL_IF
#define LOG_ALWAYS_FATAL_IF(cond, ...) \
    ( (CONDITION(cond)) \
    ? ((void)android_printAssert(#cond, LOG_TAG, ## __VA_ARGS__)) \
    : (void)0 )
#endif

#ifndef LOG_ALWAYS_FATAL
#define LOG_ALWAYS_FATAL(...) \
    ( ((void)android_printAssert(NULL, LOG_TAG, ## __VA_ARGS__)) )
#endif
#endif	/*	Temp	*/

#ifndef	__RELEASE__

#define	ASSERT( expr ) \
	__android_log_assert( #expr, LOG_TAG, "ASSERT!! [ %s : %s : %d ]\nCONDITION ( %s ) is false.\n", \
			__FILE__, __func__, __LINE__, #expr );

#define  LOGUNK( fmt, ...) \
	__android_log_print( ANDROID_LOG_UNKNOWN, LOG_TAG, "[%s:%d T(%d)] "fmt, __func__, __LINE__, gettid(), ##__VA_ARGS__ )
#define  LOGDEF( fmt, ...) \
	__android_log_print( ANDROID_LOG_DEFAULT, LOG_TAG, "[%s:%d T(%d)] "fmt, __func__, __LINE__, gettid(), ##__VA_ARGS__ )
#define  LOGV( fmt, ...) \
	__android_log_print( ANDROID_LOG_VERBOSE, LOG_TAG, "[%s:%d T(%d)] "fmt, __func__, __LINE__, gettid(), ##__VA_ARGS__ )
#define  LOGD( fmt, ...) \
	__android_log_print( ANDROID_LOG_DEBUG, LOG_TAG, "[%s:%d T(%d)] "fmt, __func__, __LINE__, gettid(), ##__VA_ARGS__ )

#else	//	__RELEASE__

#define	ASSERT( expr )

#define  LOGUNK( fmt, ...)
#define  LOGDEF( fmt, ...)
#define  LOGV( fmt, ...)
#define  LOGD( fmt, ...)

#endif	//	__RELEASE__

#ifdef __DEBUG__
#define	DEBUG_PRINTF		LOGD
#else	//	__DEBUG__
#define	DEBUG_PRINTF( fmt, ... )
#endif	//	__DEBUG__

#define	VERB_PRINTF			LOGV
#define	INFO_PRINTF			LOGI
#define	ERROR_PRINTF		LOGE

#ifdef	__DEBUG_RF__
#define	DEBUG_RF			LOGD
#define	DEBUG_CLASS_RF		LOGD
#else	//	__DEBUG_RF__
#define	DEBUG_RF( fmt, ... )
#define	DEBUG_CLASS_RF( fmt, ... )
#endif	//	__DEBUG_RF__

#ifdef	__DEBUG_THREAD__
#define	THREAD_PRINTF		LOGD
#define	MUTEX_PRINTF		LOGD
#else	//	__DEBUG_THREAD__
#define	THREAD_PRINTF( fmt, ... )
#define	MUTEX_PRINTF( fmt, ... )
#endif	//	__DEBUG_THREAD__

#define	INFO_RF				LOGV
#define	THREAD_INFO_PRINTF	LOGV

#define  LOGI( fmt, ...) \
	__android_log_print( ANDROID_LOG_INFO, LOG_TAG, "[%s:%d T(%d)] "fmt, __func__, __LINE__, gettid(), ##__VA_ARGS__ )
#define  LOGW( fmt, ...) \
	__android_log_print( ANDROID_LOG_WARN, LOG_TAG, "[%s:%d T(%d)] "fmt, __func__, __LINE__, gettid(), ##__VA_ARGS__ )
#define  LOGE( fmt, ...) \
	__android_log_print( ANDROID_LOG_ERROR, LOG_TAG, "[%s:%d T(%d)] "fmt, __func__, __LINE__, gettid(), ##__VA_ARGS__ )
#define  LOGF( fmt, ...) \
	__android_log_print( ANDROID_FATAL_ERROR, LOG_TAG, "[%s:%d T(%d)] "fmt, __func__, __LINE__, gettid(), ##__VA_ARGS__ )
#define  LOGS( fmt, ...) \
	__android_log_print( ANDROID_SILENT_ERROR, LOG_TAG, "[%s:%d T(%d)] "fmt, __func__, __LINE__, gettid(), ##__VA_ARGS__ )

#else	//	__ANDROID__

#define	ASSERT( expr )	assert( expr )

#define  LOGUNK( fmt, ...)
#define  LOGDEF( fmt, ...)
#define  LOGV( fmt, ...)
#define  LOGD( fmt, ...)
#define  LOGI( fmt, ...)

#define  LOGW( fmt, ...)
#define  LOGE( fmt, ...)
#define  LOGF( fmt, ...)
#define  LOGS( fmt, ...)

#define		BLACK			"\033[0;33m"
#define		RED				"\033[0;31m"
#define		GREEN			"\033[0;32m"
#define		YELLOW			"\033[0;33m"
#define		BLUE			"\033[0;34m"
#define		MAGENTA			"\033[0;35m"
#define		CYAN			"\033[0;36m"
#define		WHITE			"\033[0;37m"

#define		BLACK_BOLD		"\033[1;33m"
#define		RED_BOLD		"\033[1;31m"
#define		GREEN_BOLD		"\033[1;32m"
#define		YELLOW_BOLD		"\033[1;33m"
#define		BLUE_BOLD		"\033[1;34m"
#define		MAGENTA_BOLD	"\033[1;35m"
#define		CYAN_BOLD		"\033[1;36m"
#define		WHITE_BOLD		"\033[1;37m"

#define		ENDCOLOR		"\033[0m"

#ifdef	__DEBUG_RF__
#define	DEBUG_RF( fmt, ... )	\
     printf( "[%16s:%4d P(%5d) T(%5d)] DBG\t| "fmt, __FUNCTION__, __LINE__, getpid(), gettid(), ##__VA_ARGS__ )
#define	DEBUG_CLASS_RF( fmt, ... )	\
     printf( "[%16.16s:%4d P(%5d) T(%5d)] DBG\t| "fmt, __FUNCTION__, __LINE__, getpid(), gettid(), ##__VA_ARGS__ )
     //printf( "[%16.16s:%4d P(%5d) T(%5d)] DBG\t| "fmt, __PRETTY_FUNCTION__, __LINE__, getpid(), gettid(), ##__VA_ARGS__ )
#else	//	__DEBUG_RF__
#define	DEBUG_RF( fmt, ... )
#define	DEBUG_CLASS_RF( fmt, ... )
#endif	//	__DEBUG_RF__

#ifdef	__DEBUG_THREAD__

#define	THREAD_PRINTF( fmt, args... )		\
     printf( "[%16s:%4d P(%5d) T(%5d)] DBG\t|%16s| "fmt, __FUNCTION__, __LINE__, getpid(), gettid(), Thread::GetName().c_str(), ##args )

#define	MUTEX_PRINTF( fmt, args... )		\
     printf( "[%16s:%4d P(%5d) T(%5d)] DBG\t| MUTEX| %16s| "fmt, __FUNCTION__, __LINE__, getpid(), gettid(), Mutex::GetName().c_str(), ##args )
#else	//	__DEBUG_THREAD__
#define	THREAD_PRINTF( fmt, args... )
#define	MUTEX_PRINTF( fmt, args... )
#endif	//	__DEBUG_THREAD__


#define	THREAD_INFO_PRINTF( fmt, args... )		\
     printf( "[%16s:%4d P(%5d) T(%5d)] INFO\t|%16s| "fmt, __FUNCTION__, __LINE__, getpid(), gettid(), Thread::GetName().c_str(), ##args )

#define	INFO_RF( fmt, ... )	\
     printf( "[%16s:%4d P(%5d) T(%5d)] INFO\t| "fmt, __FUNCTION__, __LINE__, getpid(), gettid(), ##__VA_ARGS__ )

#ifdef __DEBUG__
#define	DEBUG_PRINTF( fmt, ... )	\
     printf( "[%16s:%4d P(%5d) T(%5d)] INFO\t| "fmt, __FUNCTION__, __LINE__, getpid(), gettid(), ##__VA_ARGS__ )
#else	//	__DEBUG__
#define	DEBUG_PRINTF( fmt, args... )
#endif//	__DEBUG__

#define	VERB_PRINTF( fmt, ... )	\
     printf( "[%16s:%4d P(%5d) T(%5d)] VERB\t| "fmt, __FUNCTION__, __LINE__, getpid(), gettid(), ##__VA_ARGS__ )
#define	INFO_PRINTF( fmt, ... )	\
     printf( "[%16s:%4d P(%5d) T(%5d)] INFO\t| "fmt, __FUNCTION__, __LINE__, getpid(), gettid(), ##__VA_ARGS__ )

#define	ERROR_PRINTF( fmt, ... )	\
     printf( RED_BOLD "[%16s:%4d P(%5d) T(%5d)] ERR\t| "fmt ENDCOLOR, __FUNCTION__, __LINE__, getpid(), gettid(), ##__VA_ARGS__ )

#endif	//	__ANDROID__

#endif	//	_DEBUG_H_
