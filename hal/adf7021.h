#ifndef	_ADF_7021_H_
#define	_ADF_7021_H_

#include	"rf.h"
#include	"reader.h"
#include	"commander.h"

#include	"../drv/pf_cmd.h"

#ifdef __cplusplus
extern "C"
{
#endif	//	__cplusplus

#ifndef MAX_SLOT_INDEX
#define MAX_SLOT_INDEX	( 22 )
#endif	//	MAX_SLOT_INDEX

/*	Callback definitions	*/
typedef void (*pfnAddDeviceCallback)( void *pArg );
typedef void (*pfnDeviceInfoCallback)( void *pArg );

typedef void (*pfnSlotConflictCallback)( void *pArg );
typedef void (*pfnScanResultCallback)( void *pArg );
typedef void (*pfnCollarPairingModeCallback)( void *pArg );

typedef void (*pfneCollarCommandFailureCallback)( void *pArg );
typedef void (*pfneCollarNickSuccessCallback)( void *pArg );
typedef void (*pfnBatteryLevelCallback)( void *pArg );

typedef void (*pfnSharingInfoCallback)( void *pArg );
typedef void (*pfnSharingSuccessCallback)( void *pArg );
typedef void (*pfnSharingCancelCallback)( void *pArg );

typedef struct
{
	pfnAddDeviceCallback			AddDeviceCallback;
	pfnDeviceInfoCallback			DeviceInfoCallback;

	pfnSlotConflictCallback 		SlotConflictCallback;
	pfnScanResultCallback			ScanResultCallback;
	pfnCollarPairingModeCallback	CollarPairingModeCallback;

	pfneCollarCommandFailureCallback	eCollarCommandFailureCallback;
	pfneCollarNickSuccessCallback		eCollarNickSuccessCallback;
	pfnBatteryLevelCallback				batteryLevelCallback;

	pfnSharingInfoCallback				SharingInfoCallback;
	pfnSharingSuccessCallback			SharingSuccessCallback;
	pfnSharingCancelCallback			SharingCancelCallback;

} ADF_CALLBACKS;

#ifdef __cplusplus
}
#endif	//	__cplusplus

namespace Pettra
{
	class Adf7021;

	typedef struct DEVICE_DETAIL_INFO_S
	{
		int id;

		int slotId;
		int frequencyId;
		int gpsRate;

		bool shared;

		int batteryLevel;
		int gpsSensitivity;
		int rfSensitivity;
		int rescueMode;

		double latitude;
		double longitude;
		double altitude;
		double speed;

		int dogState;
		int barkState;

		int bearing;
	} DEVICE_DETAIL_INFO;

/*	NOTE: HalModule MUST Not Use virtual functions.
 *	by jyhuh 2018-03-29 11:05:2018
 *	*/
class AdfHwDevice : public hw_device_t
{
private:
	const Adf7021 *m_pDevice;

protected:
public:
	explicit AdfHwDevice( const Adf7021 *pDevice = NULL );
	~AdfHwDevice();

	const Adf7021 * GetDevice( void ) const { return m_pDevice; };

	/*	extends		*/
	int	( *open )( const AdfHwDevice *pDevice );
	int	( *start )( const AdfHwDevice *pDevice );
	int	( *stop )( const AdfHwDevice *pDevice );
	//int	( *signal )( const AdfHwDevice *pDevice );
	int	( *setCallbacks )( const AdfHwDevice *pDevice, const ADF_CALLBACKS *pCallbacks );
	int ( *update )( const AdfHwDevice *pDevice, const std::map< int, ::DEVICE_INFO > &deviceMap );

	int ( *sendCommand )( const AdfHwDevice *pDevice,  const uint32_t command, const void * const pArg );
	int ( *getConnectorInfo )( const AdfHwDevice *pDevice, ::CONNECTOR_INFO &connectorInfo );
};

class Adf7021 : public RfDevice
{
	static const ssize_t s_size = 128;	/*	128 * 512 = 64 kB	*/
private:
	Reader			*m_pReader;
	Commander		*m_pCommander;
	ADF_CALLBACKS	m_callbacks;
	std::map< uint16_t, ::CH_SLOT > m_slotMap;		//	key: id
	std::map< int, ::DEVICE_INFO > m_deviceMap;	//	key: slot
#ifdef	__USE_FILE__
	mutable int m_fileFd;
	std::string m_fileName;
#endif	//	__USE_FILE__
	::DEVICE_INFO	m_changingDevInfo;

	/*	hw_device_t->close()	*/
	static int CloseDevice( hw_device_t *hw_device );

	/*	Interfaces of hw_device_t extends.	*/
	static int OpenDevice( const AdfHwDevice *pDevice );
	static int StartThread( const AdfHwDevice *pDevice );
	static int StopThread( const AdfHwDevice *pDevice );
	//static int SignalThread( const AdfHwDevice *pDevice );
	static int SetCallbacks( const AdfHwDevice *pDevice, const ADF_CALLBACKS *pCallbacks );
	static int SendCommand( const AdfHwDevice *pDevice,  const uint32_t command, const void * const pArg = NULL );
	static int GetConnectorInfo( const AdfHwDevice *pDevice, ::CONNECTOR_INFO &connectorInfo );
	static int Update( const AdfHwDevice *pDevice, const std::map< int, ::DEVICE_INFO > &deviceMap );

protected:
#ifdef	__USE_FILE__
	bool OpenFile( void ) const;
	bool CloseFile( void ) const;
	bool UpdateFile( void );
#endif	//	__USE_FILE__
	bool LoadDeviceMap( void );
	bool Update( const std::map< int, ::DEVICE_INFO > &deviceMap );
	bool UpdateDevice( const ::DEVICE_INFO &devInfo );
	bool AddDevice( const ::DEVICE_INFO &devInfo );
	bool RemoveDevice( const ::DEVICE_INFO &devInfo );
	bool ChangeDevice( const ::DEVICE_INFO &devInfo );

	bool ProcessPacket( const ::ADF_PACKET * const pPacket );
	bool UpdateDeviceDetailInfo( const ::PF_RX_COMMAND * const pRxCmd );

	const ::DEVICE_INFO GetDeviceInfo( const uint16_t id, const int slot = -1 );
	bool PostProcessCommand( const uint32_t command, const void * const pArg );

	size_t	Send( const uint8_t *pPacket );
	size_t	Receive( const uint8_t *pPacket );
	::ADF_PACKET* Receive( void );

	void* ThreadFunc( void * pArg );
public:
	explicit Adf7021( const RfDevice::PROTOCOL protocol = RfDevice::PATH_FINDER, \
			const std::string devName = "/dev/adf7021" );
	virtual ~Adf7021();

	static HalDevice* Create( void * const pArg = NULL );

	bool Init( void );
	bool SetCallbacks( const ADF_CALLBACKS *pCallbacks );
	bool Open( void );
	bool Close( void );
	bool OpenAccessors( void );
	bool CloseAccessors( void );

	bool SendCommand( const uint32_t command, const void * const pArg = NULL );
	bool ValidateCommand( const uint32_t command, const void * const pArg );
	bool GetConnectorInfo( ::CONNECTOR_INFO &connectorInfo );

	/*	HalDevice: hw_module_t->methods->open()	*/
	int OpenDevice( const hw_module_t *hw_module, const char *name, hw_device_t **hw_device ) const;

	bool SetSlotInfo( const ::DEVICE_INFO &devInfo );
};
}	//	namespace Pettra
#endif	//	_ADF_7021_H_
/*	vim: set ft=cpp:	*/
