#ifndef	_COMMANDER_H_
#define	_COMMANDER_H_

//#include	<sys/poll.h>

#include	"device_accessor.h"

#include	"../drv/adf7021.h"

namespace Pettra
{

#if 0
static const uint32_t	CMD_START_PAIR		= ADF_CMD_START_PAIR;
static const uint32_t	CMD_CANCEL_PAIR		= ADF_CMD_CANCEL_PAIR;
#endif

class Commander : public DeviceAccessor< ::IOCTL_CMD >
{
public:

private:

protected:
	void* ThreadFunc( void * pArg );

	bool ProcessCommand( const ::IOCTL_CMD * const pCommand );

public:
	Commander();
	~Commander();

	bool Close( void ) const;

	void  Clear( void );

	const ::IOCTL_CMD* GenerateCommand( const uint32_t command, const void * const pArg = NULL );
	bool GetConnectorInfo( ::CONNECTOR_INFO &connectorInfo );
};

}	//	namespace Pettra
#endif	//	_COMMANDER_H_
/*	vim: set ft=cpp:	*/

