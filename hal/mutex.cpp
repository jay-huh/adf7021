#include	"mutex.h"

#include	<errno.h>

#include	"debug.h"

using namespace Pettra;

Mutex::Mutex( std::string name )
	: m_name( name )
{
	MUTEX_PRINTF( "Constructor\n" );

	pthread_mutex_init( & m_mutex, NULL );
	pthread_cond_init( & m_cond, NULL );

	m_condition = false;
}

Mutex::~Mutex()
{
	MUTEX_PRINTF( "Destructor\n" );

	Unlock();

	pthread_mutex_destroy( & m_mutex );
	pthread_cond_destroy( & m_cond );
}

bool Mutex::Lock( int timeout ) const
{
	MUTEX_PRINTF( "LOCK: timeout: %d\n", timeout );

	if( timeout > 0 )
	{
		struct timespec	ts;

		clock_gettime( CLOCK_REALTIME, &ts );

		if( timeout > 1000 )
		{
			ts.tv_sec += ( timeout / 1000 );
			timeout %= 1000;
		}

		ts.tv_nsec += ( timeout * 1000000 );
		if( ts.tv_nsec > 1000000000 )
		{
			ts.tv_sec++;
			ts.tv_nsec -= 1000000000;
		}

		pthread_mutex_timedlock( & m_mutex, & ts );
	}
	else
	{
		pthread_mutex_lock( & m_mutex );
	}

	return true;
}

void Mutex::Unlock() const
{
	MUTEX_PRINTF( "UNLOCK:\n" );
	pthread_mutex_unlock( & m_mutex );

	return ;
}

bool Mutex::Wait( int timeout ) const
{
	MUTEX_PRINTF( "WAIT: timeout: %d\n", timeout );

	//m_status = STATUS_WAIT;
	if( timeout > 0 )
	{
		struct timespec	ts;

		clock_gettime( CLOCK_REALTIME, & ts );

		if( timeout > 1000 )
		{
			ts.tv_sec += ( timeout / 1000 );
			timeout %= 1000;
		}

		ts.tv_nsec += ( timeout * 1000000 );
		if( ts.tv_nsec > 1000000000 )
		{
			ts.tv_sec++;
			ts.tv_nsec -= 1000000000;
		}

		m_condition = false;
		while( m_condition == false )
		{
			if( pthread_cond_timedwait( & m_cond, & m_mutex, & ts ) == ETIMEDOUT )
			{
				MUTEX_PRINTF( "TIMEOUT: timeout: %d\n", timeout );
				break;
			}
		}
	}
	else
	{
		m_condition = false;
		while( m_condition == false )
		{
			pthread_cond_wait( & m_cond, & m_mutex );
		}
	}
	//m_status = STATUS_RUNNING;

	return true;
}

void Mutex::Signal() const
{
	MUTEX_PRINTF( "SIGNAL\n" );
	//if( m_status == STATUS_WAIT )
	{
		m_condition = true;
		pthread_cond_signal( & m_cond );
	}

	return ;
}

void Mutex::Broadcast() const
{
	MUTEX_PRINTF( "BROADCAST\n" );
	m_condition = true;
	pthread_cond_broadcast( & m_cond );

	return ;
}
