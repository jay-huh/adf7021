#ifndef _HAL_MODULE_H_
#define _HAL_MODULE_H_

#include	<map>
#include	<string>

#include	<hardware/hardware.h>

namespace Pettra
{

class HalDevice;

/*	NOTE: HalModule MUST Not Use virtual functions.
 *	by jyhuh 2018-03-29 11:05:2018
 *	*/
class HalModule : public hw_module_t
{
public:
	static const char *s_moduleId[];

#ifndef		HAL_HARDWARE_MODULE_ID
	#define		HAL_HARDWARE_MODULE_ID "test_jyhuh"
#endif	//	HAL_HARDWARE_MODULE_ID

#ifndef		HAL_HARDWARE_MODULE_NAME
	#define		HAL_HARDWARE_MODULE_NAME "Android HAL Module"
#endif	//	HAL_HARDWARE_MODULE_NAME

	typedef std::pair< std::string, int >	HAL_IDX_PAIR;

	typedef enum HAL_MODULE_INDEX_E
	{
		HAL_IDX_BAD = -1,
		HAL_IDX_TEST = 0,
		HAL_IDX_RF,
		HAL_IDX_ADF,

		HAL_IDX_END,
	} HAL_MODULE_INDEX;

	/*	NOTE: Use extend fields for module extension.
	 *	such as, function ptrs, data ans so on...
	 *	*/
	union
	{
		uint32_t	extends[ 512 ];

		struct
		{
			int (*test)( int arg );
		} TEST;
	};

private:
	HalDevice	*m_pHalDevice;
	static const hw_module_methods_t	s_methods;
	std::map< std::string, int >		m_halModuleMap;

	static int Open( const hw_module_t* hw_module, const char* name, hw_device_t** hw_device );

public:
	HalModule();
	~HalModule();

	const hw_module_t * GetModulePtr( void ) const { return dynamic_cast< const hw_module_t * >( this ); };
	const char * GetId( void ) const { return id; };
	const char * GetName( void ) const { return name; };

	void SetHalDevice( const HalDevice *pDevice );
	const HalDevice * GetHalDevice( void ) const { return m_pHalDevice; };

	int GetHalIndex( const std::string name ) const;
};
}	//	namespace Pettra

extern Pettra::HalModule	HAL_MODULE_INFO_SYM;

#endif	//	_HAL_MODULE_H_
/*	vim: set ft=cpp:	*/
