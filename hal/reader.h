#ifndef	_READER_H_
#define	_READER_H_

#include	<sys/poll.h>	//	nfds_t

#include	"device_accessor.h"

#include	"../drv/adf7021.h"

namespace Pettra
{
class Reader : public DeviceAccessor< ::ADF_PACKET >
{
private:

protected:
	void* ThreadFunc( void * pArg );
	int Poll( struct pollfd fds[], nfds_t numFds );

public:
	Reader();
	~Reader();

	bool Close( void ) const;

	void  Clear( void );
};

//typedef Reader< ADF_PACKET >	AdfPacketReader;

}	//	namespace Pettra
#endif	//	_READER_H_
/*	vim: set ft=cpp:	*/
