#include	"commander.h"

//#include	<unistd.h>	//	read
//#include	<sys/poll.h>
//#include	<string.h>	//	strlen

#include	<errno.h>

#include	"../drv/pf_cmd.h"

#include	"debug.h"

using namespace Pettra;

Commander::Commander()
	: DeviceAccessor< ::IOCTL_CMD >( "COMMANDER", "CMD_Q", "CMD_T" )
{
	this->Clear();
}

Commander::~Commander()
{
	this->Clear();
}

void Commander::Clear( void )
{
	::IOCTL_CMD	*pPacket = NULL;

	while( this->IsEmpty() == false )
	{
		THREAD_INFO_PRINTF( ">>> IsEmpty: %d size: %d pPacket: %p\n",
				this->IsEmpty(), this->Size(), pPacket );

		pPacket = this->Dequeue();

		assert( pPacket != NULL );

		THREAD_INFO_PRINTF( "DELETE: pPacket: %p\n", pPacket );

		delete pPacket;
		pPacket = NULL;
	}

	return;
}

bool Commander::Close( void ) const
{
	//THREAD_INFO_PRINTF( "CLOSE: Commander fd: %d\n", this->GetFd() );
	THREAD_INFO_PRINTF( "CLOSE: Commander\n" );

	//return DeviceAccessor< ::IOCTL_CMD >::Close();
	return this->DeviceAccessor::Close();
}

void* Commander::ThreadFunc( void *pArg )
{
	::IOCTL_CMD	*pCommand = NULL;

	//this->Init();

	THREAD_INFO_PRINTF( "START: %s IsRunning(): %d fd: %d\n", this->GetName().c_str(), this->IsRunning(), this->GetFd() );

	while( this->IsRunning() == true )
	{
		Thread::Lock();
		while( ( this->IsEmpty() == true ) && ( this->IsRunning() == true ) )
		{
			Thread::Wait( 100 );	//	100ms timeout
		}
		Thread::Unlock();

		while( this->IsEmpty() == false )
		{
			pCommand = this->Dequeue();

			assert( pCommand != NULL );

			/*	TODO: Process Command...	*/
			ProcessCommand( pCommand );

			delete pCommand;
			pCommand = NULL;
		}
	}

	THREAD_INFO_PRINTF( "END: %s IsRunning(): %d\n", this->GetName().c_str(),this->IsRunning() );

	return NULL;
}

const ::IOCTL_CMD* Commander::GenerateCommand( const uint32_t command, const void * const pArg )
{
	::IOCTL_CMD	*pCommand = NULL;
	const IOCTL_CMD_DATA * const pCmdData = reinterpret_cast< const ::IOCTL_CMD_DATA * const >( pArg );

	assert( command != 0x0 );

	pCommand = new ::IOCTL_CMD();

	pCommand->cmd = command;
	pCommand->length = _IOC_SIZE( command );

	if( pCmdData != NULL )
	{
		pCommand->data.rf_cmd = pCmdData->rf_cmd;
		::memcpy( pCommand->data.data, pCmdData->data, pCommand->length );
	}
	else
	{
		pCommand->data.rf_cmd = PF_CMD_UNDEFINED;
	}
	THREAD_INFO_PRINTF( "cmd: 0x%08x 0x%2x\n", pCommand->cmd, pCommand->data.rf_cmd );

	return static_cast< const ::IOCTL_CMD* >( pCommand );
}

bool Commander::ProcessCommand( const ::IOCTL_CMD * const pCommand )
{
	int fd = GetFd();

	assert( pCommand != NULL );
	assert( pCommand->cmd != 0x0 );

	THREAD_INFO_PRINTF( "CMD: 0x%08x length: %d\n", pCommand->cmd, pCommand->length );
	//THREAD_INFO_PRINTF( "sizeof( ADF_PACKET ): %d MAX_PACKET_LEN: %d\n", sizeof( ADF_PACKET ), MAX_PACKET_LEN );

	if( fd < 0 ) return false;

	if( ::ioctl( fd, pCommand->cmd, &pCommand->data ) != 0 )
	{
		ERROR_PRINTF( "Adf7021: ioctl cmd: 0x%08x len: %d failed!!!!\n", pCommand->cmd, pCommand->length );
		return false;
	}

	return true;
}

bool Commander::GetConnectorInfo( ::CONNECTOR_INFO &connectorInfo )
{
	int fd = GetFd();

	THREAD_INFO_PRINTF( "GetConnectorInfo()\n" );

	if( fd < 0 ) return false;

	if( ::ioctl( fd, ADF_CMD_GET_CONNECTOR_INFO, &connectorInfo ) != 0 )
	{
		ERROR_PRINTF( "Adf7021: ioctl cmd: ADF_CMD_GET_CONNECTOR_INFO  failed!!!!\n" );
		return false;
	}
	THREAD_INFO_PRINTF( "CONNECTOR_INFO: id 0x%04x\n", connectorInfo.id );

	return true;
}
