#include	"thread.h"

#include	<stdio.h>
#include	<time.h>
#include	<sys/prctl.h>

#include	<errno.h>
#include	<string.h>	//	strerror()

#include	"debug.h"

using namespace Pettra;

Thread::Thread( const std::string name, const int priority )
: Mutex( name ), m_name( name ), m_priority( priority )
{
	m_id = 0;
	m_isRunning = false;
	m_status = STATUS_STOPPED;
}

Thread::~Thread()
{
	if( m_isRunning == true )
	{
		Join();
	}
}

void Thread::SetThreadName( void ) const
{
	int ret = prctl( PR_SET_NAME, (unsigned long)( GetName().c_str() ) );
	if(ret < 0) {
		THREAD_PRINTF( "ERROR - %s(0x%08x)\n", strerror(errno), errno);
	} else {
		char pBuf[ 16 ] = { 0x0, };
		prctl( PR_GET_NAME, (unsigned long)pBuf );
		THREAD_PRINTF( "SET_NAME: %s\n", pBuf);
	}
	return ;
}

bool Thread::Start() const
{
	THREAD_PRINTF( "START priority: %d\n", m_priority );

	int  policy, prev_policy;
	struct sched_param schedParam, prev_schedParam;

	pthread_getschedparam( pthread_self(), &prev_policy, &prev_schedParam );

	policy = SCHED_OTHER;
	schedParam.sched_priority = 0;

	pthread_setschedparam( pthread_self(), policy, &schedParam );
	if( pthread_create( & m_id, NULL, Run, ( void * )this ) != 0 )
	{
		return false;
	}
	m_isRunning = true;
	m_status = STATUS_RUNNING;

	pthread_setschedparam( pthread_self(), prev_policy, &prev_schedParam );

	return true;
}

void *Thread::Run( void * pArg )
{
	Thread	*pThread = static_cast< Thread * >( pArg );
	void *pRetVal;

	pThread->SetThreadName();

	pRetVal = pThread->ThreadFunc( pArg );

	THREAD_INFO_PRINTF( "RET: %s pRetVal: %p\n", pThread->GetName().c_str(), pRetVal );

	pthread_exit( ( void * )pRetVal );

	return NULL;
}

bool Thread::Stop() const
{
	THREAD_PRINTF( "STOP priority: %d\n", m_priority );

	m_isRunning = false;

	Lock();
	Broadcast();
	Unlock();

	return true;
}

bool Thread::Join( void **ppRetVal ) const
{
	void *pRet = NULL;

	Stop();

	pthread_join( m_id, ( void ** )&pRet );

	m_status = STATUS_STOPPED;

	if( ppRetVal != NULL )
	{
		*ppRetVal = pRet;
	}
	THREAD_INFO_PRINTF( "JOIN END: %s pRet: %p\n", GetName().c_str(), pRet );

	return true;
}
