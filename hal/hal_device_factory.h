#ifndef _HAL_DEVICE_FACTORY_H_
#define _HAL_DEVICE_FACTORY_H_

#include	<map>
#include	<string>

namespace Pettra
{
class HalDevice;

class HalDeviceFactory
{
public:
	typedef HalDevice *( * pFnCreateHalDevice )( void * const pArg );
	typedef std::pair< int, pFnCreateHalDevice >	HAL_PFN_PAIR;

private:
	HalDeviceFactory();

	std::map< int , pFnCreateHalDevice >		m_factoryMap;

	void Register( const int index, const pFnCreateHalDevice pFnCreate );

protected:
public:
	~HalDeviceFactory();

	static HalDeviceFactory *GetInstance( void )
	{
		static HalDeviceFactory _instance;
		return &_instance;
	};

	const HalDevice* CreateHalDevice( const int index );
};

}	//	namespace Pettra
#endif	//	_HAL_DEVICE_FACTORY_H_
/*	vim: set ft=cpp:	*/
