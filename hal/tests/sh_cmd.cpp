
#include <stdio.h>
#include <unistd.h>

#include <assert.h>

#include	"simple_sh.h"

#include	"adf7021.h"

extern Pettra::AdfHwDevice	*g_pAdfHwDevice;

/*
   Function Declarations for builtin shell commands:
   */
static int do_help(char **args);
static int do_exit(char **args);

static int do_cd(char **args);
static int do_test(char **args);
static int do_pairing( char **args );
int do_sleep( char **args );
int do_changePeriod( char **args );
int do_changeSlotFreq( char **args );
int do_scanSlotFreq( char **args );
int do_deleteDevice( char **args );
int do_nickOne( char **args );
int do_startConst( char **args );
int do_startTone( char **args );
int do_stopEClollar( char **args );
int do_startShareTx( char **args );
int do_startShareRx( char **args );
int do_cancelShare( char **args );
int do_stat( char **args );

/*
   List of builtin commands, followed by their corresponding functions.
   */
const BUILTIN_FN_PAIR s_builtinFuncPair[ ] = {
	{ "help", do_help },
	{ "exit", do_exit },
	{ "stat", do_stat },
	{ "test", do_test },
	{ "pair", do_pairing },
	{ "sleep", do_sleep },
	{ "nick", do_nickOne },
	{ "const", do_startConst },
	{ "tone", do_startTone },
	{ "sh_tx", do_startShareTx },
	{ "sh_rx", do_startShareRx },
	{ "sh_stop", do_cancelShare },
	{ "stop", do_stopEClollar },
	{ "ch_period", do_changePeriod },
	{ "ch_slot_freq", do_changeSlotFreq },
	{ "scan", do_scanSlotFreq },
	{ "del", do_deleteDevice },
};

const size_t g_numBuiltinFuncPair = ( sizeof( s_builtinFuncPair ) / sizeof( BUILTIN_FN_PAIR ) );
const BUILTIN_FN_PAIR *g_pBuiltinFuncPair = s_builtinFuncPair;

/*
   Builtin function implementations.
   */

/**
  @brief Builtin command: print help.
  @param args List of args.  Not examined.
  @return Always returns 1, to continue executing.
  */
int do_help(char **args)
{
	int i;
	printf("The following are built in:\n");

	for (i = 0; i < sh_num_builtins(); i++) {
		printf("  %s\n", s_builtinFuncPair[ i ].name );
	}
	return 1;
}

/**
  @brief Builtin command: exit.
  @param args List of args.  Not examined.
  @return Always returns 0, to terminate execution.
  */
int do_exit(char **args)
{
	return 0;
}

/**
  @brief Bultin command: change directory.
  @param args List of args.  args[0] is "cd".  args[1] is the directory.
  @return Always returns 1, to continue executing.
  */
int do_cd(char **args)
{
	if (args[1] == NULL) {
		fprintf(stderr, SH_NAME ": expected argument to \"cd\"\n" );
	} else {
		if (chdir(args[1]) != 0) {
			perror( SH_NAME );
		}
	}
	return 1;
}

int do_test(char **args)
{
	int i;

	assert( *args );

	for( i = 1 ; args[ i ] != NULL; i++  )
	{
		printf( SH_NAME ": args[ %d ]: \'%s\'\n", i, args[ i ] );
	}
	return 1;
}

int do_pairing( char **args )
{
	IOCTL_CMD_DATA	cmdData;
	int i;

	assert( *args );

	printf( SH_NAME ": PAIRING %s\n", args[ 1 ] );
#if 0
	if( args[ 1 ] == NULL)
	{
		printf( "usage: %s [ start | stop ]\n", args[ 0 ] );
		return 1;
	}
#endif

	if( ( args[ 1 ] != NULL ) && ( strcmp( args[ 1 ], "stop" ) == 0 ) )
	{
		g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_PAIR_CANCEL, NULL );
	}
	else
	{
		cmdData.rf_cmd = PF_CMD_PAIRING;
		g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_PAIR_START, &cmdData );
	}

	return 1;
}

int do_changePeriod( char **args )
{
	IOCTL_CMD_DATA	cmdData;
	int i;

	assert( *args );

	printf( SH_NAME ": CHANGE_PERIOD %s %s\n", args[ 1 ], args[ 2 ] );
	if( ( args[ 1 ] == NULL ) ||( args[ 2 ] == NULL ) )
	{
		printf( "usage: %s id period\n", args[ 0 ] );
		return 1;
	}

	cmdData.rf_cmd = PF_CMD_CHANGE_PERIOD_REQ;
	cmdData.changePeriod.id = strtol( args[ 1 ], NULL, 16 );
	cmdData.changePeriod.period = ( ::PERIOD )atoi( args[ 2 ] );

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_CHANGE_PERIOD, &cmdData );

	return 1;
}

int do_changeSlotFreq( char **args )
{
	IOCTL_CMD_DATA	cmdData;
	int i;

	assert( *args );

	printf( SH_NAME ": CHANGE_SLOT_FREQ: %s %s %s\n", args[ 1 ], args[ 2 ], args[ 3 ] );
	if( ( args[ 1 ] == NULL ) ||( args[ 2 ] == NULL ) || ( args[ 3 ] == NULL ) )
	{
		printf( "usage: %s id slotId frequency\n", args[ 0 ] );
		return 1;
	}

	cmdData.rf_cmd = PF_CMD_CHANGE_SLOT_REQ;
	cmdData.changeSlotFreq.id = strtol( args[ 1 ], NULL, 16 );
	cmdData.changeSlotFreq.slotId = atoi( args[ 2 ] );
	cmdData.changeSlotFreq.freq = ( ::FREQ )atoi( args[ 3 ] );

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_CHANGE_SLOT_FREQ, &cmdData );

	return 1;
}

int do_scanSlotFreq( char **args )
{
	IOCTL_CMD_DATA	cmdData;
	int i;

	assert( *args );

	printf( SH_NAME ": SCAN_SLOT_FREQ: %s %s\n", args[ 1 ], args[ 2 ] );
	if( ( args[ 1 ] == NULL ) ||( args[ 2 ] == NULL ) )
	{
		printf( "usage: %s slotId frequency\n", args[ 0 ] );
		return 1;
	}

	cmdData.rf_cmd = PF_CMD_UNDEFINED;
	cmdData.changeSlotFreq.slotId = atoi( args[ 1 ] );
	cmdData.changeSlotFreq.freq = ( ::FREQ )atoi( args[ 2 ] );

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_SCAN_SLOT_FREQ, &cmdData );

	return 1;
}

int do_deleteDevice( char **args )
{
	IOCTL_CMD_DATA	cmdData;
	int i;

	assert( *args );

	printf( SH_NAME ": DELETE_DEVICE %s\n", args[ 1 ] );
	if( args[ 1 ] == NULL )
	{
		printf( "usage: %s id\n", args[ 0 ] );
		return 1;
	}

	cmdData.rf_cmd = PF_CMD_DELETE_DOG;
	cmdData.deleteDevice.id = strtol( args[ 1 ], NULL, 16 );

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_DELETE_DEVICE, &cmdData );

	return 1;
}

int do_sleep( char **args )
{
	IOCTL_CMD_DATA	cmdData;
	int i;

	assert( *args );

	printf( SH_NAME ": SLEEP %s %s\n", args[ 1 ], args[ 2 ] );

	cmdData.rf_cmd = PF_CMD_SLEEP_MODE;
	//cmdData.sleepMode.id = SLEEP_ALL_DOGS_ID;
	cmdData.sleepMode.mode = false;
	if( ( args[ 1 ] != NULL ) && ( strcmp( args[ 1 ], "on" ) == 0 ) )
	{
		cmdData.sleepMode.mode = true;
	}

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_SLEEP_MODE, &cmdData );

	return 1;
}

int do_nickOne( char **args )
{
	IOCTL_CMD_DATA	cmdData;
	int i;

	assert( *args );

	printf( SH_NAME ": NICK_ONE: %s %s\n", args[ 1 ], args[ 2 ] );
	if( ( args[ 1 ] == NULL ) ||( args[ 2 ] == NULL ) )
	{
		printf( "usage: %s id level\n", args[ 0 ] );
		return 1;
	}

	cmdData.rf_cmd = PF_CMD_NICK_ONE;
	cmdData.eCollar.id = strtol( args[ 1 ], NULL, 16 );
	cmdData.eCollar.level = atoi( args[ 2 ] );

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_EC_NICK_ONE, &cmdData );

	return 1;
}

int do_startConst( char **args )
{
	IOCTL_CMD_DATA	cmdData;
	int i;

	assert( *args );

	printf( SH_NAME ": START_CONST: %s %s\n", args[ 1 ], args[ 2 ] );
	if( ( args[ 1 ] == NULL ) ||( args[ 2 ] == NULL ) )
	{
		printf( "usage: %s id level\n", args[ 0 ] );
		return 1;
	}

	cmdData.rf_cmd = PF_CMD_NICK_REQ;
	cmdData.eCollar.id = strtol( args[ 1 ], NULL, 16 );
	cmdData.eCollar.level = atoi( args[ 2 ] );

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_EC_START_CONST, &cmdData );

	return 1;
}

int do_startTone( char **args )
{
	IOCTL_CMD_DATA	cmdData;
	int i;

	assert( *args );

	printf( SH_NAME ": START_TONE: %s\n", args[ 1 ] );
	if( args[ 1 ] == NULL )
	{
		printf( "usage: %s id\n", args[ 0 ] );
		return 1;
	}

	cmdData.rf_cmd = PF_CMD_TONE;
	cmdData.eCollar.id = strtol( args[ 1 ], NULL, 16 );
	cmdData.eCollar.level = 0x14;

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_EC_START_TONE, &cmdData );

	return 1;
}

int do_stopEClollar( char **args )
{
	int i;

	assert( *args );

	printf( SH_NAME ": STOP_E_COLLAR:\n" );

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_EC_STOP, NULL );

	return 1;
}

int do_startShareTx( char **args )
{
	IOCTL_CMD_DATA	cmdData;
	int i;

	assert( *args );

	printf( SH_NAME ": SH_TX: %s %s\n", args[ 1 ], args[ 2 ] );
	if( ( args[ 1 ] == NULL ) ||( args[ 2 ] == NULL ) )
	{
		printf( "usage: %s id name\n", args[ 0 ] );
		return 1;
	}

	cmdData.devInfo.id = strtol( args[ 1 ], NULL, 16 );
	cmdData.devInfo.slot = -1;
	cmdData.devInfo.freq = -1;
	cmdData.rf_cmd = ( IS_HUNTER( cmdData.devInfo.id ) == true ) ? PF_CMD_SHARE_HUNTER_INFO : PF_CMD_SHARE_DOG_INFO;
	::memcpy( cmdData.devInfo.name, args[ 2 ], HUNTER_NAME_LEN );

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_SHARING_SEND_INFO, &cmdData );

	return 1;
}

int do_startShareRx( char **args )
{
	assert( *args );

	printf( SH_NAME ": SHARE_RX: %s\n", args[ 0 ] );

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_SHARING_WAIT, NULL );

	return 1;
}

int do_cancelShare( char **args )
{
	assert( *args );

	printf( SH_NAME ": CANCEL_SHARE: %s\n", args[ 0 ] );

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_SHARING_CANCEL, NULL );

	return 1;
}

int do_stat( char **args )
{
	IOCTL_CMD_DATA	cmdData;
	int i;

	assert( *args );

	printf( SH_NAME ": STAT\n" );

	cmdData.rf_cmd = PF_CMD_UNDEFINED;
	cmdData.stat.clear = false;
	if( ( args[ 1 ] != NULL ) && ( strcmp( args[ 1 ], "clear" ) == 0 ) )
	{
		cmdData.stat.clear = true;
	}

	g_pAdfHwDevice->sendCommand( g_pAdfHwDevice, ADF_CMD_STAT, &cmdData );

	return 1;
}

