LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := adf-test-android
#LOCAL_MODULE_TAGS := tests
LOCAL_MODULE_TAGS := eng optional	#	eng, user, optional
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/system/bin

ifeq ($(TARGET_ARCH_ABI), $(filter $(TARGET_ARCH_ABI), armeabi-v7a))
	LOCAL_CFLAGS := -march=armv7-a -mfloat-abi=softfp -mfpu=neon
	LOCAL_LDFLAGS := -march=armv7-a -Wl,--fix-cortex-a8
endif

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += \
	-D LOG_TAG=\"adf-test\" \

LOCAL_CFLAGS += -D __ANDROID_LOG__
LOCAL_CFLAGS += -D __USE_SH__
#LOCAL_CFLAGS += -D __RELEASE__

LOCAL_SRC_FILES := \
	simple_sh.cpp \
	sh_cmd.cpp \
	main.cpp

LOCAL_C_INCLUDES := \
	external/stlport/stlport \
	bionic \
	$(LOCAL_PATH)/../../hal

LOCAL_SHARED_LIBRARIES := \
	libstlport \
	libhardware \

LOCAL_LDLIBS := \
	-llog

include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := queue-test
LOCAL_MODULE_TAGS := tests

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += \
	-D LOG_TAG=\"queue-test\" \

#LOCAL_CFLAGS += -D __ANDROID_LOG__
#LOCAL_CFLAGS += -D __DEBUG_RF__
#LOCAL_CFLAGS += -D __DEBUG_THREAD__

LOCAL_SRC_FILES := \
	queue-test.cpp

LOCAL_C_INCLUDES := \
	external/stlport/stlport \
	bionic

LOCAL_SHARED_LIBRARIES := \
	libstlport

LOCAL_LDLIBS := \
	-llog

include $(BUILD_EXECUTABLE)
