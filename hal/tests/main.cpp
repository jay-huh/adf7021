#include	<signal.h>	//	signal
#include	<fcntl.h>	//	open, close

#include	"debug.h"

#include	"hal_device_factory.h"
#include	"adf7021.h"

#ifdef __USE_SH__
#include	"simple_sh.h"
#endif	//	__USE_SH__

static void addDeviceCallback( void *pArg );
static void deviceInfoCallback( void *pArg );

static void slotConflictCallback( void *pArg );
static void scanResultCallback( void *pArg );
static void collarPairingModeCallback( void *pArg ) {};

static void eCollarCommandFailureCallback( void *pArg );
static void eCollarNickSuccessCallback( void *pArg );
static void batteryLevelCallback( void *pArg );

static void sharingInfoCallback( void *pArg );
static void sharingSuccessCallback( void *pArg );
static void sharingCancelCallback( void *pArg );

static void scanResultCallback( void *pArg )
{
	::CH_SLOT *pChSlot = NULL;

	assert( pArg != NULL );

	pChSlot = reinterpret_cast< ::CH_SLOT * >( pArg );

	LOGV( "%s: slotId: %d freq: %d", __func__, pChSlot->slot, pChSlot->ch );
	return;
}

void slotConflictCallback( void *pArg )
{
	int *pSlotId = NULL;

	assert( pArg != NULL );

	pSlotId = reinterpret_cast< int * >( pArg );

	LOGV( "%s: slotId: %d", __func__, *pSlotId );

	return;
}

void eCollarNickSuccessCallback( void *pArg )
{
	LOGV( "%s\n", __func__ );
	return;
}

void eCollarCommandFailureCallback( void *pArg )
{
	LOGV( "%s\n", __func__ );
	return;
}

void batteryLevelCallback( void *pArg )
{
	int *pLevel = NULL;

	assert( pArg != NULL );

	pLevel = reinterpret_cast< int * >( pArg );

	LOGV( "%s: level: %d", __func__, *pLevel );

	return;
}

void sharingInfoCallback( void *pArg )
{
	::DEVICE_INFO *pInfo = NULL;

	assert( pArg != NULL );

	pInfo = reinterpret_cast< ::DEVICE_INFO * >( pArg );

	LOGV( "%s: id 0x%04x | f %d s %d p %d | shared: %d name: %16s\n", __func__, \
			pInfo->id, pInfo->freq, pInfo->slot, pInfo->period, pInfo->shared, pInfo->name );

	return;
}

void sharingSuccessCallback( void *pArg )
{
	LOGV( "%s\n", __func__ );
	return;
}

void sharingCancelCallback( void *pArg )
{
	LOGV( "%s\n", __func__ );
	return;
}

static ADF_CALLBACKS s_callbacks =
{
	AddDeviceCallback : addDeviceCallback,
	DeviceInfoCallback : deviceInfoCallback,

	SlotConflictCallback : slotConflictCallback,
	ScanResultCallback : scanResultCallback,
	CollarPairingModeCallback : collarPairingModeCallback,

	eCollarCommandFailureCallback : eCollarCommandFailureCallback,
	eCollarNickSuccessCallback: eCollarNickSuccessCallback,
	batteryLevelCallback : batteryLevelCallback,

	SharingInfoCallback : sharingInfoCallback,
	SharingSuccessCallback : sharingSuccessCallback,
	SharingCancelCallback : sharingCancelCallback,
};

void addDeviceCallback( void *pArg )
{
	::DEVICE_INFO *pInfo = NULL;

	if( pArg == NULL ) return;

	//INFO_PRINTF( "addDeviceCallback\n");

	pInfo = reinterpret_cast< ::DEVICE_INFO * >( pArg );

	INFO_PRINTF( "AddDevice: id 0x%04x ch: %d slot: %d\n", \
			pInfo->id, pInfo->freq, pInfo->slot );

	return;
}

void printGPSInfo( ::PF_RX_COMMAND *pRxCmd )
{
	assert( pRxCmd != NULL );

	::GPS_INFO *pInfo = NULL;
	pInfo = &pRxCmd->gps_info;
#if 1
	static int lastSlot = 100;

	if( lastSlot >= pInfo->ch_slot.slot )
	{
		LOGV( "first:-------------------------" );
	}

	int id;
	int period;
	int slot;
	int freq;
	int batteryLevel;
	int rescueMode;
	int gpsSensitivity;
	int rfSensitivity;
	int barkState;
	double altitude;
	double speed;
	int dogState;

	int bearing = 0;	//	Not used
	bool shared = false;

	double latitude = ( pRxCmd->gps_info.lat_int / 100 ) + \
					  ( ( double )( pRxCmd->gps_info.lat_int % 100 ) + \
						( ( double )( pRxCmd->gps_info.lat_frac * 10 ) / 100000 ) ) / 60;
	double longitude = ( pRxCmd->gps_info.long_int / 100 ) + \
					   ( ( double )( pRxCmd->gps_info.long_int % 100 ) + \
						 ( ( double )( pRxCmd->gps_info.long_frac * 10 ) / 100000 ) ) / 60;

	int tmp_lat = pInfo->lat_int * -1;
	int tmp_lng = pInfo->long_int * -1;

	double n_latitude = ( tmp_lat / 100 ) + \
					  ( ( double )( pRxCmd->gps_info.lat_int % 100 ) + \
						( ( double )( pRxCmd->gps_info.lat_frac * 10 ) / 100000 ) ) / 60;
	double n_longitude = ( tmp_lng / 100 ) + \
					   ( ( double )( pRxCmd->gps_info.long_int % 100 ) + \
						 ( ( double )( pRxCmd->gps_info.long_frac * 10 ) / 100000 ) ) / 60;

	//double latitude_1 = (double)( ( long )( latitude * 10000 ) ) / 10000;
	//double longitude_1 = (double)( ( long )( longitude * 10000 ) ) / 10000;

	altitude = pInfo->altitude / 10.0;	//	m
	/*	GPS RMC spd field[ knots ] 1 knots = 1.852 km/h = 1852 / 3600 m/s
	 *	Collar F/W: velocity = spd * 10000 / 3600 => m/s * 10 ???
	 *
	 *	reverse calculate RMS spd = velocity * 3600 / 10000
	 *	velocity = spd * 1852 * 100 / 3600 cm/s
	 *	*/
	//speed = pInfo.velocity / 10.0;		//	m/s
	speed = ( double )pInfo->velocity * 1852 / 10000;		//	m/s

	id = pInfo->dog_id;
	slot = pInfo->ch_slot.slot;
	freq = pInfo->ch_slot.ch;
	period = pInfo->dog_info.period;

	batteryLevel = pInfo->battery.level;
	rescueMode = pInfo->dog_info.rescue_mode;
	gpsSensitivity = pInfo->dog_info.gps_rssi;
	rfSensitivity = pRxCmd->rssi_dBm;
	barkState = pInfo->dog_info.bark;
	dogState = pInfo->dog_info.three_axis;

	LOGV( "DeviceInfo: pInfo: %p id: 0x%04x [s %d f %d p %d] GPS: lat: %.3f lng: %.3f\n", \
			pInfo, id, slot, freq, period, latitude, longitude );
	LOGV( "DeviceInfo: lat: %d.%d %d %f %f lng: %d.%d %d %f %f\n", \
			pInfo->lat_int, pInfo->lat_frac, tmp_lat, latitude, n_latitude, pInfo->long_int, pInfo->long_frac, \
			tmp_lng, longitude, n_longitude );
	LOGV( "DeviceInfo: GPS: altitude: %.2f m speed: %d %.2f m/s rssi: %d dBm\n", \
			altitude, pInfo->velocity, speed, rfSensitivity );

	lastSlot = pInfo->ch_slot.slot;
#else
	LOGV( "dog_id: 0x%04x", pInfo->dog_id );
	LOGV( "hunter_id: 0x%04x", pInfo->hunter_id );
	LOGV( "lat_int: %d", pInfo->lat_int );
	LOGV( "lat_frac: %d", pInfo->lat_frac );
	LOGV( "long_int: %d", pInfo->long_int );
	LOGV( "long_frac: %d", pInfo->long_frac );
	LOGV( "altitude: %d", pInfo->altitude );
	LOGV( "velocity: %d", pInfo->velocity );
	LOGV( "ch_slot: 0x%02x ch: %d slot: %d", pInfo->ch_slot, pInfo->ch_slot.ch, pInfo->ch_slot.slot );
	LOGV( "battery: %d", pInfo->battery );
	LOGV( "dog_info: 0x%04x", pInfo->dog_info );
	LOGV( "checksum: 0x%04x", pInfo->checksum );
#endif
	return;
}

void deviceInfoCallback( void *pArg )
{
	Pettra::DEVICE_DETAIL_INFO *pInfo = reinterpret_cast< Pettra::DEVICE_DETAIL_INFO * >( pArg );

	if( pArg == NULL ) return;

	static int lastSlot = 100;

	if( lastSlot >= pInfo->slotId )
	{
		LOGV( "first:-------------------------" );
	}

	LOGV( "DeviceInfo: id: 0x%04x [s %d f %d p %d] GPS: lat: %.3f lng: %.3f\n", \
			pInfo->id, pInfo->slotId, pInfo->frequencyId, pInfo->gpsRate, pInfo->latitude, pInfo->longitude );
	LOGV( "DeviceInfo: altitude: %.2f m speed: %.2f m/s rssi: %d dBm\n", \
			pInfo->altitude, pInfo->speed, pInfo->rfSensitivity );

	lastSlot = pInfo->slotId;
	return;
}

Pettra::AdfHwDevice	*g_pAdfHwDevice = NULL;

#ifndef __USE_SH__
bool g_isRunning = false;

void sig_handler( int signo )
{
	INFO_PRINTF( ">>>>>>>>>>>>>>SIGNAL: %d\n", signo );

	g_isRunning = false;

	return ;
}
#endif	//	__USE_SH__

#define PAIR_INFO_FILE	"/data/misc/adf/pair_info"

bool LoadFile( const std::string &fileName, std::map< int, ::DEVICE_INFO > &deviceMap )
{
	::DEVICE_INFO devInfo;
	::CH_SLOT ch_slot;
	ssize_t len;
	int i = 0;

	int ret;
	int fd = -1;
	int flag = O_RDWR;
	//flag |= O_CREAT | O_TRUNC;

	fd = ::open( fileName.c_str(), flag, 0666 );
	if( fd < 0 )
	{
		LOGE( "open \"%s\" failed!! err: %s( 0x%08x )\n", fileName.c_str(), strerror(errno), errno);
		return false;
	}
	::lseek( fd, 0, SEEK_SET );
	for( int i = 0 ; i < MAX_SLOT_INDEX ; i++ )
	{
		len = ::read( fd, &devInfo, sizeof( ::DEVICE_INFO ) );
		if( len <= 0 )
		{
			break;
		}
		if( devInfo.id != 0x0 )
		{
			deviceMap.insert( std::pair< int, ::DEVICE_INFO >( devInfo.slot, devInfo ) );
		}
		LOGV( "LOAD %d: slot: %d freq: %d id: 0x%04x\n", i, devInfo.slot, devInfo.freq, devInfo.id );
	}
	ret = ::close( fd );
	if( ret < 0 )
	{
		LOGE( "close \"%s\" failed!!  err: %s( 0x%08x )\n", fileName.c_str(), strerror(errno), errno);
		return false;
	}
	LOGV( "LOAD: deviceMap.sizei: %d\n", deviceMap.size() );

	return true;
}

int main()
{
	Pettra::HalModule		*pModule = NULL;
	Pettra::AdfHwDevice	*pHw_device = NULL;
	//const char *pModuleId = Pettra::HalModule::ADF_HARDWARE_MODULE_ID;
	const char *pModuleId = "adf";

	std::map< int, ::DEVICE_INFO > deviceMap;

#ifndef __USE_SH__
	sighandler_t	old_sighandler;

	old_sighandler = ::signal( SIGINT, ( sighandler_t )sig_handler );
#endif	//	__USE_SH__

	deviceMap.clear();
	LoadFile( PAIR_INFO_FILE, deviceMap );

#ifndef __BUILD_BY_MAKEFILE__	//	Android.mk
	if( hw_get_module( pModuleId, ( const hw_module_t ** )( &pModule ) ) != 0 )
	{
		LOGE( "%s module not found!!", pModuleId );
		ERROR_PRINTF( "%s module not found!!\n", pModuleId );
		return EXIT_FAILURE;
	}
#else	//	Makefile
	pModule = dynamic_cast< Pettra::HalModule * >( &::HAL_MODULE_INFO_SYM );
	if( ::HAL_MODULE_INFO_SYM.GetModulePtr() != pModule )
	{
		ERROR_PRINTF( "%s module not found!!\n", pModuleId );
		return EXIT_FAILURE;
	}
#endif

	if( pModule->methods->open( pModule, pModuleId, ( hw_device_t ** )( &pHw_device ) )!= 0 )
	{
		ERROR_PRINTF( "pModule->methods->open failed!!!\n" );
		return EXIT_FAILURE;
	}

	assert( pHw_device != NULL );

	g_pAdfHwDevice = pHw_device;

	pHw_device->open( pHw_device );
	//( const_cast< Pettra::Adf7021 * >( pHw_device->GetDevice() ) )->SetCallbacks( &s_callbacks );
	pHw_device->setCallbacks( pHw_device, &s_callbacks );

	pHw_device->start( pHw_device );

	usleep( 10 );
	pHw_device->update( pHw_device, deviceMap );
#ifdef __USE_SH__
	sh_loop();
#else	//	__USE_SH__
	int cnt = 0;
	g_isRunning = true;
	while( g_isRunning == true )
	{
		sleep( 1 );
		//if( cnt++ >= 30 ) g_isRunning = false;
		//pHw_device->signal( pHw_device );
	}
#endif	//	__USE_SH__

	pHw_device->stop( pHw_device );

	pHw_device->close( pHw_device );

#ifndef __USE_SH__
	::signal( SIGINT, old_sighandler );
#endif	//	__USE_SH__

	return EXIT_SUCCESS;
}
