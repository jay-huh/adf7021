#include "simple_sh.h"

#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <assert.h>

#ifndef SH_NAME
#define SH_NAME       "lsh"
#endif	//	SH_NAME

//static size_t s_numBuiltinFuncPair = 0;

int sh_num_builtins() {
	return g_numBuiltinFuncPair;
}

/**
  @brief Launch a program and wait for it to terminate.
  @param args Null terminated list of arguments (including program).
  @return Always returns 1, to continue execution.
  */
int sh_launch(char **args)
{
	pid_t pid;
	int status;

	pid = fork();
	if (pid == 0) {
		// Child process
		if (execvp(args[0], args) == -1) {
			perror( SH_NAME );
		}
		exit(EXIT_FAILURE);
	} else if (pid < 0) {
		// Error forking
		perror( SH_NAME );
	} else {
		// Parent process
		do {
			waitpid(pid, &status, WUNTRACED);
		} while (!WIFEXITED(status) && !WIFSIGNALED(status));
	}

	return 1;
}

/**
  @brief Execute shell built-in or launch program.
  @param args Null terminated list of arguments.
  @return 1 if the shell should continue running, 0 if it should terminate
  */
int sh_execute(char **args)
{
	int i;

	if (args[0] == NULL) {
		// An empty command was entered.
		return 1;
	}

	for (i = 0; i < sh_num_builtins(); i++) {
		if (strcmp( args[0], g_pBuiltinFuncPair[ i ].name) == 0) {
			return g_pBuiltinFuncPair[ i ].function( args );
		}
	}

	return sh_launch(args);
}

#define READLINE_BUFSIZE 64
/**
  @brief Read a line of input from stdin.
  @return The line from stdin.
  */
char *sh_read_line(void)
{
	int bufsize = READLINE_BUFSIZE;
	int position = 0;
	char *buffer = ( char * )malloc(sizeof(char) * bufsize);
	int c;

	if (!buffer) {
		fprintf(stderr, SH_NAME ": allocation error\n");
		exit(EXIT_FAILURE);
	}

	while (1) {
		// Read a character
		c = getchar();

		if (c == EOF) {
			exit(EXIT_SUCCESS);
		} else if (c == '\n') {
			buffer[position] = '\0';
			return buffer;
		} else {
			buffer[position] = c;
		}
		position++;

		// If we have exceeded the buffer, reallocate.
		if (position >= bufsize) {
			bufsize += READLINE_BUFSIZE;
			buffer = ( char * )realloc(buffer, bufsize);
			//printf( "INFO: realloc: %d => %d\n", position, bufsize );
			if (!buffer) {
				fprintf(stderr, SH_NAME ": allocation error\n");
				exit(EXIT_FAILURE);
			}
		}
	}
}

#define TOK_BUFSIZE 64
#define TOK_DELIM " \t\r\n\a"
/**
  @brief Split a line into tokens (very naively).
  @param line The line.
  @return Null-terminated array of tokens.
  */
char **sh_split_line(char *line)
{
	int bufsize = TOK_BUFSIZE, position = 0;
	char **tokens = ( char ** )malloc(bufsize * sizeof(char*));
	char *token, **tokens_backup;

	if (!tokens) {
		fprintf(stderr, SH_NAME ": allocation error\n");
		exit(EXIT_FAILURE);
	}

	token = strtok(line, TOK_DELIM);
	while (token != NULL) {
		tokens[position] = token;
		position++;

		if (position >= bufsize) {
			bufsize += TOK_BUFSIZE;
			tokens_backup = tokens;
			tokens = ( char ** )realloc(tokens, bufsize * sizeof(char*));
			if (!tokens) {
				free(tokens_backup);
				fprintf(stderr, SH_NAME ": allocation error\n");
				exit(EXIT_FAILURE);
			}
		}

		token = strtok(NULL, TOK_DELIM);
	}
	tokens[position] = NULL;
	return tokens;
}

/**
  @brief Loop getting input and executing it.
  */
void sh_loop(void)
{
	char *line;
	char **args;
	int status;
	int i;

#if 0
	for( i = 0 ; i < MAX_BUILTIN_FN ; i++ )
	{
		if( s_builtinFuncPair[ i ].name == NULL )
		{
			break;
		}
	}
	s_numBuiltinFuncPair = i;
#endif
	printf( SH_NAME ": # of builtin functions: %d\n", g_numBuiltinFuncPair );

	do {
		printf( SH_NAME " # " );
		line = sh_read_line();
		args = sh_split_line(line);
		status = sh_execute(args);

		free(line);
		free(args);
	} while (status);
}

#if 0
/**
  @brief Main entry point.
  @param argc Argument count.
  @param argv Argument vector.
  @return status code
  */
int main(int argc, char **argv)
{
	// Load config files, if any.

	// Run command loop.
	sh_loop();

	// Perform any shutdown/cleanup.

	return EXIT_SUCCESS;
}
#endif
