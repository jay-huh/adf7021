#ifndef __SIMPLE_SH_H__
#define __SIMPLE_SH_H__

#include <sys/types.h>

#define MAX_BUILTIN_FN	( 256 )
#define SH_NAME "adf_test"

typedef int ( *pFnBuiltinFunc ) (char **);

typedef struct BUILTIN_FN_PAIR_S
{
	const char		*name;
	pFnBuiltinFunc	function;
} BUILTIN_FN_PAIR;

extern const BUILTIN_FN_PAIR *g_pBuiltinFuncPair;
extern const size_t g_numBuiltinFuncPair;

int sh_num_builtins();
void sh_loop(void);

#endif	//	__SIMPLE_SH_H__
