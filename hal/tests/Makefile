TOP_DIR ?= $(PWD)/../../../../..

LIB_NAME := adf
TARGET := adf-test

SRCS := \
	simple_sh.cpp \
	main.cpp

OBJS := $(SRCS:.c=.o)
OBJS := $(OBJS:.cpp=.o)

INC	:= \
	-I$(PWD)/.. \
	-I$(TOP_DIR)/system/core/include \
	-I$(TOP_DIR)/hardware/libhardware/include

LIBS := \
	-llog

DEPEND_FILE := $(TARGET).dep
MAP_FILE := $(TARGET).map

# Make standalone toolchain: an ndk directory.
#sudo ./build/tools/make-standalone-toolchain.sh --arch=arm --platform=android-22 --stl=stlport --install-dir=/opt/pettra --toolchain=arm-linux-androideabi-4.9 --verbose
CROSS_COMPILE ?= /opt/pettra/bin/arm-linux-androideabi-

CC := $(CROSS_COMPILE)gcc
CXX := $(CROSS_COMPILE)g++

CFLAGS := \
	-Wall $(INC) -g \
	-fdiagnostics-color=auto

CXXFLAGS :=

LDFLAGS := \
	-Wall $(LIBS) \
	-fdiagnostics-color=auto

#	Neon options
CFLAGS += \
	-march=armv7-a -mfloat-abi=softfp -mfpu=neon

LDFLAGS += \
	-march=armv7-a -Wl,--fix-cortex-a8 \
	-Wl,-Map,$(MAP_FILE)

#	Android options
CFLAGS += -D__BUILD_BY_MAKEFILE__
CFLAGS += -fPIE  #-lstlport_shared
CFLAGS += -D __USE_SH__
#CFLAGS += -D __DEBUG_RF__
#CFLAGS += -D __DEBUG_THREAD__

LDFLAGS := \
	-pie \
	-L ../ \
	-l$(LIB_NAME) \
	-Wl,-Map,$(MAP_FILE)

.PHONY: all clean

all: depend $(TARGET)

$(TARGET): $(OBJS)
	@echo "[ LD  ] Linking $@ from $^"
	@$(CXX) $(LDFLAGS) -o $@ $^

clean:
	@echo "Cleaning..."
	@rm -f $(OBJS) $(TARGET)* $(DEPEND_FILE) *.map

#%.o: %.c
.c.o:
	@echo "[ CC  ] Compiling $<"
	@$(CC) $(CFLAGS) -c $< -o $@

#%.o: %.cpp
#	@echo "Compiling $<"
#	@$(CXX) $(CFLAGS) $*.cpp -o $*.o

#%.o: %.cpp
.cpp.o:
	@echo "[ CXX ] Compiling $<"
	@$(CXX) $(CFLAGS) $(CXXFLAGS) -c $< -o $@

depend: $(SRCS)
	@echo "Make dependency file: $(DEPEND_FILE)"
	@$(CXX) -MM $(SRCS) > $(DEPEND_FILE)

-include $(DEPEND_FILE)
