#include	"../queue.h"

#include	<iostream>
#include	<stdio.h>

#include	"../mutex.cpp"
#include	"../../drv/pf_cmd.h"

//#define		Q_ITEMS	( 1 * 1024 * 1024 )
#define		Q_ITEMS	( 10 )

int main(int argc, char *argv[])
{
#if 1
	int i = 0;
	Pettra::Queue< ::PF_RX_COMMAND * >	queue;
	::PF_RX_COMMAND	cmd[ Q_ITEMS ];
	::PF_RX_COMMAND tmpCmd;
	::PF_RX_COMMAND *pTmpCmd = NULL;

	for( i = 0 ; i < Q_ITEMS ; i++ )
	{
		cmd[ i ].dummy = 0x55;
		cmd[ i ].command = i;

		cmd[ i ].gps_info.dog_id =  0x2000 + ( i << 4 );
		cmd[ i ].gps_info.hunter_id =  0x8000 + ( i << 8 );
	}

#if 1	//	pointer struct type...
	pTmpCmd = queue.Dequeue();
	printf( "Dequeuei at Empty: addr: 0x%p sizeof: %u\n", pTmpCmd, sizeof( ::PF_RX_COMMAND ) );

	for( i = 0 ; i < Q_ITEMS ; i++ )
	{
		queue.Enqueue( &cmd[ i ] );
		printf( " Enqueue: idx(%d) dummy: 0x%x cmd: 0x%x\n", i, cmd[ i ].dummy, cmd[ i ].command );
		printf( " Enqueue: idx(%d) addr: %p ID: 0x%x H_ID: 0x%x\n", i, &cmd[ i ], cmd[ i ].ack.hunter_id, cmd[ i ].ack.checksum );
	}

	printf( "===============================\n" );
	i = 0;

	while( queue.IsEmpty() == false )
	{
		pTmpCmd = queue.Dequeue();

		printf( "Dequeue: idx(%d) addr: %p dummy: 0x%x cmd: 0x%x\n", i, pTmpCmd, pTmpCmd->dummy, pTmpCmd->command );
		printf( "Dequeue: idx(%d) ID: 0x%x H_ID: 0x%x\n", i++, pTmpCmd->ack.hunter_id, pTmpCmd->ack.checksum );
		//if( i > 5 ) queue.Clear();
	}
	pTmpCmd = queue.Dequeue();
	printf( "Dequeuei at Empty: addr: 0x%p\n", pTmpCmd );

#else	//	struct type...
	tmpCmd = queue.Dequeue();
	printf( "Dequeuei at Empty: dummy: 0x%x cmd: 0x%x\n", tmpCmd.dummy, tmpCmd.command );
	printf( "Dequeue: ID: 0x%x H_ID: 0x%x\n", tmpCmd.ack.id, tmpCmd.ack.checksum );

	for( i = 0 ; i < Q_ITEMS ; i++ )
	{
		queue.Enqueue( cmd[ i ] );
		printf( " Enqueue: idx(%d) dummy: 0x%x cmd: 0x%x\n", i, cmd[ i ].dummy, cmd[ i ].command );
		printf( " Enqueue: idx(%d) ID: 0x%x H_ID: 0x%x\n", i, cmd[ i ].ack.id, cmd[ i ].ack.checksum );
	}

	printf( "===============================\n" );
	i = 0;

	while( queue.IsEmpty() == false )
	{
		tmpCmd = queue.Dequeue();

		printf( "Dequeue: idx(%d) dummy: 0x%x cmd: 0x%x\n", i, tmpCmd.dummy, tmpCmd.command );
		printf( "Dequeue: idx(%d) ID: 0x%x H_ID: 0x%x\n", i++, tmpCmd.ack.id, tmpCmd.ack.checksum );
		//if( i > 5 ) queue.Clear();
	}
	tmpCmd = queue.Dequeue();
	printf( "Dequeuei at Empty: dummy: 0x%x cmd: 0x%x\n", tmpCmd.dummy, tmpCmd.command );
	printf( "Dequeue: ID: 0x%x H_ID: 0x%x\n", tmpCmd.ack.id, tmpCmd.ack.checksum );

#endif

	return 0;
#else	//	Fundamental type test( int, double ... )
	long long i = 0;
	Pettra::Queue< int * >	queue;
	int	data[ Q_ITEMS ] = { 0, };

	for( i = 0 ; i < Q_ITEMS ; i++ )
	{
		data[ i ] = i * 3;
	}

	std::cout << " Dequeue at Empty: " << queue.Dequeue() << std::endl;

	for( i = 0 ; i < Q_ITEMS ; i++ )
	{
		queue.Enqueue( &data[ i ] );
		std::cout << " Enqueue: " << i
			<< " data: " << data [ i ]
			<< " addr: " << &data[ i ]
			<< std::endl;;
	}

	i = 0;

	while( queue.IsEmpty() == false )
	{
		int *pInt = queue.Dequeue();

		std::cout << " Dequeue: " << i++
			<< " data: " << *pInt
			<< " addr: " << pInt
			<< std::endl;;
		//if( i > 5 ) queue.Clear();
	}
	std::cout << " Dequeue at Empty: " << queue.Dequeue() << std::endl;

	return 0;
#endif
}
