#ifndef	_CHECKSUM_H_
#define	_CHECKSUM_H_

#include	<stdint.h>
#include	<string>

namespace Pettra
{
	class Checksum
	{
	public:
		typedef enum	CHECKSUM_E
		{
			ARITHMETIC_SUM = 0,
			CRC_16,

			CHECKSUM_END
		} CHECKSUM;

	private:
		const CHECKSUM		m_type;
		const std::string	m_name;

	protected:

	public:
		explicit Checksum( const CHECKSUM type, const std::string name )
			: m_type( type ), m_name( name ) { };

		virtual uint32_t checksum( uint8_t *pData, size_t length ) = 0;

		virtual ~Checksum();

	};

	class ArithmeticSum: public Checksum
	{
	public:
		ArithmeticSum( const CHECKSUM type = ARITHMETIC_SUM, const std::string name = "SUM" )
			: Checksum( type, name ) { };

		virtual uint32_t checksum( uint8_t *pData, size_t length )
		{
			uint32_t	exor = 0x0;
			uint32_t	sum = 0x0;
			uint32_t	sum_1 = 0x0;
			uint32_t		ret;
			uint32_t		i;

			for( i = 0 ; i < ( length - 2 ) ; i++ )
			{
				exor ^= pData[ i ];
				sum_1 += pData[ i ];
				sum += sum_1;
			}
			*( pData + i++ ) = exor & 0xFF;
			*( pData + i ) = ( ( sum >> 8 ) & 0xFF ) ^ ( sum & 0xFF );

			ret = ( *( pData + i ) << 8 ) + ( *( pData + ( i - 1 ) ) & 0xFF );

			return ret;
		}

		virtual ~ArithmeticSum();
	};

	class Crc16: public Checksum
	{
	private:
		static const uint16_t CRCTable[ 256 ];
	public:
		Crc16( const CHECKSUM type = CRC_16, const std::string name = "CRC_16" )
			: Checksum( type, name ) { };

		virtual uint32_t checksum( uint8_t *pData, size_t length )
		{
#if 0
			uint8_t nTemp;
			uint16_t wCRCWord = 0;

			wLength -= 2;

			while (wLength--) {
				nTemp = (uint8_t) (*nData++ ^ wCRCWord);
				wCRCWord >>= 8;
				wCRCWord ^= wCRCTable[nTemp];
			}
#else
			uint8_t	tmp;
			uint16_t	CRCWord = 0;

			length -= 2;

			while( length-- )
			{
				tmp = ( *pData ++ ^ CRCWord ) & 0xFF;
				CRCWord = ( ( CRCWord >> 8 ) & 0xFF ) ^ CRCTable[ tmp ];
			}
#endif
			return ( uint32_t )CRCWord;
		}

		virtual ~Crc16();
	};
}	//	namespace Pettra

#endif	//	_CHECKSUM_H_
/*	vim: set ft=cpp:	*/
