#include	"debug.h"

#include	<sys/syscall.h>	//	for SYS_xxxx definitions


#ifdef __cplusplus
extern "C" {
#endif

int gettid(void)
{
	return ( int )syscall( __NR_gettid );
}

#ifdef __cplusplus
}
#endif

