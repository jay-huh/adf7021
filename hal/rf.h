#ifndef	_RF_H_
#define	_RF_H_

#include	"checksum.h"

#include	<string>

#include	"hal_device.h"
#include	"thread.h"

namespace Pettra
{

class RfDevice : public HalDevice, public Thread
{
public:
	typedef enum	IC_TYPE_E
	{
		ADF_7021 = 0,

		IC_TYPE_END
	} IC_TYPE;

	typedef enum	PROTOCOL_E
	{
		PATH_FINDER = 0,

		PROTOCOL_END
	} PROTOCOL;

	typedef struct	FACTORY_ARG_S
	{
		PROTOCOL	protocol;
		std::string	devName;
	} FACTORY_ARG;

private:
	const IC_TYPE		m_icType;
	const PROTOCOL		m_protocol;
	const std::string	m_devName;

	Checksum			*m_pChecksum;

protected:
	mutable int			m_fd;

	virtual bool	Init( void );
	virtual size_t	Send( const uint8_t *pPacket );
	virtual size_t	Receive( const uint8_t *pPacket );

public:
	explicit RfDevice( const IC_TYPE icType, \
			const PROTOCOL protocol, \
			const std::string devName, \
			const std::string threadName, \
			const int priority );
	virtual ~RfDevice();

	virtual bool Open( void ) const;
	virtual bool Close( void ) const;
	int Dup( void ) const;
};

}	//	namespace Pettra
#endif	//	_RF_H_
/*	vim: set ft=cpp:	*/
