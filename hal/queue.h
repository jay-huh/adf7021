#ifndef _QUEUE_H_
#define _QUEUE_H_

#include	<queue>

#include	"mutex.h"

namespace Pettra
{
template< typename T >
class Queue : public Mutex
{
private:
	const std::string	m_name;
	std::queue< T >		m_queue;

protected:
public:
	explicit Queue( const std::string name = "undef" ) : Mutex( name ), m_name( name ) {};
	virtual ~Queue()
	{
		this->Clear();
	}

	void Enqueue( const T& item )
	//void Enqueue( const T item )
	{
		m_queue.push( item );
	}

	const T Dequeue( void )
	{
		T	item = T();

		Lock();

		if( this->IsEmpty() == false )
		{
			item = m_queue.front();
			m_queue.pop();
		}

		Unlock();

		return item;
	}


	const T Peek( void ) const
	{
		T	item = T();

		Lock();

		if( this->IsEmpty() == false )
		{
			item = m_queue.front();
		}

		Unlock();

		return item;
	}

	virtual bool IsEmpty( void ) const
	{
		return m_queue.empty();
	}

	std::size_t Size( void )
	{
		return m_queue.size();
	}

	virtual void  Clear( void )
	{
#if 1
		Lock();

		std::queue< T > empty;
		std::swap( m_queue, empty );

		Unlock();
#else
		while( this->IsEmpty() == false )
		{
			this->Dequeue();
		}
#endif
		return ;
	}
};
//template class Queue< int >;
}	//	namespace Pettra

#endif	//	_QUEUE_H_
/*	vim: set ft=cpp:	*/
