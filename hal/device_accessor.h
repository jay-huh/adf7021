#ifndef	_DEVUCE_ACCESSOR_H
#define _DEVUCE_ACCESSOR_H

//#include	<string>

#include	"queue.h"
#include	"thread.h"

#include	"debug.h"

namespace Pettra
{

template< typename T >
class DeviceAccessor : public Queue< T* >, public Thread
{
private:
	const std::string m_name;
	mutable int m_fd;

protected:

public:
	explicit DeviceAccessor( const std::string name = "undef", \
			const std::string queueName = "undef", const std::string threadName = "undef" )
		: Queue< T* >( queueName ), Thread( threadName ), m_name( name )
	{
		m_fd = -1;
	};
	virtual ~DeviceAccessor()
	{
		if( m_fd >= 0 )
		{
			Close();
		}
	};

	const std::string& GetName( void ) const { return m_name; };

	void SetFd( const int fd ) { m_fd = fd; };
	const int& GetFd( void ) const { return m_fd; };

	virtual void Init( void ) {};
	virtual bool Open( void ) const
	{
		THREAD_INFO_PRINTF( "OPEN: DeviceAccessor: %s \n", m_name.c_str() );
		if( m_fd >= 0 ) return true;
#if 0
		m_fd = ::open( m_devName.c_str(), O_RDWR );
		if( m_fd < 0 )
		{
			ERROR_PRINTF( "RfDevice: open \"%s\" failed!! err: %s( 0x%08x )\n", m_devName.c_str(), strerror(errno), errno);
			return false;
		}
#endif
		return true;
	}

	virtual bool Close( void ) const
	{
		int ret;
		THREAD_INFO_PRINTF( "CLOSE: DeviceAccessor: %s m_fd: %d\n", m_name.c_str(), m_fd );

		if( m_fd < 0 ) return true;

		ret = ::close( m_fd );
		if( ret < 0 )
		{
			ERROR_PRINTF( "close fd(%d) failed!!  err: %s( 0x%08x )\n", m_fd, strerror(errno), errno);
			return false;
		}
		m_fd = -1;

		return true;
	};
};

//typedef DeviceAccessor< ADF_PACKET >	AdfPacketDeviceAccessor;

}	//	namespace Pettra
#endif	//	_DEVUCE_ACCESSOR_H
/*	vim: set ft=cpp:	*/
