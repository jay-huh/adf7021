#include	"rf.h"

#include	<fcntl.h>

#include	"debug.h"

using namespace		Pettra;

//template< typename T >
//RfDevice< T >::RfDevice( const IC_TYPE icType, const PROTOCOL protocol, const std::string devName )
//	: m_icType( icType ), m_protocol( protocol ), m_devName( devName )
RfDevice::RfDevice( const IC_TYPE icType, \
				const PROTOCOL protocol, \
				const std::string devName, \
				const std::string threadName, \
				const int priority )
	:	Thread( threadName, priority ),	m_icType( icType ), m_protocol( protocol ), m_devName( devName )
{
	m_fd = -1;
	THREAD_PRINTF( "RfDevice: Constuctor\n" );
}

RfDevice::~RfDevice()
{
	THREAD_PRINTF( "RfDevice: Destructor\n" );
	if( m_fd >= 0 )
	{
		Close();
	}
}

bool RfDevice::Open( void ) const
{
	THREAD_INFO_PRINTF( "RfDevice: open device: %s \n", m_devName.c_str() );
	if( m_fd >= 0 ) return true;

	m_fd = ::open( m_devName.c_str(), O_RDWR );
	if( m_fd < 0 )
	{
		ERROR_PRINTF( "RfDevice: open \"%s\" failed!! err: %s( 0x%08x )\n", m_devName.c_str(), strerror(errno), errno);
		return false;
	}

	return true;
}

bool RfDevice::Close( void ) const
{
	int ret;
	THREAD_INFO_PRINTF( "RfDevice: close device: %s m_fd: %d\n", m_devName.c_str(), m_fd );

	if( m_fd < 0 ) return true;

	ret = ::close( m_fd );
	if( ret < 0 )
	{
		ERROR_PRINTF( "RfDevice: close \"%s\" failed!!  err: %s( 0x%08x )\n", m_devName.c_str(), strerror(errno), errno);
		return false;
	}
	m_fd = -1;

	return true;
}
int RfDevice::Dup( void ) const
{
	return ::dup( m_fd );
}

bool RfDevice::Init( void )
{
	THREAD_INFO_PRINTF( "RfDevice: INIT: this: %p\n", this );
	return true;
}

size_t RfDevice::Send( const uint8_t *pPacket )
{
	return 0;
}

size_t RfDevice::Receive( const uint8_t *pPacket )
{
	return 0;
}

//template class RfDevice< ADF_PACKET >;
