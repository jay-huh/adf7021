#ifndef _MUTEX_H_
#define _MUTEX_H_

#include	<string>

#include	<pthread.h>

namespace Pettra
{
class Mutex
{
private:
	mutable pthread_cond_t		m_cond;
	mutable pthread_mutex_t		m_mutex;

	/*	Avoid spurious wakeup	*/
	mutable bool				m_condition;

	const std::string	m_name;

protected:
public:
	explicit Mutex( const std::string name = "undef" );
	virtual ~Mutex();

	const std::string& GetName( void ) const { return m_name; }

	bool Lock( int timeout = 0 ) const;
	void Unlock() const;
	bool Wait( int timeout = 0 ) const;
	void Signal() const;
	void Broadcast() const;
};
}	//	namespace Pettra
#endif /* ifndef _MUTEX_H_ */
/*	vim: set ft=cpp:	*/
