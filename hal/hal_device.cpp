#include	"hal_device.h"

#include	<utils/Errors.h>

#include	"debug.h"

using namespace android;
using namespace Pettra;

HalDevice::HalDevice()
{
	m_pModule = & ::HAL_MODULE_INFO_SYM;

	InitHalModule();
}

HalDevice::~HalDevice()
{
	DEBUG_CLASS_RF( "this: %p %s\n", this, __PRETTY_FUNCTION__ );
}

void HalDevice::InitHalModule( void ) const
{
	m_pModule->SetHalDevice( this );

	INFO_PRINTF( "HalModule | id: \"%s\" | name: \"%s\" |\n", m_pModule->GetId(), m_pModule->GetName() );
	//DEBUG_PRINTF( "this: %p HAL_MODULE_INFO_SYM: %p\n", this, &::HAL_MODULE_INFO_SYM );
	//DEBUG_PRINTF( "this: %p hw_module_t: %p\n", this, dynamic_cast< hw_module_t * >( this->m_pModule ) );

	return;
}

hw_module_t* HalDevice::GetModule( void ) const
{
	return dynamic_cast< hw_module_t * >( m_pModule );
}
