#ifndef _HAL_DEVICE_H_
#define _HAL_DEVICE_H_

#include	<map>
#include	<string>

#include	<hardware/hardware.h>

#include	"hal_module.h"

namespace Pettra
{

class HalModule;

class HalDevice
{
private:
	HalModule		*m_pModule;

	virtual void InitHalModule( void ) const;

protected:
	hw_module_t* GetModule( void ) const;

public:
	HalDevice();
	virtual ~HalDevice();

	virtual int OpenDevice( const hw_module_t* hw_module, const char* name, hw_device_t** hw_device ) const = 0;
};

}	//	namespace Pettra

#endif	//	_HAL_DEVICE_H_
/*	vim: set ft=cpp:	*/
