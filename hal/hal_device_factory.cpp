#include	"hal_device_factory.h"

#include	"debug.h"

#include	"hal_device.h"
#include	"adf7021.h"

using namespace Pettra;

HalDeviceFactory::HalDeviceFactory()
{
	m_factoryMap.clear();

	Register( HalModule::HAL_IDX_TEST, NULL );
	Register( HalModule::HAL_IDX_RF, NULL );
	Register( HalModule::HAL_IDX_ADF, &Adf7021::Create );
}

HalDeviceFactory::~HalDeviceFactory()
{
	m_factoryMap.clear();
}

void HalDeviceFactory::Register( const int index, const pFnCreateHalDevice pFnCreate )
{
	m_factoryMap.insert( HAL_PFN_PAIR( index, pFnCreate ) );

	return;
}

const Pettra::HalDevice* HalDeviceFactory::CreateHalDevice( const int index )
{
	HalDevice	*pHalDevice = NULL;

	assert( ( index >= 0 ) && ( index < HalModule::HAL_IDX_END ) );

	std::map< int, pFnCreateHalDevice >::const_iterator	it = m_factoryMap.find( index );

	assert( it != m_factoryMap.end() );
	assert( it->second != NULL );

	INFO_PRINTF( "CREATE: module index(%d)\n", index );

	switch( index )
	{
		case HalModule::HAL_IDX_TEST:
		case HalModule::HAL_IDX_RF:
			break;
		case HalModule::HAL_IDX_ADF:
			{
				RfDevice::FACTORY_ARG	arg = { RfDevice::PATH_FINDER, "/dev/adf7021" };

				pHalDevice = it->second( &arg );
				break;
			}
		default:
			break;
	}
	return pHalDevice;
}
