#include	"adf7021.h"

#include	<string.h>	//	memset
#include	<fcntl.h>	//	open, close

#include	<utils/Errors.h>

#include	"debug.h"

#include	"thread.h"

using namespace		android;
using namespace		Pettra;

AdfHwDevice::AdfHwDevice( const Pettra::Adf7021 *pDevice )
	: m_pDevice( pDevice )
{
	/*	hw_device_t		*/
	tag = HARDWARE_DEVICE_TAG;
	version = HARDWARE_DEVICE_API_VERSION( 1, 0);
	module = NULL;
	::memset( reserved, 0x0, sizeof( reserved ) );
	close = NULL;

	/*	extends		*/

	DEBUG_PRINTF( "AdfHwDevice: Constuctor\n" );
}

AdfHwDevice::~AdfHwDevice()
{
	DEBUG_PRINTF( "AdfHwDevice: Destructor\n" );
}

Adf7021::Adf7021( const RfDevice::PROTOCOL protocol, \
				const std::string devName )
	:	RfDevice( RfDevice::ADF_7021, protocol, devName, "ADF7021", Thread::PRIORITY_NORMAL )
{
	THREAD_PRINTF( "Adf7021: Constuctor\n" );
	m_pReader = NULL;
	m_pCommander= NULL;
	::memset( &m_callbacks, 0x0, sizeof( ADF_CALLBACKS ) );
	m_slotMap.clear();
	m_deviceMap.clear();
#ifdef	__USE_FILE__
	m_fileFd = -1;
	m_fileName = "/data/misc/adf/pair_info";
#endif	//	__USE_FILE__
}

Adf7021::~Adf7021()
{
	THREAD_PRINTF( "Adf7021: Destructor\n" );
	m_slotMap.clear();
	m_deviceMap.clear();
}

Pettra::HalDevice* Adf7021::Create( void * const pArg )
{
	RfDevice::FACTORY_ARG	*pFactoryArg = reinterpret_cast< RfDevice::FACTORY_ARG * >( pArg );

	if( pArg == NULL )
	{
		return new Adf7021();
	}

	return new Adf7021( pFactoryArg->protocol, pFactoryArg->devName );
};

bool Adf7021::Init( void )
{
	::IOCTL_CMD_DATA	cmdData;

	THREAD_INFO_PRINTF( "Adf7021: INIT: this: %p\n", this );


	cmdData.adfInit.rf_mode = ADF_RF_SPI;
	cmdData.adfInit.syncword = PF_SYNC_WORD;
	cmdData.adfInit.max_packet_length = MAX_PACKET_LEN;
	cmdData.adfInit.work_mode = ADF_WORK_NORMAL;

	return SendCommand( ADF_INIT, &cmdData );
}

bool Adf7021::SetCallbacks( const ADF_CALLBACKS *pCallbacks )
{
	assert( pCallbacks != NULL );

	m_callbacks = *pCallbacks;

	return true;
}

bool Adf7021::Open( void )
{
	THREAD_INFO_PRINTF( "Adf7021: OPEN: this: %p\n", this );

	RfDevice::Open();

	OpenAccessors();

	return true;
}

bool Adf7021::OpenAccessors( void )
{
	assert( m_pReader == NULL );

	m_pReader = new Reader();
	assert( m_pReader != NULL );
	m_pReader->SetFd( Dup() );

	assert( m_pCommander == NULL );

	m_pCommander = new Commander();
	assert( m_pCommander != NULL );
	m_pCommander->SetFd( Dup() );

	return true;
}

bool Adf7021::Close( void )
{
	THREAD_INFO_PRINTF( "Adf7021: CLOSE: this: %p m_fd: %d\n", this, m_fd );

	CloseAccessors();

	RfDevice::Close();

#ifdef	__USE_FILE__
	OpenFile();
	UpdateFile();
	CloseFile();
#endif	//	__USE_FILE__

	return true;
}

bool Adf7021::CloseAccessors( void )
{
	THREAD_INFO_PRINTF( "Adf7021: CLOSE: this: %p m_fd: %d\n", this, m_fd );
	assert( m_pReader != NULL );

	m_pReader->Close();

	delete m_pReader;
	m_pReader = NULL;

	assert( m_pCommander != NULL );

	m_pCommander->Close();

	delete m_pCommander;
	m_pCommander = NULL;

	return true;
}

#ifdef	__USE_FILE__
bool Adf7021::OpenFile( void ) const
{
	int ret;
	int flag = O_RDWR;
	flag |= O_CREAT | O_TRUNC;
	//flag |= O_DIRECT | O_SYNC;

	THREAD_INFO_PRINTF( "Adf7021: open file: %s \n", m_fileName.c_str() );
	if( m_fileFd > 0 ) return true;

	m_fileFd = ::open( m_fileName.c_str(), flag, 0666 );
	if( m_fileFd < 0 )
	{
		ERROR_PRINTF( "Adf7021: open \"%s\" failed!! err: %s( 0x%08x )\n", m_fileName.c_str(), strerror(errno), errno);
		return false;
	}
	::ftruncate( m_fileFd, sizeof( ::DEVICE_INFO ) * MAX_SLOT_INDEX );

	return true;
}

bool Adf7021::CloseFile( void ) const
{
	int ret;
	THREAD_INFO_PRINTF( "Adf7021: close file: %s m_fileFd: %d\n", m_fileName.c_str(), m_fileFd );

	if( m_fileFd < 0 ) return true;

	ret = ::close( m_fileFd );
	if( ret < 0 )
	{
		ERROR_PRINTF( "Adf7021: close \"%s\" failed!!  err: %s( 0x%08x )\n", m_fileName.c_str(), strerror(errno), errno);
		return false;
	}
	m_fileFd = -1;

	return true;
}

bool Adf7021::UpdateFile( void )
{
	::DEVICE_INFO devInfo;
	ssize_t len;
	int i = 0;
	std::map< int, ::DEVICE_INFO >::const_iterator it;

	assert( m_fileFd > 0 );

	/*	NOTE: make empty file and rewrite from map.	*/
	for( it = m_deviceMap.begin() ; it != m_deviceMap.end() ; it++ )
	{
		devInfo = it->second;
		::lseek( m_fileFd, sizeof( ::DEVICE_INFO ) * ( devInfo.slot - 1 ), SEEK_SET );
		len = ::write( m_fileFd, &devInfo, sizeof( ::DEVICE_INFO ) );
		THREAD_INFO_PRINTF( "FILE: idx %d : slot: %d freq: %d period: %d id: 0x%04x shared: %d\n", i++, \
				devInfo.slot, devInfo.freq, devInfo.period, devInfo.id, devInfo.shared );
	}

	return true;
}
#endif	//	__USE_FILE__

bool Adf7021::LoadDeviceMap( void )
{
	::DEVICE_INFO devInfo;
	::CH_SLOT ch_slot;
	int i = 0;

	std::map< int, ::DEVICE_INFO >::const_iterator it;

	for( it = m_deviceMap.begin() ; it != m_deviceMap.end() ; it++ )
	{
		devInfo = it->second;

		ch_slot.ch = devInfo.freq;
		ch_slot.slot = devInfo.slot;
		//m_slotMap.insert( std::pair< uint16_t, ::CH_SLOT >( devInfo.id, ch_slot ) );
		m_slotMap[ devInfo.id ] = ch_slot;

		SetSlotInfo( devInfo );
		THREAD_INFO_PRINTF( "LOAD idx %d : slot: %d freq: %d period: %d id: 0x%04x\n", i++, \
				devInfo.slot, devInfo.freq, devInfo.period, devInfo.id );
	}

	return true;
}

bool Adf7021::UpdateDevice( const ::DEVICE_INFO &devInfo )
{
	::CH_SLOT ch_slot;

	THREAD_INFO_PRINTF( "UPDATE: id 0x%04x slot: %d freq: %d period: %d shared: %d\n", \
			devInfo.id, devInfo.slot, devInfo.freq, devInfo.period, devInfo.shared );

	ch_slot.ch = devInfo.freq;
	ch_slot.slot = devInfo.slot;

	m_deviceMap[ devInfo.slot ] = devInfo;
	m_slotMap[ devInfo.id ] = ch_slot;

	return true;
}

bool Adf7021::AddDevice( const ::DEVICE_INFO &devInfo )
{
	return UpdateDevice( devInfo );
}

bool Adf7021::RemoveDevice( const ::DEVICE_INFO &devInfo )
{
	::CH_SLOT ch_slot;
	uint8_t buf[ sizeof( ::DEVICE_INFO )] = { 0x0, };

	THREAD_INFO_PRINTF( "REMOVE: id 0x%04x slot: %d freq: %d period: %d shared: %d\n", \
			devInfo.id, devInfo.slot, devInfo.freq, devInfo.period, devInfo.shared );

	ch_slot.ch = devInfo.freq;
	ch_slot.slot = devInfo.slot;

	m_deviceMap.erase( devInfo.slot );
	m_slotMap.erase( devInfo.id );

	return true;
}

bool Adf7021::ChangeDevice( const ::DEVICE_INFO &devInfo )
{
	::CH_SLOT ch_slot;
	ssize_t len;

	THREAD_INFO_PRINTF( "CHANGE: id 0x%04x slot: %d freq: %d period: %d shared: %d\n", \
			devInfo.id, devInfo.slot, devInfo.freq, devInfo.period, devInfo.shared );

	::DEVICE_INFO tmpDevInfo = GetDeviceInfo( devInfo.id );

	/*	Slot changed: remove previous information	*/
	if( devInfo.slot != tmpDevInfo.slot )
	{
		THREAD_INFO_PRINTF( "CHANGE: slot changed !!! Remove id: 0x%04x slot: %d -> %d\n", devInfo.id, \
				tmpDevInfo.slot, devInfo.slot );
		RemoveDevice( tmpDevInfo );
	}

	return UpdateDevice( devInfo );
}

bool Adf7021::UpdateDeviceDetailInfo( const ::PF_RX_COMMAND * const pRxCmd )
{
	DEVICE_DETAIL_INFO deviceDetailInfo;
	::GPS_INFO *pGpsInfo = NULL;
	::DEVICE_INFO devInfo;
	int16_t intPart;
	bool sign;

	assert( pRxCmd != NULL );

	pGpsInfo = const_cast< :: GPS_INFO * >( &pRxCmd->gps_info );
	devInfo = GetDeviceInfo( pGpsInfo->dog_id );

	/*	Check changed field	*/
	if(	( devInfo.slot != pGpsInfo->ch_slot.slot ) || ( devInfo.freq != pGpsInfo->ch_slot.ch ) \
			|| ( devInfo.period != pGpsInfo->dog_info.period ) )
	{
		devInfo.id = pGpsInfo->dog_id;
		devInfo.slot = pGpsInfo->ch_slot.slot;
		devInfo.freq = pGpsInfo->ch_slot.ch;
		devInfo.period = pGpsInfo->dog_info.period;

		if(	( devInfo.id == m_changingDevInfo.id ) && ( devInfo.slot == m_changingDevInfo.slot ) \
				&& ( devInfo.freq == m_changingDevInfo.freq ) && ( devInfo.period == m_changingDevInfo.period ) )
		{
			ChangeDevice( devInfo );
			SendCommand( ADF_CMD_CHANGE_END );
			m_changingDevInfo.id = 0x0;
		}
	}
	THREAD_INFO_PRINTF( "UPDATE: id 0x%04x slot: %d freq: %d period: %d\n", \
			devInfo.id, devInfo.slot, devInfo.freq, devInfo.period );

	intPart = pGpsInfo->lat_int;
	sign = ( intPart >= 0 ) ? true : false;
	//double latitude = ( pGpsInfo->lat_int / 100 ) + ( ( double )( pGpsInfo->lat_int % 100 )
	//		+ ( ( double )( pGpsInfo->lat_frac * 10 ) / 100000 ) ) / 60;

	intPart = ::abs( intPart );

	deviceDetailInfo.latitude = ( intPart / 100 ) + \
								( ( double )( intPart % 100 ) + \
								  ( ( double )( pGpsInfo->lat_frac * 10 ) / 100000 ) ) / 60;
	if( sign == false )
	{
		deviceDetailInfo.latitude *= ( -1 );
	}
	//LOGV( " lat: %f %f int: 0x%04x 0x%04x sign: %d\n", deviceDetailInfo.latitude, latitude,
	//		pGpsInfo->lat_int, intPart, sign );

	intPart = pGpsInfo->long_int;
	sign = ( intPart >= 0 ) ? true : false;
	double longitude = ( pGpsInfo->long_int / 100 ) + ( ( double )( pGpsInfo->long_int % 100 )
			+ ( ( double )( pGpsInfo->long_frac * 10 ) / 100000 ) ) / 60;

	intPart = ::abs( intPart );

	deviceDetailInfo.longitude = ( intPart / 100 ) + \
								 ( ( double )( intPart % 100 ) + \
								   ( ( double )( pGpsInfo->long_frac * 10 ) / 100000 ) ) / 60;
	if( sign == false )
	{
		deviceDetailInfo.longitude *= ( -1 );
	}
	//LOGV( " lng: %f %f int: 0x%04x 0x%04x sign: %d\n", deviceDetailInfo.longitude, longitude,
	//		pGpsInfo->long_int, intPart, sign );

	//double latitude_1 = (double)( ( long )( latitude * 10000 ) ) / 10000;
	//double longitude_1 = (double)( ( long )( longitude * 10000 ) ) / 10000;

	deviceDetailInfo.altitude = pGpsInfo->altitude / 10.0;	//	m
	/*	GPS RMC spd field[ knots ] 1 knots = 1.852 km/h = 1852 / 3600 m/s
	 *	Collar F/W: velocity = spd * 10000 / 3600 => m/s * 10 ???
	 *
	 *	reverse calculate RMS spd = velocity * 3600 / 10000
	 *	velocity = spd * 1852 * 100 / 3600 cm/s
	 *	*/
	//speed = pInfo.velocity / 10.0;		//	m/s
	deviceDetailInfo.speed = ( double )pGpsInfo->velocity * 1852 / 10000;		//	m/s

	deviceDetailInfo.id = pGpsInfo->dog_id;
	deviceDetailInfo.slotId = pGpsInfo->ch_slot.slot;
	deviceDetailInfo.frequencyId = pGpsInfo->ch_slot.ch;
	deviceDetailInfo.gpsRate = pGpsInfo->dog_info.period;

	deviceDetailInfo.shared = devInfo.shared;

	deviceDetailInfo.batteryLevel = pGpsInfo->battery.level;
	deviceDetailInfo.gpsSensitivity = pGpsInfo->dog_info.gps_rssi;
	deviceDetailInfo.rfSensitivity = pRxCmd->rssi_dBm;
	deviceDetailInfo.rescueMode = pGpsInfo->dog_info.rescue_mode;

	deviceDetailInfo.dogState = pGpsInfo->dog_info.three_axis;
	deviceDetailInfo.barkState = pGpsInfo->dog_info.bark;

	deviceDetailInfo.bearing = -1;

	if( m_callbacks.DeviceInfoCallback != NULL )
	{
		m_callbacks.DeviceInfoCallback( &deviceDetailInfo );
	}

	return true;
}

bool Adf7021::Update( const std::map< int, ::DEVICE_INFO > &deviceMap )
{
	::DEVICE_INFO devInfo;
	ssize_t len;
	int i = 0;
	std::map< int, ::DEVICE_INFO >::const_iterator it;

	m_deviceMap = deviceMap;

	for( it = m_deviceMap.begin() ; it != m_deviceMap.end() ; it++ )
	{
		devInfo = it->second;
		THREAD_INFO_PRINTF( "Update idx %d : slot: %d freq: %d period: %d id: 0x%04x\n", i++, \
				devInfo.slot, devInfo.freq, devInfo.period, devInfo.id );
	}

	return true;
}


const ::DEVICE_INFO Adf7021::GetDeviceInfo( const uint16_t id, const int slot )
{
	::DEVICE_INFO devInfo;
	int slotId = -1;
	std::map< uint16_t, ::CH_SLOT>::iterator slotMapIter;
	std::map< int, ::DEVICE_INFO >::iterator deviceMapIter;

	::memset( &devInfo, 0x0, sizeof( ::DEVICE_INFO ) );

	slotId = slot;
	if( slot < 0 )
	{
		slotMapIter = m_slotMap.find( id );
		if( slotMapIter != m_slotMap.end() )
		{
			slotId = slotMapIter->second.slot;
		}
	}
	deviceMapIter = m_deviceMap.find( slotId );
	if( deviceMapIter != m_deviceMap.end() )
	{
		devInfo = deviceMapIter->second;
	}
	return devInfo;
}

bool Adf7021::SetSlotInfo( const ::DEVICE_INFO &devInfo )
{
	IOCTL_CMD_DATA	cmdData;

	THREAD_INFO_PRINTF( "Adf7021: SetSlotInfo: this: %p\n", this );

	cmdData.rf_cmd = PF_CMD_UNDEFINED;
	cmdData.devInfo = devInfo;

	return SendCommand( ADF_CMD_SET_SLOT_INFO, &cmdData );
}

bool Adf7021::ValidateCommand( const uint32_t command, const void * const pArg )
{
	bool ret = true;
	::DEVICE_INFO devInfo;
	const IOCTL_CMD_DATA * const pCmdData = reinterpret_cast< const ::IOCTL_CMD_DATA * const >( pArg );

	/*	TODO: command valid check.	*/
	switch( command )
	{
		case ADF_CMD_EC_START_CONST :
		case ADF_CMD_EC_START_TONE :
		case ADF_CMD_EC_NICK_ONE :
			/*	NOTE: fist field of union in IOCTL_CMD is 'id'	*/
			devInfo = GetDeviceInfo( pCmdData->devInfo.id );
			if( devInfo.id == 0x0 )
			{
				THREAD_INFO_PRINTF( "EC_CMD: id 0x%04x not found.\n", pCmdData->devInfo.id );
				ret = false;
			}
			break;

		case ADF_CMD_DELETE_DEVICE :
			devInfo = GetDeviceInfo( pCmdData->deleteDevice.id );
			if( devInfo.id == 0x0 )
			{
				THREAD_INFO_PRINTF( "DELETE: id 0x%04x not found.\n", pCmdData->deleteDevice.id );
				ret = false;
			}
			break;
		case ADF_CMD_CHANGE_PERIOD :
			devInfo = GetDeviceInfo( pCmdData->changePeriod.id );
			if( ( devInfo.id == 0x0 ) || ( devInfo.period == pCmdData->changePeriod.period ) )
			{
				THREAD_INFO_PRINTF( "CH_PERIOD: id 0x%04x period( %d ) is same.\n", \
						pCmdData->changePeriod.id, pCmdData->changePeriod.period );
				ret = false;
			}
			break;
		case ADF_CMD_CHANGE_SLOT_FREQ :
			devInfo = GetDeviceInfo( pCmdData->changeSlotFreq.id );

			if( ( devInfo.id == 0x0 ) \
					|| ( ( devInfo.slot == pCmdData->changeSlotFreq.slotId ) \
						&& ( devInfo.freq == pCmdData->changeSlotFreq.freq ) ) )
			{
				THREAD_INFO_PRINTF( "CH_SLOT_FREQ: id 0x%04x [s %d f %d ] is same.\n", \
						pCmdData->changeSlotFreq.id, pCmdData->changeSlotFreq.slotId, pCmdData->changeSlotFreq.freq );
				ret = false;
			}
			break;
		default :
			break;
	}
	return ret;
}

bool Adf7021::SendCommand( const uint32_t command, const void * const pArg )
{
	THREAD_INFO_PRINTF( "cmd: 0x%08x pArg: %p\n", command, pArg );

	assert( m_pCommander != NULL );

#if 1
	if( ValidateCommand( command, pArg ) == false )
	{
		return false;
	}
#endif

	/*	TODO: Generate IOCTL_CMD and enqueue into commander queue.
	 *	*/
	const ::IOCTL_CMD * pCommand = m_pCommander->GenerateCommand( command, pArg );

	m_pCommander->Enqueue( const_cast< ::IOCTL_CMD * >( pCommand ) );

	m_pCommander->Thread::Lock();
	m_pCommander->Thread::Signal();
	m_pCommander->Thread::Unlock();

	return PostProcessCommand( command, pArg );
}

bool Adf7021::GetConnectorInfo( ::CONNECTOR_INFO &connectorInfo )
{
	assert( m_pCommander != NULL );

	THREAD_INFO_PRINTF( "Adf7021: GetConnectorInfo: this: %p\n", this );

	return m_pCommander->GetConnectorInfo( connectorInfo );
}

bool Adf7021::PostProcessCommand( const uint32_t command, const void * const pArg )
{
	::DEVICE_INFO devInfo;
	const IOCTL_CMD_DATA * const pCmdData = reinterpret_cast< const ::IOCTL_CMD_DATA * const >( pArg );

	switch( command )
	{
		case ADF_CMD_DELETE_DEVICE :
			{
				devInfo = GetDeviceInfo( pCmdData->deleteDevice.id );
				if( devInfo.id != 0x0 )
				{
					RemoveDevice( devInfo );
				}
				break;
			}
		case ADF_CMD_CHANGE_PERIOD :
			{
				devInfo = GetDeviceInfo( pCmdData->changePeriod.id );
				devInfo.period = pCmdData->changePeriod.period;
				if( devInfo.id != 0x0 )
				{
					m_changingDevInfo = devInfo;
					//ChangeDevice( devInfo );
				}
				break;
			}
		case ADF_CMD_CHANGE_SLOT_FREQ :
			{
				devInfo = GetDeviceInfo( pCmdData->changeSlotFreq.id );
				devInfo.slot = pCmdData->changeSlotFreq.slotId;
				devInfo.freq = pCmdData->changeSlotFreq.freq;
				if( devInfo.id != 0x0 )
				{
					m_changingDevInfo = devInfo;
					//ChangeDevice( devInfo );
				}
				break;
			}
		default :
			break;
	}
	return true;
}

void* Adf7021::ThreadFunc( void *pArg )
{
	::ADF_PACKET	*pPacket = NULL;;

	//OpenAccessors();
	assert( m_pReader != NULL );
	assert( m_pCommander != NULL );

	/*	TODO:	Wait until set device list from app	*/
	Lock();
	THREAD_INFO_PRINTF( "Adf7021: Wait until Update completed!!\n" );
	Wait();
	THREAD_INFO_PRINTF( "Adf7021: WAKE_UP!!!!!\n" );
	Unlock();

	Init();

	m_pReader->Start();
	m_pCommander->Start();

	LoadDeviceMap();

	THREAD_INFO_PRINTF( "Adf7021: START: IsRunning(): %d\n", this->IsRunning() );
	while( this->IsRunning() == true )
	{
		m_pReader->Thread::Lock();
		while( ( m_pReader->IsEmpty() == true ) && ( this->IsRunning() == true ) )
		{
			m_pReader->Thread::Wait( 100 );	//	100ms timeout
		}
		m_pReader->Thread::Unlock();

		while( m_pReader->IsEmpty() == false )
		{
			//THREAD_INFO_PRINTF( ">>> IsEmpty: %d size: %d pPacket: %p\n",
			//		m_reader.IsEmpty(), m_reader.Size(), pPacket );
			pPacket = m_pReader->Dequeue();

			assert( pPacket != NULL );

#if 0
			if( pPacket == NULL )
			{
				LOGW( "pPacket is NULL" );
				continue;
			}

#endif
			ProcessPacket( pPacket );

			THREAD_INFO_PRINTF( "END: pPacket: %p packetData: %p\n", pPacket, pPacket->packetData );

			delete pPacket;
			pPacket = NULL;

		}
	}
	THREAD_INFO_PRINTF( "Adf7021: STOP!!! IsRunning(): %d\n", this->IsRunning() );
	m_pCommander->Join();
	m_pCommander->Clear();
	m_pReader->Join();
	m_pReader->Clear();

	//CloseAccessors();

	THREAD_INFO_PRINTF( "Adf7021: END: IsRunning(): %d\n", this->IsRunning() );
	return NULL;
}

bool Adf7021::ProcessPacket( const ::ADF_PACKET * const pPacket )
{
	uint8_t *pPacketData = NULL;

	assert( pPacket != NULL );

	pPacketData = const_cast< uint8_t * >( pPacket->packetData );

	switch( pPacket->notify_type )
	{
		case NOTIFY_NONE :	//	GPS_INFO
			UpdateDeviceDetailInfo( reinterpret_cast< ::PF_RX_COMMAND * >( pPacketData ) );
			break;

		case NOTIFY_ADD_DEVICE :
			{
				std::map< int, ::DEVICE_INFO >::iterator it;
				::DEVICE_INFO *pInfo = reinterpret_cast< ::DEVICE_INFO * >( pPacketData );

				it = m_deviceMap.find( pInfo->slot );
				if( it == m_deviceMap.end() )
				{
					if( m_callbacks.AddDeviceCallback != NULL )
					{
						m_callbacks.AddDeviceCallback( pPacketData );
					}
					AddDevice( *pInfo );
				}
				break;
			}
		case NOTIFY_SHARE_INFO :
			{
				::DEVICE_INFO *pInfo = reinterpret_cast< ::DEVICE_INFO * >( pPacketData );

				LOGV( "NOTIFY_SHARE_INFO\n" );
				if( m_callbacks.SharingInfoCallback != NULL )
				{
					/*	id, slotId, frequencyId, deviceName	*/
					m_callbacks.SharingInfoCallback( pPacketData );
				}
				AddDevice( *pInfo );
				break;
			}
		case NOTIFY_SHARE_ACK :
			LOGV( "NOTIFY_SHARE_ACK\n" );
			if( m_callbacks.SharingSuccessCallback != NULL )
			{
				m_callbacks.SharingSuccessCallback( NULL );
			}
			break;

		case NOTIFY_SLOT_CONFLICT :
			LOGV( "NOTIFY_SLOT_CONFLICT\n" );
			if( m_callbacks.SlotConflictCallback != NULL )
			{
				/*	slotId	*/
				m_callbacks.SlotConflictCallback( pPacketData );
			}
			break;
		case NOTIFY_SHARE_CANCELLED :
			LOGV( "NOTIFY_SHARE_CANCELLED\n" );
			if( m_callbacks.SharingCancelCallback != NULL )
			{
				m_callbacks.SharingCancelCallback( NULL );
			}
			break;
		case NOTIFY_EC_ACK :
			if( m_callbacks.eCollarNickSuccessCallback != NULL )
			{
				m_callbacks.eCollarNickSuccessCallback( NULL );
			}
			break;
		case NOTIFY_EC_FAIL :
			if( m_callbacks.eCollarCommandFailureCallback != NULL )
			{
				m_callbacks.eCollarCommandFailureCallback( NULL );
			}
			break;
		case NOTIFY_BAT_LEVEL :
			if( m_callbacks.batteryLevelCallback!= NULL )
			{
				/*	level	*/
				m_callbacks.batteryLevelCallback( NULL );
			}
			break;

		case NOTIFY_SCAN :
			if( m_callbacks.ScanResultCallback != NULL )
			{
				m_callbacks.ScanResultCallback( pPacketData );
			}
			break;
		case NOTIFY_IN_PAIRING :
			if( m_callbacks.CollarPairingModeCallback != NULL )
			{
				m_callbacks.CollarPairingModeCallback( NULL );
			}
			break;

		default :
			break;

	}
	return true;
}

size_t Adf7021::Send( const uint8_t *pPacket )
{
	THREAD_INFO_PRINTF( "Adf7021: SEND: this: %p m_fd: %d\n", this, m_fd );
	return 0;
}

size_t Adf7021::Receive( const uint8_t *pPacket )
{
	THREAD_INFO_PRINTF( "Adf7021: RECV: this: %p m_fd: %d\n", this, m_fd );
	return 0;
}

::ADF_PACKET* Adf7021::Receive( void )
{
	assert( m_pReader != NULL );
	return m_pReader->Dequeue();
}

#if 0
static int open_adf7021(const struct hw_module_t* hw_module, const char* name,
		struct hw_device_t** hw_device_out)
{
	LOGV("open_adf7021 begin...");

    lazy_init_modules();

    // Create proxy device, to return later.
    sensors_poll_context_t *dev = new sensors_poll_context_t();
    memset(dev, 0, sizeof(sensors_poll_device_1_t));
    dev->proxy_device.common.tag = HARDWARE_DEVICE_TAG;
    dev->proxy_device.common.version = SENSORS_DEVICE_API_VERSION_1_3;
    dev->proxy_device.common.module = const_cast<hw_module_t*>(hw_module);
    dev->proxy_device.common.close = device__close;
    dev->proxy_device.activate = device__activate;
    dev->proxy_device.setDelay = device__setDelay;
    dev->proxy_device.poll = device__poll;
    dev->proxy_device.batch = device__batch;
    dev->proxy_device.flush = device__flush;

    dev->nextReadIndex = 0;

    // Open() the subhal modules. Remember their devices in a vector parallel to sub_hw_modules.
    for (std::vector<hw_module_t*>::iterator it = sub_hw_modules->begin();
            it != sub_hw_modules->end(); it++) {
        sensors_module_t *sensors_module = (sensors_module_t*) *it;
        struct hw_device_t* sub_hw_device;
        int sub_open_result = sensors_module->common.methods->open(*it, name, &sub_hw_device);
        if (!sub_open_result) {
            if (!HAL_VERSION_IS_COMPLIANT(sub_hw_device->version)) {
                LOGE("SENSORS_DEVICE_API_VERSION_1_3 is required for all sensor HALs");
                LOGE("This HAL reports non-compliant API level : %s",
                        apiNumToStr(sub_hw_device->version));
                LOGE("Sensors belonging to this HAL will get ignored !");
            }
            dev->addSubHwDevice(sub_hw_device);
        }
    }

    // Prepare the output param and return
    *hw_device_out = &dev->proxy_device.common;
    LOGV("...open_sensors end");

	return 0;
}
#endif

int Adf7021::OpenDevice( const hw_module_t *hw_module, const char *name, hw_device_t **hw_device) const
{
	THREAD_INFO_PRINTF( "Adf7021: OPEN_DEV: this: %p m_fd: %d\n", this, m_fd );
	assert( hw_module != NULL );
	assert( *hw_device == NULL );

	//int				ret = -EINVAL;
	AdfHwDevice	*pDevice = new AdfHwDevice( this );

	/*	hw_device_t		*/
	pDevice->tag = HARDWARE_DEVICE_TAG;
	pDevice->version = HARDWARE_DEVICE_API_VERSION( 1, 0);
	pDevice->module = const_cast< hw_module_t * >( hw_module );
	pDevice->close = &Adf7021::CloseDevice;

	/*	extends		*/
	pDevice->open = &Adf7021::OpenDevice;
	pDevice->start = &Adf7021::StartThread;
	pDevice->stop= &Adf7021::StopThread;
	//pDevice->signal = &Adf7021::SignalThread;
	pDevice->setCallbacks= &Adf7021::SetCallbacks;
	pDevice->sendCommand = &Adf7021::SendCommand;
	pDevice->getConnectorInfo = &Adf7021::GetConnectorInfo;
	pDevice->update = &Adf7021::Update;

	*hw_device = dynamic_cast< hw_device_t * >( pDevice );

	THREAD_PRINTF( "module: %p pDevice: %p hw_device: %p\n", pDevice->module, pDevice, *hw_device );

	return 0;
}

int Adf7021::CloseDevice(hw_device_t *hw_device)
{
	INFO_PRINTF( "Adf7021: S_CLOSE_DEV\n" );
	assert( hw_device != NULL );

	AdfHwDevice	*pDevice = reinterpret_cast< AdfHwDevice * >( hw_device );

	if( pDevice != NULL )
	{
		const Adf7021 * const pAdf7021 = pDevice->GetDevice();

		INFO_PRINTF( "DELETE: module: %p pDevice: %p hw_device: %p\n", pDevice->module, pDevice, hw_device );
		delete pDevice;

		if( pAdf7021 != NULL )
		{
			const_cast< Adf7021 * >( pAdf7021 )->Close();
			delete pAdf7021;
		}
	}

	return 0;
}

int Adf7021::OpenDevice( const AdfHwDevice *pDevice )
{
	INFO_PRINTF( "Adf7021: S_OPEN_DEV \n" );
	assert( pDevice != NULL );

	const Adf7021 * const pAdf7021 = pDevice->GetDevice();
	const_cast< Adf7021 * >( pAdf7021 )->Open();

	return 0;
}

int Adf7021::StartThread( const AdfHwDevice *pDevice )
{
	INFO_PRINTF( "Adf7021: S_START\n" );
	assert( pDevice != NULL );

	const Adf7021 * const pAdf7021 = pDevice->GetDevice();
	pAdf7021->Start();

	return 0;
}

int Adf7021::StopThread( const AdfHwDevice *pDevice )
{
	INFO_PRINTF( "Adf7021: S_STOP\n" );
	assert( pDevice != NULL );

	const Adf7021 * const pAdf7021 = pDevice->GetDevice();
	pAdf7021->Stop();

	//void *pRet = NULL;
	//pAdf7021->Join( &pRet );
	pAdf7021->Join();

	return 0;
}

#if 0
int Adf7021::SignalThread( const AdfHwDevice *pDevice )
{
	VERB_PRINTF( "Adf7021: S_SIGNAL\n" );
	assert( pDevice != NULL );

	const Adf7021 * const pAdf7021 = pDevice->GetDevice();
	pAdf7021->Lock();
	pAdf7021->Signal();
	pAdf7021->Unlock();

	return 0;
}
#endif

int Adf7021::SetCallbacks( const AdfHwDevice *pDevice, const ADF_CALLBACKS *pCallbacks )
{
	VERB_PRINTF( "Adf7021: S_SetCallbacks\n" );
	assert( ( pDevice != NULL ) && ( pCallbacks != NULL ) );

	const Adf7021 * const pAdf7021 = pDevice->GetDevice();
	const_cast< Adf7021 * >( pAdf7021 )->SetCallbacks( pCallbacks );

	return 0;
}

int Adf7021::SendCommand( const AdfHwDevice *pDevice, const uint32_t command, const void * const pArg )
{
	VERB_PRINTF( "Adf7021: S_SendCommand\n" );
	assert( ( pDevice != NULL ) && ( command != 0x0 ) );

	const Adf7021 * const pAdf7021 = pDevice->GetDevice();
	const_cast< Adf7021 * >( pAdf7021 )->SendCommand( command, pArg );

	return 0;
}

int Adf7021::GetConnectorInfo( const AdfHwDevice *pDevice, ::CONNECTOR_INFO &connectorInfo )
{
	VERB_PRINTF( "Adf7021: S_GetConnectorInfo\n" );
	assert( pDevice != NULL );

	const Adf7021 * const pAdf7021 = pDevice->GetDevice();
	const_cast< Adf7021 * >( pAdf7021 )->GetConnectorInfo( connectorInfo );

	return 0;
}

int Adf7021::Update( const AdfHwDevice *pDevice, const std::map< int, ::DEVICE_INFO > &deviceMap )
{
	VERB_PRINTF( "Adf7021: S_Update\n" );
	assert( pDevice != NULL );

	const Adf7021 * const pAdf7021 = pDevice->GetDevice();
	const_cast< Adf7021 * >( pAdf7021 )->Update( deviceMap );
	pAdf7021->Lock();
	pAdf7021->Signal();
	VERB_PRINTF( "Adf7021: SIGNAL: Update completed!!!\n" );
	pAdf7021->Unlock();

	return 0;
}
//template class Adf7021< ADF_PACKET >;
