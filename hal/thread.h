#ifndef	_THREAD_H_
#define	_THREAD_H_

#include	<pthread.h>

#include	<string>

#include	"mutex.h"

namespace Pettra
{
class Thread : public Mutex
{
public:
	typedef enum THREAD_STATUS_E
	{
		STATUS_STOPPED = -1,
		STATUS_RUNNING = 0,
		STATUS_WAIT = 1,
		STATUS_IDLE = 1,	//	Use in ThreadPool

		STATUS_END
	} THREAD_STATUS;

	static const int PRIORITY_LOW		= 0;
	static const int PRIORITY_NORMAL	= 1;
	static const int PRIORITY_HIGH		= 2;

private:
	mutable pthread_t		m_id;
	mutable THREAD_STATUS	m_status;
	mutable bool			m_isRunning;

	const std::string	m_name;
	const int			m_priority;

	static void			*Run( void * pArg );
protected:

	virtual	void* ThreadFunc( void * pArg ) = 0;
public:
	explicit Thread( const std::string name = "undef", const int priority = PRIORITY_NORMAL );
	virtual ~Thread();

	void SetThreadName( void ) const;
	const pthread_t& GetTid( void ) const { return m_id; }
	const THREAD_STATUS& GetStatus( void ) const { return m_status; }
	const std::string& GetName( void ) const { return m_name; }
	const int& GetPriority(void ) const { return m_priority; }
	const bool&	IsRunning( void ) const { return m_isRunning; }

	bool	Start() const;
	bool	Stop() const;
	bool	Join( void **ppRetVal = NULL ) const;
};
}	//	namespace Pettra
#endif	//	_THREAD_H_
/*	vim: set ft=cpp:	*/
