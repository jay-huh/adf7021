#include	"hal_module.h"

#include	<string.h>	//	memset

#include	<utils/Errors.h>

#include	"debug.h"

#include	"hal_device.h"
#include	"hal_device_factory.h"

using namespace android;
using namespace Pettra;

HalModule	HAL_MODULE_INFO_SYM = HalModule();

/*	Module list */
const char *HalModule::s_moduleId[] = {
	"test_jyhuh",
	"rf",
	"adf",			/*	ADF	*/
};

const hw_module_methods_t HalModule::s_methods = {
	open : HalModule::Open,
};

HalModule::HalModule()
{
	/*	hw_module_t	*/
	tag = HARDWARE_MODULE_TAG;
	module_api_version = HARDWARE_MODULE_API_VERSION( 1, 0 );
	hal_api_version = HARDWARE_HAL_API_VERSION;
	version_major = 1,
	version_minor = 0,
	id = HAL_HARDWARE_MODULE_ID;	//	MUST define in Android.mk
	name = HAL_HARDWARE_MODULE_NAME;
	author = "Dogtra Co.,Ltd.";
	methods = const_cast< hw_module_methods_t * >( & HalModule::s_methods );
	dso = NULL;
	::memset( reserved, 0x0, sizeof( reserved ) );

	/*	extends	*/
	::memset( extends, 0x0, sizeof( extends ) );
	m_pHalDevice = NULL;

	m_halModuleMap.clear();

	for( int i = 0 ; i < HAL_IDX_END ; i++ )
	{
		m_halModuleMap.insert( HAL_IDX_PAIR( s_moduleId[ i ], i ) );
	}
	DEBUG_PRINTF( "HalModule: id: %s | Constructor. this: %p\n", GetId(), this );
}

HalModule::~HalModule()
{
	DEBUG_PRINTF( "HalModule: id: %s | Destructor. this: %p\n", GetId(), this );
}

void HalModule::SetHalDevice( const HalDevice *pDevice )
{
	assert( pDevice != NULL );

	m_pHalDevice = const_cast< HalDevice * >( pDevice );

	return;
}

int HalModule::GetHalIndex( const std::string name ) const
{
	std::map< std::string, int >::const_iterator it = m_halModuleMap.find( name );

#if 1
	assert( it != m_halModuleMap.end() );
#else
	if( it == m_halModuleMap.end() )
	{
		return HAL_IDX_BAD;
	}
#endif

	return it->second;
}

int HalModule::Open( const hw_module_t* hw_module, const char* name, hw_device_t** hw_device)
{
	const HalModule * const pHalModule = static_cast< const HalModule * >( hw_module );

	assert( hw_module != NULL );
	assert( *hw_device == NULL );
#if 1
	assert( hw_module == ::HAL_MODULE_INFO_SYM.GetModulePtr() );
#else
	if( hw_module != ::HAL_MODULE_INFO_SYM.GetModulePtr() )
	{
		LOGE( "Invalid module: module %p != HAL_MODULE_INFO_SYM %p", hw_module, ::HAL_MODULE_INFO_SYM.GetModulePtr() );
		return INVALID_OPERATION;
	}
#endif

	const int index = pHalModule->GetHalIndex( hw_module->id );

	const HalDevice	* const pHalDevice = HalDeviceFactory::GetInstance()->CreateHalDevice( index );

	if( pHalDevice == NULL )
	{
		LOGE( "HalDevice not created. hw_module->id: %s", hw_module->id );
		return -EINVAL;
	}
	pHalDevice->OpenDevice( hw_module, name, hw_device );

	return 0;
}
