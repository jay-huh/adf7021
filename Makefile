export TOP_DIR := $(PWD)/../../..
TOP_TARGETS := all clean

SUB_DIRS := $(wildcard */.)
EXCLUDE_DIRS := kernel/. jni/.
SUB_DIRS := $(filter-out $(EXCLUDE_DIRS), $(SUB_DIRS))
DRV_PATH :=	drv/.

KERNEL_PATH := ~/work/gitlab/aosp-test/linux/kernel/kernel-3.4.39/
$(TOP_TARGETS): $(SUB_DIRS)

$(SUB_DIRS):
	@echo "[MAKE] $(MAKECMDGOALS) @ $@"
	@if [ "$(DRV_PATH)" != "$@" ] ; then \
		$(MAKE) -C $@ $(MAKECMDGOALS) ; \
	else \
		cd $@ && $(MAKE) $(MAKECMDGOALS) && cd .. ; \
	fi

clean:
	@echo "[$@]"
	@rm -f tags cscope.*

tags:
	@echo "[ TAGS ]"
	@sudo umount ./kernel
	@mkctags.sh -c ./
	@mkcscope.sh -c ./
	@sudo mount --bind $(KERNEL_PATH) ./kernel

.PHONY: $(TOP_TARGETS) $(SUB_DIRS)
